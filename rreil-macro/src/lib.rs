extern crate p8n_rreil_macro_hack;
extern crate proc_macro_hack;

use proc_macro_hack::proc_macro_hack;

#[proc_macro_hack(fake_call_site)]
pub use p8n_rreil_macro_hack::rreil;

#[proc_macro_hack(fake_call_site)]
pub use p8n_rreil_macro_hack::rreil_var;

#[proc_macro_hack(fake_call_site)]
pub use p8n_rreil_macro_hack::rreil_val;
