extern crate p8n_rreil_macro;
extern crate p8n_types;

use p8n_rreil_macro::rreil;

#[test]
fn main() {
    use p8n_types::{Name, Value, Variable};

    let eax = Value::var(Name::new("eax", None), 12).unwrap();
    let t0 = Variable::new(Name::new("eax", None), 12).unwrap();
    let val = Value::val(1337, 12).unwrap();
    let sz = 1;

    rreil! {
        sub t0:32, eax:32, ebx:32
        cmpltu CF:1, eax:32, ebx:32
        cmpleu CForZF:1, eax:32, ebx:32
        cmplts SFxorOF:1, eax:32, ebx:32
        cmples SFxorOForZF:1, eax:32, ebx:32
        cmpeq  ZF:1, eax:32, ebx:32
        cmplts SF:1, t0:32, [0]:32
        xor OF:1, SFxorOF:1, SF:1
    };

    rreil! {
        add (t0), (val), (eax)
        and t0:32, [2147483648]:32, eax:32
        and t1:32, [2147483648]:32, ebx:32
        sub t2:32, ebx : 32 , eax:32
        and t3:32, [2147483648]:32, t2:32
        shr SF:8, [31]:8, t3:8
        xor t4:32, t1:32, t0:32
        xor t5:32, t3:32, t0:32
        and t6:32, t5:32, t4:32
        shr OF:8, [31]:8, t6:8
        and t7:64, [4294967296]:64, t2:64
        shr CF:8, [32]:8, t7:8
        and t8:32, [4294967295]:32, t2:32
        xor t9:8, OF:8, SF:8
        sel/0/32 ebx:32, rax:64
    };

    rreil! {
        sub t0:32, eax:32, ebx:32
        cmpltu CF:1, eax:32, ebx:32
        cmpleu CForZF:1, eax:32, ebx:32
        cmplts SFxorOF:1, eax:32, ebx:32
        cmples SFxorOForZF:1, eax:32, ebx:32
        cmpeq  ZF:1, eax:32, ebx:32
        cmplts SF:1, t0:32, [0]:32
        xor OF:1, SFxorOF:1, SF:1
    };

    rreil! {
        sub rax:32, rax:32, [1]:32
        mov rax:32, [0]:32
    };

    rreil! {
        store/ram/le/32 rax:32, [0]:32
        load/ram/le/32 rax:32, [0]:32
    };

    rreil! {
        sext/32 rax:32, ax:16
        zext/32 rax:32, ax:16
        mov rax:32, tbx:32
    };

    rreil! {
        zext/64 a:64, (t0)
        call a:64
        ret
    };

    rreil! {
        and res:sz, (eax), (t0)
        cmplts SF:1, res:sz, [0]:sz
        cmpeq ZF:1, res:sz, [0]:sz
    };
}
