// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use vec_map::VecMap;
use petgraph::Direction;
use petgraph::visit::EdgeRef;
use types::{BasicBlockIndex,Guard,Function,Value,Variable,Constant,Operation,Statement,Result,NameRef,Constraint,CfgNode};

#[derive(Clone,Debug)]
pub struct Constraints {
    // at entry of bb_idx -> (base name -> constraint)
    constraints: VecMap<VecMap<Constraint>>,
}

impl Constraints {
    pub fn new(func: &Function) -> Result<Self> {
        let c = Self::collect_constraints(func)?;
        let ret = Constraints{
            constraints: c,
        };

        Ok(ret)
    }

    pub fn for_basic_block<'a>(&'a self, bb: BasicBlockIndex) -> &'a VecMap<Constraint> {
        &self.constraints[bb.index()]
    }

    fn constraints_for_basic_block(bb: BasicBlockIndex, func: &Function, mut in_effect: VecMap<Constraint>, name_to_constr: &mut VecMap<(NameRef,Constraint)>) -> Result<()> {
        for stmt in func.statements(bb) {
            match &*stmt {
                &Statement::Expression{ result: Variable{ name, bits: 1 }, ref op } => {
                    let name = func.names.base_name(name)?;

                    in_effect.remove(name.index());

                    match op {
                        &Operation::Move(Value::Variable(Variable{ name: a_name, bits: 1 })) => {
                            let a_name = func.names.base_name(a_name)?;
                            if let Some(a) = name_to_constr.get(a_name.index()).cloned() {
                                name_to_constr.insert(name.index(),a);
                            }
                        }
                        &Operation::And(Value::Variable(Variable{ name: a_name, bits: 1 }),Value::Variable(Variable{ name: b_name, bits: 1 })) => {
                            if let (Some((nam_x,mut x)),Some((nam_y,y))) = (name_to_constr.get(a_name.index()).cloned(),name_to_constr.get(b_name.index()).cloned()) {
                                if nam_x == nam_y {
                                    x.intersect_with(y);
                                    name_to_constr.insert(name.index(),(nam_x,x));
                                }
                            }
                        }
                        &Operation::InclusiveOr(Value::Variable(Variable{ name: a_name, bits: 1 }),Value::Variable(Variable{ name: b_name, bits: 1 })) => {
                            if let (Some((nam_x,mut x)),Some((nam_y,y))) = (name_to_constr.get(a_name.index()).cloned(),name_to_constr.get(b_name.index()).cloned()) {
                                if nam_x == nam_y {
                                    x.union_with(y);
                                    name_to_constr.insert(name.index(),(nam_x,x));
                                }
                            }
                        }
                        &Operation::Equal(Value::Constant(Constant{ value,.. }),Value::Variable(Variable{ name: var_name, bits })) |
                            &Operation::Equal(Value::Variable(Variable{ name: var_name, bits }),Value::Constant(Constant{ value,.. })) => {
                                let mut c = in_effect.get(var_name.index()).cloned().unwrap_or(Constraint::new(bits)?);
                                c.include(value);
                                name_to_constr.insert(name.index(),(var_name,c));
                            }
                        &Operation::LessOrEqualUnsigned(Value::Constant(Constant{ value,.. }),Value::Variable(Variable{ name: var_name, bits })) => {
                            let mut c = in_effect.get(var_name.index()).cloned().unwrap_or(Constraint::new(bits)?);
                            c.clamp_lower_bound_unsigned(value);
                            name_to_constr.insert(name.index(),(var_name,c));
                        }

                        &Operation::LessOrEqualUnsigned(Value::Variable(Variable{ name: var_name, bits }),Value::Constant(Constant{ value,.. })) => {
                            let mut c = in_effect.get(var_name.index()).cloned().unwrap_or(Constraint::new(bits)?);
                            c.clamp_upper_bound_unsigned(value);
                            name_to_constr.insert(name.index(),(var_name,c));
                        }

                        &Operation::LessOrEqualSigned(Value::Constant(Constant{ value,.. }),Value::Variable(Variable{ name: var_name, bits })) => {
                            let mut c = in_effect.get(var_name.index()).cloned().unwrap_or(Constraint::new(bits)?);
                            c.clamp_lower_bound_signed(value);
                            name_to_constr.insert(name.index(),(var_name,c));
                        }

                        &Operation::LessOrEqualSigned(Value::Variable(Variable{ name: var_name, bits }),Value::Constant(Constant{ value,.. })) => {
                            let mut c = in_effect.get(var_name.index()).cloned().unwrap_or(Constraint::new(bits)?);
                            c.clamp_upper_bound_signed(value);
                            name_to_constr.insert(name.index(),(var_name,c));
                        }

                        &Operation::LessUnsigned(Value::Constant(Constant{ value,.. }),Value::Variable(Variable{ name: var_name, bits })) => {
                            let mut c = in_effect.get(var_name.index()).cloned().unwrap_or(Constraint::new(bits)?);
                            c.clamp_lower_bound_unsigned(value.wrapping_add(1));
                            name_to_constr.insert(name.index(),(var_name,c));
                        }

                        &Operation::LessUnsigned(Value::Variable(Variable{ name: var_name, bits }),Value::Constant(Constant{ value,.. })) => {
                            let mut c = in_effect.get(var_name.index()).cloned().unwrap_or(Constraint::new(bits)?);
                            c.clamp_upper_bound_unsigned(value.wrapping_sub(1));
                            name_to_constr.insert(name.index(),(var_name,c));
                        }

                        &Operation::LessSigned(Value::Constant(Constant{ value,.. }),Value::Variable(Variable{ name: var_name, bits })) => {
                            let mut c = in_effect.get(var_name.index()).cloned().unwrap_or(Constraint::new(bits)?);
                            c.clamp_lower_bound_signed(value.wrapping_add(1));
                            name_to_constr.insert(name.index(),(var_name,c));
                        }

                        &Operation::LessSigned(Value::Variable(Variable{ name: var_name, bits }),Value::Constant(Constant{ value,.. })) => {
                            let mut c = in_effect.get(var_name.index()).cloned().unwrap_or(Constraint::new(bits)?);
                            c.clamp_upper_bound_signed(value.wrapping_sub(1));
                            name_to_constr.insert(name.index(),(var_name,c));
                        }

                        &Operation::Equal(Value::Variable(Variable{ name: var_name, bits }),_) |
                            &Operation::Equal(_,Value::Variable(Variable{ name: var_name, bits })) |
                            &Operation::LessOrEqualSigned(_,Value::Variable(Variable{ name: var_name, bits })) |
                            &Operation::LessOrEqualSigned(Value::Variable(Variable{ name: var_name, bits }),_) |
                            &Operation::LessOrEqualUnsigned(_,Value::Variable(Variable{ name: var_name, bits })) |
                            &Operation::LessOrEqualUnsigned(Value::Variable(Variable{ name: var_name, bits }),_) |
                            &Operation::LessSigned(_,Value::Variable(Variable{ name: var_name, bits })) |
                            &Operation::LessSigned(Value::Variable(Variable{ name: var_name, bits }),_) |
                            &Operation::LessUnsigned(_,Value::Variable(Variable{ name: var_name, bits })) |
                            &Operation::LessUnsigned(Value::Variable(Variable{ name: var_name, bits }),_) => {
                                name_to_constr.insert(name.index(),(var_name,Constraint::Empty{ bits: bits }));
                            }

                        _ => {
                            // clear constraints for assignee
                            name_to_constr.remove(name.index());
                            in_effect.remove(name.index());
                        }
                    }
                }
                &Statement::Expression{ result: Variable{ name,.. },.. } => {
                    in_effect.remove(name.index());
                }
                &Statement::Memory{ .. } => { /* skip */ }
                &Statement::Flow{ .. } => { /* skip */ }
            }
        }

        Ok(())
    }

    fn incoming_constraints(bb: BasicBlockIndex, by_bb: &VecMap<VecMap<(NameRef,Constraint)>>,
                            incoming: &VecMap<VecMap<(NameRef,bool)>>) -> VecMap<Constraint> {
        let mut name_to_constr = VecMap::<Constraint>::default();

        // collect variables from previous blocks
        if let Some(bs) = incoming.get(bb.index()) {
            for (bb_idx,&(flag,pol)) in bs.iter() {
                match by_bb.get(bb_idx).and_then(|x| x.get(flag.index())) {
                    Some(&(name,ref constr)) => {
                        let constr = if !pol {
                            let mut c = constr.clone();
                            c.invert();
                            c
                        } else { constr.clone() };

                        if name_to_constr.contains_key(name.index()) {
                            name_to_constr[name.index()].union_with(constr);
                        } else {
                            name_to_constr.insert(name.index(),constr);
                        }
                    }
                    None => {}
                }
            }
        }

        name_to_constr
    }

    fn collect_constraints(func: &Function) -> Result<VecMap<VecMap<Constraint>>> {
        // at entry of bb_idx -> (flag -> [(base name + constraint)])
        let mut by_bb = VecMap::<VecMap<(NameRef,Constraint)>>::with_capacity(func.basic_blocks().len());
        let mut ret = VecMap::default();
        let cfg = func.cflow_graph();
        let preds = func.basic_blocks().map(|(bb_idx,bb)| {
            let preds = cfg.edges_directed(bb.node,Direction::Incoming).filter_map(|e| {
                match (cfg.node_weight(e.source()),e.weight()) {
                    (Some(&CfgNode::BasicBlock(p)),&Guard::Predicate{ flag: Variable{ name,.. }, expected }) => {
                        Some((p.index(),(name,expected)))
                    }
                    _ => None,
                }
            }).collect::<VecMap<(NameRef,bool)>>();
            (bb_idx.index(),preds)
        }).collect::<VecMap<_>>();

        for (bb_idx,_) in func.basic_blocks() {
            // compute constrains from incoming edges
            let incoming = Self::incoming_constraints(bb_idx,&by_bb,&preds);
            ret.insert(bb_idx.index(),incoming.clone());

            // collect variables from previous blocks
            let mut name_to_constr = VecMap::default();
            Self::constraints_for_basic_block(bb_idx,func,incoming,&mut name_to_constr)?;

            by_bb.insert(bb_idx.index(),name_to_constr);
        }

        Ok(ret)
    }
}

fn insert_assume(var: Variable, constr: Constraint, bb_idx: BasicBlockIndex, func: &mut Function) -> Result<()> {
    use smallvec::SmallVec;

    let str_phi = func.strings.insert(&"__phi".into());
    let str_mphi = func.strings.insert(&"__mphi".into());
    let str_assume = func.strings.insert(&"__assume".into());
    let stmt = Statement::Expression{
        op: Operation::Assume(constr.clone(),Value::Variable(var)),
        result: var,
    };

    let non_phi = func.mnemonics(bb_idx).position(|(_,mne)| {
        mne.opcode != str_phi && mne.opcode != str_mphi
    }).unwrap_or_else(|| {
        let mnes = &func.basic_block(bb_idx).mnemonics;
        mnes.end.index() - mnes.start.index()
    });

    func.insert_mnemonic(bb_idx,non_phi,str_assume,SmallVec::default(),vec![stmt])
}

pub fn rewrite_with_assume(func: &mut Function) -> Result<()> {
    let constr = Constraints::new(&*func)?;
    for idx in 0..func.basic_blocks().len() {
        let bb_idx = BasicBlockIndex::new(idx);

        for (n,c) in constr.for_basic_block(bb_idx) {
            let var = Variable::new(n.into(),c.bits())?;
            insert_assume(var,c.clone(),bb_idx,func)?;
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use types::{Region, Function, TestArch, UUID};

    /*
     * (B0)
     * 0:  Ma10
     * 4:  Mb0
     * 7:  Mc4
     * 10: Cfc1
     * 14: Bf32
     *
     * (B1)
     * c == [2,-]
     * 18: Aaa5
     * 22: Abac
     * 26: Mc2
     * 29: J54
     *
     * (B2)
     * c == [0,1]
     * 32: Cfa0
     * 36: Bf54
     *
     * (B3)
     * a == [1,-]
     * 40: Aaa1
     * 44: Abb3
     * 48: Mc3
     * 51: J32
     *
     * (B4)
     * a == [0,0]
     * 54: Axab
     * 58: Mcc
     * 61: Maa
     * 64: Mbb
     * 67: R
     */
    #[test]
    fn loop_invariant() {
        use types::Name;

        let _ = ::simple_logger::init();
        let code = b"Ma10Mb0Mc4Cfc1Bf32Aaa5AbacMc2J54Cfa0Bf54Aaa1Abb3Mc3J32AxabMccMaaMbbR".to_vec();
        let code_reg = Region::from_buf("", 16, code, 0, None);
        let func = Function::new::<TestArch>((), 0, &code_reg, UUID::now()).unwrap();
        let c = Constraints::new(&func).unwrap();
        let a_var = func.names.index(&Name::new("a".into(),None)).unwrap();
        let c_var = func.names.index(&Name::new("c".into(),None)).unwrap();

        for (idx,bb) in func.basic_blocks() {
            let assumes = c.for_basic_block(idx);
            match bb.area.start {
                0 => assert_eq!(assumes.len(), 0),
                18 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[c_var.index()], Constraint::Unsigned{ from: 2, to: 0xffffffff, bits: 32 });
                }
                32 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[c_var.index()], Constraint::Unsigned{ from: 0, to: 1, bits: 32 });
                }
                40 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[a_var.index()], Constraint::Unsigned{ from: 1, to: 0xffffffff, bits: 32 });
                }
                54 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[a_var.index()], Constraint::Unsigned{ from: 0, to: 0, bits: 32 });
                }

                _ => unreachable!()
            }
        }
    }

    /*
     * (B0)
     * 0: Efa1
     * 4: Bf8
     *
     * (B1)
     * a != [1,1]
     * 7: R
     *
     * (B2)
     * a == [1,1]
     * 8: R
     */
    #[test]
    fn equal() {
        use types::Name;

        let _ = ::simple_logger::init();
        let code = b"Efa1Bf8RR".to_vec();
        let code_reg = Region::from_buf("", 16, code, 0, None);
        let func = Function::new::<TestArch>((), 0, &code_reg, UUID::now()).unwrap();
        let c = Constraints::new(&func).unwrap();
        let a_var = func.names.index(&Name::new("a".into(),None)).unwrap();

        for (idx,bb) in func.basic_blocks() {
            let assumes = c.for_basic_block(idx);
            match bb.area.start {
                0 => assert_eq!(assumes.len(), 0),
                7 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[a_var.index()], Constraint::Full{ bits: 32 });
                }
                8 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[a_var.index()], Constraint::Unsigned{ from: 1, to: 1, bits: 32 });
                }

                _ => unreachable!()
            }
        }
    }

    /*
     * (B0)
     * 0:  Cfa5
     * 4:  Bf16
     *
     * (B2)
     * a = [6,-]
     * 8:  Cfa10
     * 13: Bf17
     *
     * (B3)
     * 16: R
     *
     * (B4)
     * a = [6,10]
     * 17: R
     */
    #[test]
    fn upper_and_lower_bounds() {
        use types::Name;

        let _ = ::simple_logger::init();
        let code = b"Cfa5Bf16Cfa10Bf17RR".to_vec();
        let code_reg = Region::from_buf("", 16, code, 0, None);
        let func = Function::new::<TestArch>((), 0, &code_reg, UUID::now()).unwrap();
        let c = Constraints::new(&func).unwrap();
        let a_var = func.names.index(&Name::new("a".into(),None)).unwrap();

        for (idx,bb) in func.basic_blocks() {
            let assumes = c.for_basic_block(idx);
            match bb.area.start {
                0 => assert_eq!(assumes.len(), 0),
                8 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[a_var.index()], Constraint::Unsigned{ from: 6, to: 0xffffffff, bits: 32 });
                }
                16 => {
                    assert_eq!(assumes.len(), 0);
                }
                17 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[a_var.index()], Constraint::Unsigned{ from: 6, to: 10, bits: 32 });
                }
                _ => unreachable!()
            }
        }
    }

    /*
     * (B0)
     * 0:  Cfa5
     * 4:  Bf21
     *
     * (B2)
     * a = [6,-]
     * 8:  Aaa0
     * 12: Cfa10
     * 17: Bf22
     *
     * (B3)
     * 21: R
     *
     * (B4)
     * a = [0,10]
     * 22: R
     */
    #[test]
    fn incoming_modified() {
        use types::Name;

        let _ = ::simple_logger::init();
        let code = b"Cfa5Bf21Aaa0Cfa10Bf22RR".to_vec();
        let code_reg = Region::from_buf("", 16, code, 0, None);
        let func = Function::new::<TestArch>((), 0, &code_reg, UUID::now()).unwrap();
        let c = Constraints::new(&func).unwrap();
        let a_var = func.names.index(&Name::new("a".into(),None)).unwrap();

        for (idx,bb) in func.basic_blocks() {
            let assumes = c.for_basic_block(idx);
            match bb.area.start {
                0 => assert_eq!(assumes.len(), 0),
                8 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[a_var.index()], Constraint::Unsigned{ from: 6, to: 0xffffffff, bits: 32 });
                }
                21 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[a_var.index()], Constraint::Unsigned{ from: 0, to: 0xffffffff, bits: 32 });
                }
                22 => {
                    assert_eq!(assumes.len(), 1);
                    assert_eq!(assumes[a_var.index()], Constraint::Unsigned{ from: 0, to: 10, bits: 32 });
                }
                _ => unreachable!()
            }
        }
    }
}
