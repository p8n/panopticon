// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use types::{
    Guard,
    Segment,
    Function,
    Operation,
    Statement,
    Variable,
    Value,
    Result,
    CfgNode,
    FlowOperation,
    MemoryOperation,
    NameRef,
    Names,
};

use bit_set::BitSet;
use vec_map::VecMap;
use petgraph::Direction;
use hprof;

/// Computes the set of killed (VarKill) and upward exposed variables (UEvar) for each basic block
/// in `func`. Returns (VarKill,UEvar).
#[derive(Debug)]
pub struct Liveness {
    pub var_kill: Vec<BitSet>,
    pub ue_var: Vec<BitSet>,
    pub seg_kill: Vec<BitSet>,
    pub ue_seg: Vec<BitSet>,
}

impl Liveness {
    pub fn new(func: &Function) -> Result<Liveness> {
        use petgraph::visit::EdgeRef;

        let _prof = hprof::enter("compute liveness");
        let num_bb = func.basic_blocks().len();
        let mut ret = Liveness{
            var_kill: vec![BitSet::default(); num_bb],
            ue_var: vec![BitSet::default(); num_bb],
            seg_kill: vec![BitSet::default(); num_bb],
            ue_seg: vec![BitSet::default(); num_bb],
        };

        for (idx,bb) in func.basic_blocks() {
            for stmt in func.statements(bb) {
                println!("{:?}", stmt);
                match &*stmt {
                    &Statement::Expression{ op: Operation::Phi(_,_,_),.. } => { /* skip */ }
                    &Statement::Expression{ op: Operation::Initialize(_,_),.. } => { /* skip */ }
                    &Statement::Expression{ op: Operation::Load(ref segment,_,_,ref addr), ref result } => {
                        if let &Value::Variable(ref addr) = addr {
                            assert!(func.names.value(addr.name).unwrap().subscript().is_none());
                            ret.record_var_read(idx.index(),addr)?;
                        }
                        ret.record_var_write(idx.index(),result)?;
                        assert!(func.names.value(result.name).unwrap().subscript().is_none());
                        ret.record_seg_read(idx.index(),segment)?;
                    }
                    &Statement::Expression{ ref op, ref result } => {
                        for value in op.reads() {
                            if let &Value::Variable(ref var) = value {
                                assert!(func.names.value(var.name).unwrap().subscript().is_none());
                                ret.record_var_read(idx.index(),var)?;
                            }
                        }
                        ret.record_var_write(idx.index(),result)?;
                    }
                    &Statement::Flow{ op: FlowOperation::IndirectCall{ target: ref var } } => {
                        ret.record_var_read(idx.index(),var)?;
                    }
                    &Statement::Memory{ op: MemoryOperation::Store{ ref address, ref value, ref segment,.. }, ref result } => {
                        if let &Value::Variable(ref var) = address {
                            ret.record_var_read(idx.index(),var)?;
                        }
                        if let &Value::Variable(ref var) = value {
                            ret.record_var_read(idx.index(),var)?;
                        }
                        ret.record_seg_read(idx.index(),segment)?;
                        ret.record_seg_write(idx.index(),result)?;
                    }
                    &Statement::Memory{ op: MemoryOperation::MemoryPhi(_, _, _),.. } => { /* skip */ }
                    &Statement::Memory{ op: MemoryOperation::Allocate{ .. },.. } => { /* skip */ }
                    &Statement::Flow{ op: FlowOperation::Call{ .. } } => { /* skip */ }
                    &Statement::Flow{ op: FlowOperation::Return } => { /* skip */ }
                    &Statement::Flow{ op: FlowOperation::ExternalCall{ .. } } => { /* skip */ }
                }
            }

            let cfg = func.cflow_graph();
            for e in cfg.edges_directed(bb.node,Direction::Outgoing) {
                match e.weight() {
                    &Guard::Predicate{ ref flag,.. } => {
                        ret.record_var_read(idx.index(),flag)?;
                    }
                    _ => { /* skip */ }
                }

                match cfg.node_weight(e.target()) {
                    Some(&CfgNode::Value(Value::Variable(ref v))) => {
                        ret.record_var_read(idx.index(), v)?;
                    }
                    _ => { /* skip */ }
                }
            }
        }

        Self::propagate(func,&mut ret);

        Ok(ret)
    }

    fn propagate(func: &Function, liveness_sets: &mut Self) {
        let cfg = func.cflow_graph();
        let mut fixedpoint = false;

        while !fixedpoint {
            fixedpoint = true;
            for (bb_idx,bb) in func.basic_blocks().rev() {
                let vk = &liveness_sets.var_kill[bb_idx.index()];
                let sk = &liveness_sets.seg_kill[bb_idx.index()];

                for succ in cfg.neighbors_directed(bb.node,Direction::Outgoing) {
                    match cfg.node_weight(succ) {
                        Some(&CfgNode::BasicBlock(succ_idx)) => {
                            let in_uevar = liveness_sets.ue_var[succ_idx.index()].clone();
                            let in_ueseg = liveness_sets.ue_seg[succ_idx.index()].clone();

                            for var in in_uevar.into_iter() {
                                // XXX
                                if !vk.contains(var) {
                                    fixedpoint &= !liveness_sets.ue_var[bb_idx.index()].insert(var);
                                }
                            }

                            for seg in in_ueseg.into_iter() {
                                // XXX
                                if !sk.contains(seg) {
                                    fixedpoint &= !liveness_sets.ue_seg[bb_idx.index()].insert(seg);
                                }
                            }
                        }
                        _ => {}
                    }
                }
            }
        }
    }

    fn record_seg_read(&mut self, bb: usize, seg: &Segment) -> Result<()> {
        let sk = &mut self.seg_kill[bb];
        let ues = &mut self.ue_seg[bb];

        // XXX
        if !sk.contains(seg.name.index()) {
            ues.insert(seg.name.index());
        }

        Ok(())
    }

    fn record_seg_write(&mut self, bb: usize, seg: &Segment) -> Result<()> {
        let sk = &mut self.seg_kill[bb];

        sk.insert(seg.name.index());
        Ok(())
    }

    fn record_var_read(&mut self, bb: usize, var: &Variable) -> Result<()> {
        let vk = &mut self.var_kill[bb];
        let uev = &mut self.ue_var[bb];

        // XXX
        if !vk.contains(var.name.index()) {
            uev.insert(var.name.index());
        }

        Ok(())
    }

    fn record_var_write(&mut self, bb: usize, var: &Variable) -> Result<()> {
        let vk = &mut self.var_kill[bb];

        vk.insert(var.name.index());
        Ok(())
    }
}

#[derive(Debug)]
pub struct Globals {
    pub global_vars: BitSet,
    pub global_segs: BitSet,
    pub var_usage: Vec<BitSet>,
    pub seg_usage: Vec<BitSet>,
}

impl Globals {
    pub fn new(func: &Function, liveness: &Liveness) -> Result<Globals> {
        let _prof = hprof::enter("compute global var set");
        let num_bb = func.basic_blocks().len();
        let mut ret = Globals{
            global_vars: BitSet::with_capacity(func.names.len()),
            global_segs: BitSet::with_capacity(func.segments.len()),
            var_usage: vec![BitSet::with_capacity(num_bb); func.names.len()],
            seg_usage: vec![BitSet::with_capacity(num_bb); func.segments.len()],
        };

        Self::compute(&mut ret,liveness);
        Ok(ret)
    }

    fn compute(globals: &mut Self, liveness: &Liveness) {
        for uevar in liveness.ue_var.iter() {
            globals.global_vars.union_with(uevar);
        }

        for (bb,varkill) in liveness.var_kill.iter().enumerate() {
            for i in varkill.iter() { globals.var_usage[i].insert(bb); }
        }

        for ueseg in liveness.ue_seg.iter() {
            globals.global_segs.union_with(ueseg);
        }

        for (bb,segkill) in liveness.seg_kill.iter().enumerate() {
            for i in segkill.iter() { globals.seg_usage[i].insert(bb); }
        }
    }
}

#[derive(Debug)]
pub struct ReachingDefs {
    // bb -> (base name -> new subscript)
    pub gen_var: Vec<VecMap<NameRef>>,
}

impl ReachingDefs {
    pub fn new(func: &Function) -> Result<ReachingDefs> {
        let num_bb = func.basic_blocks().len();
        let mut ret = ReachingDefs{
            gen_var: vec![VecMap::default(); num_bb],
        };

        for (idx,bb) in func.basic_blocks() {
            for stmt in func.statements(bb) {
                match &*stmt {
                    /*
                    &Statement::Expression{ op: Operation::Load(ref segment,_,_,ref addr), ref result } => {
                        if let &Value::Variable(ref addr) = addr {
                            ret.record_var_read(idx.index(),addr)?;
                        }
                        ret.record_var_write(idx.index(),result)?;
                        ret.record_seg_read(idx.index(),segment)?;
                    }
                    */
                    &Statement::Expression{ ref result,.. } => {
                        ret.record_var_write(idx.index(), result, &func.names)?;
                    }
                    /*
                    &Statement::Memory{ op: MemoryOperation::Store{ ref address, ref value, ref segment,.. }, ref result } => {
                        if let &Value::Variable(ref var) = address {
                            ret.record_var_read(idx.index(),var)?;
                        }
                        if let &Value::Variable(ref var) = value {
                            ret.record_var_read(idx.index(),var)?;
                        }
                        ret.record_seg_read(idx.index(),segment)?;
                        ret.record_seg_write(idx.index(),result)?;
                    }
                    &Statement::Memory{ op: MemoryOperation::MemoryPhi(ref a,ref b,ref c), ref result } => {
                        if let &Some(ref a) = a {
                            ret.record_seg_read(idx.index(),a)?;
                        }
                        if let &Some(ref b) = b {
                            ret.record_seg_read(idx.index(),b)?;
                        }
                        if let &Some(ref c) = c {
                            ret.record_seg_read(idx.index(),c)?;
                        }
                        ret.record_seg_write(idx.index(),result)?;
                    }
                    */
                    _ => { /* skip */ }
                }
            }
        }

        Self::propagate(func, &mut ret);

        Ok(ret)
    }

    fn propagate(func: &Function, sets: &mut Self) {
        let cfg = func.cflow_graph();
        let mut fixedpoint = false;

        while !fixedpoint {
            fixedpoint = true;
            for (bb_idx,bb) in func.basic_blocks() {

                for pred in cfg.neighbors_directed(bb.node, Direction::Incoming) {
                    match cfg.node_weight(pred) {
                        Some(&CfgNode::BasicBlock(pred_idx)) => {
                            let in_gen = sets.gen_var[pred_idx.index()].clone();
                            let gen = &mut sets.gen_var[bb_idx.index()];

                            for (base, name) in in_gen {
                                if !gen.contains_key(base) {
                                    fixedpoint = false;
                                    gen.insert(base, name);
                                }
                            }
                        }
                        _ => {}
                    }
                }
            }
        }
    }
/*
    fn record_seg_write(&mut self, bb: usize, seg: &Segment) -> Result<()> {
        let sk = &mut self.seg_kill[bb];

        sk.insert(seg.name.index());
        Ok(())
    }
*/
    fn record_var_write(&mut self, bb: usize, var: &Variable, names: &Names) -> Result<()> {
        let gen = &mut self.gen_var[bb];
        let base = names.base_name(var.name)?;

        gen.insert(base.index(), var.name);
        Ok(())
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use types::{Region, Function, Name, TestArch, UUID};
    use std::iter::FromIterator;
    use simple_logger;

    /*
     * (B0)
     * 0:  Mi1   ; mov i 1
     *
     * (B1)
     * 3:  Cxi1  ; cmp x i 1
     * 7:  Bx14  ; brlt x 14
     *
     * (B2)
     * 11: Ms0   ; mov s 0
     *
     * (B3)
     * 14: Assi  ; add s s i
     * 18: Aiii  ; add i i i
     * 22: Cxi1  ; cmp x i 1
     * 26: Bx3   ; brlt x 3
     *
     * (B4)
     * 29: Mss   ; use s
     * 32: R     ; ret
     */
    #[test]
    fn live_variables_analysis() {
        use std::iter;
        use types::Name;

        let _ = simple_logger::init();
        let data = b"Mi1Cxi1Bx14Ms0AssiAiiiCxi1Bx3Mss".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let lvs = Liveness::new(&func).unwrap();
        let i = func.names.index(&Name::new("i".into(), None)).unwrap();
        let x = func.names.index(&Name::new("x".into(), None)).unwrap();
        let s = func.names.index(&Name::new("s".into(), None)).unwrap();
        let i_set = BitSet::from_iter(iter::once(i.index()));
        let x_set = BitSet::from_iter(iter::once(x.index()));
        let s_set = BitSet::from_iter(iter::once(s.index()));
        let is_set = BitSet::from_iter(vec![i.index(),s.index()].into_iter());
        let isx_set = BitSet::from_iter(vec![i.index(),s.index(),x.index()].into_iter());

        assert_eq!(lvs.ue_var.len(), 5);
        assert_eq!(lvs.ue_var[0], s_set);
        assert_eq!(lvs.ue_var[1], is_set);
        assert_eq!(lvs.ue_var[2], i_set);
        assert_eq!(lvs.ue_var[3], is_set);
        assert_eq!(lvs.ue_var[4], s_set);

        assert_eq!(lvs.var_kill.len(), 5);
        assert_eq!(lvs.var_kill[0], i_set);
        assert_eq!(lvs.var_kill[1], x_set);
        assert_eq!(lvs.var_kill[2], s_set);
        assert_eq!(lvs.var_kill[3], isx_set);
        assert_eq!(lvs.var_kill[4], s_set);
    }

    /*
     * (B0)
     * 0:  Mix   ; mov i x
     * 3:  J9    ; B1
     *
     * (B1)
     * 9:  Mis   ; mov i s
     * 12: R     ; ret
     */
    #[test]
    fn ue_vars_trans() {
        use std::iter;

        let _ = simple_logger::init();
        let data = b"MixJ9xxxxMisR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let lvs = Liveness::new(&func).unwrap();
        let x = func.names.index(&Name::new("x".into(), None)).unwrap();
        let s = func.names.index(&Name::new("s".into(), None)).unwrap();
        let s_set = BitSet::from_iter(iter::once(s.index()));
        let xs_set = BitSet::from_iter(vec![x.index(),s.index()].into_iter());

        assert_eq!(lvs.ue_var.len(), 2);
        assert_eq!(lvs.ue_var[0], xs_set);
        assert_eq!(lvs.ue_var[1], s_set);
    }

    /*
     * (B0)
     * 0:  Mi1   ; mov i 1
     *
     * (B1)
     * 3:  Cxi1  ; cmp x i 1
     * 7:  Bx3   ; brlt x 3
     *
     * (B2)
     * 10: Ms0   ; mov s 0
     * 13: Assi  ; add s s i
     * 17: Aiii  ; add i i i
     * 21: My1   ; mov y 1
     * 24: Aiiy  ; add i i y
     * 28: Cxi1  ; cmp x i 1
     * 32: Bx3   ; brlt x _
     *
     * (B3)
     * 37: My4   ; mov y 4
     * 41: Mss   ; use s
     * 44: R     ; ret
     */
    #[test]
    fn global_variables() {
        let _ = simple_logger::init();
        let data = b"Mi1Cxi1Bx3Ms0AssiAiiiMy1AiiyCxi1Bx3My4MssR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let lvs = Liveness::new(&func).unwrap();
        let gbls = Globals::new(&func,&lvs).unwrap();
        let i = func.names.index(&Name::new("i".into(), None)).unwrap();
        let x = func.names.index(&Name::new("x".into(), None)).unwrap();
        let s = func.names.index(&Name::new("s".into(), None)).unwrap();
        let y = func.names.index(&Name::new("y".into(), None)).unwrap();
        let isx_set = BitSet::from_iter(vec![i.index(),s.index()].into_iter());
        let i_usage = BitSet::from_iter(vec![0,2].into_iter());
        let s_usage = BitSet::from_iter(vec![2,3].into_iter());
        let x_usage = BitSet::from_iter(vec![1,2].into_iter());
        let y_usage = BitSet::from_iter(vec![2,3].into_iter());

        assert_eq!(gbls.global_vars, isx_set);
        assert_eq!(gbls.var_usage.len(), 4);
        assert_eq!(gbls.var_usage[i.index()], i_usage);
        assert_eq!(gbls.var_usage[s.index()], s_usage);
        assert_eq!(gbls.var_usage[x.index()], x_usage);
        assert_eq!(gbls.var_usage[y.index()], y_usage);
    }

    /*
     * 00: Mb3  ; mov b 3
     * 03: Mc5  ; mov c 5
     * 06: Xabc ; mul a b c
     * 10: R    ; ret
     */
    #[test]
    fn reaching_defs_single() {
        use rewrite_to_ssa;

        let _ = simple_logger::init();
        let data = b"Mb3Mc5XabcR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let reach = ReachingDefs::new(&func).unwrap();
        let base_a = func.names.index(&Name::new("a".into(), None)).unwrap();
        let base_b = func.names.index(&Name::new("b".into(), None)).unwrap();
        let base_c = func.names.index(&Name::new("c".into(), None)).unwrap();

        assert_eq!(reach.gen_var.len(), 1);
        assert!(reach.gen_var[0].contains_key(base_a.index()));
        assert!(reach.gen_var[0].contains_key(base_b.index()));
        assert!(reach.gen_var[0].contains_key(base_c.index()));
    }

    /*
     * 00: Mb3  ; mov b 3
     * 03: Mf1  ; mov f 1
     * 06: Bf16 ; br f L1
     *  L2:
     * 10: Mc1  ; mov c 1
     * 13: J19  ; jmp L3
     *  L1:
     * 16: Mc2  ; mov c 2
     *  L3:
     * 19: Mb1  ; mov b 1
     * 22: R    ; ret
     */
    #[test]
    fn reaching_defs_branch() {
        use rewrite_to_ssa;

        let _ = simple_logger::init();
        let data = b"Mb3Mf1Bf16Mc1J19Mc2Mb1R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let reach = ReachingDefs::new(&func).unwrap();
        let base_b = func.names.index(&Name::new("b".into(), None)).unwrap();
        let base_f = func.names.index(&Name::new("f".into(), None)).unwrap();
        let base_c = func.names.index(&Name::new("c".into(), None)).unwrap();

        println!("{:?}", reach);
        assert_eq!(reach.gen_var.len(), 4);
        assert_eq!(reach.gen_var[3].len(), 3);
        assert!(reach.gen_var[3].contains_key(base_b.index()));
        assert!(reach.gen_var[3].contains_key(base_f.index()));
        assert!(reach.gen_var[3].contains_key(base_c.index()));
        assert_eq!(reach.gen_var[3][base_f.index()], reach.gen_var[1][base_f.index()]);
        assert_eq!(reach.gen_var[2][base_f.index()], reach.gen_var[1][base_f.index()]);
        assert_eq!(reach.gen_var[0][base_f.index()], reach.gen_var[1][base_f.index()]);

        assert!(reach.gen_var[1][base_b.index()] == reach.gen_var[0][base_b.index()]);
        assert!(reach.gen_var[2][base_b.index()] == reach.gen_var[0][base_b.index()]);
        assert!(reach.gen_var[3][base_b.index()] != reach.gen_var[1][base_b.index()]);
        assert!(reach.gen_var[3][base_b.index()] != reach.gen_var[2][base_b.index()]);
    }
}
