// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use types::{
    Statement, NameRef, Function, FlowOperation, Operation, Result, Value,
    Variable, Constant,
};

use std::collections::HashMap;
use std::borrow::Cow;
use std::iter::FromIterator;

use cassowary;
use cassowary::strength::{REQUIRED};
use cassowary::WeightedRelation::{EQ};
use vec_map::VecMap;

#[derive(Clone,PartialEq,Eq,Debug)]
enum Offset {
    Absolute(u64),
    Relative{ region: Cow<'static,str>, offset: u64 },
}

pub fn guilfanovs_method(func: &Function, stack_regs: &[NameRef]) -> Result<Vec<i64>> {
    let (constr,vars) = guilfanov_constraint_set(func,stack_regs)?;
    let mut solver = cassowary::Solver::new();

    solver.add_constraints(&constr).map_err(|e| format!("{:?}",e))?;

    let solution = HashMap::<cassowary::Variable,f64>::from_iter(solver.fetch_changes().iter().cloned());

    Ok(vars.into_iter().map(|var| {
        solution.get(&var).map(|&val| val as i64).unwrap_or(0)
    }).collect())
}

fn guilfanov_constraint_set(func: &Function, stack_regs: &[NameRef])
    -> Result<(Vec<cassowary::Constraint>,Vec<cassowary::Variable>)>
    {
    use petgraph::Direction;

    let cfg = func.cflow_graph();
    let vars = vec![(cassowary::Variable::new(),cassowary::Variable::new()); func.basic_blocks().len()];
    let mut target = VecMap::default();
    let mut constr = Vec::default();
    let mut idx = 0;

    for (bb_idx,_) in func.basic_blocks() {
        let mut env = VecMap::<Offset>::from_iter(
            stack_regs.iter().map(|n| (n.index(),Offset::Relative{ region: "".into(), offset: 0 })));

        for stmt in func.statements(bb_idx) {

            match &*stmt {
                // Calls
                &Statement::Flow{ op: FlowOperation::Call{ .. } }
                | &Statement::Flow{ op: FlowOperation::ExternalCall{ .. } }
                | &Statement::Flow{ op: FlowOperation::IndirectCall{ .. } } => {
                    let reg: Cow<'static,str> = format!("call_{:x}_{}",bb_idx.index(),idx).into();
                    for n in stack_regs.iter() {
                        let offset = if let Some(&Offset::Relative{ offset,.. }) = env.get(n.index()) {
                            offset
                        } else {
                            0
                        };
                        env.insert(n.index(),Offset::Relative{ region: reg.clone(), offset: offset });
                    }
                    idx += 1;
                }

                &Statement::Flow{ op: FlowOperation::Return } => {
                    for r in stack_regs.iter() {
                        env.insert(r.index(),Offset::Relative{ region: "return".into(), offset: 0 });
                    }
                }

                // Addition
                &Statement::Expression{
                    op: Operation::Add(Value::Constant(Constant{ value: v1,.. }),Value::Constant(Constant{ value: v2,.. })),
                    result: Variable{ ref name,.. }
                } => {
                    env.insert(name.index(),Offset::Absolute(v1.wrapping_add(v2)));
                }
                &Statement::Expression{
                    op: Operation::Add(Value::Variable(Variable{ name: ref var,.. }),Value::Constant(Constant{ value: val,.. })),
                    result: Variable{ ref name,.. }
                } |
                &Statement::Expression{
                    op: Operation::Add(Value::Constant(Constant{ value: val,.. }),Value::Variable(Variable{ name: ref var,.. })),
                    result: Variable{ ref name,.. }
                } => {
                    match env.get(var.index()).cloned() {
                        Some(Offset::Absolute(val2)) => {
                            env.insert(name.index(),Offset::Absolute(val.wrapping_add(val2)));
                        }
                        Some(Offset::Relative{ ref region, offset }) => {
                            env.insert(name.index(),Offset::Relative{ region: region.clone(), offset: offset.wrapping_add(val) });
                        }
                        None => {
                            env.remove(name.index());
                        }
                    }
                }

                // Subtraction
                &Statement::Expression{
                    op: Operation::Subtract(Value::Constant(Constant{ value: v1,.. }),Value::Constant(Constant{ value: v2,.. })),
                    result: Variable{ ref name,.. }
                } => {
                    env.insert(name.index(),Offset::Absolute(v1.wrapping_sub(v2)));
                }
                &Statement::Expression{
                    op: Operation::Subtract(Value::Variable(Variable{ name: ref var,.. }),Value::Constant(Constant{ value: val,.. })),
                    result: Variable{ ref name,.. }
                } => {
                    match env.get(var.index()).cloned() {
                        Some(Offset::Absolute(val2)) => {
                            env.insert(name.index(),Offset::Absolute(val2.wrapping_sub(val)));
                        }
                        Some(Offset::Relative{ ref region, offset }) => {
                            env.insert(name.index(),Offset::Relative{ region: region.clone(), offset: offset.wrapping_sub(val) });
                        }
                        None => {
                            env.remove(name.index());
                        }
                    }
                }
                &Statement::Expression{
                    op: Operation::Subtract(Value::Constant(Constant{ value: val,.. }),Value::Variable(Variable{ name: ref var,.. })),
                    result: Variable{ ref name,.. }
                } => {
                    match env.get(var.index()).cloned() {
                        Some(Offset::Absolute(val2)) => {
                            env.insert(name.index(),Offset::Absolute(val.wrapping_sub(val2)));
                        }
                        Some(Offset::Relative{ ref region, offset }) => {
                            env.insert(name.index(),Offset::Relative{ region: region.clone(), offset: val.wrapping_sub(offset) });
                        }
                        None => {
                            env.remove(name.index());
                        }
                    }
                }

                // Move
                &Statement::Expression{ op: Operation::Move(Value::Constant(Constant{ value,.. })), result: Variable{ ref name,.. } } => {
                    env.insert(name.index(),Offset::Absolute(value));
                }
                &Statement::Expression{ op: Operation::Move(Value::Variable(Variable{ name: ref var,.. })), result: Variable{ ref name,.. } } => {
                    let maybe_val = env.get(var.index()).cloned();

                    if let Some(val) = maybe_val {
                        env.insert(name.index(),val.clone());
                    } else {
                        env.remove(name.index());
                    }
                }

                // ZeroExtend
                &Statement::Expression{ op: Operation::ZeroExtend(_,Value::Variable(Variable{ name: ref var,.. })), result: Variable{ ref name,.. } } => {
                    let maybe_val = env.get(var.index()).cloned();

                    if let Some(val) = maybe_val {
                        env.insert(name.index(),val.clone());
                    } else {
                        env.remove(name.index());
                    }
                }

                // SignExtend
                &Statement::Expression{ op: Operation::SignExtend(_,Value::Variable(Variable{ name: ref var,.. })), result: Variable{ ref name,.. } } => {
                    let maybe_val = env.get(var.index()).cloned();

                    if let Some(val) = maybe_val {
                        env.insert(name.index(),val.clone());
                    } else {
                        env.remove(name.index());
                    }
                }

                // Init
                &Statement::Expression{ op: Operation::Initialize(reg,_), result: Variable{ ref name,.. } } => {
                    if stack_regs.iter().find(|&&x| func.names.value(x).ok().map(|x| x.base()) == func.strings.value(reg).ok()).is_some() {
                        env.insert(name.index(),Offset::Relative{ region: "".into(), offset: 0 });
                    } else {
                        env.remove(name.index());
                    }
                }

                // Catch all
                &Statement::Expression{ ref op, result: Variable{ ref name,.. } } => {
                    let mut op = op.clone();

                    for rv in op.reads_mut() {
                        if let Value::Variable(Variable{ ref name, bits,.. }) = rv.clone() {
                            if let Some(&Offset::Absolute(value)) = env.get(name.index()) {
                                *rv = Value::Constant(Constant{ value: value, bits: bits });
                            }
                        }
                    }

                    if let Value::Constant(Constant{ value,.. }) = op.execute()? {
                        env.insert(name.index(),Offset::Absolute(value));
                    } else {
                        env.remove(name.index());
                    }
                }
                &Statement::Memory{ .. } => {}
            }
        }

        let val = stack_regs.first().and_then(|x| env.get(x.index()));
        let non_reg: Cow<'static,str> = "".into();
        let (in_var,out_var) = vars[bb_idx.index()];
        let (c,v) = match val {
            Some(&Offset::Absolute(_)) => {
                let v = cassowary::Variable::new();
                (out_var - in_var |EQ(REQUIRED)| v,Some(v))
            }
            Some(&Offset::Relative{ ref region, offset }) if non_reg == *region => {
                (out_var - in_var |EQ(REQUIRED)| offset as i64 as f64,None)
            }
            Some(&Offset::Relative{ offset,.. }) => {
                let v = cassowary::Variable::new();
                (out_var - in_var |EQ(REQUIRED)| v + offset as i64 as f64,Some(v))
            }
            None => {
                let v = cassowary::Variable::new();
                (out_var - in_var |EQ(REQUIRED)| v,Some(v))
            }
        };

        if let Some(v) = v { target.insert(bb_idx.index(),v); }
        constr.push(c);
    }

    for e in cfg.edge_references() {
        use petgraph::visit::EdgeRef;

        let (_,out_var) = vars[e.source().index()];
        let (in_var,_) = vars[e.target().index()];
        constr.push(out_var |EQ(REQUIRED)| in_var);
    }

    for vx in cfg.node_indices().filter(|&vx| cfg.edges_directed(vx, Direction::Outgoing).next().is_none()) {
        let (_,out_var) = vars[vx.index()];
        constr.push(out_var |EQ(REQUIRED)| 0.0);
    }

    let entry = func.basic_block(func.entry_point()).node;
    let (in_var,_) = vars[entry.index()];
    constr.push(in_var |EQ(REQUIRED)| 0.0);

    Ok((constr,target.into_iter().map(|x| x.1).collect()))
}

#[cfg(test)]
mod tests {
    use super::*;
    use types::{Region, Name, TestArch, UUID};
    use simple_logger;

    #[test]
    fn hexblog_example() {
        // (0)
        // ## cmp [esp+enabled], 0
        // 00   Axs1
        // 04   Lxx
        // ## jz 4
        // 07   Efx0
        // 11   Bf74
        //
        // (1)
        // ## mov eax, [esp+p]
        // 15   Axs2
        // 19   Lax
        // ## mov ecx, [eax]
        // 22   Lca
        // ## mov edx, [ecx]
        // 25   Ldc
        // ## push 1
        // 28   Uss4
        // 32   Ss1
        // ## push eax
        // 35   Uss4
        // 39   Ssa
        // ## call edx
        // 42   Gd
        // ## test eax, eax
        // 44   Efa0
        // ## jz 3
        // 48   Bf66
        //
        // (2)
        // ## add [esp+counter], 1
        // 52   Ays3
        // 56   Lxy
        // 59   Axx1
        // 63   Syx
        //
        // (3)
        // ## mov eax, [esp+counter]
        // 66   Axs3
        // 70   Lax
        // ## retn
        // 73   R
        //
        // (4)
        // ## or eax, 0xffffffff
        // 74   Ma1
        // ## retn
        // 75   R

        let _ = simple_logger::init();
        let reg = Region::from_buf("", 16, b"Axs1LxxEfx0Bf74Axs2LaxLcaLdcUss4Ss1Uss4SsaGdEfa0Bf66Ays3LxyAxx1SyxAxs3LaxRMa1R".to_vec(), None, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let s = func.names.index(&Name::new("s".into(), None)).unwrap();
        let adjustments = guilfanovs_method(&func, &[s]).unwrap();

        assert_eq!(adjustments, vec![0,8,0]);
    }
}
