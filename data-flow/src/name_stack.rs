// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use types::{Name,NameRef,Names,Table,Result,BasicBlockIndex};
use std::default::Default;
use std::fmt::Debug;
use std::hash::Hash;
use std::marker::PhantomData;
use vec_map::VecMap;

#[derive(Clone,Debug)]
pub struct NameStack<T: Sized + From<usize> + Into<usize> + Clone + Copy + PartialEq + Eq + Hash + Debug> {
    counter: VecMap<usize>,
    stack: VecMap<Vec<T>>,
    base_names: VecMap<T>,
    checkpoints: VecMap<VecMap<T>>,
    table: PhantomData<Table<Name,T>>,
}

impl<T: Sized + From<usize> + Into<usize> + Clone + Copy + PartialEq + Eq + Hash + Debug> Default for NameStack<T> {
    fn default() -> NameStack<T> {
        NameStack{
            counter: VecMap::default(),
            stack: VecMap::default(),
            base_names: VecMap::default(),
            checkpoints: VecMap::default(),
            table: PhantomData::default(),
        }
    }
}

impl<T: Sized + From<usize> + Into<usize> + Clone + Copy + PartialEq + Eq + Hash + Debug> NameStack<T> {
    pub fn new(names: &Table<Name,T>) -> NameStack<T> {
        let mut ret = NameStack{
            counter: VecMap::default(),
            stack: VecMap::default(),
            base_names: VecMap::default(),
            checkpoints: VecMap::default(),
            table: PhantomData::default(),
        };

        for idx in 0..names.len() {
            let is_base = names.value(idx.into())
                .map(|x| x.subscript().is_none())
                .unwrap_or(false);
            if is_base {
                ret.counter.insert(idx,0);
                ret.stack.insert(idx,vec![]);
            }
        }

        ret
    }

    pub fn with_metainfo(base: T, bb_idx: BasicBlockIndex, meta: NameStackMetainfo<T>) -> NameStack<T> {
        let mut ret = NameStack{
            counter: VecMap::default(),
            stack: VecMap::default(),
            base_names: meta.base_names,
            checkpoints: meta.checkpoints,
            table: PhantomData::default(),
        };
        let name = ret.base_names.get(base.clone().into()).cloned().unwrap_or(base.clone());
        let subscript = meta.next_subscripts[name.clone().into()];

        ret.counter.insert(name.clone().into(),subscript);
        ret.stack.insert(name.into(),vec![ret.checkpoints[bb_idx.index()][name.clone().into()]]);

        ret
    }

    pub fn new_name(&mut self, var: &mut T, names: &mut Table<Name,T>) -> Result<()> {
        if !self.counter.contains_key(var.clone().into()) {
            if let Some(base) = self.base_names.get(var.clone().into()) {
                if !self.counter.contains_key(base.clone().into()) { return Ok(()); }
                *var = base.clone();
            } else {
                return Ok(());
            }
        }

        let i = self.counter[var.clone().into()];
        let name = names.value(*var)
            .map(|x| x.base().clone()).unwrap();
        let new = names.insert(&Name::new(name,Some(i)));

        self.base_names.insert(new.clone().into(),var.clone());
        self.counter[var.clone().into()] += 1;
        self.stack[var.clone().into()].push(new);
        *var = new;

        Ok(())
    }

    pub fn pop(&mut self, mut var: T) {
        if !self.counter.contains_key(var.clone().into()) {
            if let Some(base) = self.base_names.get(var.clone().into()) {
                if !self.counter.contains_key(base.clone().into()) { return; }
                var = base.clone();
            } else {
                return;
            }
        }

        self.stack[var.clone().into()].pop();
    }

    pub fn top(&self, var: &mut T) -> bool {
        if !self.counter.contains_key(var.clone().into()) {
            if let Some(base) = self.base_names.get(var.clone().into()) {
                if !self.counter.contains_key(base.clone().into()) { return false; }
                *var = base.clone();
            } else {
                return false;
            }
        }

        let stack = &self.stack[var.clone().into()];
        if stack.is_empty() {
            println!("{:?}", var);
        }
        let lst = stack.len() - 1;

        *var = stack[lst];
        true
    }

    pub fn checkpoint(&mut self, bb_idx: BasicBlockIndex) {
        let num_nams = self.stack.len();
        self.checkpoints.entry(bb_idx.index()).or_insert_with(|| {
            VecMap::<T>::with_capacity(num_nams)
        });

        let state = &mut self.checkpoints[bb_idx.index()];
        for (i,stack) in self.stack.iter() {
            if !stack.is_empty() {
                state.insert(i,stack.last().cloned().unwrap());
            }
        }
    }

    pub fn into_metainfo(self) -> NameStackMetainfo<T> {
        NameStackMetainfo{
            next_subscripts: self.counter,
            base_names: self.base_names,
            checkpoints: self.checkpoints,
            table: PhantomData::default(),
        }
    }
}

#[derive(Clone,Debug)]
pub struct NameStackMetainfo<T: Sized + From<usize> + Into<usize> + Clone + Copy + PartialEq + Eq + Hash + Debug> {
    next_subscripts: VecMap<usize>,
    base_names: VecMap<T>,
    checkpoints: VecMap<VecMap<T>>,
    table: PhantomData<Table<Name,T>>,
}

impl<T: Sized + From<usize> + Into<usize> + Clone + Copy + PartialEq + Eq + Hash + Debug> NameStackMetainfo<T> {
    pub fn merge(old: &mut NameStackMetainfo<T>, new: NameStackMetainfo<T>) {
        for (idx,subsc) in new.next_subscripts {
            old.next_subscripts.insert(idx,subsc);
        }

        for (bb_idx,chk_pnt) in new.checkpoints {
            for (idx,active) in chk_pnt {
                old.checkpoints[bb_idx].insert(idx,active);
            }
        }

        old.base_names = new.base_names;
    }

    pub fn active_definition(&self, bb_idx: BasicBlockIndex, base: T) -> Result<T> {
        let base = self.base_names
            .get(base.clone().into())
            .cloned()
            .unwrap_or(base.clone());
        Ok(self.checkpoints[bb_idx.index()][base.clone().into()])
    }
}

impl NameStackMetainfo<NameRef> {
    pub fn new_name(&mut self, name: NameRef, names: &mut Names) -> Result<NameRef> {
        let base = names.base_name(name)?;
        let base_val = names.value(base)?.clone();
        let base_idx: usize = base.clone().into();
        let subs: usize = self.next_subscripts[base_idx];
        let ret = names.insert(&Name::new(base_val.base().clone(),subs));

        self.next_subscripts[base_idx] += 1;
        self.base_names.insert(ret.clone().into(),base);
        Ok(ret)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn name_stack_top() {
        let mut names = Names::default();

        for i in 0..11 {
            names.insert(&Name::new(format!("v{}",i).into(),None));
        }

        let mut stack = NameStack::new(&names);

        for i in 0..11 {
            let mut t = names.insert(&Name::new(format!("v{}",i).into(),None));
            let mut s = t;

            stack.new_name(&mut s,&mut names).unwrap();
            stack.top(&mut t);

            assert_eq!(t, names.insert(&Name::new(format!("v{}",i).into(),Some(0))));
        }
    }

    #[test]
    fn name_stack_new() {
        let mut names = Names::default();
        for i in 0..11 {
            names.insert(&Name::new(format!("v{}",i).into(),None));
        }

        let mut stack = NameStack::new(&names);
        for i in 0..11 {
            let mut t = names.insert(&Name::new(format!("v{}",i).into(),None));
            stack.new_name(&mut t,&mut names).unwrap();
            stack.new_name(&mut t,&mut names).unwrap();
            assert_eq!(t, names.insert(&Name::new(format!("v{}",i).into(),Some(1))));
            stack.pop(t);
            stack.top(&mut t);
            assert_eq!(t, names.insert(&Name::new(format!("v{}",i).into(),Some(0))));
        }
    }

    #[test]
    fn name_stack_continue() {
        let mut names = Names::default();

        let _ = names.insert(&Name::new("a".into(),None));
        let b = names.insert(&Name::new("b".into(),None));
        let mut stack = NameStack::new(&names);

        let mut t = names.insert(&Name::new("a".into(),None));
        let mut s = names.insert(&Name::new("b".into(),None));

        stack.new_name(&mut s,&mut names).unwrap();
        println!("1: {:?}",names.value(s));
        assert_eq!(s, names.insert(&Name::new("b".into(),Some(0))));

        stack.new_name(&mut t,&mut names).unwrap();
        println!("2: {:?}",names.value(t));
        assert_eq!(t, names.insert(&Name::new("a".into(),Some(0))));

        println!("3.0: {:?}",names.value(t));
        stack.new_name(&mut t,&mut names).unwrap();
        println!("3.1: {:?}",names.value(t));
        assert_eq!(t, names.insert(&Name::new("a".into(),Some(1))));

        stack.checkpoint(BasicBlockIndex::new(0));
        stack.new_name(&mut s,&mut names).unwrap();
        println!("4: {:?}",names.value(s));

        stack.pop(t);
        stack.pop(t);
        stack.pop(s);
        stack.pop(s);

        let meta = stack.into_metainfo();
        let mut stack = NameStack::with_metainfo(b,BasicBlockIndex::new(0),meta);

        let mut s = names.insert(&Name::new("b".into(),Some(0)));
        stack.top(&mut s);
        assert_eq!(s, names.insert(&Name::new("b".into(),Some(0))));
        stack.new_name(&mut s,&mut names).unwrap();
        assert_eq!(s, names.insert(&Name::new("b".into(),Some(2))));
    }

    #[test]
    fn name_stack_meta_new_def() {
        let mut names = Names::default();

        let _ = names.insert(&Name::new("a".into(),None));
        let b = names.insert(&Name::new("b".into(),None));
        let mut stack = NameStack::new(&names);

        let mut t = names.insert(&Name::new("a".into(),None));
        let mut s = names.insert(&Name::new("b".into(),None));

        stack.new_name(&mut s,&mut names).unwrap();
        println!("1: {:?}",names.value(s));
        assert_eq!(s, names.insert(&Name::new("b".into(),Some(0))));

        stack.new_name(&mut t,&mut names).unwrap();
        println!("2: {:?}",names.value(t));
        assert_eq!(t, names.insert(&Name::new("a".into(),Some(0))));

        println!("3.0: {:?}",names.value(t));
        stack.new_name(&mut t,&mut names).unwrap();
        println!("3.1: {:?}",names.value(t));
        assert_eq!(t, names.insert(&Name::new("a".into(),Some(1))));

        stack.checkpoint(BasicBlockIndex::new(0));
        stack.new_name(&mut s,&mut names).unwrap();
        println!("4: {:?}",names.value(s));

        stack.pop(t);
        stack.pop(t);
        stack.pop(s);
        stack.pop(s);

        let mut meta = stack.into_metainfo();
        let new = meta.new_name(b,&mut names).unwrap();
        println!("new: {:?}",names.value(new));
        assert_eq!(new, names.insert(&Name::new("b".into(),Some(2))));
    }

    #[test]
    fn name_stack_meta_active_def() {
        let mut names = Names::default();

        let _ = names.insert(&Name::new("a".into(),None));
        let b = names.insert(&Name::new("b".into(),None));
        let mut stack = NameStack::new(&names);

        let mut t = names.insert(&Name::new("a".into(),None));
        let mut s = names.insert(&Name::new("b".into(),None));

        stack.new_name(&mut s,&mut names).unwrap();
        println!("1: {:?}",names.value(s));
        assert_eq!(s, names.insert(&Name::new("b".into(),Some(0))));

        stack.new_name(&mut t,&mut names).unwrap();
        println!("2: {:?}",names.value(t));
        assert_eq!(t, names.insert(&Name::new("a".into(),Some(0))));

        println!("3.0: {:?}",names.value(t));
        stack.new_name(&mut t,&mut names).unwrap();
        println!("3.1: {:?}",names.value(t));
        assert_eq!(t, names.insert(&Name::new("a".into(),Some(1))));

        stack.checkpoint(BasicBlockIndex::new(0));
        stack.new_name(&mut s,&mut names).unwrap();
        println!("4: {:?}",names.value(s));

        stack.pop(t);
        stack.pop(t);
        stack.pop(s);
        stack.pop(s);

        let meta = stack.into_metainfo();
        let act = meta.active_definition(BasicBlockIndex::new(0),b).unwrap();
        println!("active: {:?}",names.value(act));
        assert_eq!(act, names.insert(&Name::new("b".into(),Some(0))));
    }
}
