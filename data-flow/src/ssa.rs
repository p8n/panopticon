// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use petgraph::Direction;
use petgraph::graph::{NodeIndex,EdgeIndex};
use petgraph::algo::dominators;
use petgraph::algo::dominators::Dominators;
use petgraph::visit::EdgeRef;
use bit_set::BitSet;
use smallvec::SmallVec;
use hprof;
use vec_map::VecMap;

use types::{
    Guard, Function, Result, CfgNode, Statement, Operation, BasicBlockIndex,
    Value, Variable, RewriteControl, NameRef, Name, Names, MemoryOperation,
    FlowOperation, Segment, SegmentRef, Segments, StrRef
};
use {
    NameStack, NameStackMetainfo, Globals
};

pub struct SsaMetainfo {
    pub var_name_stack: NameStackMetainfo<NameRef>,
    pub globals: Globals,
    pub phi: PhiFunctions,
    pub var_bits: VecMap<usize>,
}

#[derive(Clone,Debug)]
pub struct PhiFunctions {
    /// Map from basic blocks to base names
    pub values: Vec<BitSet>,
    pub segments: Vec<BitSet>,
}

impl PhiFunctions {
    pub fn new(func: &Function, globals: &Globals, domfronts: &Vec<BitSet>) -> Result<PhiFunctions> {
        let _prof = hprof::enter("phi function set");
        let mut values = vec![BitSet::with_capacity(func.names.len()); func.basic_blocks().len()];
        let mut segments = vec![BitSet::with_capacity(func.segments.len()); func.basic_blocks().len()];

        for g_idx in globals.global_segs.iter() {
            let mut worklist = globals.seg_usage[g_idx].clone();

            while !worklist.is_empty() {
                let bb_idx = worklist.iter().next().unwrap();

                worklist.remove(bb_idx);
                for df_idx in domfronts[bb_idx].iter() {
                    if !segments[df_idx].contains(g_idx) {
                        segments[df_idx].insert(g_idx);
                        worklist.insert(df_idx);
                    }
                }
            }
        }

        for g_idx in globals.global_vars.iter() {
            let mut worklist = globals.var_usage[g_idx].clone();

            while !worklist.is_empty() {
                let bb_idx = worklist.iter().next().unwrap();

                worklist.remove(bb_idx);
                for df_idx in domfronts[bb_idx].iter() {
                    if !values[df_idx].contains(g_idx) {
                        values[df_idx].insert(g_idx);
                        worklist.insert(df_idx);
                    }
                }
            }
        }

        Ok(PhiFunctions{
            values: values,
            segments: segments,
        })
    }
}

fn dominance_frontiers(func: &Function, doms: &Dominators<NodeIndex>) -> Result<Vec<BitSet>> {
    let _prof = hprof::enter("collect dom frontiers");
    let cfg = func.cflow_graph();
    let num_bb = func.basic_blocks().len();
    let mut ret = vec![BitSet::with_capacity(num_bb); num_bb];

    for n in cfg.node_indices() {
        if let Some(&CfgNode::BasicBlock(bb_idx)) = cfg.node_weight(n) {
            let preds = cfg.neighbors_directed(n,Direction::Incoming).collect::<Vec<_>>();

            if preds.len() > 1 {
                for p in preds {
                    let mut runner = p;
                    let idom = doms.immediate_dominator(n);

                    while idom.is_some() && Some(runner) != idom {
                        if let Some(&CfgNode::BasicBlock(run_idx)) = cfg.node_weight(runner) {
                            ret[run_idx.index()].insert(bb_idx.index());
                            runner = doms.immediate_dominator(runner)
                                .ok_or(format!("{:?} has not immediate dominator", runner))?;
                        } else {
                            return Err("Internal error: cfg node has a predecessor that's not a basic block".into());
                        }
                    }
                }
            }
        }
    }

    Ok(ret)
}

fn clear_data_flow_information(func: &mut Function) -> Result<()> {
    let str_init = func.strings.insert(&"__init".into());
    let str_alloc = func.strings.insert(&"__alloc".into());
    let str_phi = func.strings.insert(&"__phi".into());
    let str_mphi = func.strings.insert(&"__memory_phi".into());

    // Remove Initialize and Allocate instructions
    // Remove all Phi functions
    let indices = func.basic_blocks().map(|(i,_)| i).collect::<Vec<_>>();
    for idx in indices {
        loop {
            let mut remove = None;

            for (mne_idx, mne) in func.mnemonics(idx) {
                let del = str_phi == mne.opcode || str_mphi == mne.opcode || mne.opcode == str_init || mne.opcode == str_alloc;

                if del {
                    remove = Some(mne_idx);
                    break;
                }
            }

            if let Some(idx) = remove {
                func.remove_mnemonic(idx)?;
            } else {
                break;
            }
        }
    }

    Ok(())
}

fn fix_uninitialized(func: &mut Function, uninit_var: BitSet, bits: &VecMap<usize>, uninit_seg: BitSet) -> Result<()> {
    let _prof = hprof::enter("fix unassigned vars & segs");
    let init_stmts = uninit_var.iter().map(|bit| {
        let nameref = NameRef::new(bit);
        let name = func.names.value(nameref).unwrap();
        println!("{:?}", name);
        let s = func.strings.insert(name.base());
        let b = bits.get(bit).unwrap();

        Statement::Expression{
            op: Operation::Initialize(s,*b),
            result: Variable::new(nameref,*b).unwrap(),
        }
    }).collect::<Vec<_>>();
    let alloc_stmts = uninit_seg.iter().map(|bit| {
        let segref = SegmentRef::new(bit);
        let seg = func.segments.value(segref).unwrap();
        let s = func.strings.insert(seg.base());

        Statement::Memory{
            op: MemoryOperation::Allocate{ base: s },
            result: Segment{ name: segref },
        }
    }).collect::<Vec<_>>();
    let entry = func.entry_point();
    let str_init = func.strings.insert(&"__init".into());
    let str_alloc = func.strings.insert(&"__alloc".into());
  
    if !alloc_stmts.is_empty() {
        func.insert_mnemonic(entry,0,str_alloc,SmallVec::default(),alloc_stmts)?;
    }

    if !init_stmts.is_empty() {
        func.insert_mnemonic(entry,0,str_init,SmallVec::default(),init_stmts)?;
    }

    Ok(())
}

#[derive(Debug,PartialEq,Eq,PartialOrd,Ord)]
enum DomTreeEvent {
    Enter{
        index: BasicBlockIndex,
        successors: SmallVec<[(BasicBlockIndex,usize); 2]>,
        out_edges: SmallVec<[EdgeIndex; 2]>,
        start: u64,
        num_in_edges: usize,
    },
    Leave(BasicBlockIndex),
}

fn insert_phi_mnemonic(func: &mut Function, num_in_edges: usize, bb_idx: BasicBlockIndex, base: NameRef,
                       bits: usize, str_phi: StrRef) -> Result<()> {

    let num_phis = if num_in_edges <= 3 { 1 } else { (num_in_edges + 1) / 2 };
    let mut stmts = Vec::with_capacity(num_phis);

    debug!("insert {} ({} edges) phi nodes for {:?}",num_phis,num_in_edges,func.names.value(base));

    for i in 0..num_phis {
        let prev = if i > 0 {
            Value::var(base,bits)?
        } else {
            Value::undef()
        };

        stmts.push(Statement::Expression{
            op: Operation::Phi(
                prev,
                Value::undef(),
                Value::undef()),
            result: Variable::new(base,bits)?,
        });
    }

    func.insert_mnemonic(bb_idx,0,str_phi,SmallVec::default(),stmts)
}

fn insert_phi_operations(func: &mut Function, phis: &PhiFunctions,
                         dom_events: &Vec<DomTreeEvent>, bits: &VecMap<usize>) -> Result<()> {
    let _prof = hprof::enter("insert phi ops");
    let str_phi = func.strings.insert(&"__phi".into());
    let str_mphi = func.strings.insert(&"__memory_phi".into());
 
    for ev in dom_events {
        match ev {
            &DomTreeEvent::Enter{ index: bb_idx, num_in_edges,.. } => {
                // Insert new Phi functions
                for var_idx in phis.values[bb_idx.index()].iter() {
                    let name = NameRef::new(var_idx);
                    let b = *bits.get(var_idx).unwrap();

                    insert_phi_mnemonic(func,num_in_edges,bb_idx,name,b,str_phi)?;
                }

                // Insert new MemoryPhi functions
                for seg_idx in phis.segments[bb_idx.index()].iter() {
                    let seg = Segment{ name: SegmentRef::new(seg_idx) };
                    let num_phis = if num_in_edges <= 3 { 1 } else { (num_in_edges + 1) / 2 };
                    let mut stmts = Vec::with_capacity(num_phis);

                    for i in 0..num_phis {
                        let prev = if i > 0 {
                            Some(seg.clone())
                        } else {
                            None
                        };

                        stmts.push(Statement::Memory{
                            op: MemoryOperation::MemoryPhi(prev,None,None),
                            result: seg.clone(),
                        });
                    }

                    func.insert_mnemonic(bb_idx,0,str_mphi,SmallVec::default(),stmts)?;
                }

            }
            &DomTreeEvent::Leave(_) => { /* skip */ }
        }
    }

    Ok(())
}

fn assign_subscripts(func: &mut Function, dom_events: &[DomTreeEvent],
                     var_name_stack: &mut NameStack<NameRef>,
                     seg_name_stack: &mut NameStack<SegmentRef>) -> Result<()> {

    let _prof = hprof::enter("assign subscripts");

    for ev in dom_events.iter() {
        match ev {
            &DomTreeEvent::Enter{ index: bb_idx, ref successors, ref out_edges,.. } => {
                debug!("rewrite {}",bb_idx.index());

                var_name_stack.checkpoint(bb_idx);
                seg_name_stack.checkpoint(bb_idx);

                // Rewrite operations
                func.rewrite_basic_block(bb_idx,|stmt,names,_,segments| {
                    use types::Statement::*;
                    debug!("  handle {:?}",stmt);

                    match stmt {
                        &mut Expression{ op: Operation::Phi(_,_,_), ref mut result } => {
                            debug!("    new name for res {:?} ({:?})",result.name,names.value(result.name));
                            var_name_stack.new_name(&mut result.name,names)?;
                            debug!("     = {:?} ({:?})",result.name,names.value(result.name));
                        }
                        &mut Memory{ op: MemoryOperation::MemoryPhi(_,_,_), ref mut result } => {
                            seg_name_stack.new_name(&mut result.name,segments)?;
                        }
                        &mut Expression{ ref mut op, ref mut result } => {

                            for v in op.reads_mut() {
                                if let &mut Value::Variable(ref mut var) = v {
                                    debug!("    assign subscript {:?} ({:?})",var.name,names.value(var.name));
                                    var_name_stack.top(&mut var.name);
                                    debug!("     = {:?} ({:?})",var.name,names.value(var.name));
                                }
                            }

                            if let &mut Operation::Load(Segment{ ref mut name },_,_,_) = op {
                                debug!("    assign mem subscript {:?}",name);
                                seg_name_stack.top(name);
                            }

                            var_name_stack.new_name(&mut result.name,names)?;
                            debug!("    assign result {:?} ({:?})",result.name,names.value(result.name));
                        }
                        &mut Flow{ op: FlowOperation::IndirectCall{ ref mut target } } => {
                                var_name_stack.top(&mut target.name);
                        }
                        &mut Flow{ op: FlowOperation::Call{ .. } } => { /* skip */ }
                        &mut Flow{ op: FlowOperation::ExternalCall{ .. } } => { /* skip */ }
                        &mut Flow{ op: FlowOperation::Return } => { /* skip */ }
                        &mut Memory{ op: MemoryOperation::Store{ ref mut segment, ref mut address, ref mut value,.. }, ref mut result } => {
                            if let &mut Value::Variable(ref mut var) = address {
                                var_name_stack.top(&mut var.name);
                            }
                            if let &mut Value::Variable(ref mut var) = value {
                                var_name_stack.top(&mut var.name);
                            }

                            seg_name_stack.top(&mut segment.name);

                            seg_name_stack.new_name(&mut result.name,segments)?;
                        }
                        &mut Memory{ op: MemoryOperation::Allocate{ .. }, ref mut result } => {
                            seg_name_stack.new_name(&mut result.name,segments)?;
                        }
                    }

                    Ok(RewriteControl::Continue)
                })?;

                // Rewrite guards & indirect jumps
                for &edge in out_edges.iter() {
                    match func.cflow_graph_mut().edge_weight_mut(edge) {
                        Some(&mut Guard::Predicate{ flag: Variable{ ref mut name,.. },.. }) => {
                            debug!("  handle guard {:?}", name);
                            var_name_stack.top(name);
                        }
                        _ => {}
                    }

                    let dst = func.cflow_graph().edge_endpoints(edge).unwrap().1;
                    match func.cflow_graph_mut().node_weight_mut(dst) {
                        Some(&mut CfgNode::Value(Value::Variable(Variable{ ref mut name,.. }))) => {
                            debug!("  handle indir jmp {:?}", name);
                            var_name_stack.top(name);
                        }
                        _ => {}
                    }
                }

                // Fill Phi function args of all successor nodes
                'outer: for &(succ,phi_slot) in successors {
                    debug!("in {} handle successor {}",bb_idx.index(),succ.index());

                    let mut phi_slot_offset = 0;

                    func.rewrite_basic_block(succ,|stmt,names,_,segments| {
                        use types::Statement::*;
                        use types::Operation::Phi;
                        use types::MemoryOperation::MemoryPhi;

                        match stmt {
                            &mut Expression{ op: Phi(ref mut a, ref mut b, ref mut c), ref result } => {
                                debug!("  phi for {:?} ({:?})",result,names.value(result.name));

                                let mut nam = result.name.clone();
                                if var_name_stack.top(&mut nam) {
                                    match phi_slot - phi_slot_offset {
                                        0 => {
                                            debug!("    set 1st to {:?} ({:?})",nam,names.value(nam));
                                            *a = Value::var(nam,result.bits)?;
                                            phi_slot_offset = 0;
                                        }
                                        1 => {
                                            debug!("    set 2nd to {:?} ({:?})",nam,names.value(nam));
                                            *b = Value::var(nam,result.bits)?;
                                            phi_slot_offset = 0;
                                        }
                                        2 => {
                                            debug!("    set 3rd to {:?} ({:?})",nam,names.value(nam));
                                            *c = Value::var(nam,result.bits)?;
                                            phi_slot_offset = 0;
                                        }
                                        _ => {
                                            /* fall-thru to next Phi */
                                            if phi_slot_offset == 0 {
                                                phi_slot_offset += 3;
                                            } else {
                                                phi_slot_offset += 2;
                                            }
                                        }
                                    }
                                }

                                Ok(RewriteControl::Continue)
                            }
                            &mut Memory{ op: MemoryPhi(ref mut a, ref mut b, ref mut c), ref result } => {
                                debug!("  mphi for {:?} ({:?})",result,segments.value(result.name));

                                let mut nam = result.name;
                                debug!("base is {:?}",nam);
                                seg_name_stack.top(&mut nam);

                                if *a == None {
                                    debug!("    set 1st to {:?} ({:?})",nam,segments.value(nam));
                                    *a = Some(Segment{ name: nam });
                                } else if *b == None {
                                    debug!("    set 2nd to {:?} ({:?})",nam,segments.value(nam));
                                    *b = Some(Segment{ name: nam });
                                } else if *c == None {
                                    debug!("    set 3rd to {:?} ({:?})",nam,segments.value(nam));
                                    *c = Some(Segment{ name: nam });
                                } else {
                                    /* fall-thru to next MemoryPhi */
                                }

                                Ok(RewriteControl::Continue)
                            }
                            _ => {
                                /* skip non __*phi mnemonics */
                                Ok(RewriteControl::Break)
                            }
                        }
                    })?;
                }
            }
            &DomTreeEvent::Leave(bb_idx) => {
                // Pop ssa names
                func.rewrite_basic_block(bb_idx,|stmt,_,_,_| {
                    use types::Statement::*;
                    debug!("pop {}",bb_idx.index());

                    match stmt {
                        &mut Expression{ ref mut result,.. } => {
                            let nam = result.name;
                            var_name_stack.pop(nam);
                        }
                        &mut Flow{ .. } => { /* skip */ }
                        &mut Memory{ ref mut result,.. } => {
                            let nam = result.name;
                            seg_name_stack.pop(nam);
                        }
                    }

                    Ok(RewriteControl::Continue)
                })?;
            }
        }
    }

    Ok(())
}

fn dominator_tree(func: &Function, doms: &Dominators<NodeIndex>) -> Result<Vec<DomTreeEvent>> {
    use petgraph::visit::IntoNodeReferences;

    let _prof = hprof::enter("dominator tree");
    let num_bb = func.basic_blocks().len();
    let cfg = func.cflow_graph();
    let mut tree = vec![BitSet::with_capacity(num_bb); num_bb];

    for (bb_idx,bb) in func.basic_blocks() {
        if let Some(idom) = doms.immediate_dominator(bb.node) {
            if let Some(&CfgNode::BasicBlock(p_idx)) = cfg.node_weight(idom) {
                tree[p_idx.index()].insert(bb_idx.index());
            }
        }
    }

    let mut completed = BitSet::with_capacity(num_bb);
    let mut processing = BitSet::with_capacity(num_bb);
    let mut stack = Vec::with_capacity(num_bb);
    let entry_idx = match cfg.node_weight(doms.root()) {
        Some(&CfgNode::BasicBlock(i)) => i,
        _ => { return Err("Internal error: dominator tree root isn't a basic block".into()); }
    };
    let mut ret = Vec::with_capacity(num_bb * 2);
    let succs = cfg.node_references().filter_map(|(idx,r)| {
        match r {
                &CfgNode::BasicBlock(_) => {
                    Some(cfg.neighbors_directed(idx,Direction::Outgoing).filter_map(|y| {
                        match cfg.node_weight(y) {
                            Some(&CfgNode::BasicBlock(j)) => Some(j),
                            _ => None
                        }
                    }).collect::<SmallVec<[BasicBlockIndex; 2]>>())
                }
                _ => None
            }
    }).collect::<Vec<_>>();
    let mut preds = cfg.node_references().filter_map(|(idx,r)| {
        match r {
                &CfgNode::BasicBlock(_) => {
                    Some(cfg.neighbors_directed(idx,Direction::Incoming).filter_map(|y| {
                        match cfg.node_weight(y) {
                            Some(&CfgNode::BasicBlock(j)) => Some(j),
                            _ => None
                        }
                    }).collect::<SmallVec<[BasicBlockIndex; 3]>>())
                }
                _ => None
            }
    }).collect::<Vec<_>>();

    preds.iter_mut().for_each(|x| x.sort());
    stack.push(entry_idx.index());

    {
        let entry = func.basic_block(entry_idx);
        let in_edges = cfg.edges_directed(entry.node,Direction::Incoming).count();
        let succ = succs[entry.node.index()].iter().map(|&x_idx| {
            let x = func.basic_block(x_idx);
            let pos = preds[x.node.index()].iter().position(|&y| y == entry_idx).unwrap();

            (x_idx,pos)
        }).collect::<SmallVec<[(BasicBlockIndex,usize); 2]>>();
        let out_edges = cfg.edges_directed(entry.node,Direction::Outgoing).map(|x| {
            x.id()
        }).collect::<SmallVec<[EdgeIndex; 2]>>();

        ret.push(DomTreeEvent::Enter{
            index: entry_idx,
            start: entry.area.start,
            num_in_edges: in_edges,
            successors: succ,
            out_edges: out_edges,
        });
    }

    'outer: while !stack.is_empty() {
        let n = *stack.last().unwrap();

        for m in tree[n].iter() {
            if !processing.contains(m) {
                // Enter node
                let bb_idx = BasicBlockIndex::new(m);
                let bb = func.basic_block(bb_idx);
                let in_edges = cfg.edges_directed(bb.node,Direction::Incoming).count();
                let succ = succs[bb.node.index()].iter().map(|&x_idx| {
                    let x = func.basic_block(x_idx);
                    let pos = preds[x.node.index()].iter().position(|&y| y == bb_idx).unwrap();

                    (x_idx,pos)
                }).collect::<SmallVec<[(BasicBlockIndex,usize); 2]>>();
                let out_edges = cfg.edges_directed(bb.node,Direction::Outgoing).map(|x| {
                    x.id()
                }).collect::<SmallVec<[EdgeIndex; 2]>>();

                ret.push(DomTreeEvent::Enter{
                    index: bb_idx,
                    start: bb.area.start,
                    num_in_edges: in_edges,
                    successors: succ,
                    out_edges: out_edges,
                });
                stack.push(m);
                processing.insert(m);
                continue 'outer;
            }
        }

        // Leave node
        ret.push(DomTreeEvent::Leave(BasicBlockIndex::new(n)));
        completed.insert(n);
        stack.pop();
    }

    Ok(ret)
}

fn remove_subscripts(func: &mut Function) -> Result<VecMap<usize>> {
    let mut var_replacement = VecMap::<NameRef>::default();
    let mut seg_replacement = VecMap::<SegmentRef>::default();
    let mut var_bits = VecMap::<usize>::default();
    let mut make_var_subscript_free = |name: &mut NameRef,names: &mut Names| -> Result<()> {
        if let Some(n) = var_replacement.get(name.index()).cloned() {
            *name = n;
        } else {
            let n = names.value(*name).map(|x| x.base().clone())?;
            let r = names.insert(&Name::new(n,None));

            var_replacement.insert(name.index(),r);
            *name = r;
        }
        Ok(())
    };
    let mut make_seg_subscript_free = |seg: &mut SegmentRef,segments: &mut Segments| -> Result<()> {
        if let Some(n) = seg_replacement.get(seg.index()).cloned() {
            *seg = n;
        } else {
            let n = segments.value(*seg).map(|x| x.base().clone())?;
            let r = segments.insert(&Name::new(n,None));

            seg_replacement.insert(seg.index(),r);
            *seg = r;
        }
        Ok(())
    };
    let bb_idxs = func.basic_blocks().map(|x| x.0).collect::<Vec<_>>();

    for bb_idx in bb_idxs {
        func.rewrite_basic_block(bb_idx,|stmt,names,_,segments| -> Result<RewriteControl> {
            match stmt {
                &mut Statement::Expression{ op: Operation::Phi(_,_,_),.. } => { panic!(); }
                &mut Statement::Memory{ op: MemoryOperation::MemoryPhi(_,_,_),.. } => { panic!(); }
                &mut Statement::Expression{ ref mut result, ref mut op } => {
                    //var_bits.insert(result.name.index(),result.bits);
                    make_var_subscript_free(&mut result.name,names)?;
                    var_bits.insert(result.name.index(),result.bits);

                    for v in op.reads_mut() {
                        match v {
                            &mut Value::Variable(Variable{ ref mut name, bits }) => {
                                //var_bits.insert(name.index(),bits);
                                make_var_subscript_free(name,names)?;
                                var_bits.insert(name.index(),bits);
                            }
                            _ => { /* fall-thru */  }
                        }
                    }

                    if let &mut Operation::Load(ref mut seg,_,_,_) = op {
                        make_seg_subscript_free(&mut seg.name,segments)?;
                    }
                }
                &mut Statement::Flow{ op: FlowOperation::Call{ .. } } => { /* fall-thru */ }
                &mut Statement::Flow{ op: FlowOperation::ExternalCall{ .. } } => { /* fall-thru */ }
                &mut Statement::Flow{ op: FlowOperation::IndirectCall{ target: Variable{ ref mut name, bits } } } => {
                    //var_bits.insert(name.index(),bits);
                    make_var_subscript_free(name,names)?;
                    var_bits.insert(name.index(),bits);
                }
                &mut Statement::Flow{ op: FlowOperation::Return } => { /* fall-thru */ }
                &mut Statement::Memory{ op: MemoryOperation::Store{ ref mut address, ref mut value, ref mut segment,.. },ref mut result } => {
                    if let &mut Value::Variable(Variable{ ref mut name, bits }) = address {
                        //var_bits.insert(name.index(),bits);
                        make_var_subscript_free(name,names)?;
                        var_bits.insert(name.index(),bits);
                    }

                    if let &mut Value::Variable(Variable{ ref mut name, bits }) = value {
                        //var_bits.insert(name.index(),bits);
                        make_var_subscript_free(name,names)?;
                        var_bits.insert(name.index(),bits);
                    }
                    make_seg_subscript_free(&mut result.name,segments)?;
                    make_seg_subscript_free(&mut segment.name,segments)?;
                }
                &mut Statement::Memory{ ref mut result,.. } => {
                    make_seg_subscript_free(&mut result.name,segments)?;
                }
            }

            Ok(RewriteControl::Continue)
        })?;
    }

    for stmt in func.statements(..) {
        match &*stmt {
            &Statement::Expression{ ref result,.. } => {
                println!("{:?}", stmt);
                assert!(func.names.value(result.name).unwrap().subscript().is_none());
            }
            _ => {}
        }
    }

    let mut names = func.names.clone();
    for guard in func.cflow_graph_mut().edge_weights_mut() {
        match guard {
            &mut Guard::Predicate{ flag: Variable{ ref mut name, bits },.. } => {
                //var_bits.insert(name.index(),bits);
                make_var_subscript_free(name,&mut names)?;
                var_bits.insert(name.index(),bits);
            }
            _ => { /* skip */ }
        }
    }

    for node in func.cflow_graph_mut().node_weights_mut() {
        match node {
            &mut CfgNode::Value(Value::Variable(Variable{ ref mut name, bits })) => {
                //var_bits.insert(name.index(),bits);
                make_var_subscript_free(name,&mut names)?;
                var_bits.insert(name.index(),bits);
            }
            _ => { /* skip */ }
        }
    }

    func.names = names;

    Ok(var_bits)
}

pub fn rewrite_to_ssa(func: &mut Function) -> Result<SsaMetainfo> {
    use Liveness;

    clear_data_flow_information(func)?;
    clear_data_flow_information(func)?;

    let entry = func.entry_point();
    let doms = dominators::simple_fast(func.cflow_graph(),func.basic_block(entry).node);
    let dom_events = dominator_tree(func,&doms)?;
    let bits = remove_subscripts(func)?;
    let live = Liveness::new(&func)?;
    let globals = Globals::new(&func,&live)?;
    let df = dominance_frontiers(&func,&doms)?;
    let phis = PhiFunctions::new(&func,&globals,&df)?;
    let mut uninit = live.ue_var[entry.index()].clone();
    let mut unalloc = live.ue_seg[entry.index()].clone();


    for v in uninit.iter() {
        println!("{:?}", func.names.value(NameRef::new(v)));
    }

    uninit.union_with(&globals.global_vars.difference(&live.var_kill[entry.index()]).collect());
    unalloc.union_with(&globals.global_segs.difference(&live.seg_kill[entry.index()]).collect());
    if func.name == "fn_db60" {
        println!("{:?}", uninit);
        println!("{:?}", unalloc);
    }
    fix_uninitialized(func,uninit,&bits,unalloc)?;
    insert_phi_operations(func,&phis,&dom_events,&bits)?;

    let mut var_name_stack = NameStack::new(&func.names);
    let mut seg_name_stack = NameStack::new(&func.segments);

    assign_subscripts(func,&dom_events,&mut var_name_stack,&mut seg_name_stack)?;

    Ok(SsaMetainfo{
        var_name_stack: var_name_stack.into_metainfo(),
        globals: globals,
        phi: phis,
        var_bits: bits,
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use types::{Region, TestArch, Function, Variable, UUID};
    use {Liveness,Globals};
    use simple_logger;
    use std::iter;
    use std::iter::FromIterator;

    /*
     * (B0)
     * 0:  Mx1  ; mov x 1
     *
     * (B1)
     * 3:  Mx1  ; mov x 1
     * 6:  Cfx1 ; cmp f x 1
     * 10: Bf28 ; brle f (B5)
     *
     * (B2)
     * 14: Mx1  ; mov x 1
     *
     * (B3)
     * 17: Mx1  ; mov x 1
     * 20: Cfx1 ; cmp f x 1
     * 24: Bf3  ; brle f (B1)
     *
     * (B4)
     * 27: R    ; ret
     *
     * (B5)
     * 28: Mx1  ; mov x 1
     * 31: Cfx1 ; cmp f x 1
     * 35: Bf48 ; brle f (B8)
     *
     * (B6)
     * 39:  Mx1 ; mov x 1
     *
     * (B7)
     * 42:  Mx1 ; mov x 1
     * 45:  J17 ; jmp (B3)
     *
     * (B8)
     * 48:  J42 ; jmp (B7)
     */
    #[test]
    fn dom_frontiers() {
        let _ = simple_logger::init();
        let data = b"Mx1Mx1Cfx1Bf28Mx1Mx1Cfx1Bf3RMx1Cfx1Bf48Mx1Mx1J17J42".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let ent_idx = func.entry_point();
        let doms = dominators::simple_fast(func.cflow_graph(),func.basic_block(ent_idx).node);
        let df = dominance_frontiers(&func,&doms).unwrap();
        let mut bb1 = None;
        let mut bb3 = None;
        let mut bb7 = None;

        for (idx,bb) in func.basic_blocks() {
            match bb.area.start {
                3 => { bb1 = Some(idx); }
                17 => { bb3 = Some(idx); }
                42 => { bb7 = Some(idx); }
                _ => {}
            };
        }

        assert!(bb1.is_some() && bb3.is_some() && bb7.is_some());

        let set_1 = BitSet::from_iter(iter::once(bb1.unwrap().index()));
        let set_3 = BitSet::from_iter(iter::once(bb3.unwrap().index()));
        let set_7 = BitSet::from_iter(iter::once(bb7.unwrap().index()));

        for (idx,bb) in func.basic_blocks() {
            let expected_df = match bb.area.start {
                0 => BitSet::default(),
                3 => set_1.clone(),
                14 => set_3.clone(),
                17 => set_1.clone(),
                27 => BitSet::default(),
                28 => set_3.clone(),
                39 => set_7.clone(),
                42 => set_3.clone(),
                48 => set_7.clone(),
                a => unreachable!("unexpected address {}",a),
            };

            debug!("check {:?}: {:?}",idx,bb);
            assert_eq!(df[idx.index()], expected_df);
        }
    }

    /*
     * (B0)
     * 0:  Mi1  ; mov i 1
     *
     * (B1)
     * 3:  Ma1  ; mov a 1
     * 6:  Mc1  ; mov c 1
     * 9:  Cfac ; cmp f a c
     * 13: Bf46 ; br f (B5)
     *
     * (B2)
     * 17: Mb1  ; mov b 1
     * 20: Mc1  ; mov c 1
     * 23: Md1  ; mov d 1
     *
     * (B3)
     * 26: Ayab ; add y a b
     * 30: Azcd ; add z c d
     * 34: Aii1 ; add i i 1
     * 38: Cfi1 ; cmp f i 1
     * 42: Bf3  ; br f (B1)
     *
     * (B4)
     * 45: R    ; ret
     *
     * (B5)
     * 46: Ma1  ; mov a 1
     * 49: Md1  ; mov d 1
     * 52: Cfad ; cmp f a d
     * 56: Bf69 ; br f (B8)
     *
     * (B6)
     * 60:  Md1 ; mov d 1
     *
     * (B7)
     * 63:  Mb1 ; mov b 1
     * 66:  J26 ; jmp (B3)
     *
     * (B8)
     * 69:  Mc1 ; mov c 1
     * 72:  J63 ; jmp (B7)
     */
    #[test]
    fn phi_placement() {
        let _ = simple_logger::init();
        let data = b"Mi1Ma1Mc1CfacBf46Mb1Mc1Md1AyabAzcdAii1Cfi1Bf3RMa1Md1CfadBf69Md1Mb1J26Mc1J63".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let ent_idx = func.entry_point();
        let live = Liveness::new(&func).unwrap();
        let globals = Globals::new(&func,&live).unwrap();
        let doms = dominators::simple_fast(func.cflow_graph(),func.basic_block(ent_idx).node);
        let df = dominance_frontiers(&func,&doms).unwrap();
        let phis = PhiFunctions::new(&func,&globals,&df).unwrap();
        let i = func.names.index(&Name::new("i".into(),None)).unwrap();
        let a = func.names.index(&Name::new("a".into(),None)).unwrap();
        let b = func.names.index(&Name::new("b".into(),None)).unwrap();
        let c = func.names.index(&Name::new("c".into(),None)).unwrap();
        let d = func.names.index(&Name::new("d".into(),None)).unwrap();
        let abcdi_set = BitSet::from_iter(vec![a.index(),b.index(),c.index(),d.index(),i.index()].into_iter());
        let abcd_set = BitSet::from_iter(vec![a.index(),b.index(),c.index(),d.index()].into_iter());
        let cd_set = BitSet::from_iter(vec![c.index(),d.index()].into_iter());

        for (idx,bb) in func.basic_blocks() {
            let expected_phi = match bb.area.start {
                0 => BitSet::default(),
                3 => abcdi_set.clone(),
                17 => BitSet::default(),
                26 => abcd_set.clone(),
                45 => BitSet::default(),
                46 => BitSet::default(),
                60 => BitSet::default(),
                63 => cd_set.clone(),
                69 => BitSet::default(),
                a => unreachable!("unexpected address {}",a),
            };

            debug!("check {:?}: {:?}",idx,bb);
            assert_eq!(phis.values[idx.index()], expected_phi);
        }
    }

    /*
     * (B0)
     * 0:  Mi1  ; mov i s
     * 3:  Cfi0 ; cmp f i 0
     * 7:  Bf18 ; br f (B2)
     *
     * (B1)
     * 11: Aii3 ; add i i 3
     * 15: J22  ; jmp (B3)
     *
     * (B2)
     * 18: Ai23 ; add i i 3
     *
     * (B3)
     * 22: Ms3  ; mov s 3
     * 25: Aisx ; add i s x
     * 29: R    ; ret
     */
    #[test]
    fn uninitialized_variables() {
        let _ = simple_logger::init();
        let data = b"MisCfi0Bf18Aii3J22Aii3Ms3AisxR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let ent_idx = func.entry_point();
        let (uninit,bits,unalloc) = {
            let bits = remove_subscripts(&mut func).unwrap();
            let live = Liveness::new(&func).unwrap();
            let s = func.names.index(&Name::new("s".into(), None)).unwrap();
            let x = func.names.index(&Name::new("x".into(), None)).unwrap();
            let xs_set = BitSet::from_iter(vec![x.index(),s.index()].into_iter());
            let ram_set = BitSet::from_iter(iter::empty());
            let uninit = live.ue_var[ent_idx.index()].clone();
            let unalloc = live.ue_seg[ent_idx.index()].clone();

            assert_eq!(uninit, xs_set);
            assert_eq!(unalloc, ram_set);

            (uninit,bits,unalloc)
        };
        let _ = fix_uninitialized(&mut func,uninit.clone(),&bits,unalloc.clone()).unwrap();

        let str_init = func.strings.insert(&"__init".into());
        let mnes = func.mnemonics(ent_idx).collect::<Vec<_>>();
        assert_eq!(mnes.len(), 4);

        assert_eq!(mnes[0].1.opcode, str_init);
        let stmts = func.statements(ent_idx).collect::<Vec<_>>();
        assert_eq!(stmts.len(), 4);
        if let &Statement::Expression{ op: Operation::Initialize(name,len), result: Variable{ name: res_name, bits: res_bits,.. } } = &*stmts[0] {
            let name = func.strings.value(name).map(|x| x.clone()).unwrap();
            let res_name = func.names.value(res_name).map(|x| x.base().clone()).unwrap();
            assert_eq!(name, res_name);
            assert_eq!(len, res_bits);
            assert!(name == "x" || name == "s");
        } else {
            unreachable!()
        }
        if let &Statement::Expression{ op: Operation::Initialize(name,len), result: Variable{ name: res_name, bits: res_bits,.. } } = &*stmts[1] {
            let name = func.strings.value(name).map(|x| x.clone()).unwrap();
            let res_name = func.names.value(res_name).map(|x| x.base().clone()).unwrap();
             assert_eq!(name, res_name);
            assert_eq!(len, res_bits);
            assert!(name == "x" || name == "s");
        } else {
            unreachable!()
        }

        let live = Liveness::new(&func).unwrap();
        let uninit2 = live.ue_var[ent_idx.index()].clone();
        let unalloc2 = live.ue_seg[ent_idx.index()].clone();

        assert_eq!(uninit, uninit2);
        assert_eq!(unalloc, unalloc2);
    }

    #[test]
    fn uninitialized_variables_reentrant() {
        let _ = simple_logger::init();
        let data = b"MisCfi0Bf18Aii3J22Aii3Ms3AisxR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let ent_idx = func.entry_point();
        let bits = remove_subscripts(&mut func).unwrap();
        let (uninit,unalloc) = {
            let live = Liveness::new(&func).unwrap();
            let s = func.names.index(&Name::new("s".into(), None)).unwrap();
            let x = func.names.index(&Name::new("x".into(), None)).unwrap();
            let xs_set = BitSet::from_iter(vec![x.index(),s.index()].into_iter());
            let ram_set = BitSet::from_iter(iter::empty());
            let uninit = live.ue_var[ent_idx.index()].clone();
            let unalloc = live.ue_seg[ent_idx.index()].clone();

            assert_eq!(uninit, xs_set);
            assert_eq!(unalloc, ram_set);

            (uninit,unalloc)
        };
        debug!("before 1st");
        let _ = fix_uninitialized(&mut func,uninit.clone(),&bits,unalloc.clone()).unwrap();
        debug!("after 1st");

        let str_init = func.strings.insert(&"__init".into());

        {
            let mnes = func.mnemonics(ent_idx).collect::<Vec<_>>();
            assert_eq!(mnes.len(), 4);

            assert_eq!(mnes[0].1.opcode, str_init);
            let stmts = func.statements(ent_idx).collect::<Vec<_>>();
            assert_eq!(stmts.len(), 4);
            if let &Statement::Expression{ op: Operation::Initialize(name,len), result: Variable{ name: res_name, bits: res_bits,.. } } = &*stmts[0] {
                let name = func.strings.value(name).map(|x| x.clone()).unwrap();
                let res_name = func.names.value(res_name).map(|x| x.base().clone()).unwrap();
                assert_eq!(name, res_name);
                assert_eq!(len, res_bits);
                assert!(name == "x" || name == "s");
            } else {
                unreachable!()
            }
            if let &Statement::Expression{ op: Operation::Initialize(name,len), result: Variable{ name: res_name, bits: res_bits,.. } } = &*stmts[1] {
                let name = func.strings.value(name).map(|x| x.clone()).unwrap();
                let res_name = func.names.value(res_name).map(|x| x.base().clone()).unwrap();
                assert_eq!(name, res_name);
                assert_eq!(len, res_bits);
                assert!(name == "x" || name == "s");
            } else {
                unreachable!()
            }

            let live = Liveness::new(&func).unwrap();
            let uninit2 = live.ue_var[ent_idx.index()].clone();

            assert_eq!(uninit, uninit2);
        }

        debug!("before 2nd");
        let _ = fix_uninitialized(&mut func,uninit.clone(),&bits,unalloc.clone()).unwrap();
        debug!("done");

        {
            let mnes = func.mnemonics(ent_idx).collect::<Vec<_>>();
            assert_eq!(mnes.len(), 4);

            assert_eq!(mnes[0].1.opcode, str_init);
            let stmts = func.statements(ent_idx).collect::<Vec<_>>();
            assert_eq!(stmts.len(), 4);
            if let &Statement::Expression{ op: Operation::Initialize(name,len), result: Variable{ name: res_name, bits: res_bits,.. } } = &*stmts[0] {
                let name = func.strings.value(name).map(|x| x.clone()).unwrap();
                let res_name = func.names.value(res_name).map(|x| x.base().clone()).unwrap();
                assert_eq!(name, res_name);
                assert_eq!(len, res_bits);
                assert!(name == "x" || name == "s");
            } else {
                unreachable!()
            }
            if let &Statement::Expression{ op: Operation::Initialize(name,len), result: Variable{ name: res_name, bits: res_bits,.. } } = &*stmts[1] {
                let name = func.strings.value(name).map(|x| x.clone()).unwrap();
                let res_name = func.names.value(res_name).map(|x| x.base().clone()).unwrap();
                assert_eq!(name, res_name);
                assert_eq!(len, res_bits);
                assert!(name == "x" || name == "s");
            } else {
                unreachable!()
            }


            let live = Liveness::new(&func).unwrap();
            let uninit2 = live.ue_var[ent_idx.index()].clone();

            assert_eq!(uninit, uninit2);
        }
    }

    /*
     * (B0)
     * 0:  Mi1  ; mov i 1
     *
     * (B1)
     * 3:  Ma1  ; mov a 1
     * 6:  Mc1  ; mov c 1
     * 9:  Cfac ; cmp f a c
     * 13: Bf46 ; br f (B5)
     *
     * (B2)
     * 17: Mb1  ; mov b 1
     * 20: Mc1  ; mov c 1
     * 23: Md1  ; mov d 1
     *
     * (B3)
     * 26: Ayab ; add y a b
     * 30: Azcd ; add z c d
     * 34: Aii1 ; add i i 1
     * 38: Cfi1 ; cmp f i 1
     * 42: Bf3  ; br f (B1)
     *
     * (B4)
     * 45: R    ; ret
     *
     * (B5)
     * 46: Ma1  ; mov a 1
     * 49: Md1  ; mov d 1
     * 52: Cfad ; cmp f a d
     * 56: Bf69 ; br f (B8)
     *
     * (B6)
     * 60:  Md1 ; mov d 1
     *
     * (B7)
     * 63:  Mb1 ; mov b 1
     * 66:  J26 ; jmp (B3)
     *
     * (B8)
     * 69:  Mc1 ; mov c 1
     * 72:  J63 ; jmp (B7)
     */
    #[test]
    fn dominator_tree_events() {
        let _ = simple_logger::init();
        let data = b"Mi1Ma1Mc1CfacBf46Mb1Mc1Md1AyabAzcdAii1Cfi1Bf3RMa1Md1CfadBf69Md1Mb1J26Mc1J63".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let ent_idx = func.entry_point();
        let doms = dominators::simple_fast(func.cflow_graph(),func.basic_block(ent_idx).node);
        let mut events = dominator_tree(&func,&doms).unwrap();

        let mut ctx = BitSet::with_capacity(func.basic_blocks().len());
        for ev in events.iter() {
            match ev {
                &DomTreeEvent::Enter{ ref index, num_in_edges, ref successors, start,.. } => {
                    debug!("enter {}",index.index());
                    assert!(ctx.insert(index.index()));
                    match start {
                        0 => {
                            assert_eq!(num_in_edges, 0);
                            assert_eq!(successors.len(), 1);
                            assert_eq!(func.basic_block(successors[0].0).area.start, 3);
                        }
                        3 => {
                            assert_eq!(num_in_edges, 2);
                            assert_eq!(successors.len(), 2);
                            let succ0_addr = func.basic_block(successors[0].0).area.start;
                            let succ1_addr = func.basic_block(successors[1].0).area.start;
                            assert!(succ0_addr == 17 || succ0_addr == 46);
                            assert!(succ1_addr == 17 || succ1_addr == 46);
                        }
                        17 => {
                            assert_eq!(num_in_edges, 1);
                            assert_eq!(successors.len(), 1);
                            assert_eq!(func.basic_block(successors[0].0).area.start, 26);
                        }
                        26 => {
                            assert_eq!(num_in_edges, 2);
                            assert_eq!(successors.len(), 2);
                            let succ0_addr = func.basic_block(successors[0].0).area.start;
                            let succ1_addr = func.basic_block(successors[1].0).area.start;
                            assert!(succ0_addr == 3 || succ0_addr == 45);
                            assert!(succ1_addr == 3 || succ1_addr == 45);
                        }
                        45 => {
                            assert_eq!(num_in_edges, 1);
                            assert_eq!(successors.len(), 0);
                        }
                        46 => {
                            assert_eq!(num_in_edges, 1);
                            assert_eq!(successors.len(), 2);
                            let succ0_addr = func.basic_block(successors[0].0).area.start;
                            let succ1_addr = func.basic_block(successors[1].0).area.start;
                            assert!(succ0_addr == 60 || succ0_addr == 69);
                            assert!(succ1_addr == 60 || succ1_addr == 69);
                        }
                        60 => {
                            assert_eq!(num_in_edges, 1);
                            assert_eq!(successors.len(), 1);
                            assert_eq!(func.basic_block(successors[0].0).area.start, 63);
                        }
                        63 => {
                            assert_eq!(num_in_edges, 2);
                            assert_eq!(successors.len(), 1);
                            assert_eq!(func.basic_block(successors[0].0).area.start, 26);
                        }
                        69 => {
                            assert_eq!(num_in_edges, 1);
                            assert_eq!(successors.len(), 1);
                            assert_eq!(func.basic_block(successors[0].0).area.start, 63);
                        }
                        _ => { unreachable!() }
                    }
                }
                &DomTreeEvent::Leave(ref index) => {
                    debug!("leave {}",index.index());
                    assert!(index.index() <= 8);
                    assert!(ctx.remove(index.index()));
                }
            }
        }

        assert!(ctx.is_empty());

        assert_eq!(events.len(), 9*2);
        events.sort();
        events.dedup();
        assert_eq!(events.len(), 9*2);
    }

    #[test]
    fn phi_rename() {
        let _ = simple_logger::init();
        let data = b"Mi1Ma1Mc1CfacBf46Mb1Mc1Md1AyabAzcdAii1Cfi1Bf3RMa1Md1CfadBf69Md1Mb1J26Mc1J63".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        for s in func.statements(..) {
            debug!("{:?}",s);
        }
    }
}
