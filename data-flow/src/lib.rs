// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//! Collection of data flow algorithms.
//!
//! This module contains algorithms to convert RREIL code into SSA form. Aside from SSA form this
//! module implements functions to compute liveness sets and basic reverse data flow information.

extern crate p8n_types as types;
extern crate bit_set;
extern crate petgraph;
extern crate smallvec;
extern crate hprof;
extern crate vec_map;
#[macro_use] extern crate log;
extern crate cassowary;

#[cfg(test)]
extern crate simple_logger;

mod liveness;
pub use liveness::{
    Liveness,
    Globals,
    ReachingDefs,
};

mod ssa;
pub use ssa::{SsaMetainfo,rewrite_to_ssa};

mod assume;
pub use assume::rewrite_with_assume;

mod name_stack;
pub use name_stack::{NameStack,NameStackMetainfo};

mod guilfanov;
pub use guilfanov::guilfanovs_method;
