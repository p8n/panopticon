// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//use std::env;
//use std::fs::File;
//use std::io::Write;
//use std::path::Path;
//
//#[derive(Clone, Copy, Debug)]
//enum Argument {
//    Literal,
//    LiteralWidth,
//    NoOffset,
//    Constant,
//    Undefined,
//}
//
//impl Argument {
//    pub fn match_expr(&self, pos: &'static str) -> String {
//        match self {
//            &Argument::Literal => format!("( ${}:expr )", pos),
//            &Argument::LiteralWidth => {
//                format!("( ${}:expr ) : ${}_w:tt", pos, pos)
//            }
//            &Argument::NoOffset => format!("${}:tt : ${}_w:tt", pos, pos),
//            &Argument::Constant => format!("[ ${}:tt ] : ${}_w:tt", pos, pos),
//            &Argument::Undefined => "?".to_string(),
//        }
//    }
//
//    pub fn arg_expr(&self, pos: &'static str) -> String {
//        match self {
//            &Argument::Literal => format!("( ${} )", pos),
//            &Argument::LiteralWidth => format!("( ${} ) : ${}_w", pos, pos),
//            &Argument::NoOffset => format!("${} : ${}_w", pos, pos),
//            &Argument::Constant => format!("[ ${} ] : ${}_w", pos, pos),
//            &Argument::Undefined => "?".to_string(),
//        }
//    }
//}
//
//const BOILERPLATE2: &'static str = "
//let ret: $crate::Result<Vec<$crate::Statement>> = match stmt[0].sanity_check() {
//    Ok(()) => {
//        {let _ = &$segs;}
//        {let _ = &$names;}
//        let mut tail: $crate::Result<Vec<$crate::Statement>> = { rreil2!( ($names,$segs) { $($cdr)* } ) };
//        match tail {
//            Ok(ref mut other) => {
//                stmt.extend(other.drain(..));
//                Ok(stmt)
//            }
//            Err(e) => Err(e),
//        }
//    }
//    Err(e) => Err(e).into(),
//};
//
//ret
//";
//
//const VARIABLES: &'static [Argument] =
//    &[Argument::Literal, Argument::LiteralWidth, Argument::NoOffset];
//
//const VALUES: &'static [Argument] = &[
//    Argument::Literal,
//    Argument::LiteralWidth,
//    Argument::NoOffset,
//    Argument::Constant,
//    Argument::Undefined,
//];
//
//fn write_binary_operations(f: &mut File) {
//    f.write_all(
//        b"
//#[macro_export]
//macro_rules! rreil2_binop {
//    ",
//    )
//    .unwrap();
//
//    for a in VARIABLES.iter() {
//        for b in VALUES.iter() {
//            for c in VALUES.iter() {
//                f.write_fmt(
//                    format_args!(
//                        "
//    // {:?} := {:?}, {:?}
//    ( $names:tt # $segs:tt # $op:ident # {}, {} , {} ; $($cdr:tt)*) => {{{{
//        let mut stmt = vec![$crate::Statement::Expression{{
//            op: $crate::Operation::$op(rreil_val!($names # {}),rreil_val!($names # {})),
//            result: rreil_var!($names # {})
//        }}];
//        {}
//    }}}};
//                ",
//                a,
//                b,
//                c,
//                a.match_expr("a"),
//                b.match_expr("b"),
//                c.match_expr("c"),
//                b.arg_expr("b"),
//                c.arg_expr("c"),
//                a.arg_expr("a"),
//                BOILERPLATE2
//                )
//                    )
//                    .unwrap();
//            }
//        }
//    }
//    f.write_all(
//        b"}
//    ",
//    )
//    .unwrap();
//}
//
//fn write_unary_operations(f: &mut File) {
//    f.write_all(
//        b"
//#[macro_export]
//macro_rules! rreil2_unop {
//    ",
//    )
//    .unwrap();
//
//    for a in VARIABLES.iter() {
//        for b in VALUES.iter() {
//            f.write_fmt(format_args!(
//                "
//    // {:?} := {:?}
//    ( $names:tt # $segs:tt # $op:ident # {}, {} ; $($cdr:tt)*) => {{{{
//        let mut stmt = vec![$crate::Statement::Expression{{
//            op: $crate::Operation::$op(rreil_val!($names # {})),
//            result: rreil_var!($names # {})
//        }}];
//        {}
//    }}}};
//                ",
//                a,
//                b,
//                a.match_expr("a"),
//                b.match_expr("b"),
//                b.arg_expr("b"),
//                a.arg_expr("a"),
//                BOILERPLATE2
//            ))
//            .unwrap();
//        }
//    }
//    f.write_all(
//        b"}
//    ",
//    )
//    .unwrap();
//}
//
//fn write_store_operation(f: &mut File) {
//    f.write_all(
//        b"
//#[macro_export]
//macro_rules! rreil_store {
//    ",
//    )
//    .unwrap();
//
//    for addr in VALUES.iter() {
//        for val in VALUES.iter() {
//            // Little Endian Store
//            f.write_fmt(format_args!("
//    // {:?} := {:?}
//    ( $names:tt # $segs:tt # $bank:ident # le # $sz:tt # {} , {} ; $($cdr:tt)*) => {{{{
//        let mut stmt = vec![$crate::Statement::Memory{{
//            op: $crate::MemoryOperation::Store{{
//                segment: $crate::Segment{{
//                    name: $segs.insert(&$crate::Name::new(stringify!($bank).into(),None)),
//                }},
//                endianess: $crate::Endianess::Little,
//                bytes: rreil_imm!($sz),
//                address: rreil_val!($names # {}),
//                value: rreil_val!($names # {}),
//            }},
//            result: $crate::Segment{{
//                name: $segs.insert(&$crate::Name::new(stringify!($bank).into(),None)),
//            }},
//        }}];
//        {}
//    }}}};
//            ",addr,val,
//            addr.match_expr("addr"),val.match_expr("val"),
//            addr.arg_expr("addr"),val.arg_expr("val"),
//            BOILERPLATE2)).unwrap();
//
//            // Big Endian Store
//            f.write_fmt(format_args!("
//    // *({:?}) := {:?}
//    ( $names:tt # $segs:tt # $bank:ident # be # $sz:tt # {} , {} ; $($cdr:tt)*) => {{{{
//        let mut stmt = vec![$crate::Statement::Memory{{
//            op: $crate::MemoryOperation::Store{{
//                segment: $crate::Segment{{
//                    name: $segs.insert(&$crate::Name::new(stringify!($bank).into(),None)),
//                }},
//                endianess: $crate::Endianess::Big,
//                bytes: rreil_imm!($sz),
//                address: rreil_val!($names # {}),
//                value: rreil_val!($names # {}),
//            }},
//            result: $crate::Segment{{
//                name: $segs.insert(&$crate::Name::new(stringify!($bank).into(),None)),
//            }},
//        }}];
//        {}
//    }}}};
//            ",addr,val,
//            addr.match_expr("addr"),val.match_expr("val"),
//            addr.arg_expr("addr"),val.arg_expr("val"),
//            BOILERPLATE2)).unwrap();
//        }
//    }
//
//    f.write_all(b"}").unwrap();
//}
//
//fn write_load_operation(f: &mut File) {
//    f.write_all(
//        b"
//#[macro_export]
//macro_rules! rreil_load {
//    ",
//    )
//    .unwrap();
//
//    for res in VARIABLES.iter() {
//        for addr in VALUES.iter() {
//            // Little Endian Load
//            f.write_fmt(format_args!("
//    // {:?} := *({:?})
//    ( $names:tt # $segs:tt # $bank:ident # le # $sz:tt # {} , {} ; $($cdr:tt)*) => {{{{
//        let mut stmt = vec![$crate::Statement::Expression{{
//            op: $crate::Operation::Load(
//                $crate::Segment{{
//                    name: $segs.insert(&$crate::Name::new(stringify!($bank).into(),None)),
//                }},
//                $crate::Endianess::Little,
//                rreil_imm!($sz),
//                rreil_val!($names # {})
//            ),
//            result: rreil_var!($names # {}),
//        }}];
//        {}
//    }}}};
//            ",res,addr,
//            res.match_expr("res"),addr.match_expr("addr"),
//            addr.arg_expr("addr"),res.arg_expr("res"),
//            BOILERPLATE2)).unwrap();
//
//            // Big Endian Store
//            f.write_fmt(format_args!("
//    // {:?} := *({:?})
//    ( $names:tt # $segs:tt # $bank:ident # be # $sz:tt # {} , {} ; $($cdr:tt)*) => {{{{
//        let mut stmt = vec![$crate::Statement::Expression{{
//            op: $crate::Operation::Load(
//                $crate::Segment{{
//                    name: $segs.insert(&$crate::Name::new(stringify!($bank).into(),None)),
//                }},
//                $crate::Endianess::Big,
//                rreil_imm!($sz),
//                rreil_val!($names # {})
//            ),
//            result: rreil_var!($names # {}),
//        }}];
//        {}
//    }}}};
//            ",res,addr,
//            res.match_expr("res"),addr.match_expr("addr"),
//            addr.arg_expr("addr"),res.arg_expr("res"),
//            BOILERPLATE2)).unwrap();
//        }
//    }
//
//    f.write_all(b"}").unwrap();
//}
//
//fn write_extraction_operations(f: &mut File) {
//    f.write_all(
//        b"
//#[macro_export]
//macro_rules! rreil2_extop {
//    ",
//    )
//    .unwrap();
//
//    for a in VARIABLES.iter() {
//        for b in VALUES.iter() {
//            f.write_fmt(format_args!(
//                "
//    // {:?} := {:?}
//    ( $names:tt # $segs:tt # $op:ident # $sz:tt # {}, {} ; $($cdr:tt)*) => {{{{
//        let mut stmt = vec![$crate::Statement::Expression{{
//            op: $crate::Operation::$op(rreil_imm!($sz),rreil_val!($names # {})),
//            result: rreil_var!($names # {})
//        }}];
//        {}
//    }}}};
//                ",
//                a,
//                b,
//                a.match_expr("a"),
//                b.match_expr("b"),
//                b.arg_expr("b"),
//                a.arg_expr("a"),
//                BOILERPLATE2
//            ))
//            .unwrap();
//        }
//    }
//    f.write_all(
//        b"}
//    ",
//    )
//    .unwrap();
//}
//
//fn write_selection_operations(f: &mut File) {
//    f.write_all(
//        b"
//#[macro_export]
//macro_rules! rreil2_selop {
//    ",
//    )
//    .unwrap();
//
//    for a in VARIABLES.iter() {
//        for b in VALUES.iter() {
//            f.write_fmt(
//                format_args!(
//                    "
//    // {:?} := {:?}
//    ( $names:tt # $segs:tt # $op:ident # $off:tt # $sz:tt # {}, {} ; $($cdr:tt)*) => {{{{
//        let mut stmt = vec![$crate::Statement::Expression{{
//            op: $crate::Operation::$op(rreil_imm!($off),rreil_imm!($sz),rreil_val!($names # {})),
//            result: rreil_var!($names # {})
//        }}];
//        {}
//    }}}};
//                ",
//                a,
//                b,
//                a.match_expr("a"),
//                b.match_expr("b"),
//                b.arg_expr("b"),
//                a.arg_expr("a"),
//                BOILERPLATE2
//                )
//                )
//                .unwrap();
//        }
//    }
//    f.write_all(
//        b"}
//    ",
//    )
//    .unwrap();
//}
//
//fn write_call_operations(f: &mut File) {
//    f.write_all(
//        b"
//#[macro_export]
//macro_rules! rreil2_callop {
//    ",
//    )
//    .unwrap();
//
//    for a in VALUES.iter() {
//        f.write_fmt(format_args!(
//            "
//    // call {:?}
//    ( $names:tt # $segs:tt # {} ; $($cdr:tt)* ) => {{{{
//        let mut stmt = vec![$crate::Statement::Flow{{
//            op: $crate::FlowOperation::IndirectCall{{
//                target: rreil_var!($names # {})
//            }}
//        }}];
//        {}
//    }}}};
//                ",
//            a,
//            a.match_expr("a"),
//            a.arg_expr("a"),
//            BOILERPLATE2
//        ))
//        .unwrap();
//    }
//    f.write_all(
//        b"}
//    ",
//    )
//    .unwrap();
//}
//
//fn write_ret_operations(f: &mut File) {
//    f.write_fmt(format_args!(
//        "
//#[macro_export]
//macro_rules! rreil2_retop {{
//    // ret
//    ( $names:tt # $segs:tt # ; $($cdr:tt)* ) => {{{{
//        let mut stmt = vec![$crate::Statement::Flow{{
//            op: $crate::FlowOperation::Return,
//        }}];
//        {}
//    }}}};
//}}
//",
//        BOILERPLATE2
//    ))
//    .unwrap();
//}

extern crate string_cache_codegen;

use std::env;
use std::path::Path;

fn main() {
    string_cache_codegen::AtomType::new("atoms::Atom", "atm!")
        .atoms(
            [
                "a",
                "aaa",
                "aad",
                "aam",
                "aas",
                "adc",
                "adcx",
                "add",
                "aesdeclast",
                "aesenc",
                "aesimc",
                "AH",
                "AL",
                "__alloc",
                "and",
                "arpl",
                "AX",
                "b",
                "BH",
                "BL",
                "blendvpd",
                "blendvps",
                "BND0",
                "BND1",
                "BND2",
                "BND3",
                "bndmov",
                "bound",
                "BP",
                "BPL",
                "broadcastf128",
                "broadcasti128",
                "broadcastsd",
                "broadcastss",
                "bsf",
                "bsr",
                "bswap",
                "bt",
                "btc",
                "btr",
                "bts",
                "BX",
                "c",
                "call",
                "cbw",
                "CH",
                "CL",
                "clc",
                "cld",
                "clgi",
                "cli",
                "clts",
                "cmc",
                "cmova",
                "cmovae",
                "cmovb",
                "cmovbe",
                "cmovg",
                "cmovge",
                "cmovl",
                "cmovle",
                "cmovno",
                "cmovnp",
                "cmovns",
                "cmovnz",
                "cmovo",
                "cmovp",
                "cmovs",
                "cmovz",
                "cmp",
                "cmpsb",
                "cmpsw",
                "cmpxchg",
                "comisd",
                "comiss",
                "cpuid",
                "CR0",
                "CR1",
                "CR10",
                "CR11",
                "CR12",
                "CR13",
                "CR14",
                "CR15",
                "CR2",
                "CR3",
                "CR4",
                "CR5",
                "CR6",
                "CR7",
                "CR8",
                "CR9",
                "crc32",
                "CS",
                "cvtdq2pd",
                "cvtdq2ps",
                "cvtpd2dq",
                "cvtpd2pi",
                "cvtpd2ps",
                "cvtpi2pd",
                "cvtpi2ps",
                "cvtps2dq",
                "cvtps2pd",
                "cvtps2pi",
                "cvtsd2si",
                "cvtss2si",
                "cvttpd2dq",
                "cvttpd2pi",
                "cvttps2dq",
                "cvttps2pi",
                "cvttsd2si",
                "cvttss2si",
                "cwd",
                "CX",
                "d",
                "daa",
                "das",
                "dec",
                "DH",
                "DI",
                "DIL",
                "div",
                "DL",
                "DR0",
                "DR1",
                "DR10",
                "DR11",
                "DR12",
                "DR13",
                "DR14",
                "DR15",
                "DR2",
                "DR3",
                "DR4",
                "DR5",
                "DR6",
                "DR7",
                "DR8",
                "DR9",
                "DS",
                "DX",
                "e",
                "EAX",
                "EBP",
                "EBX",
                "ECX",
                "EDI",
                "EDX",
                "EIP",
                "emms",
                "endbr32",
                "endbr64",
                "enter",
                "ES",
                "ESI",
                "ESP",
                "extractps",
                "f",
                "f2xm1",
                "fabs",
                "fadd",
                "faddp",
                "fbld",
                "fbstp",
                "fchs",
                "fclex",
                "fcmovb",
                "fcmovbe",
                "fcmove",
                "fcmovnb",
                "fcmovnbe",
                "fcmovne",
                "fcmovnu",
                "fcmovu",
                "fcom",
                "fcomi",
                "fcomip",
                "fcomp",
                "fcompp",
                "fcos",
                "fdecstp",
                "fdiv",
                "fdivp",
                "fdivr",
                "fdivrp",
                "ffree",
                "fiadd",
                "ficom",
                "ficomp",
                "fidiv",
                "fidivr",
                "fild",
                "fimul",
                "fincstp",
                "finit",
                "fist",
                "fistp",
                "fisttp",
                "fisub",
                "fisubr",
                "fld",
                "fld1",
                "fldcw",
                "fldenv",
                "fldl2e",
                "fldl2g",
                "fldl2t",
                "fldln2",
                "fldpi",
                "fldz",
                "fmul",
                "fmulp",
                "fnop",
                "fpatan",
                "fperm",
                "fperm1",
                "fptan",
                "frndintx",
                "frstor",
                "FS",
                "fsave",
                "fscale",
                "fsin",
                "fsincos",
                "fsqrt",
                "fst",
                "fstcw",
                "fstenv",
                "fstp",
                "fstsw",
                "fsub",
                "fsubp",
                "fsubr",
                "fsubrp",
                "ftst",
                "fucom",
                "fucomi",
                "fucomip",
                "fucomp",
                "fucompp",
                "fxam",
                "fxch",
                "fxtract",
                "fyl2x",
                "fyl2xp1",
                "g",
                "getsec",
                "group 15",
                "group 3",
                "group 9",
                "GS",
                "h",
                "hlt",
                "i",
                "idiv",
                "imul",
                "in",
                "inc",
                "__init",
                "insb",
                "insw",
                "int",
                "int1",
                "int3",
                "into",
                "invd",
                "invept",
                "invlpg",
                "invpcid",
                "invvpid",
                "IP",
                "iretw",
                "j",
                "ja",
                "jae",
                "jb",
                "jbe",
                "je",
                "jg",
                "jge",
                "jl",
                "jle",
                "jmp",
                "jne",
                "jno",
                "jnp",
                "jns",
                "jo",
                "jp",
                "jrcxz",
                "js",
                "k",
                "l",
                "lahf",
                "lar",
                "lddqu",
                "lds",
                "lea",
                "leave",
                "les",
                "lfs",
                "lgdt",
                "lgs",
                "__libc_start_main",
                "lidt",
                "lldt",
                "lmsw",
                "lock",
                "lodsb",
                "lodsw",
                "loop",
                "loope",
                "loopne",
                "lsl",
                "lss",
                "ltr",
                "lzcnt",
                "m",
                "maskmovdqu",
                "maskmovq",
                "__memory_phi",
                "MM0",
                "MM1",
                "MM2",
                "MM3",
                "MM4",
                "MM5",
                "MM6",
                "MM7",
                "MMX0",
                "MMX1",
                "MMX2",
                "MMX3",
                "MMX4",
                "MMX5",
                "MMX6",
                "MMX7",
                "monitor",
                "montmul",
                "mov",
                "movapd",
                "movaps",
                "movbe",
                "movd",
                "movddup",
                "movdq2q",
                "movdqa",
                "movdqu",
                "movhpd",
                "movhps",
                "movlpd",
                "movlps",
                "movmskpd",
                "movmskps",
                "movntdq",
                "movntdqa",
                "movnti",
                "movntpd",
                "movntps",
                "movntq",
                "movq",
                "movsb",
                "movshdup",
                "movsldup",
                "movsw",
                "movsx",
                "movupd",
                "movups",
                "movzx",
                "mul",
                "n",
                "neg",
                "nop",
                "not",
                "o",
                "or",
                "out",
                "outsb",
                "outsw",
                "p",
                "pabsb",
                "pabsd",
                "pabsw",
                "packssdw",
                "packsswb",
                "packuswb",
                "paddb",
                "paddd",
                "paddq",
                "paddsb",
                "paddsw",
                "paddusb",
                "paddusw",
                "paddw",
                "palignr",
                "pand",
                "pandn",
                "pavgb",
                "pavgusb",
                "pavgw",
                "pblendvb",
                "pboradcastw",
                "pbroadcastb",
                "pbroadcastd",
                "pbroadcastq",
                "pcmpeqb",
                "pcmpeqd",
                "pcmpeqw",
                "pcmpgtb",
                "pcmpgtd",
                "pcmpgtw",
                "pextrb",
                "pextrd",
                "pextrw",
                "pf2id",
                "pf2iw",
                "pfacc",
                "pfadd",
                "pfcmpeq",
                "pfcmpge",
                "pfcmpgt",
                "pfmax",
                "pfmin",
                "pfmul",
                "pfnacc",
                "pfpnacc",
                "pfrcp",
                "pfrcpit1",
                "pfrcpit2",
                "pfrsqit1",
                "pfrsqrt",
                "pfsub",
                "pfsubr",
                "phaddd",
                "phaddsw",
                "phaddw",
                "__phi",
                "phminposuw",
                "phsubd",
                "phsubsw",
                "phsubw",
                "pi2fd",
                "pi2fw",
                "pinsrw",
                "pmaddubsw",
                "pmaddwd",
                "pmaxsw",
                "pmaxub",
                "pminsw",
                "pminub",
                "pmovmskb",
                "pmovsxbd",
                "pmovsxbq",
                "pmovsxbw",
                "pmovsxdq",
                "pmovsxwd",
                "pmovsxwq",
                "pmovzxbd",
                "pmovzxbq",
                "pmovzxbw",
                "pmovzxdq",
                "pmovzxwd",
                "pmovzxwq",
                "pmulhrsw",
                "pmulhrw",
                "pmulhuw",
                "pmulhw",
                "pmullw",
                "pmuludq",
                "pop",
                "popa",
                "popcnt",
                "popfw",
                "por",
                "prefetch",
                "psadbw",
                "pshufb",
                "psignb",
                "psignd",
                "psignw",
                "pslld",
                "psllq",
                "psllw",
                "psrad",
                "psraw",
                "psrld",
                "psrlq",
                "psrlw",
                "psubb",
                "psubd",
                "psubq",
                "psubsb",
                "psubsw",
                "psubusb",
                "psubusw",
                "psubw",
                "pswapd",
                "ptest",
                "punpckhbw",
                "punpckhdq",
                "punpckhwd",
                "punpcklbw",
                "punpckldq",
                "punpcklwd",
                "push",
                "pusha",
                "pushfw",
                "pxor",
                "q",
                "r",
                "R10",
                "R10B",
                "R10D",
                "R10W",
                "R11",
                "R11B",
                "R11D",
                "R11W",
                "R12",
                "R12B",
                "R12D",
                "R12W",
                "R13",
                "R13B",
                "R13D",
                "R13W",
                "R14",
                "R14B",
                "R14D",
                "R14W",
                "R15",
                "R15B",
                "R15D",
                "R15W",
                "R8",
                "R8B",
                "R8D",
                "R8W",
                "R9",
                "R9B",
                "R9D",
                "R9W",
                "RAX",
                "RBP",
                "RBX",
                "rcl",
                "rcpps",
                "rcr",
                "RCX",
                "RDI",
                "rdmsr",
                "rdpmc",
                "rdtsc",
                "RDX",
                "rep",
                "repne",
                "ret",
                "retf",
                "retn",
                "retnf",
                "RIP",
                "rol",
                "ror",
                "RSI",
                "rsm",
                "RSP",
                "rsqrtps",
                "s",
                "sahf",
                "salc",
                "sar",
                "sbb",
                "scasb",
                "scasw",
                "seta",
                "setae",
                "setb",
                "setbe",
                "setg",
                "setge",
                "setl",
                "setle",
                "setno",
                "setnp",
                "setns",
                "setnz",
                "seto",
                "setp",
                "sets",
                "setz",
                "sgdt",
                "sha1msg1",
                "sha1msg2",
                "sha1nexte",
                "sha256msg1",
                "sha256msg2",
                "sha256rnds2",
                "shl",
                "shr",
                "SI",
                "sidt",
                "SIL",
                "sldt",
                "smsw",
                "SP",
                "SPL",
                "sqrtpd",
                "sqrtps",
                "SS",
                "ST0",
                "ST1",
                "ST2",
                "ST3",
                "ST4",
                "ST5",
                "ST6",
                "ST7",
                "stc",
                "std",
                "sti",
                "stosb",
                "stosw",
                "str",
                "sub",
                "__summary",
                "syscall",
                "sysenter",
                "sysexit",
                "sysret",
                "t",
                "__tail_call",
                "test",
                "testpd",
                "testps",
                "tzcnt",
                "u",
                "ucomisd",
                "ucomiss",
                "ud1",
                "ud2",
                "v",
                "vaddpd",
                "vaddps",
                "vaddsd",
                "vaddss",
                "vaddsubpd",
                "vaddsubps",
                "vaesdec",
                "vaesenclast",
                "vaeskeygenassist",
                "vandn",
                "vandnpd",
                "vandnps",
                "vandpd",
                "vandps",
                "vbextr",
                "vblendd",
                "vblendpd",
                "vblendps",
                "vblendvb",
                "vblendvpd",
                "vblendvps",
                "vbzhi",
                "vcmppd",
                "vcmpps",
                "vcmpsd",
                "vcmpss",
                "vcvtph2ps",
                "vcvtsd2ss",
                "vcvtsi2sd",
                "vcvtsi2ss",
                "vcvtss2sd",
                "vdivpd",
                "vdivps",
                "vdivsd",
                "vdivss",
                "vdppd",
                "vdpps",
                "verr",
                "verw",
                "vextractf128",
                "vextracti128",
                "vfmadd132ps",
                "vfmadd132ss",
                "vfmadd213ps",
                "vfmadd213ss",
                "vfmadd231ps",
                "vfmadd231ss",
                "vfmaddsub132ps",
                "vfmaddsub231ps",
                "vfmaddsub232ps",
                "vfmnadd132ps",
                "vfmnsub132ps",
                "vfmsub132ps",
                "vfmsub132ss",
                "vfmsub213ps",
                "vfmsub213ss",
                "vfmsub231ps",
                "vfmsub231ss",
                "vfmsubadd132ps",
                "vfmsubadd231ps",
                "vfmsubadd232ps",
                "vfnmadd213ps",
                "vfnmadd213ss",
                "vfnmadd231ps",
                "vfnmadd231ss",
                "vfnmsub213ps",
                "vfnmsub213ss",
                "vfnmsub231ps",
                "vfnmsub231ss",
                "vgatherdd",
                "vgatherdps",
                "vgatherqd",
                "vgatherqps",
                "vhaddpd",
                "vhaddps",
                "vhsubpd",
                "vhsubps",
                "vinsertf128",
                "vinserti128",
                "vinsertps",
                "vmaskmovpd",
                "vmaskmovps",
                "vmaxpd",
                "vmaxps",
                "vmaxsd",
                "vmaxss",
                "vminpd",
                "vminps",
                "vminsd",
                "vminss",
                "vmovd",
                "vmovhpd",
                "vmovhps",
                "vmovlpd",
                "vmovlps",
                "vmovq2dq",
                "vmovsd",
                "vmovss",
                "vmpsadbw",
                "vmread",
                "vmulpd",
                "vmulps",
                "vmulsd",
                "vmulss",
                "vmwrite",
                "vorpd",
                "vorps",
                "vpackssdw",
                "vpacksswb",
                "vpackusdw",
                "vpackuswb",
                "vpaddb",
                "vpaddd",
                "vpaddq",
                "vpaddsb",
                "vpaddsw",
                "vpaddusb",
                "vpaddusw",
                "vpaddw",
                "vpalignr",
                "vpand",
                "vpandn",
                "vpavgb",
                "vpavgw",
                "vpblendw",
                "vpclmulqdq",
                "vpcmpeqb",
                "vpcmpeqd",
                "vpcmpeqq",
                "vpcmpeqw",
                "vpcmpestri",
                "vpcmpestrm",
                "vpcmpgtb",
                "vpcmpgtd",
                "vpcmpgtq",
                "vpcmpgtw",
                "vpcmpistri",
                "vpcmpistrm",
                "vperm2f128",
                "vperm2i128",
                "vpermd",
                "vpermilp",
                "vpermilpd",
                "vpermilps",
                "vpermpd",
                "vpermq",
                "vpextrw",
                "vphaddd",
                "vphaddsw",
                "vphaddw",
                "vphsubd",
                "vphsubsw",
                "vphsubw",
                "vpinsrb",
                "vpinsrd",
                "vpinsrw",
                "vpmaddubsw",
                "vpmaddwd",
                "vpmaskmovd",
                "vpmaxsb",
                "vpmaxsd",
                "vpmaxsw",
                "vpmaxub",
                "vpmaxud",
                "vpmaxuw",
                "vpminsb",
                "vpminsd",
                "vpminsw",
                "vpminub",
                "vpminud",
                "vpminuw",
                "vpmuldq",
                "vpmulhrsw",
                "vpmulhuw",
                "vpmulhw",
                "vpmulld",
                "vpmullw",
                "vpor",
                "vpsadbw",
                "vpshufb",
                "vpshufd",
                "vpshufhw",
                "vpshuflw",
                "vpshufw",
                "vpsignb",
                "vpsignd",
                "vpsignw",
                "vpslld",
                "vpslldq",
                "vpsllq",
                "vpsllw",
                "vpsrad",
                "vpsraw",
                "vpsrld",
                "vpsrldq",
                "vpsrlq",
                "vpsrlw",
                "vpsubb",
                "vpsubd",
                "vpsubq",
                "vpsubsb",
                "vpsubsw",
                "vpsubusb",
                "vpsubusw",
                "vpsubw",
                "vpunpckhbw",
                "vpunpckhdq",
                "vpunpckhqdq",
                "vpunpckhwd",
                "vpunpcklbw",
                "vpunpckldq",
                "vpunpcklqdq",
                "vpunpcklwd",
                "vpxor",
                "vrcpss",
                "vroundpd",
                "vroundps",
                "vroundsd",
                "vroundss",
                "vrsqrtss",
                "vsha1rnds4",
                "vshld",
                "vshlx",
                "vshrd",
                "vshufpd",
                "vshufps",
                "vsqrtsd",
                "vsqrtss",
                "vsubpd",
                "vsubps",
                "vsubsd",
                "vsubss",
                "vunpckhpd",
                "vunpckhps",
                "vunpcklpd",
                "vunpcklps",
                "vxorpd",
                "vxorps",
                "w",
                "wait",
                "wbinvd",
                "wrmsr",
                "x",
                "xadd",
                "xchg",
                "xcryptecb",
                "xlatb",
                "XMM0",
                "XMM1",
                "XMM10",
                "XMM11",
                "XMM12",
                "XMM13",
                "XMM14",
                "XMM15",
                "XMM2",
                "XMM3",
                "XMM4",
                "XMM5",
                "XMM6",
                "XMM7",
                "XMM8",
                "XMM9",
                "xor",
                "y",
                "YMM0",
                "YMM1",
                "YMM10",
                "YMM11",
                "YMM12",
                "YMM13",
                "YMM14",
                "YMM15",
                "YMM2",
                "YMM3",
                "YMM4",
                "YMM5",
                "YMM6",
                "YMM7",
                "YMM8",
                "YMM9",
                "z",
            ]
            .iter(),
        )
        .write_to_file(
            &Path::new(&env::var("OUT_DIR").unwrap()).join("atoms.rs"),
        )
        .unwrap()
}
//
//fn main() {
//    let out_dir = env::var("OUT_DIR").unwrap();
//    let dest_path = Path::new(&out_dir).join("rreil.rs");
//    let mut f = File::create(&dest_path).unwrap();
//
//    write_binary_operations(&mut f);
//    write_unary_operations(&mut f);
//    write_store_operation(&mut f);
//    write_load_operation(&mut f);
//    write_ret_operations(&mut f);
//    write_call_operations(&mut f);
//    write_extraction_operations(&mut f);
//    write_selection_operations(&mut f);
//}
