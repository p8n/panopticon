// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use crate::Variable;

/// Branch condition
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Guard {
    /// Guard is constant true
    True,
    /// Guard is constant false
    False,
    /// Guard depends on a one bit RREIL value.
    Predicate {
        /// Flag value. Must be `0` or `1`.
        flag: Variable,
        /// Expected value of `flag`. If `flag` is `1` and `expected` is true or if
        /// `flag` is `1` and `expected` is true the guard is true. Otherwise its false.
        expected: bool,
    },
}
