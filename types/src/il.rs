// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use crate::{Atom, Constant, Constraint, Result, Segment, Value, Variable};
use quickcheck::{Arbitrary, Gen};
use rand::Rng;
use ron_uuid::UUID;
use smallvec::SmallVec;
use std::fmt;
use std::ops::Range;

/// Address range with sub-byte pecision. Used to order instructions that don't occupy any space in
/// the binary (e.g. Phi).
#[derive(Clone, PartialEq, Eq)]
pub struct Area {
    /// First byte (inclusive).
    pub start: u64,
    /// Last byte (exclusive).
    pub end: u64,
}

impl From<Range<u64>> for Area {
    fn from(r: Range<u64>) -> Self {
        Area { start: r.start, end: r.end }
    }
}

impl fmt::Debug for Area {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}..", self.start)?;
        write!(f, "{}", self.end)
    }
}

/// Byte order.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Endianess {
    /// Least significant byte first.
    Little,
    /// Most significant byte first.
    Big,
}

impl Arbitrary for Endianess {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        match g.gen_range(0, 1) {
            0 => Endianess::Little,
            1 => Endianess::Big,
            _ => unreachable!(),
        }
    }
}

/// A RREIL operation.
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Operation {
    /// Integer addition
    Add { left: Value, right: Value },
    /// Integer subtraction
    Subtract { left: Value, right: Value },
    /// Unsigned integer multiplication
    Multiply { left: Value, right: Value },
    /// Unsigned integer division
    DivideUnsigned { left: Value, right: Value },
    /// Signed integer division
    DivideSigned { left: Value, right: Value },
    /// Bitwise left shift
    ShiftLeft { left: Value, right: Value },
    /// Bitwise logical right shift
    ShiftRightUnsigned { left: Value, right: Value },
    /// Bitwise arithmetic right shift
    ShiftRightSigned { left: Value, right: Value },
    /// Integer modulo
    Modulo { left: Value, right: Value },
    /// Bitwise logical and
    And { left: Value, right: Value },
    /// Bitwise logical or
    InclusiveOr { left: Value, right: Value },
    /// Bitwise logical xor
    ExclusiveOr { left: Value, right: Value },

    /// Compare both operands for equality and returns `1` or `0`
    Equal { left: Value, right: Value },
    /// Returns `1` if the first operand is less than or equal to the second and `0` otherwise.
    /// Comparison assumes unsigned values.
    LessOrEqualUnsigned { left: Value, right: Value },
    /// Returns `1` if the first operand is less than or equal to the second and `0` otherwise.
    /// Comparison assumes signed values.
    LessOrEqualSigned { left: Value, right: Value },
    /// Returns `1` if the first operand is less than the second and `0` otherwise.
    /// Comparison assumes unsigned values.
    LessUnsigned { left: Value, right: Value },
    /// Returns `1` if the first operand is less than the second and `0` otherwise.
    /// Comparison assumes signed values.
    LessSigned { left: Value, right: Value },

    /// Zero extends the operand.
    ZeroExtend { bits: u16, value: Value },
    /// Sign extends the operand.
    SignExtend { bits: u16, value: Value },
    /// Copies the operand without modification.
    Move { value: Value },
    /// Initializes a global variable.
    Initialize { base: Atom, bits: u16 },
    /// Copies only the range [self.0, self.0 + self.1] of bits from the operand.
    Select { start: u16, bits: u16, value: Value },
    /// Asserts that the second operand is constraint by the first.
    Assume { constraint: Constraint, value: Value },
    /// Reads a memory cell
    Load { segment: Segment, endianess: Endianess, bytes: u8, address: Value },

    /// SSA Phi function
    Phi { first: Value, second: Value, third: Value },
}

impl Operation {
    /// Returns references values that are read by the operation.
    pub fn reads<'x>(&'x self) -> SmallVec<[&'x Value; 3]> {
        use Operation::*;

        let mut ret = SmallVec::new();

        match self {
            &Add { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &Subtract { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &Multiply { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &DivideUnsigned { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &DivideSigned { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &ShiftLeft { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &ShiftRightUnsigned { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &ShiftRightSigned { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &Modulo { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &And { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &InclusiveOr { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &ExclusiveOr { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }

            &Equal { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &LessOrEqualUnsigned { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &LessOrEqualSigned { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &LessUnsigned { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }
            &LessSigned { ref left, ref right } => {
                ret.push(left);
                ret.push(right);
            }

            &ZeroExtend { ref value, .. } => {
                ret.push(value);
            }
            &SignExtend { ref value, .. } => {
                ret.push(value);
            }
            &Move { ref value } => {
                ret.push(value);
            }
            &Initialize { .. } => {}
            &Select { ref value, .. } => {
                ret.push(value);
            }
            &Assume { ref value, .. } => {
                ret.push(value);
            }

            &Load { ref address, .. } => {
                ret.push(address);
            }

            &Phi { ref first, ref second, ref third } => {
                ret.push(first);
                ret.push(second);
                ret.push(third);
            }
        }

        ret
    }

    /// Returns mutables references values that are read by the operation.
    pub fn reads_mut<'x>(&'x mut self) -> SmallVec<[&'x mut Value; 3]> {
        use Operation::*;

        let mut ret = SmallVec::new();

        match self {
            &mut Add { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut Subtract { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut Multiply { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut DivideUnsigned { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut DivideSigned { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut ShiftLeft { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut ShiftRightUnsigned { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut ShiftRightSigned { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut Modulo { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut And { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut InclusiveOr { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut ExclusiveOr { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }

            &mut Equal { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut LessOrEqualUnsigned { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut LessOrEqualSigned { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut LessUnsigned { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }
            &mut LessSigned { ref mut left, ref mut right } => {
                ret.push(left);
                ret.push(right);
            }

            &mut ZeroExtend { ref mut value, .. } => {
                ret.push(value);
            }
            &mut SignExtend { ref mut value, .. } => {
                ret.push(value);
            }
            &mut Move { ref mut value } => {
                ret.push(value);
            }
            &mut Initialize { .. } => {}
            &mut Select { ref mut value, .. } => {
                ret.push(value);
            }
            &mut Assume { ref mut value, .. } => {
                ret.push(value);
            }

            &mut Load { ref mut address, .. } => {
                ret.push(address);
            }

            &mut Phi { ref mut first, ref mut second, ref mut third } => {
                ret.push(first);
                ret.push(second);
                ret.push(third);
            }
        }

        ret
    }

    /// Executes a RREIL operation returning the result.
    pub fn execute(&self) -> Result<Value> {
        use std::num::Wrapping;
        use std::u64;
        use Operation::*;

        match self.clone() {
            Add { left: Value::Constant(a), right: Value::Constant(b) } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to add value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let mask = a.mask();
                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();
                Value::val(((a + b) & mask).0, bits)
            }
            Add { left: Value::Constant(Constant { value: 0, .. }), right } => {
                Ok(right)
            }
            Add { left, right: Value::Constant(Constant { value: 0, .. }) } => {
                Ok(left)
            }
            Add { .. } => Ok(Value::Undefined),

            Subtract {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to subtract value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let mask = a.mask();
                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();
                Value::val(((a - b) & mask).0, bits)
            }
            Subtract {
                left,
                right: Value::Constant(Constant { value: 0, .. }),
            } => Ok(left),
            Subtract { .. } => Ok(Value::Undefined),

            Multiply {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to multiply value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let mask = a.mask();
                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();
                Value::val(((a * b) & mask).0, bits)
            }
            Multiply {
                left: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            Multiply {
                right: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            Multiply {
                left: Value::Constant(Constant { value: 1, .. }),
                right,
            } => Ok(right),
            Multiply {
                left,
                right: Value::Constant(Constant { value: 1, .. }),
            } => Ok(left),
            Multiply { .. } => Ok(Value::Undefined),

            DivideUnsigned {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to divide value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let mask = a.mask();
                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                if b == Wrapping(0) {
                    Ok(Value::Undefined)
                } else {
                    Value::val(((a / b) & mask).0, bits)
                }
            }
            DivideUnsigned {
                left,
                right: Value::Constant(Constant { value: 1, .. }),
            } => Ok(left),
            DivideUnsigned {
                left: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            DivideUnsigned { .. } => Ok(Value::Undefined),

            DivideSigned {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to divide value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let bits = a.bits;
                let mut a = Wrapping(a.value as i64);
                let mut b = Wrapping(b.value as i64);

                if bits < 64 {
                    let sign_bit = Wrapping(1 << (bits - 1));
                    let m = Wrapping(1 << bits);

                    if sign_bit & a != Wrapping(0) {
                        a = a - m;
                    }
                    if sign_bit & b != Wrapping(0) {
                        b = b - m;
                    }
                    a = a % m;
                    b = b % m;
                }

                if b == Wrapping(0) {
                    Ok(Value::Undefined)
                } else {
                    if bits < 64 {
                        let m = 1 << bits;
                        Value::val((a / b).0 as u64 % m, bits)
                    } else {
                        Value::val((a / b).0 as u64, bits)
                    }
                }
            }
            DivideSigned {
                left,
                right: Value::Constant(Constant { value: 1, .. }),
            } => Ok(left),
            DivideSigned {
                left: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            DivideSigned { .. } => Ok(Value::Undefined),

            Modulo {
                right: Value::Constant(Constant { value: 0, .. }),
                ..
            } => Ok(Value::Undefined),
            Modulo { left: Value::Constant(a), right: Value::Constant(b) } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to mod value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let mask = a.mask();
                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                if b == Wrapping(0) {
                    Ok(Value::Undefined)
                } else {
                    Value::val(((a % b) & mask).0, bits)
                }
            }
            Modulo {
                left: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            Modulo {
                right: Value::Constant(Constant { value: 1, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            Modulo { .. } => Ok(Value::Undefined),

            ShiftLeft {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                let mask = a.mask();
                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                Value::val(((a << (b.0 as usize)) & mask).0, bits)
            }
            ShiftLeft {
                left: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            ShiftLeft {
                left,
                right: Value::Constant(Constant { value: 0, .. }),
            } => Ok(left),
            ShiftLeft { .. } => Ok(Value::Undefined),

            ShiftRightUnsigned {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                use std::cmp;

                if a.bits != b.bits {
                    return Err(format!(
                        "tried to mod value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let mask = a.mask();
                let bits = a.bits as usize;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                if b.0 >= bits as u64 {
                    Value::val(0, bits as u16)
                } else {
                    Value::val(
                        ((a >> cmp::min(cmp::min(64, bits), b.0 as usize))
                            & mask)
                            .0,
                        bits as u16,
                    )
                }
            }
            ShiftRightUnsigned {
                left: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            ShiftRightUnsigned {
                left,
                right: Value::Constant(Constant { value: 0, .. }),
            } => Ok(left),
            ShiftRightUnsigned { .. } => Ok(Value::Undefined),

            ShiftRightSigned {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to mod value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let mask = a.mask();
                let bits = a.bits;
                let mut a = Wrapping(a.value as i64);
                let b = Wrapping(b.value as i64);

                if bits < 64 {
                    let sign_bit = Wrapping(1 << (bits - 1));
                    if sign_bit & a != Wrapping(0) {
                        a = a | Wrapping(-1i64 << bits);
                    }
                }

                if b.0 as u64 >= bits as u64 {
                    let ret = if a < Wrapping(0) {
                        if bits < 64 {
                            Value::Constant(Constant {
                                value: (1 << bits) - 1,
                                bits: bits,
                            })
                        } else {
                            Value::Constant(Constant {
                                value: u64::MAX,
                                bits: bits,
                            })
                        }
                    } else {
                        Value::Constant(Constant { value: 0, bits: bits })
                    };
                    Ok(ret)
                } else {
                    if bits < 64 {
                        Value::val(
                            (a >> (b.0 as usize)).0 as u64 & mask.0,
                            bits,
                        )
                    } else {
                        Value::val((a >> (b.0 as usize)).0 as u64, bits)
                    }
                }
            }
            ShiftRightSigned {
                left: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            ShiftRightSigned {
                left,
                right: Value::Constant(Constant { value: 0, .. }),
            } => Ok(left),
            ShiftRightSigned { .. } => Ok(Value::Undefined),

            And { left: Value::Constant(a), right: Value::Constant(b) } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to and value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                Value::val((a & b).0 as u64, bits)
            }
            And {
                right: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            And {
                left: Value::Constant(Constant { value: 0, bits: s }),
                ..
            } => Ok(Value::Constant(Constant { value: 0, bits: s })),
            And { .. } => Ok(Value::Undefined),

            InclusiveOr {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to or value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                Value::val((a | b).0 as u64, bits)
            }
            InclusiveOr {
                left,
                right: Value::Constant(Constant { value: 0, .. }),
            } => Ok(left),
            InclusiveOr {
                left: Value::Constant(Constant { value: 0, .. }),
                right,
            } => Ok(right),
            InclusiveOr { .. } => Ok(Value::Undefined),

            ExclusiveOr {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to xor value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                Value::val((a ^ b).0 as u64, bits)
            }
            ExclusiveOr { .. } => Ok(Value::Undefined),

            Equal { left: Value::Constant(a), right: Value::Constant(b) } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to compare value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                if a == b {
                    Value::val(1, 1)
                } else {
                    Value::val(0, 1)
                }
            }
            Equal { .. } => Ok(Value::Undefined),

            LessOrEqualUnsigned {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to compare value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                if a <= b {
                    Value::val(1, 1)
                } else {
                    Value::val(0, 1)
                }
            }
            LessOrEqualUnsigned {
                left: Value::Constant(Constant { value: 0, .. }),
                ..
            } => Ok(Value::Constant(Constant { value: 1, bits: 1 })),
            LessOrEqualUnsigned { .. } => Ok(Value::Undefined),

            LessOrEqualSigned {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to compare value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let bits = a.bits;
                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();
                let mask = Wrapping(if bits < 64 {
                    (1u64 << (bits - 1)) - 1
                } else {
                    u64::MAX
                });
                let sign_mask =
                    Wrapping(if bits < 64 { 1u64 << (bits - 1) } else { 0 });
                if (a & sign_mask) ^ (b & sign_mask) != Wrapping(0) {
                    Value::val(
                        if a & sign_mask != Wrapping(0) { 1 } else { 0 },
                        1,
                    )
                } else {
                    Value::val(if (a & mask) <= (b & mask) { 1 } else { 0 }, 1)
                }
            }
            LessOrEqualSigned { .. } => Ok(Value::Undefined),

            LessUnsigned {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to compare value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let a: Wrapping<_> = a.into();
                let b: Wrapping<_> = b.into();

                if a < b {
                    Value::val(1, 1)
                } else {
                    Value::val(0, 1)
                }
            }
            LessUnsigned { .. } => Ok(Value::Undefined),

            LessSigned {
                left: Value::Constant(a),
                right: Value::Constant(b),
            } => {
                if a.bits != b.bits {
                    return Err(format!(
                        "tried to compare value of {} and {} bits",
                        a.bits, b.bits
                    )
                    .into());
                }

                let bits = a.bits;
                let mut a: Wrapping<_> = a.into();
                let mut b: Wrapping<_> = b.into();

                if bits < 64 {
                    let sign_bit = Wrapping(1 << (bits - 1));
                    let m = Wrapping(1 << bits);

                    if sign_bit & a != Wrapping(0) {
                        a = a - m;
                    }
                    if sign_bit & b != Wrapping(0) {
                        b = b - m;
                    }
                    a = a % m;
                    b = b % m;
                }

                if a < b {
                    Value::val(1, 1)
                } else {
                    Value::val(0, 1)
                }
            }
            LessSigned { .. } => Ok(Value::Undefined),

            ZeroExtend {
                bits: s1,
                value: Value::Constant(Constant { value: v, bits: s0 }),
            } => {
                let mask1 = if s1 < 64 { (1u64 << s1) - 1 } else { u64::MAX };
                let mask0 = if s0 < 64 { (1u64 << s0) - 1 } else { u64::MAX };
                Value::val((v & mask0) & mask1, s1)
            }
            ZeroExtend {
                bits,
                value: Value::Variable(Variable { name, .. }),
            } => Value::var(name, bits),
            ZeroExtend { value: Value::Undefined, .. } => Ok(Value::Undefined),

            SignExtend {
                bits: t,
                value: Value::Constant(Constant { value: v, bits: s }),
            } => {
                let mask0 = if s < 64 { (1u64 << s) - 1 } else { u64::MAX };
                let mask1 = if t < 64 { (1u64 << t) - 1 } else { u64::MAX };
                let sign = if s < 64 { 1u64 << (s - 1) } else { 0 };

                if v & sign == 0 {
                    Value::val((v & mask0) & mask1, t)
                } else {
                    let mask = mask1 & !mask0;
                    Value::val((v & mask0) | mask, t)
                }
            }
            SignExtend {
                bits,
                value: Value::Variable(Variable { name, .. }),
            } => Value::var(name, bits),
            SignExtend { value: Value::Undefined, .. } => Ok(Value::Undefined),

            Move { value } => Ok(value),

            Initialize { .. } => Ok(Value::Undefined),

            Select { start, bits, value: Value::Constant(a) } => {
                let Constant { value: a_value, bits: a_bits } = a;

                if start + bits < 64 && a_bits <= start + bits {
                    let val = a_value >> start;
                    let mask = (1 << bits) - 1;

                    Value::val(val & mask, bits)
                } else {
                    Ok(Value::Undefined)
                }
            }
            Select { .. } => Ok(Value::Undefined),
            Assume { .. } => Ok(Value::Undefined),

            Load { .. } => Ok(Value::Undefined),

            Phi {
                first: a @ Value::Variable(_),
                second: Value::Undefined,
                third: Value::Undefined,
            } => Ok(a),
            Phi {
                first: ref a @ Value::Variable(_),
                second: ref b @ Value::Variable(_),
                third: Value::Undefined,
            } if *a == *b => Ok(a.clone()),
            Phi {
                first: ref a @ Value::Variable(_),
                second: ref b @ Value::Variable(_),
                third: ref c @ Value::Variable(_),
            } if *a == *b && *b == *c => Ok(a.clone()),
            Phi { .. } => Ok(Value::Undefined),
        }
    }
}

impl Arbitrary for Operation {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        loop {
            let op = match g.gen_range(0, 26) {
                0 => {
                    Operation::Add {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                1 => {
                    Operation::Subtract {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                2 => {
                    Operation::Multiply {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                3 => {
                    Operation::DivideUnsigned {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                4 => {
                    Operation::DivideSigned {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                5 => {
                    Operation::ShiftLeft {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                6 => {
                    Operation::ShiftRightUnsigned {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                7 => {
                    Operation::ShiftRightSigned {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                8 => {
                    Operation::Modulo {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                9 => {
                    Operation::And {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                10 => {
                    Operation::InclusiveOr {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                11 => {
                    Operation::ExclusiveOr {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }

                12 => {
                    Operation::Equal {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                13 => {
                    Operation::LessOrEqualUnsigned {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                14 => {
                    Operation::LessOrEqualSigned {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                15 => {
                    Operation::LessUnsigned {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }
                16 => {
                    Operation::LessSigned {
                        left: Value::arbitrary(g),
                        right: Value::arbitrary(g),
                    }
                }

                17 => {
                    Operation::ZeroExtend {
                        bits: g.gen(),
                        value: Value::arbitrary(g),
                    }
                }
                18 => {
                    Operation::SignExtend {
                        bits: g.gen(),
                        value: Value::arbitrary(g),
                    }
                }

                19 => Operation::Move { value: Value::arbitrary(g) },
                20 => {
                    Operation::Initialize {
                        base: String::arbitrary(g).into(),
                        bits: g.gen(),
                    }
                }

                21 => {
                    let v = Value::arbitrary(g);
                    let off = g.gen_range(0, v.bits().unwrap_or(0) + 1);
                    let sz = g.gen_range(0, v.bits().unwrap_or(0) + 1 - off);
                    Operation::Select { start: off, bits: sz, value: v }
                }
                22 => {
                    Operation::Assume {
                        constraint: Constraint::arbitrary(g),
                        value: Value::arbitrary(g),
                    }
                }

                23 => {
                    Operation::Load {
                        segment: Segment::arbitrary(g),
                        endianess: Endianess::arbitrary(g),
                        bytes: g.gen(),
                        address: Value::arbitrary(g),
                    }
                }

                24 => {
                    // XXX: make sizes equal?
                    let a = Value::arbitrary(g);
                    let b = Value::arbitrary(g);
                    Operation::Phi {
                        first: a,
                        second: b,
                        third: Value::undef(),
                    }
                }
                25 => {
                    let a = Value::arbitrary(g);
                    let b = Value::arbitrary(g);
                    let c = Value::arbitrary(g);
                    Operation::Phi { first: a, second: b, third: c }
                }

                _ => unreachable!(),
            };

            match op {
                Operation::Add {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::Subtract {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::Multiply {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::DivideUnsigned {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::DivideSigned {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::Modulo {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::ShiftLeft {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::ShiftRightUnsigned {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::ShiftRightSigned {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::And {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::InclusiveOr {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::ExclusiveOr {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::Equal {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::LessOrEqualUnsigned {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::LessOrEqualSigned {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::LessUnsigned {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::LessSigned {
                    left: Value::Undefined,
                    right: Value::Undefined,
                } => {}
                Operation::ZeroExtend { value: Value::Undefined, .. } => {}
                Operation::SignExtend { value: Value::Undefined, .. } => {}
                Operation::Select { value: Value::Undefined, .. } => {}
                Operation::Phi { first: Value::Undefined, .. } => {}
                Operation::Phi { second: Value::Undefined, .. } => {}
                Operation::Phi { second: Value::Constant(_), .. } => {}
                Operation::Phi { third: Value::Constant(_), .. } => {}

                _ => {
                    return op;
                }
            }
        }
    }
}

/// Things that can be called.
#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub enum CallTarget {
    /// A known, analysied function.
    Function(UUID),
    /// An external, unknown function.
    External(Atom),
}

/// A memory operation.
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum MemoryOperation {
    /// Writes a memory cell
    Store {
        /// Memory segment before the store.
        segment: Segment,
        /// Byte order if `bytes` > 1
        endianess: Endianess,
        /// Number of bytes to be written.
        bytes: u8,
        /// Address inside `segment` written to.
        address: Value,
        /// Value that is written.
        value: Value,
    },

    /// Memory phi. Merges up to three memory segments.
    MemoryPhi(Option<Segment>, Option<Segment>, Option<Segment>),

    /// Introduces a new memory segment into the function context.
    Allocate {
        /// Segment base name.
        base: Atom,
    },
}

/// Call/Return operations.
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum FlowOperation {
    /// Function call.
    Call {
        /// Function call target.
        function: UUID,
    },
    /// Call to an imported function.
    ExternalCall {
        /// Name of the imported function.
        external: Atom,
    },
    /// Call to an unknown value.
    IndirectCall {
        /// Call target
        target: Variable,
    },
    /// Return.
    Return,
}

/// A single IL statement.
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Statement {
    /// A single RREIL statement.
    Expression {
        /// Value that the operation result is assigned to
        result: Variable,
        /// Operation and its arguments
        op: Operation,
    },
    /// Interprocedural control flow operation
    Flow {
        /// Operation
        op: FlowOperation,
    },
    /// A memory operation
    Memory {
        /// Operation
        op: MemoryOperation,
        /// Memory segment resulting from the memory operation.
        result: Segment,
    },
}

impl Statement {
    /// Does a simple sanity check. The functions returns Err if
    /// - The argument size are not equal
    /// - The result has not the same size as `assignee`
    /// - The select operation arguments are out of range
    pub fn sanity_check(&self) -> Result<()> {
        use std::cmp;

        // check that argument sizes match
        let typecheck_binop = |a: &Value,
                               b: &Value,
                               result: &Variable|
         -> Result<()> {
            if !(a.bits() == None || b.bits() == None || a.bits() == b.bits()) {
                return Err(format!(
                    "Argument sizes mismatch: {:?} vs. {:?}",
                    a, b
                )
                .into());
            }

            if cmp::max(a.bits().unwrap_or(0), b.bits().unwrap_or(0))
                != result.bits
            {
                return Err(format!(
                    "Operation result and result sizes mismatch ({:?})",
                    self
                )
                .into());
            }

            Ok(())
        };
        let typecheck_cmpop = |a: &Value,
                               b: &Value,
                               result: &Variable|
         -> Result<()> {
            if !(a.bits() == None || b.bits() == None || a.bits() == b.bits()) {
                return Err("Argument sizes mismatch".into());
            }

            if result.bits != 1 {
                return Err("Compare operation result not a flag".into());
            }

            Ok(())
        };
        let typecheck_unop =
            |a: &Value, sz: Option<u16>, result: &Variable| -> Result<()> {
                if sz.is_none() {
                    // zext?
                    if !(a.bits() == None || Some(result.bits) <= a.bits()) {
                        return Err(
                            "Operation result and result sizes mismatch".into(),
                        );
                    }
                } else {
                    if !(a.bits() == None || Some(result.bits) == sz) {
                        return Err(
                            "Operation result and result sizes mismatch".into(),
                        );
                    }
                }
                Ok(())
            };

        match self {
            &Statement::Expression {
                op: Operation::Add { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::Subtract { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::Multiply { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::DivideUnsigned { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::DivideSigned { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::ShiftLeft { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::ShiftRightUnsigned { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::ShiftRightSigned { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::Modulo { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::And { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::ExclusiveOr { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),
            &Statement::Expression {
                op: Operation::InclusiveOr { ref left, ref right },
                ref result,
            } => typecheck_binop(left, right, result),

            &Statement::Expression {
                op: Operation::Equal { ref left, ref right },
                ref result,
            } => typecheck_cmpop(left, right, result),
            &Statement::Expression {
                op: Operation::LessOrEqualUnsigned { ref left, ref right },
                ref result,
            } => typecheck_cmpop(left, right, result),
            &Statement::Expression {
                op: Operation::LessOrEqualSigned { ref left, ref right },
                ref result,
            } => typecheck_cmpop(left, right, result),
            &Statement::Expression {
                op: Operation::LessUnsigned { ref left, ref right },
                ref result,
            } => typecheck_cmpop(left, right, result),
            &Statement::Expression {
                op: Operation::LessSigned { ref left, ref right },
                ref result,
            } => typecheck_cmpop(left, right, result),

            &Statement::Expression {
                op: Operation::SignExtend { bits, ref value },
                ref result,
            } => typecheck_unop(value, Some(bits), result),
            &Statement::Expression {
                op: Operation::ZeroExtend { bits, ref value },
                ref result,
            } => typecheck_unop(value, Some(bits), result),
            &Statement::Expression {
                op: Operation::Move { ref value },
                ref result,
            } => typecheck_unop(value, None, result),
            &Statement::Expression {
                op: Operation::Select { start, bits, ref value },
                ref result,
            } => {
                if !(result.bits == bits
                    && start + bits <= value.bits().unwrap_or(start + bits))
                {
                    Err("Ill-sized Select operation".into())
                } else {
                    Ok(())
                }
            }

            &Statement::Expression {
                op: Operation::Initialize { bits, .. },
                ref result,
            } => {
                if result.bits != bits {
                    Err("Operation result and result sizes mismatch".into())
                } else {
                    Ok(())
                }
            }

            /*
            &Statement::Expression { op: Operation::Call(_), ref result } => {
                if !(result == &Lvalue::Undefined) {
                    return Err("Call operation can only be assigned to Undefined".into());
                } else {
                    Ok(())
                }
            }

            &Statement::Expression{ op: Operation::Load(_,_,ref sz,_), ref result } => {
                if !result.bits.is_none() && result.bits != Some(*sz) {
                    return Err(format!("Memory operation with invalid size. Expected {:?} got {:?}",Some(*sz),result.bits).into());
                } else if *sz == 0 {
                    return Err("Memory operation of size 0".into());
                } else if *sz % 8 != 0 {
                    return Err("Memory operation not byte aligned".into());
                } else {
                    Ok(())
                }
            }

            &Statement::Expression{ op: Operation::Store(_,_,sz,_,ref val), ref result } => {
                if val.bits().is_some() && result.bits.is_some() && val.bits() != result.bits {
                    return Err("Memory store value with inconsitend size".into());
                } else if sz == 0 {
                    return Err("Memory operation of size 0".into());
                } else if val.bits().is_some() && val.bits() != Some(sz) {
                    return Err(format!("Memory store value with inconsitend size: {:?} != {}",val.bits(),sz).into());
                } else if sz % 8 != 0 {
                    return Err("Memory operation not byte aligned".into());
                } else {
                    Ok(())
                }
            }


            &Statement::Expression { op: Operation::Phi(ref vec), ref result } => {
                if !(vec.iter().all(|rv| rv.bits() == result.bits) && result.bits != None) {
                    return Err("Phi arguments must have equal sizes and can't be Undefined".into());
                } else {
                    Ok(())
                }
            }
            */
            _ => Ok(()),
        }
        .map_err(|_| format!("Failed sanity check for {:?}", self))?;

        Ok(())
    }
}

impl Arbitrary for Statement {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        loop {
            let stmt = match g.gen_range(0, 7) {
                0 => {
                    Statement::Expression {
                        result: Variable::arbitrary(g),
                        op: Operation::arbitrary(g),
                    }
                }
                1 => {
                    Statement::Flow {
                        op: FlowOperation::Call {
                            function: UUID::arbitrary(g),
                        },
                    }
                }
                2 => {
                    Statement::Flow {
                        op: FlowOperation::ExternalCall {
                            external: String::arbitrary(g).into(),
                        },
                    }
                }
                3 => {
                    Statement::Flow {
                        op: FlowOperation::IndirectCall {
                            target: Variable::arbitrary(g),
                        },
                    }
                }
                4 => Statement::Flow { op: FlowOperation::Return },
                5 => {
                    let mut addr = Value::arbitrary(g);
                    let mut val = Value::arbitrary(g);

                    while addr == Value::Undefined && val == Value::Undefined {
                        addr = Value::arbitrary(g);
                        val = Value::arbitrary(g);
                    }

                    Statement::Memory {
                        op: MemoryOperation::Store {
                            segment: Segment::arbitrary(g),
                            endianess: Endianess::arbitrary(g),
                            bytes: g.gen_range(1, 11),
                            address: addr,
                            value: val,
                        },
                        result: Segment::arbitrary(g),
                    }
                }
                6 => {
                    let op = match g.gen_range(0, 4) {
                        0 => MemoryOperation::MemoryPhi(None, None, None),
                        1 => {
                            MemoryOperation::MemoryPhi(
                                Some(Segment::arbitrary(g)),
                                None,
                                None,
                            )
                        }
                        2 => {
                            MemoryOperation::MemoryPhi(
                                Some(Segment::arbitrary(g)),
                                Some(Segment::arbitrary(g)),
                                None,
                            )
                        }
                        3 => {
                            MemoryOperation::MemoryPhi(
                                Some(Segment::arbitrary(g)),
                                Some(Segment::arbitrary(g)),
                                Some(Segment::arbitrary(g)),
                            )
                        }
                        _ => unreachable!(),
                    };

                    Statement::Memory { op: op, result: Segment::arbitrary(g) }
                }
                _ => unreachable!(),
            };

            if stmt.sanity_check().is_ok() {
                return stmt;
            }
        }
    }
}
