// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//! A Basic Block is a uninterrupted sequence of machine code.

use std::ops::{Add, AddAssign, Range, Sub, SubAssign};
use std::usize;

use ron_uuid::UUID;
use smallvec::SmallVec;

use crate::{Area, Code, Function, Guard, MnemonicIndex, Value, Variable};

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Target {
    Variable(Variable),
    BasicBlock(usize),
}

/// An uninterrupted sequence of machine code.
/// Basic blocks cover a continuous address range.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct BasicBlock {
    /// Fixed UUID of this basic block. Never changes.
    pub uuid: UUID,
    /// Previous mnemonic
    pub mnemonic: MnemonicIndex,
    /// Bytes covered byte the basic block.
    pub area: Area,
    /// Previous statement
    pub statement: u16,
    pub basic_block: BasicBlockIndex,
    pub next: SmallVec<[(Target, Guard); 2]>,
}

impl BasicBlock {
    /// Address range covered.
    pub fn area<'a>(&'a self) -> &'a Area {
        &self.area
    }
}

/// Index of the basic block.
#[derive(Clone, Copy, Debug, PartialOrd, Ord, PartialEq, Eq)]
pub struct BasicBlockIndex {
    index: usize,
}

impl BasicBlockIndex {
    /// Creates an index for basic block number `i`.
    pub fn new(i: usize) -> BasicBlockIndex {
        BasicBlockIndex { index: i }
    }
    /// Numeric index.
    pub fn index(&self) -> usize {
        self.index
    }
}

impl AddAssign<usize> for BasicBlockIndex {
    fn add_assign(&mut self, i: usize) {
        self.index += i
    }
}

impl SubAssign<usize> for BasicBlockIndex {
    fn sub_assign(&mut self, i: usize) {
        self.index -= 1
    }
}

impl Add<usize> for BasicBlockIndex {
    type Output = Self;

    fn add(self, i: usize) -> Self {
        BasicBlockIndex::new(self.index() + i)
    }
}

impl Sub<usize> for BasicBlockIndex {
    type Output = Self;

    fn sub(self, i: usize) -> Self {
        BasicBlockIndex::new(self.index() - i)
    }
}

impl Default for BasicBlockIndex {
    fn default() -> Self {
        BasicBlockIndex::new(usize::default())
    }
}

#[derive(Clone)]
/// Iterator over basic blocks.
pub struct BasicBlockIterator<'a> {
    function: &'a Function,
    next: usize,
}

impl<'a> BasicBlockIterator<'a> {
    /// Create a new basic block iterator.
    pub fn new(function: &'a Function) -> BasicBlockIterator<'a> {
        BasicBlockIterator { function: function, next: 0 }
    }
}

impl<'a> Iterator for BasicBlockIterator<'a> {
    type Item = &'a BasicBlock;

    fn next(&mut self) -> Option<&'a BasicBlock> {
        for i in self.next..self.function.code.len() {
            match &self.function.code[i] {
                &Code::BasicBlock(ref bb) => {
                    self.next = i + 1;
                    return Some(bb);
                }
                _ => {}
            }
        }

        None
    }
}
