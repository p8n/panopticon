// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use std::ops::{Add, AddAssign, Range, Sub, SubAssign};
use std::usize;

use smallvec::SmallVec;

use crate::{Area, Atom, BasicBlockIndex, Code, Function, Value};

/// Native ISA mnemonic.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Mnemonic {
    /// Addresses this mnemonic occupies.
    pub area: Area,
    /// Disassembled opcode.
    pub opcode: Atom,
    /// Arguments, in and out.
    pub operands: SmallVec<[Value; 3]>,
    pub basic_block: BasicBlockIndex,
    pub mnemonic: MnemonicIndex,
    /// Next statement
    pub statement: u16,
}
/*
impl Mnemonic {
    /// Create a new argument and IL-statement less mnemonic.
    pub fn new(a: Area, s: Atom) -> Mnemonic {
        Mnemonic {
            area: a,
            opcode: s,
            operands: SmallVec::default(),
            basic_block: BasicBlockIndex::default(),
            mnemonic: MnemonicIndex::default(),
            statements: 0..0,
        }
    }
}
*/

/*
/// Internal to `Mnemonic`
#[derive(Clone,Debug)]
pub enum Argument {
    /// Internal to `Mnemonic`
    Literal(StrRef),
    /// Internal to `Mnemonic`
    Value {
        /// Internal to `Mnemonic`
        has_sign: bool,
        /// Internal to `Mnemonic`
        value: Value,
    },
    /// Internal to `Mnemonic`
    Pointer {
        /// Internal to `Mnemonic`
        is_code: bool,
        /// Internal to `Mnemonic`
        region: StrRef,
        /// Internal to `Mnemonic`
        address: Value,
    },
}

macro_rules! arg {
    ( { u : $val:expr } $cdr:tt ) => {
        Argument::Value{
            has_sign: false,
            value: ($val).into(),
        }
    }
    ( { s : $val:expr } $cdr:tt ) => {
        Argument::Value{
            has_sign: true,
            value: ($val).into(),
        }
    }
    ( { p : $val:expr : $bank:expr } $cdr:tt ) => {
        Argument::Pointer{
            is_code: false,
            region: ($bank).into(),
            address: ($val).into(),
        }
    }
    ( { c : $val:expr : $bank:expr } $cdr:tt ) => {
        Argument::Pointer{
            is_code: false,
            region: ($bank).into(),
            address: ($val).into(),
        }
    }
    ( ) => {}
}

arg!({ u : Variable::new("test",1,None) } "sss");
arg!({ s : Variable::new("test",1,None) } "sss");

impl Argument {
    /// format := '{' type '}'
    /// type := 'u' ':' value | # unsigned
    ///         's' ':' value | # signed
    ///         'p' ':' value ':' bank |  # data pointer
    ///         'c' ':' value ':' bank |  # code pointer
    /// value := digit+ | xdigit+ | # constant
    ///          alpha alphanum* | # variable
    /// bank := alpha alphanum*
     pub fn parse(mut j: Chars) -> Result<Vec<Argument>> {
        named!(main, tag!("{"
*/

/// Index of a mnemonic in a function. Local to a specific function.
#[derive(Clone, Copy, Debug, PartialOrd, Ord, PartialEq, Eq)]
pub struct MnemonicIndex {
    index: usize,
}

impl MnemonicIndex {
    /// Create a new MnemonicIndex for the `i`th mnemonic.
    pub fn new(i: usize) -> MnemonicIndex {
        MnemonicIndex { index: i }
    }

    /// Returns the numeric index of this mnemonic. Can be used for arthimetic.
    pub fn index(&self) -> usize {
        self.index
    }
}

impl AddAssign<usize> for MnemonicIndex {
    fn add_assign(&mut self, i: usize) {
        self.index += i
    }
}

impl SubAssign<usize> for MnemonicIndex {
    fn sub_assign(&mut self, i: usize) {
        self.index -= 1
    }
}

impl Add<usize> for MnemonicIndex {
    type Output = Self;

    fn add(self, i: usize) -> Self {
        MnemonicIndex::new(self.index() + i)
    }
}

impl Sub<usize> for MnemonicIndex {
    type Output = Self;

    fn sub(self, i: usize) -> Self {
        MnemonicIndex::new(self.index() - i)
    }
}

impl Default for MnemonicIndex {
    fn default() -> Self {
        MnemonicIndex::new(usize::default())
    }
}

/// Iterator over a range of mnemonics
#[derive(Clone)]
pub struct MnemonicIterator<'a> {
    function: &'a Function,
    next: usize,
}

impl<'a> MnemonicIterator<'a> {
    /// Creates a new iterator over mnemonics `[index, max]`, both inclusive.
    pub fn new(function: &'a Function, next: usize) -> MnemonicIterator<'a> {
        MnemonicIterator { function: function, next: next }
    }
}

impl<'a> Iterator for MnemonicIterator<'a> {
    type Item = &'a Mnemonic;

    fn next(&mut self) -> Option<&'a Mnemonic> {
        for i in self.next..self.function.code.len() {
            match &self.function.code[i] {
                &Code::BasicBlock(_) => {
                    return None;
                }
                &Code::Mnemonic(ref bb) => {
                    self.next = i + 1;
                    return Some(bb);
                }
                &Code::Statement { .. } => {}
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ron_uuid::UUID;

    /*
     * (B0)
     * 0:  Mix   ; mov i x
     * 3:  J9    ; B1
     *
     * (B1)
     * 9:  Mis   ; mov i s
     * 12: R     ; ret
     */
    #[test]
    fn mnemonic_iter() {
        use crate::{Region, TestArch};

        let _ = simple_logger::init();
        let data = b"MixJ9xxxxMisR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        let bb0 = func.basic_blocks().next().unwrap();
        let bb1 = func.basic_blocks().skip(1).next().unwrap();

        assert_eq!(bb0.mnemonic, MnemonicIndex::new(0));
        assert_eq!(bb1.mnemonic, MnemonicIndex::new(2));
        assert_eq!(func.mnemonics(bb0.basic_block).count(), 2);
        assert_eq!(func.mnemonics(bb1.basic_block).count(), 2);
    }
}
