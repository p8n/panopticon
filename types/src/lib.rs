// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2019  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#![recursion_limit = "1024"]
#![warn(missing_docs)]

extern crate precomputed_hash;
extern crate string_cache;
#[macro_use]
extern crate log;
extern crate byteorder;
extern crate goblin;
extern crate num_traits;
extern crate rand;
//extern crate leb128;
extern crate memmap;
extern crate petgraph;
extern crate simple_logger;
extern crate smallvec;
//extern crate chrono;
//extern crate vec_map;
extern crate ron_uuid;

#[cfg(not(test))]
extern crate quickcheck;
#[cfg(test)]
#[macro_use]
extern crate quickcheck;

pub use ron_uuid::UUID;

mod errors;
pub use crate::errors::{Error, Result};

mod atoms {
    include!(concat!(env!("OUT_DIR"), "/atoms.rs"));
}
pub use crate::atoms::Atom;

mod value;
pub use crate::value::{Constant, Name, Segment, Value, Variable};

mod constraint;
pub use crate::constraint::Constraint;

mod guard;
pub use self::guard::Guard;

mod il;
pub use crate::il::{
    Area, CallTarget, Endianess, FlowOperation, MemoryOperation, Operation,
    Statement,
};

//mod bitcode;
//pub use self::bitcode::{
//    Bitcode,
//    BitcodeIter
//};

mod function;
pub use self::function::{Code, Function};

//mod statements;
//pub use self::statements::{
//    RewriteControl,
//    Statements,
//};

mod basic_block;
pub use self::basic_block::{
    BasicBlock, BasicBlockIndex, BasicBlockIterator, Target,
};

mod mnemonic;
pub use self::mnemonic::{Mnemonic, MnemonicIndex, MnemonicIterator};

mod region;
pub use self::region::Region;

mod architecture;
pub use self::architecture::{Architecture, Match, TestArch};

mod loader;
pub use self::loader::{Image, Machine};

/// Our string type.
pub type Str = ::std::borrow::Cow<'static, str>;
