// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2019  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use goblin;
use std::result;

#[derive(Debug)]
pub enum Error {
    Message(String),
    Goblin(goblin::error::Error),
    Io(std::io::Error),
}

impl From<String> for Error {
    fn from(s: String) -> Error {
        Error::Message(s)
    }
}

impl From<&str> for Error {
    fn from(s: &str) -> Error {
        Error::Message(s.to_string())
    }
}

impl From<goblin::error::Error> for Error {
    fn from(s: goblin::error::Error) -> Error {
        Error::Goblin(s)
    }
}

impl From<std::io::Error> for Error {
    fn from(s: std::io::Error) -> Error {
        Error::Io(s)
    }
}

impl From<std::string::FromUtf8Error> for Error {
    fn from(s: std::string::FromUtf8Error) -> Error {
        Error::Message(format!("{}", s))
    }
}

pub type Result<T> = result::Result<T, Error>;
