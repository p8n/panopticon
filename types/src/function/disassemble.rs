// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;
use std::ops::Range;
use std::usize;

use ron_uuid::UUID;
use smallvec::SmallVec;

use super::types::{AddrOrVar, Code, CtrlTransfers};
use super::Function;
use crate::{
    Architecture, Area, BasicBlock, BasicBlockIndex, Constant, Guard, Match,
    Mnemonic, MnemonicIndex, Region, Result, Statement, Target, Value,
    Variable,
};

impl Function {
    /// Creates a new function by disassembling from `region` starting at `start`.
    pub fn new<A: Architecture>(
        init: A::Configuration,
        start: u64,
        region: &Region,
        uuid: UUID,
    ) -> Result<Function> {
        let mut mnemonics = Vec::default();
        let mut by_source = CtrlTransfers::default();
        let mut by_destination = CtrlTransfers::default();
        let mut func = Function {
            name: format!("fn_{:x}", start).into(),
            uuid: uuid,
            region: region.uuid().clone(),
            code: Vec::default(),
        };

        Self::disassemble::<A>(
            init,
            vec![start],
            region,
            &mut mnemonics,
            &mut by_source,
            &mut by_destination,
        )?;
        Self::assemble_function(
            &mut func,
            start,
            mnemonics,
            by_source,
            by_destination,
        )?;

        Ok(func)
    }

    /// Continues disassembling of `region`. The function looks for resolved, indirect control flow
    /// edges.
    pub fn resolve_indirect_jumps<A, F>(
        &mut self,
        init: A::Configuration,
        region: &Region,
        mut resolver: F,
    ) -> Result<()>
    where
        A: Architecture,
        F: FnMut(&Variable) -> Option<SmallVec<[u64; 1]>>,
    {
        let mut by_source = CtrlTransfers::new();
        let mut by_destination = CtrlTransfers::new();
        let mut mnemonics: Vec<(Mnemonic, Vec<Statement>)> = Vec::default();
        let mut starts = Vec::new();

        for c in self.code.iter() {
            match c {
                &Code::BasicBlock(BasicBlock {
                    ref next,
                    ref area,
                    basic_block: src,
                    ..
                }) => {
                    let last_mne = Self::last_mnemonic(src, &self.code);
                    let src_addr = last_mne
                        .and_then(|i| self.code[i].area())
                        .map(|a| a.start)
                        .unwrap();

                    for &(ref tgt, ref gu) in next.iter() {
                        match tgt {
                            &Target::BasicBlock(tgt_idx) => {
                                let tgt_addr = self.code[tgt_idx]
                                    .area()
                                    .map(|a| a.start)
                                    .unwrap();

                                by_source
                                    .entry(src_addr)
                                    .or_insert_with(SmallVec::new)
                                    .push((
                                        AddrOrVar::Address(tgt_addr),
                                        gu.clone(),
                                    ));
                                by_destination
                                    .entry(tgt_addr)
                                    .or_insert_with(SmallVec::new)
                                    .push((
                                        AddrOrVar::Address(src_addr),
                                        gu.clone(),
                                    ));
                            }
                            &Target::Variable(ref var) => {
                                match resolver(var) {
                                    None => {
                                        by_source
                                            .entry(src_addr)
                                            .or_insert_with(SmallVec::new)
                                            .push((
                                                AddrOrVar::Variable(
                                                    var.clone(),
                                                ),
                                                gu.clone(),
                                            ));
                                    }
                                    Some(v) => {
                                        for tgt_addr in v {
                                            starts.push(tgt_addr);
                                            by_source
                                                .entry(src_addr)
                                                .or_insert_with(SmallVec::new)
                                                .push((
                                                    AddrOrVar::Address(
                                                        tgt_addr,
                                                    ),
                                                    gu.clone(),
                                                ));
                                            by_destination
                                                .entry(tgt_addr)
                                                .or_insert_with(SmallVec::new)
                                                .push((
                                                    AddrOrVar::Address(
                                                        src_addr,
                                                    ),
                                                    gu.clone(),
                                                ));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                &Code::Mnemonic(ref mne) => {
                    if let Some((prev, _)) = mnemonics.last() {
                        by_source
                            .entry(prev.area.start)
                            .or_insert_with(SmallVec::new)
                            .push((
                                AddrOrVar::Address(mne.area.start),
                                Guard::True,
                            ));
                        by_destination
                            .entry(mne.area.start)
                            .or_insert_with(SmallVec::new)
                            .push((
                                AddrOrVar::Address(prev.area.start),
                                Guard::True,
                            ));
                    }
                    mnemonics.push((mne.clone(), Vec::default()));
                }
                &Code::Statement { ref il, .. } => {
                    mnemonics.last_mut().unwrap().1.push(il.clone());
                }
            }
        }

        mnemonics.sort_by_key(|&(ref mne, _)| mne.area.start);

        let entry = self.entry_address();
        Self::disassemble::<A>(
            init,
            starts,
            region,
            &mut mnemonics,
            &mut by_source,
            &mut by_destination,
        )?;

        Self::assemble_function(
            self,
            entry,
            mnemonics,
            by_source,
            by_destination,
        )
    }

    fn disassemble<A: Architecture>(
        init: A::Configuration,
        starts: Vec<u64>,
        region: &Region,
        mnemonics: &mut Vec<(Mnemonic, Vec<Statement>)>,
        by_source: &mut HashMap<u64, SmallVec<[(AddrOrVar, Guard); 2]>>,
        by_destination: &mut HashMap<u64, SmallVec<[(AddrOrVar, Guard); 2]>>,
    ) -> Result<()> {
        let mut todo = HashSet::<u64>::from_iter(starts.into_iter());

        while let Some(addr) = todo.iter().next().cloned() {
            assert!(todo.remove(&addr));

            match mnemonics
                .binary_search_by_key(&addr, |&(ref x, _)| x.area.start)
            {
                // Already disassembled here
                Ok(pos) => {
                    let mne = &mnemonics[pos].0;

                    if mne.area.start != addr {
                        error!(
                            "{:#x}: Jump inside mnemonic {} at {:#x}",
                            addr, mne.opcode, mne.area.start
                        );
                    }
                }
                // New mnemonic
                Err(pos) => {
                    let mut matches = Vec::with_capacity(2);

                    match A::decode(region, addr, &init, &mut matches) {
                        Ok(()) => {
                            // Matched mnemonics
                            if matches.is_empty() {
                                error!("{:#x}: Unrecognized instruction", addr);
                            } else {
                                for m in matches.into_iter().rev() {
                                    trace!("{:x}: {}", m.area.start, m.opcode);
                                    Self::match_to_mnemonic(
                                        m,
                                        pos,
                                        mnemonics,
                                        by_source,
                                        by_destination,
                                        &mut todo,
                                    )?;
                                }
                            }
                        }
                        Err(e) => {
                            error!(
                                "{:#x} Failed to disassemble: {:?}",
                                addr, e
                            );
                        }
                    }
                }
            }
        }

        Ok(())
    }

    fn assemble_function(
        function: &mut Function,
        entry: u64,
        mut mnemonics: Vec<(Mnemonic, Vec<Statement>)>,
        by_source: HashMap<u64, SmallVec<[(AddrOrVar, Guard); 2]>>,
        by_destination: HashMap<u64, SmallVec<[(AddrOrVar, Guard); 2]>>,
    ) -> Result<()> {
        let mut basic_blocks = Vec::<(BasicBlock, Range<usize>)>::new();
        let mut idx = 0;

        // Partition mnemonics into basic blocks
        while idx < mnemonics.len() {
            if mnemonics.len() - idx > 1 {
                let next_bb = mnemonics.as_slice()[idx..]
                    .windows(2)
                    .position(|x| {
                        Self::is_basic_block_boundary(
                            &x[0].0,
                            &x[1].0,
                            entry,
                            &by_source,
                            &by_destination,
                        )
                    })
                    .map(|x| x + 1 + idx)
                    .unwrap_or(mnemonics.len());
                let bb = BasicBlock {
                    uuid: UUID::now(),
                    mnemonic: MnemonicIndex::default(),
                    area: Area {
                        start: mnemonics[idx].0.area.start,
                        end: mnemonics[next_bb - 1].0.area.end,
                    },
                    statement: 0,
                    basic_block: BasicBlockIndex::default(),
                    next: Default::default(),
                };

                basic_blocks.push((bb, idx..next_bb));
                idx = next_bb;
            } else {
                let bb = BasicBlock {
                    uuid: UUID::now(),
                    mnemonic: MnemonicIndex::default(),
                    area: mnemonics[idx].0.area.clone(),
                    statement: 0,
                    basic_block: BasicBlockIndex::default(),
                    next: Default::default(),
                };

                basic_blocks.push((bb, idx..mnemonics.len()));
                break;
            }
        }

        // Build temporary control flow graph
        let mut tmp_next = vec![
            SmallVec::<[(usize, Guard); 2]>::default();
            basic_blocks.len()
        ];

        for (bb_idx, (_, mnes)) in basic_blocks.iter().enumerate() {
            let last_mne = &mnemonics[mnes.end - 1].0;

            if let Some(ct) = by_source.get(&last_mne.area.start) {
                for &(ref val, ref guard) in ct.iter() {
                    match val {
                        &AddrOrVar::Address(addr) => {
                            let maybe_pos = basic_blocks
                                .binary_search_by_key(&addr, |&(ref bb, _)| {
                                    bb.area.start
                                });

                            match maybe_pos {
                                Ok(i) => {
                                    tmp_next[bb_idx].push((i, guard.clone()));
                                }
                                Err(_) => {}
                            }
                        }
                        &AddrOrVar::Variable(_) => {}
                    }
                }
            }
        }

        let entry_idx = basic_blocks
            .iter()
            .position(|(bb, _)| bb.area.start == entry)
            .ok_or("Internal error: no basic block at the entry point")?;

        // reverse post order
        // node index -> po order
        let mut po_nums: Vec<Option<usize>> = vec![None; basic_blocks.len()];

        Self::compute_postorder(&mut po_nums, entry_idx, &tmp_next);
        assert!(
            po_nums.iter().all(|x| x.is_some()),
            "Control flow graph not connected"
        );

        let mut rev_postorder: Vec<usize> =
            Vec::from_iter(0..basic_blocks.len());
        rev_postorder.sort_by_key(|&i| po_nums[i].unwrap());
        rev_postorder.reverse();

        // Fill code array
        function.code.reserve(
            basic_blocks.len()
                + mnemonics.iter().map(|(_, s)| 1 + s.len()).sum::<usize>(),
        );
        function.code.clear();

        let mut bb_to_code = Vec::from_iter(0..basic_blocks.len());
        for &idx in rev_postorder.iter() {
            let (bb, mnes) = &mut basic_blocks[idx];
            let sl = mnes.start..mnes.end;

            bb.basic_block = BasicBlockIndex::default();
            bb.mnemonic = MnemonicIndex::default();
            bb.statement = 0;

            bb_to_code[idx] = function.code.len();
            function.code.push(Code::BasicBlock(bb.clone()));

            for &mut (ref mut mne, ref mut instr) in
                mnemonics.as_mut_slice()[sl].iter_mut()
            {
                function.code.push(Code::Mnemonic(mne.clone()));
                function.code.extend(instr.drain(..).map(|s| {
                    Code::Statement {
                        basic_block: BasicBlockIndex::default(),
                        mnemonic: MnemonicIndex::default(),
                        statement: 0,
                        il: s,
                    }
                }));
            }
        }

        // set BasicBlock::next field
        // XXX: sort by address
        for &bb_idx in rev_postorder.iter() {
            let c_idx = bb_to_code[bb_idx];

            match &mut function.code[c_idx] {
                &mut Code::BasicBlock(BasicBlock { ref mut next, .. }) => {
                    let &(_, ref mnes) = &basic_blocks[bb_idx];
                    let last_mne = &mnemonics[mnes.end - 1].0;

                    match by_source.get(&last_mne.area.start) {
                        Some(ct) => {
                            for &(ref val, ref guard) in ct.iter() {
                                match val {
                                    &AddrOrVar::Address(_) => {}
                                    &AddrOrVar::Variable(ref var) => {
                                        next.push((
                                            Target::Variable(var.clone()),
                                            guard.clone(),
                                        ));
                                    }
                                }
                            }
                        }
                        None => {}
                    }
                    for &(nx_bb, ref guard) in &tmp_next[bb_idx] {
                        next.push((
                            Target::BasicBlock(bb_to_code[nx_bb]),
                            guard.clone(),
                        ));
                    }
                }
                &mut Code::Mnemonic(_) => {}
                &mut Code::Statement { .. } => {}
            }
        }

        let l = function.code.len();
        Self::adjust_code_vector(&mut function.code, 0..l);

        Ok(())
    }

    fn compute_postorder(
        po_nums: &mut [Option<usize>],
        entry: usize,
        edges: &[SmallVec<[(usize, Guard); 2]>],
    ) {
        let mut current = 0;
        let mut stack = Vec::<usize>::with_capacity(edges.len());
        let mut in_flight = vec![false; po_nums.len()];

        stack.push(entry);
        in_flight[entry] = true;

        'outer: while let Some(node) = stack.last().cloned() {
            in_flight[node] = true;
            for &(next, _) in edges[node].iter() {
                if po_nums[next].is_none() && !in_flight[next] {
                    in_flight[next] = true;
                    stack.push(next);
                    continue 'outer;
                }
            }

            po_nums[node] = Some(current);
            current += 1;
            stack.pop();
        }
    }

    fn match_to_mnemonic(
        m: Match,
        insert_at: usize,
        mnemonics: &mut Vec<(Mnemonic, Vec<Statement>)>,
        by_source: &mut CtrlTransfers,
        by_destination: &mut CtrlTransfers,
        todo: &mut HashSet<u64>,
    ) -> Result<()> {
        let this_mne = Mnemonic {
            area: m.area.into(),
            opcode: m.opcode,
            operands: m.operands,
            basic_block: BasicBlockIndex::default(),
            mnemonic: MnemonicIndex::default(),
            statement: 0,
        };
        mnemonics.insert(insert_at, (this_mne, m.instructions));

        // New control transfers
        for (origin, tgt, gu) in m.jumps {
            trace!("jump to {:?}", tgt);
            match tgt {
                Value::Constant(Constant { value, .. }) => {
                    by_source
                        .entry(origin)
                        .or_insert_with(SmallVec::default)
                        .push((AddrOrVar::Address(value), gu.clone()));
                    by_destination
                        .entry(value)
                        .or_insert_with(SmallVec::default)
                        .push((AddrOrVar::Address(origin), gu));
                    todo.insert(value);
                }
                Value::Variable(Variable { name, bits }) => {
                    by_source
                        .entry(origin)
                        .or_insert_with(SmallVec::default)
                        .push((
                            AddrOrVar::Variable(Variable::new(name, bits)?),
                            gu,
                        ));
                }
                Value::Undefined => {}
            }
        }

        Ok(())
    }

    fn is_basic_block_boundary(
        a: &Mnemonic,
        b: &Mnemonic,
        entry: u64,
        by_source: &CtrlTransfers,
        by_destination: &CtrlTransfers,
    ) -> bool {
        // if next mnemonics aren't adjacent
        let mut new_bb = a.area.end != b.area.start;

        println!("{:?}--{:?}: {}", a.area, b.area, new_bb);
        // or any following jumps aren't to adjacent mnemonics
        new_bb |= by_source
            .get(&a.area.start)
            .map(|v| {
                v.iter().any(|&(ref opt_dest, _)| {
                    match opt_dest {
                        &AddrOrVar::Address(r) => b.area.start != r,
                        _ => false,
                    }
                })
            })
            .unwrap_or(false);
        println!("{:?}--{:?}: {}", a.area, b.area, new_bb);

        // or any jumps pointing to the next that aren't from here
        new_bb |= by_destination
            .get(&b.area.start)
            .map(|v| {
                v.iter().any(|&(ref opt_src, _)| {
                    match opt_src {
                        &AddrOrVar::Address(r) => a.area.start != r,
                        _ => false,
                    }
                })
            })
            .unwrap_or(false);
        println!("{:?}--{:?}: {}", a.area, b.area, new_bb);

        // or the entry point does not point here
        new_bb |= b.area.start == entry;
        println!("{:?}--{:?}: {}", a.area, b.area, new_bb);

        new_bb
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{Atom, Name, Region, TestArch};
    use simple_logger;

    #[test]
    fn single_instruction() {
        let _ = simple_logger::init();
        let reg = Region::from_buf("", 16, b"Ma0R".to_vec(), 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert_eq!(func.basic_blocks().count(), 1);

        assert_eq!(func.entry_address(), 0);
        assert_eq!(func.basic_blocks().count(), 1);

        let bb = func.basic_blocks().next().unwrap();
        assert_eq!(bb.area(), &(0..4).into());
        assert_eq!(func.mnemonics(bb.basic_block).count(), 2);

        let mne = func.mnemonics(bb.basic_block).next().unwrap();
        assert_eq!(mne.opcode, Atom::from("mov"));
        let mne = func.mnemonics(bb.basic_block).skip(1).next().unwrap();
        assert_eq!(mne.opcode, Atom::from("ret"));
    }

    #[test]
    fn branch() {
        let _ = simple_logger::init();
        let reg = Region::from_buf("", 16, b"Bf4RR".to_vec(), 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert_eq!(func.basic_blocks().count(), 3);

        for bb in func.basic_blocks() {
            let mnes = func.mnemonics(bb.basic_block).collect::<Vec<_>>();

            if bb.area.start == 0 {
                assert_eq!(mnes.len(), 1);
                assert_eq!(mnes[0].opcode, Atom::from("br"));
                assert_eq!(mnes[0].area, (0..3).into());
                assert_eq!(bb.area, (0..3).into());
                assert_eq!(bb.next.len(), 2);

                match &bb.next[0] {
                    &(Target::BasicBlock(i), _) => {
                        match &func.code[i] {
                            &Code::BasicBlock(BasicBlock {
                                ref area, ..
                            }) => {
                                assert!(*area == (3..4).into());
                            }
                            _ => unreachable!(),
                        }
                    }
                    _ => unreachable!(),
                }

                match &bb.next[1] {
                    &(Target::BasicBlock(i), _) => {
                        match &func.code[i] {
                            &Code::BasicBlock(BasicBlock {
                                ref area, ..
                            }) => {
                                assert!(*area == (4..5).into());
                            }
                            _ => unreachable!(),
                        }
                    }
                    _ => unreachable!(),
                }
            } else if bb.area.start == 3 {
                assert_eq!(mnes.len(), 1);
                assert_eq!(mnes[0].opcode, Atom::from("ret"));
                assert_eq!(mnes[0].area, (3..4).into());
                assert_eq!(bb.area, (3..4).into());
                assert!(bb.next.is_empty());
            } else if bb.area.start == 4 {
                assert_eq!(mnes.len(), 1);
                assert_eq!(mnes[0].opcode, Atom::from("ret"));
                assert_eq!(mnes[0].area, (4..5).into());
                assert_eq!(bb.area, (4..5).into());
                assert!(bb.next.is_empty());
            } else {
                unreachable!();
            }
        }

        let entry_bb = func.entry_point();
        assert_eq!(func.basic_block(entry_bb.basic_block).area, (0..3).into());
    }

    #[test]
    fn single_loop() {
        let _ = simple_logger::init();
        let reg = Region::from_buf("", 16, b"AaaaAxxxJ0".to_vec(), 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert_eq!(func.basic_blocks().count(), 1);

        let bb = func.basic_blocks().next().unwrap();
        let mnes = func.mnemonics(bb.basic_block).collect::<Vec<_>>();

        if bb.area.start == 0 {
            assert_eq!(mnes.len(), 3);
            assert_eq!(mnes[0].opcode, Atom::from("add"));
            assert_eq!(mnes[0].area, (0..4).into());
            assert_eq!(mnes[1].opcode, Atom::from("add"));
            assert_eq!(mnes[1].area, (4..8).into());
            assert_eq!(mnes[2].opcode, Atom::from("jmp"));
            assert_eq!(mnes[2].area, (8..10).into());
            assert_eq!(bb.area, (0..10).into());
            assert_eq!(bb.next.len(), 1);

            match &bb.next[0] {
                &(Target::BasicBlock(i), _) => {
                    match &func.code[i] {
                        &Code::BasicBlock(BasicBlock { ref area, .. }) => {
                            assert!(*area == (0..10).into());
                        }
                        _ => unreachable!(),
                    }
                }
                _ => unreachable!(),
            }
        } else {
            unreachable!();
        }
    }

    #[test]
    fn empty_function() {
        let reg = Region::from_buf("", 16, vec![], 0, None);
        assert!(Function::new::<TestArch>((), 0, &reg, UUID::now()).is_err());
    }

    #[test]
    fn resolve_indirect() {
        let _ = simple_logger::init();
        let reg = Region::from_buf("", 16, b"AaaaJxAxxxR".to_vec(), 0, None);
        let mut func =
            Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let a = Name::new("x", None);

        assert_eq!(func.basic_blocks().count(), 1);

        for bb in func.basic_blocks() {
            assert!(bb.area == (0..6).into());
            assert_eq!(bb.next.len(), 1);
            match bb.next.first() {
                Some(&(Target::Variable(ref var), _)) => {
                    assert!(var.name == a);
                    assert_eq!(var.bits, 32);
                }
                _ => unreachable!(),
            }
        }

        let mut replaced = false;
        assert!(func
            .resolve_indirect_jumps::<TestArch, _>((), &reg, |x| {
                if *x == Variable::new(a.clone(), 32).unwrap() {
                    replaced = true;
                    Some(vec![6].into())
                } else {
                    Default::default()
                }
            })
            .is_ok());
        assert!(replaced);

        eprintln!(
            "2nd round bb {:#?}",
            func.basic_blocks().collect::<Vec<_>>()
        );
        assert_eq!(func.basic_blocks().count(), 1);

        for bb in func.basic_blocks() {
            assert!(bb.area == (0..11).into());
            assert_eq!(bb.next.len(), 0);
        }
    }

    #[test]
    fn entry_split() {
        let _ = simple_logger::init();
        let reg = Region::from_buf("", 16, b"AaaaAaaaJx".to_vec(), 0, None);
        let mut func =
            Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let a = Name::new("x", None);

        assert!(func
            .resolve_indirect_jumps::<TestArch, _>((), &reg, |x| {
                if *x == Variable::new(a.clone(), 32).unwrap() {
                    Some(vec![4].into())
                } else {
                    Default::default()
                }
            })
            .is_ok());

        assert_eq!(func.basic_blocks().count(), 2);

        let mut seen_bb0 = false;
        let mut seen_bb1 = false;

        for bb in func.basic_blocks() {
            if !seen_bb0 && bb.area == (0..4).into() {
                seen_bb0 = true;
            } else if !seen_bb1 && bb.area == (4..10).into() {
                seen_bb1 = true;
            } else {
                unreachable!()
            }
        }

        assert_eq!(func.entry_address(), 0);
    }

    #[test]
    fn resolve_indirect_to_multiple() {
        let _ = simple_logger::init();
        let reg = Region::from_buf("", 16, b"AaaaJxAxxxRRRR".to_vec(), 0, None);
        let mut func =
            Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let a = Name::new("x", None);

        assert_eq!(func.basic_blocks().count(), 1);

        match func.basic_blocks().next() {
            Some(bb) => {
                assert!(bb.area == (0..6).into());
                assert_eq!(bb.next.len(), 1);
                match &bb.next[0] {
                    &(Target::Variable(ref var), _) => {
                        assert_eq!(var.bits, 32);
                        assert_eq!(var.name, a);
                    }
                    _ => unreachable!(),
                }
            }
            _ => unreachable!(),
        }

        assert!(func
            .resolve_indirect_jumps::<TestArch, _>((), &reg, |x| {
                if *x == Variable::new(a.clone(), 32).unwrap() {
                    Some(vec![6, 11, 12, 13].into())
                } else {
                    Default::default()
                }
            })
            .is_ok());

        assert_eq!(func.basic_blocks().count(), 5);

        for bb in func.basic_blocks() {
            assert!(
                bb.area == (0..6).into()
                    || bb.area == (11..12).into()
                    || bb.area == (12..13).into()
                    || bb.area == (13..14).into()
                    || bb.area == (6..11).into()
            );
        }
    }

    #[test]
    fn issue_51_treat_entry_point_as_incoming_edge() {
        let _ = simple_logger::init();
        let reg = Region::from_buf("", 16, b"AaaaAxxxJ0".to_vec(), 0, None);
        let func = Function::new::<TestArch>((), 4, &reg, UUID::now()).unwrap();

        assert_eq!(func.cflow_graph.node_count(), 2);
        assert_eq!(func.cflow_graph.edge_count(), 2);

        let mut bb0_vx = None;
        let mut bb1_vx = None;

        for vx in func.cflow_graph.node_indices() {
            if let Some(&CfgNode::BasicBlock(bb_idx)) =
                func.cflow_graph().node_weight(vx)
            {
                let bb = func.basic_block(bb_idx);
                let mnes = func.mnemonics(bb_idx).collect::<Vec<_>>();

                if bb.area.start == 0 {
                    assert_eq!(mnes.len(), 1);
                    assert_eq!(bb.area, (0..4).into());
                    bb0_vx = Some(vx);
                } else if bb.area.start == 4 {
                    assert_eq!(mnes.len(), 2);
                    assert_eq!(bb.area, (4..10).into());
                    bb1_vx = Some(vx);
                } else {
                    unreachable!();
                }
            } else {
                unreachable!();
            }
        }

        assert!(bb0_vx.is_some() && bb1_vx.is_some());
        let entry_idx = func.entry_point();
        assert_eq!(func.basic_block(entry_idx).node, bb1_vx.unwrap());
        assert!(func
            .cflow_graph
            .find_edge(bb0_vx.unwrap(), bb1_vx.unwrap())
            .is_some());
        assert!(func
            .cflow_graph
            .find_edge(bb1_vx.unwrap(), bb0_vx.unwrap())
            .is_some());
    }
}
