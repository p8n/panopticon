/// Node in the control flow graph.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum CfgNode<'a> {
    /// Basic block
    BasicBlock(&'a BasicBlock),
    /// Indirect jump to this value.
    Variable(Variable),
}

struct CtrlFlowGraph<'a> {
    function: &'a Function,
    pub graph: Graph<CfgNode<'a>, Guard>,
}

impl CtrlFlowGraph {
    fn new(func: &Function) -> Self {
        let mut graph = Graph::default();
        let mut rev = HashMap::default();

        for bb_idx in function.basic_blocks() {
            let bb = function.basic_block(bb_idx);
            let node = graph.add_node(CfgNode::BasicBlock(bb));

            rev.insert(bb_idx, node);
        }

        for bb_idx in function.basic_blocks() {
            let bb = function.basic_block(bb_idx);
            let src = rev[bb_idx];

            for &(ref tgt, ref gu) in bb.next {
                match tgt {
                    &Target::BasicBlock(idx) => {
                        match &function.code[idx] {
                            &Code::BasicBlock(BasicBlock {
                                ref basic_block,
                                ..
                            }) => {
                                let tgt = rev[basic_block];

                                graph.add_edge(src, tgt, gu.clone());
                            }
                            _ => unreachable!(),
                        }
                    }
                    &Target::Variable(ref var) => {
                        let tgt =
                            graph.add_node(CfgNode::Variable(var.clone()));

                        graph.add_edge(src, tgt, gu.clone());
                    }
                }
            }
        }

        CtrlFlowGraph { function: func, graph: graph }
    }
}
