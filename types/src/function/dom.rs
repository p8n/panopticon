fn slt(cfg: &[SmallVec<[(usize, Guard); 2>]) -> Result<()> {
    // step1
    let mut pred = vec![SmallVec::<[usize; 2]>::default(); cfg.len()];
    let mut semi = vec![0usize; cfg.len()];
    let mut vertex = vec![0usize; cfg.len()];
    let mut parent = vec![0usize; cfg.len()];

    fn dfs(v: usize, mut n: usize, pred: &mut [SmallVec<[usize; 2]>], semi: &mut [usize], vertex: &mut [usize], parent: &mut [usize]) {
        n += 1;
        semi[v] = n;
        vertex[n] = v;

        for &(w, _) in cfg[v] {
            if semi[w] == 0 {
                parent[w] = v;
                dfs(v, n, pred, semi, vertex, parent);
            }
            pred[w].append(v);
        }
    }


    dfs(r, n, &mut pred, &mut semi, &mut vertex, &mut parent);


}

fn dominator_tree(code: &[Code], doms: &Dominators<NodeIndex>) -> Result<Vec<DomTreeEvent>> {
    use petgraph::visit::IntoNodeReferences;

    let _prof = hprof::enter("dominator tree");
    let num_bb = func.basic_blocks().len();
    let cfg = func.cflow_graph();
    let mut tree = vec![BitSet::with_capacity(num_bb); num_bb];

    for (bb_idx,bb) in func.basic_blocks() {
        if let Some(idom) = doms.immediate_dominator(bb.node) {
            if let Some(&CfgNode::BasicBlock(p_idx)) = cfg.node_weight(idom) {
                tree[p_idx.index()].insert(bb_idx.index());
            }
        }
    }

    let mut completed = BitSet::with_capacity(num_bb);
    let mut processing = BitSet::with_capacity(num_bb);
    let mut stack = Vec::with_capacity(num_bb);
    let entry_idx = match cfg.node_weight(doms.root()) {
        Some(&CfgNode::BasicBlock(i)) => i,
        _ => { return Err("Internal error: dominator tree root isn't a basic block".into()); }
    };
    let mut ret = Vec::with_capacity(num_bb * 2);
    let succs = cfg.node_references().filter_map(|(idx,r)| {
        match r {
                &CfgNode::BasicBlock(_) => {
                    Some(cfg.neighbors_directed(idx,Direction::Outgoing).filter_map(|y| {
                        match cfg.node_weight(y) {
                            Some(&CfgNode::BasicBlock(j)) => Some(j),
                            _ => None
                        }
                    }).collect::<SmallVec<[BasicBlockIndex; 2]>>())
                }
                _ => None
            }
    }).collect::<Vec<_>>();
    let mut preds = cfg.node_references().filter_map(|(idx,r)| {
        match r {
                &CfgNode::BasicBlock(_) => {
                    Some(cfg.neighbors_directed(idx,Direction::Incoming).filter_map(|y| {
                        match cfg.node_weight(y) {
                            Some(&CfgNode::BasicBlock(j)) => Some(j),
                            _ => None
                        }
                    }).collect::<SmallVec<[BasicBlockIndex; 3]>>())
                }
                _ => None
            }
    }).collect::<Vec<_>>();

    preds.iter_mut().for_each(|x| x.sort());
    stack.push(entry_idx.index());

    {
        let entry = func.basic_block(entry_idx);
        let in_edges = cfg.edges_directed(entry.node,Direction::Incoming).count();
        let succ = succs[entry.node.index()].iter().map(|&x_idx| {
            let x = func.basic_block(x_idx);
            let pos = preds[x.node.index()].iter().position(|&y| y == entry_idx).unwrap();

            (x_idx,pos)
        }).collect::<SmallVec<[(BasicBlockIndex,usize); 2]>>();
        let out_edges = cfg.edges_directed(entry.node,Direction::Outgoing).map(|x| {
            x.id()
        }).collect::<SmallVec<[EdgeIndex; 2]>>();

        ret.push(DomTreeEvent::Enter{
            index: entry_idx,
            start: entry.area.start,
            num_in_edges: in_edges,
            successors: succ,
            out_edges: out_edges,
        });
    }

    'outer: while !stack.is_empty() {
        let n = *stack.last().unwrap();

        for m in tree[n].iter() {
            if !processing.contains(m) {
                // Enter node
                let bb_idx = BasicBlockIndex::new(m);
                let bb = func.basic_block(bb_idx);
                let in_edges = cfg.edges_directed(bb.node,Direction::Incoming).count();
                let succ = succs[bb.node.index()].iter().map(|&x_idx| {
                    let x = func.basic_block(x_idx);
                    let pos = preds[x.node.index()].iter().position(|&y| y == bb_idx).unwrap();

                    (x_idx,pos)
                }).collect::<SmallVec<[(BasicBlockIndex,usize); 2]>>();
                let out_edges = cfg.edges_directed(bb.node,Direction::Outgoing).map(|x| {
                    x.id()
                }).collect::<SmallVec<[EdgeIndex; 2]>>();

                ret.push(DomTreeEvent::Enter{
                    index: bb_idx,
                    start: bb.area.start,
                    num_in_edges: in_edges,
                    successors: succ,
                    out_edges: out_edges,
                });
                stack.push(m);
                processing.insert(m);
                continue 'outer;
            }
        }

        // Leave node
        ret.push(DomTreeEvent::Leave(BasicBlockIndex::new(n)));
        completed.insert(n);
        stack.pop();
    }

    Ok(ret)
}

pub fn dominance_fontiers
