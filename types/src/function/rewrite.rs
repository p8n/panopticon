/*
 * (B0)
 * 0:  Mi1  ; mov i 1
 * 3:  Cfi0 ; cmp f i 0
 * 7:  Bf18 ; br f (B2)
 *
 * (B1)
 * 11: Aii3 ; add i i 3
 * 15: J22  ; jmp (B3)
 *
 * (B2)
 * 18: Aii3 ; add i i 3
 *
 * (B3)
 * 22: Ms3  ; mov s 3
 * 25: R    ; ret
 */
#[test]
fn rewrite_increase() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
    let mut b0_idx = None;
    let mut b1_idx = None;
    let mut b2_idx = None;
    let mut b3_idx = None;

    func.pack().unwrap();

    for (idx, bb) in func.basic_blocks() {
        if bb.area.start == 0 {
            b0_idx = Some(idx);
        } else if bb.area.start == 11 {
            b1_idx = Some(idx);
        } else if bb.area.start == 18 {
            b2_idx = Some(idx);
        } else if bb.area.start == 22 {
            b3_idx = Some(idx);
        } else {
            unreachable!()
        }
    }

    assert!(
        b0_idx.is_some()
            && b1_idx.is_some()
            && b2_idx.is_some()
            && b3_idx.is_some()
    );

    let _ = func
        .rewrite_basic_block(b2_idx.unwrap(), |stmt, _, _, _| {
            match stmt {
                &mut Statement::Expression {
                    op:
                        Operation::Add(
                            Value::Constant(ref mut a),
                            Value::Constant(ref mut b),
                        ),
                    ..
                } => {
                    *a = Constant::new(0xffffffff, 32).unwrap();
                    *b = Constant::new(0x11111111, 32).unwrap();
                }
                _ => {}
            }

            Ok(RewriteControl::Continue)
        })
        .unwrap();

    let b0 = func.statements(b0_idx.unwrap()).collect::<Vec<_>>();
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(_)),
        ..
    } = &*b0[0]
    {
    } else {
        unreachable!()
    }
    if let &Statement::Expression {
        op:
            Operation::LessOrEqualUnsigned(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b0[1]
    {
    } else {
        unreachable!()
    }
    assert_eq!(b0.len(), 2);
    let mne0 = func.mnemonics(b0_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(mne0.len(), 3);

    let b1 = func.statements(b1_idx.unwrap()).collect::<Vec<_>>();
    if let &Statement::Expression {
        op: Operation::Add(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b1[0]
    {
    } else {
        unreachable!()
    }
    assert_eq!(b1.len(), 1);
    let mne1 = func.mnemonics(b1_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(mne1.len(), 2);

    let b2 = func.statements(b2_idx.unwrap()).collect::<Vec<_>>();
    if let &Statement::Expression {
        op: Operation::Add(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b2[0]
    {
    } else {
        unreachable!()
    }
    assert_eq!(b2.len(), 1);
    let mne2 = func.mnemonics(b2_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(mne2.len(), 1);

    let b3 = func.statements(b3_idx.unwrap()).collect::<Vec<_>>();
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(_)),
        ..
    } = &*b3[0]
    {
    } else {
        unreachable!()
    }
    assert_eq!(b3.len(), 2);
    let mne3 = func.mnemonics(b3_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(mne3.len(), 2);
}

/*
 * (B0)
 * 0:  Mi1  ; mov i 1
 * 3:  Cfi0 ; cmp f i 0
 * 7:  Bf18 ; br f (B2)
 *
 * (B1)
 * 11: Aii3 ; add i i 3
 * 15: J22  ; jmp (B3)
 *
 * (B2)
 * 18: Aii3 ; add i i 3
 *
 * (B3)
 * 22: Ms3  ; mov s 3
 * 25: R    ; ret
 */
#[test]
fn rewrite_rename() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
    let mut b0_idx = None;
    let mut b1_idx = None;
    let mut b2_idx = None;
    let mut b3_idx = None;

    for (idx, bb) in func.basic_blocks() {
        if bb.area.start == 0 {
            b0_idx = Some(idx);
        } else if bb.area.start == 11 {
            b1_idx = Some(idx);
        } else if bb.area.start == 18 {
            b2_idx = Some(idx);
        } else if bb.area.start == 22 {
            b3_idx = Some(idx);
        } else {
            unreachable!()
        }
    }

    assert!(
        b0_idx.is_some()
            && b1_idx.is_some()
            && b2_idx.is_some()
            && b3_idx.is_some()
    );
    fn f(
        stmt: &mut Statement,
        names: &mut Names,
        _: &mut Strings,
        _: &mut Segments,
    ) -> ::Result<::RewriteControl> {
        match stmt {
            &mut Statement::Expression {
                result: Variable { ref mut name, .. },
                ..
            } => {
                let new_name = names.value(*name)?.clone();
                let new_name = Name::new(
                    new_name.base().to_string().to_uppercase().into(),
                    new_name.subscript(),
                );
                *name = names.insert(&new_name);
            }
            _ => {}
        }

        Ok(RewriteControl::Continue)
    }

    let _ = func.rewrite_basic_block(b0_idx.unwrap(), &f).unwrap();
    let _ = func.rewrite_basic_block(b1_idx.unwrap(), &f).unwrap();
    let _ = func.rewrite_basic_block(b2_idx.unwrap(), &f).unwrap();
    let _ = func.rewrite_basic_block(b3_idx.unwrap(), &f).unwrap();

    let b0 = func.statements(b0_idx.unwrap()).collect::<Vec<_>>();
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(_)),
        ..
    } = &*b0[0]
    {
    } else {
        unreachable!()
    }
    if let &Statement::Expression {
        op:
            Operation::LessOrEqualUnsigned(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b0[1]
    {
    } else {
        unreachable!()
    }
    assert_eq!(b0.len(), 2);

    let b1 = func.statements(b1_idx.unwrap()).collect::<Vec<_>>();
    if let &Statement::Expression {
        op: Operation::Add(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b1[0]
    {
    } else {
        unreachable!()
    }
    assert_eq!(b1.len(), 1);

    let b2 = func.statements(b2_idx.unwrap()).collect::<Vec<_>>();
    if let &Statement::Expression {
        op: Operation::Add(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b2[0]
    {
    } else {
        unreachable!()
    }
    assert_eq!(b2.len(), 1);

    let b3 = func.statements(b3_idx.unwrap()).collect::<Vec<_>>();
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(_)),
        ..
    } = &*b3[0]
    {
    } else {
        unreachable!()
    }
    assert_eq!(b3.len(), 2);

    for stmt in func.statements(..) {
        match &*stmt {
            &Statement::Expression {
                result: Variable { ref name, .. },
                ..
            } => {
                assert!(func
                    .names
                    .value(*name)
                    .unwrap()
                    .base()
                    .chars()
                    .all(|x| x.is_uppercase()));
            }
            _ => {}
        }
    }
}

/*
 * (B0)
 * 0:  Mi1  ; mov i 1
 * 3:  Cfi0 ; cmp f i 0
 * 7:  Bf18 ; br f (B2)
 *
 * (B1)
 *          ; test
 * 11: Aii3 ; add i i 3
 * 15: J22  ; jmp (B3)
 *
 * (B2)
 * 18: Ai2i ; add i 2 i
 *
 * (B3)
 * 22: Ms3  ; mov s 3
 * 25: R    ; ret
 */
#[test]
fn rewrite_prepend_mnemonic_unpacked() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Ai2iMs3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
    let mut b0_idx = None;
    let mut b1_idx = None;
    let mut b2_idx = None;
    let mut b3_idx = None;

    for (idx, bb) in func.basic_blocks() {
        if bb.area.start == 0 {
            b0_idx = Some(idx);
        } else if bb.area.start == 11 {
            b1_idx = Some(idx);
        } else if bb.area.start == 18 {
            b2_idx = Some(idx);
        } else if bb.area.start == 22 {
            b3_idx = Some(idx);
        } else {
            unreachable!()
        }
    }

    assert!(
        b0_idx.is_some()
            && b1_idx.is_some()
            && b2_idx.is_some()
            && b3_idx.is_some()
    );

    let x = func.names.insert(&Name::new("x".into(), None));
    let stmts = vec![
        Statement::Expression {
            op: Operation::And(
                Value::val(42, 32).unwrap(),
                Value::var(x, 32).unwrap(),
            ),
            result: Variable::new(x, 32).unwrap(),
        },
        Statement::Expression {
            op: Operation::Subtract(
                Value::val(42, 32).unwrap(),
                Value::var(x, 32).unwrap(),
            ),
            result: Variable::new(x, 32).unwrap(),
        },
    ];
    debug!("{:?}", func);

    {
        debug!("pre-read bb 0");
        let b0 = func.basic_block(b0_idx.unwrap());
        debug!("b0: {:?}", b0);

        debug!("pre-read bb 1");
        let b1 = func.basic_block(b1_idx.unwrap());
        debug!("b1: {:?}", b1);
    }

    let test = func.strings.insert(&"test".into());
    let _ = func
        .insert_mnemonic(b1_idx.unwrap(), 1, test, SmallVec::default(), stmts)
        .unwrap();
    debug!("{:?}", func);

    debug!("read bb 0");
    let b0 = func.statements(b0_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(b0.len(), 2);
    debug!("b0: {:?}", b0);
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(_)),
        ..
    } = &*b0[0]
    {
    } else {
        unreachable!()
    }
    if let &Statement::Expression {
        op:
            Operation::LessOrEqualUnsigned(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b0[1]
    {
    } else {
        unreachable!()
    }
    let mne0 = func.mnemonics(b0_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(mne0.len(), 3);
    let b0 = func.basic_block(b0_idx.unwrap());
    debug!("b0: {:?}", b0);
    assert_eq!(func.mnemonic(b0.mnemonics.start).area.start, 0);
    assert_eq!(
        func.mnemonic(MnemonicIndex::new(b0.mnemonics.start.index() + 1))
            .area
            .start,
        3
    );
    assert_eq!(
        func.mnemonic(MnemonicIndex::new(b0.mnemonics.start.index() + 2))
            .area
            .start,
        7
    );

    debug!("read bb 1");
    let b1 = func.statements(b1_idx.unwrap()).collect::<Vec<_>>();
    debug!("b1: {:?}", b1);
    assert_eq!(b1.len(), 3);
    if let &Statement::Expression {
        op: Operation::Add(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b1[0]
    {
    } else {
        unreachable!()
    }
    if let &Statement::Expression {
        op: Operation::And(Value::Constant(_), Value::Variable(_)),
        ..
    } = &*b1[1]
    {
    } else {
        unreachable!()
    }
    if let &Statement::Expression {
        op: Operation::Subtract(Value::Constant(_), Value::Variable(_)),
        ..
    } = &*b1[2]
    {
    } else {
        unreachable!()
    }
    let mne1 = func.mnemonics(b1_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(mne1.len(), 3);
    let b1 = func.basic_block(b1_idx.unwrap());
    debug!("b1: {:?}", b1);
    assert_eq!(func.mnemonic(b1.mnemonics.start).area.start, 11);
    assert_eq!(
        func.mnemonic(MnemonicIndex::new(b1.mnemonics.start.index() + 1))
            .area
            .start,
        15
    );
    assert_eq!(
        func.mnemonic(MnemonicIndex::new(b1.mnemonics.start.index() + 2))
            .area
            .start,
        15
    );

    debug!("read bb 2");
    let b2 = func.statements(b2_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(b2.len(), 1);
    if let &Statement::Expression {
        op: Operation::Add(Value::Constant(_), Value::Variable(_)),
        ..
    } = &*b2[0]
    {
    } else {
        unreachable!()
    }
    let mne2 = func.mnemonics(b2_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(mne2.len(), 1);
    let b2 = func.basic_block(b2_idx.unwrap());
    debug!("b2: {:?}", b2);

    debug!("read bb 3");
    let b3 = func.statements(b3_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(b3.len(), 2);
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(_)),
        ..
    } = &*b3[0]
    {
    } else {
        unreachable!()
    }
    let mne3 = func.mnemonics(b3_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(mne3.len(), 2);
    let b3 = func.basic_block(b3_idx.unwrap());
    debug!("b3: {:?}", b3);
}

#[test]
fn rewrite_remove_mnemonic() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
    let mut b0_idx = None;
    let mut b1_idx = None;
    let mut b2_idx = None;
    let mut b3_idx = None;

    for (idx, bb) in func.basic_blocks() {
        if bb.area.start == 0 {
            b0_idx = Some(idx);
        } else if bb.area.start == 11 {
            b1_idx = Some(idx);
        } else if bb.area.start == 18 {
            b2_idx = Some(idx);
        } else if bb.area.start == 22 {
            b3_idx = Some(idx);
        } else {
            unreachable!()
        }
    }

    assert!(
        b0_idx.is_some()
            && b1_idx.is_some()
            && b2_idx.is_some()
            && b3_idx.is_some()
    );

    debug!("{:?}", func);
    let _ = func.drop_first_mnemonic(b1_idx.unwrap()).unwrap();
    debug!("{:?}", func);

    let b0 = func.statements(b0_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(b0.len(), 2);
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(_)),
        ..
    } = &*b0[0]
    {
    } else {
        unreachable!()
    }
    if let &Statement::Expression {
        op:
            Operation::LessOrEqualUnsigned(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b0[1]
    {
    } else {
        unreachable!()
    }

    let b1 = func.statements(b1_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(b1.len(), 0);

    let b2 = func.statements(b2_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(b2.len(), 1);
    if let &Statement::Expression {
        op: Operation::Add(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b2[0]
    {
    } else {
        unreachable!()
    }

    let b3 = func.statements(b3_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(b3.len(), 2);
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(_)),
        ..
    } = &*b3[0]
    {
    } else {
        unreachable!()
    }
}

#[test]
fn remove_mnemonic_mid() {
    let _ = simple_logger::init();
    let data = b"Mi1Mi2Cfi0Bf21Aii3J25Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
    let mut b0_idx = None;
    let mut b1_idx = None;
    let mut b2_idx = None;
    let mut b3_idx = None;

    for (idx, bb) in func.basic_blocks() {
        if bb.area.start == 0 {
            b0_idx = Some(idx);
        } else if bb.area.start == 14 {
            b1_idx = Some(idx);
        } else if bb.area.start == 21 {
            b2_idx = Some(idx);
        } else if bb.area.start == 25 {
            b3_idx = Some(idx);
        } else {
            unreachable!()
        }
    }

    assert!(
        b0_idx.is_some()
            && b1_idx.is_some()
            && b2_idx.is_some()
            && b3_idx.is_some()
    );

    debug!("{:?}", func);
    let _ = func.remove_mnemonic(MnemonicIndex::new(1)).unwrap();
    debug!("{:?}", func);

    let b0 = func.statements(b0_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(func.mnemonics(b0_idx.unwrap()).count(), 3);
    assert_eq!(b0.len(), 2);
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(Constant { value: 1, .. })),
        ..
    } = &*b0[0]
    {
    } else {
        unreachable!()
    }
    if let &Statement::Expression {
        op:
            Operation::LessOrEqualUnsigned(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b0[1]
    {
    } else {
        unreachable!()
    }

    let b1 = func.statements(b1_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(b1.len(), 1);
    if let &Statement::Expression {
        op: Operation::Add(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b1[0]
    {
    } else {
        unreachable!()
    }

    let b2 = func.statements(b2_idx.unwrap()).collect::<Vec<_>>();

    assert_eq!(b2.len(), 1);
    if let &Statement::Expression {
        op: Operation::Add(Value::Variable(_), Value::Constant(_)),
        ..
    } = &*b2[0]
    {
    } else {
        unreachable!()
    }

    let b3 = func.statements(b3_idx.unwrap()).collect::<Vec<_>>();
    assert_eq!(b3.len(), 2);
    if let &Statement::Expression {
        op: Operation::Move(Value::Constant(_)),
        ..
    } = &*b3[0]
    {
    } else {
        unreachable!()
    }
}

/*
 * (B0)
 * 0:  Mi1  ; mov i 1
 * 3:  Cfi0 ; cmp f i 0
 * 7:  Bf18 ; br f (B2)
 *
 * (B1)
 * 11: Aii3 ; add i i 3
 * 15: J22  ; jmp (B3)
 *
 * (B2)
 * 18: Aii3 ; add i i 3
 *
 * (B3)
 * 22: Ms3  ; mov s 3
 * 25: R    ; ret
 */
#[test]
fn delete_single_dead_end() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

    func.retain_basic_blocks(|func, bb| {
        let bb = func.basic_block(bb);
        bb.area.start != 22
    })
    .unwrap();

    assert_eq!(func.cflow_graph().node_count(), 3);
}

#[test]
fn delete_middle() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

    func.retain_basic_blocks(|func, bb| {
        let bb = func.basic_block(bb);
        bb.area.start != 11
    })
    .unwrap();

    assert_eq!(func.cflow_graph().node_count(), 3);
}

#[test]
fn delete_multiple() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

    func.retain_basic_blocks(|func, bb| {
        let bb = func.basic_block(bb);
        bb.area.start == 11
    })
    .unwrap();

    assert_eq!(func.cflow_graph().node_count(), 2);
}

#[test]
fn delete_all() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

    func.retain_basic_blocks(|_, _| false).unwrap();

    assert_eq!(func.cflow_graph().node_count(), 1);
}

#[test]
fn delete_none() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

    func.retain_basic_blocks(|_, _| true).unwrap();

    assert_eq!(func.cflow_graph().node_count(), 4);
}

#[test]
fn delete_twice() {
    let _ = simple_logger::init();
    let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
    let reg = Region::from_buf("", 16, data, 0, None);
    let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

    func.retain_basic_blocks(|func, bb| {
        let bb = func.basic_block(bb);
        bb.area.start != 11
    })
    .unwrap();

    func.retain_basic_blocks(|func, bb| {
        let bb = func.basic_block(bb);
        bb.area.start != 11
    })
    .unwrap();

    assert_eq!(func.cflow_graph().node_count(), 3);
}
