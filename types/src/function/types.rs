// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use std::collections::HashMap;

use smallvec::SmallVec;

use crate::{
    Area, BasicBlock, BasicBlockIndex, Guard, Mnemonic, MnemonicIndex,
    Statement, Target, Variable,
};

pub type CtrlTransfers = HashMap<u64, SmallVec<[(AddrOrVar, Guard); 2]>>;

/// Signaling for `Statement::rewrite`.
pub enum RewriteControl {
    /// Continue with the next IL statement.
    Continue,
    /// Stop iteration.
    Break,
}

#[derive(Debug)]
pub enum AddrOrVar {
    Address(u64),
    Variable(Variable),
}

/// Iterator over all indirect, unresolved jumps/branches.
//pub struct IndirectJumps<'a> {
//    pub graph: &'a Graph<CfgNode, Guard>,
//    pub iterator: NodeIndices<u32>,
//}
//
//impl<'a> Iterator for IndirectJumps<'a> {
//    type Item = Variable;
//
//    fn next(&mut self) -> Option<Variable> {
//        while let Some(idx) = self.iterator.next() {
//            match self.graph.node_weight(idx) {
//                Some(&CfgNode::Value(Value::Variable(ref v))) => {
//                    return Some(v.clone());
//                }
//                _ => {}
//            }
//        }
//
//        None
//    }
//}

#[derive(Debug)]
pub enum Code {
    BasicBlock(BasicBlock),
    Mnemonic(Mnemonic),
    Statement {
        basic_block: BasicBlockIndex,
        mnemonic: MnemonicIndex,
        statement: u16,
        il: Statement,
    },
}

impl Code {
    pub fn is_basic_block(&self) -> bool {
        match self {
            Code::BasicBlock(_) => true,
            Code::Mnemonic(_) => false,
            Code::Statement { .. } => false,
        }
    }

    pub fn is_mnemonic(&self) -> bool {
        match self {
            Code::BasicBlock(_) => false,
            Code::Mnemonic(_) => true,
            Code::Statement { .. } => false,
        }
    }

    pub fn basic_block(&self) -> BasicBlockIndex {
        match self {
            &Code::BasicBlock(BasicBlock { basic_block, .. }) => basic_block,
            &Code::Mnemonic(Mnemonic { basic_block, .. }) => basic_block,
            &Code::Statement { basic_block, .. } => basic_block,
        }
    }

    pub fn mnemonic(&self) -> MnemonicIndex {
        match self {
            &Code::BasicBlock(BasicBlock { mnemonic, .. }) => mnemonic,
            &Code::Mnemonic(Mnemonic { mnemonic, .. }) => mnemonic,
            &Code::Statement { mnemonic, .. } => mnemonic,
        }
    }

    pub fn statement(&self) -> u16 {
        match self {
            &Code::BasicBlock(BasicBlock { statement, .. }) => statement,
            &Code::Mnemonic(Mnemonic { statement, .. }) => statement,
            &Code::Statement { statement, .. } => statement,
        }
    }

    pub fn area<'a>(&'a self) -> Option<&'a Area> {
        match self {
            &Code::BasicBlock(BasicBlock { ref area, .. }) => Some(area),
            &Code::Mnemonic(Mnemonic { ref area, .. }) => Some(area),
            &Code::Statement { .. } => None,
        }
    }
}
