p8n-types
=========

Basic types for representing binary programs. Part of [Panopticon](https://gitlab.com/p8n/panopticon). The [documentation](https://docs.rs/p8n-types) is hosted on `docs.rs`.

Usage
-----

```toml
# Cargo.toml
[dependencies]
p8n-types = "2.0.1"
```

License
-------

This project is licensed under

 * GNU Lesser General Public License, Version 2.1 or later, ([LICENSE](LICENSE) or
   https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html.
