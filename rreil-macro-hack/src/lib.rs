extern crate proc_macro;
extern crate proc_macro_hack;

use proc_macro::{Delimiter, TokenStream, TokenTree};
use proc_macro_hack::proc_macro_hack;

#[derive(Debug)]
enum Arg {
    Expression(String),
    Constant(String, String),
    Variable(String, String),
    Undefined,
}

const NS: &'static str = "::p8n_types";
const SIMPLE_OPS: &'static [&'static str] = &[
    "add", "sub", "mul", "div", "divs", "shl", "shr", "shrs", "mod", "and",
    "xor", "or", "cmpeq", "cmpleu", "cmples", "cmpltu", "cmplts",
];

#[proc_macro_hack]
pub fn rreil(item: TokenStream) -> TokenStream {
    let mut ret = String::default();
    let mut strm = item.into_iter();

    while let Some(tt) = strm.next() {
        match tt {
            TokenTree::Ident(i) => {
                let op = i.to_string();
                if SIMPLE_OPS.iter().find(|x| x == &&op).is_some() {
                    let op = decode_op(&op);
                    let lv = decode_rvalue(&mut strm);
                    decode_comma(&mut strm);
                    let arg1 = decode_rvalue(&mut strm);
                    decode_comma(&mut strm);
                    let arg2 = decode_rvalue(&mut strm);
                    finalize_expr_line(&mut ret, op, lv, arg1, arg2);
                } else if op == "sel" {
                    let start = decode_number('/', &mut strm);
                    let w = decode_number('/', &mut strm);
                    let lv = decode_rvalue(&mut strm);
                    decode_comma(&mut strm);
                    let arg1 = decode_rvalue(&mut strm);
                    finalize_sel_line(&mut ret, start, w, lv, arg1);
                } else if op == "load" || op == "store" {
                    let reg = decode_delimited('/', &mut strm);
                    let endian = decode_delimited('/', &mut strm);
                    let w = decode_number('/', &mut strm);
                    let lv = decode_rvalue(&mut strm);
                    decode_comma(&mut strm);
                    let arg1 = decode_rvalue(&mut strm);
                    finalize_mem_line(
                        &mut ret,
                        op == "load",
                        reg,
                        endian,
                        w,
                        lv,
                        arg1,
                    );
                } else if op == "zext" || op == "sext" {
                    let w = decode_number('/', &mut strm);
                    let lv = decode_rvalue(&mut strm);
                    decode_comma(&mut strm);
                    let arg1 = decode_rvalue(&mut strm);
                    finalize_ext_line(&mut ret, op == "zext", w, lv, arg1);
                } else if op == "mov" {
                    let lv = decode_rvalue(&mut strm);
                    decode_comma(&mut strm);
                    let arg1 = decode_rvalue(&mut strm);
                    finalize_mov_line(&mut ret, lv, arg1);
                } else if op == "ret" {
                    finalize_ret_line(&mut ret);
                } else if op == "call" {
                    let lv = decode_rvalue(&mut strm);
                    finalize_call_line(&mut ret, lv);
                } else {
                    panic!(
                        "{:?}: {} is not a valid rreil operation",
                        i.span(),
                        op
                    );
                }
            }
            tt => panic!("cannot process line starting with: {:?}", tt),
        }
    }

    format!(
        "{{ let ret: {}::Result<Vec<{}::Statement>> = Ok(vec![{}]); ret }}",
        NS, NS, ret
    )
    .parse()
    .unwrap()
}

#[proc_macro_hack]
pub fn rreil_var(item: TokenStream) -> TokenStream {
    let mut strm = item.into_iter();

    encode_lvalue(decode_rvalue(&mut strm)).parse().unwrap()
}

#[proc_macro_hack]
pub fn rreil_val(item: TokenStream) -> TokenStream {
    let mut strm = item.into_iter();

    encode_rvalue(decode_rvalue(&mut strm)).parse().unwrap()
}

fn decode_op(s: &str) -> String {
    match s {
        "add" => "Add".to_string(),
        "sub" => "Subtract".to_string(),
        "mul" => "Multiply".to_string(),
        "div" => "DivideUnsigned".to_string(),
        "divs" => "DivideSigned".to_string(),
        "shl" => "ShiftLeft".to_string(),
        "shr" => "ShiftRightUnsigned".to_string(),
        "shrs" => "ShiftRightSigned".to_string(),
        "mod" => "Modulo".to_string(),
        "and" => "And".to_string(),
        "xor" => "ExclusiveOr".to_string(),
        "or" => "InclusiveOr".to_string(),
        "cmpeq" => "Equal".to_string(),
        "cmpleu" => "LessOrEqualUnsigned".to_string(),
        "cmples" => "LessOrEqualSigned".to_string(),
        "cmpltu" => "LessUnsigned".to_string(),
        "cmplts" => "LessSigned".to_string(),
        _ => {
            panic!("not a operation: {}", s);
        }
    }
}

fn decode_comma<I>(strm: &mut I)
where
    I: Iterator<Item = TokenTree>,
{
    match strm.next() {
        Some(TokenTree::Punct(ref p)) if p.as_char() == ',' => (),
        _ => {
            panic!("expected comma");
        }
    }
}

fn decode_width<I>(strm: &mut I) -> String
where
    I: Iterator<Item = TokenTree>,
{
    decode_number(':', strm)
}

fn decode_delimited<I>(ch: char, strm: &mut I) -> String
where
    I: Iterator<Item = TokenTree>,
{
    let colon = strm.next();
    let id = strm.next();

    match (colon, id) {
        (Some(TokenTree::Punct(p)), Some(TokenTree::Ident(n))) => {
            if p.as_char() == ch {
                n.to_string()
            } else {
                panic!("expecting '{}', got {:?}", ch, p.as_char());
            }
        }
        (colon, id) => panic!("not a ident: {:?}{:?}", colon, id),
    }
}

fn decode_number<I>(ch: char, strm: &mut I) -> String
where
    I: Iterator<Item = TokenTree>,
{
    let colon = strm.next();
    let num = strm.next();

    match (colon, num) {
        (Some(TokenTree::Punct(p)), Some(TokenTree::Literal(n))) => {
            if p.as_char() == ch {
                n.to_string()
            } else {
                panic!("expecting '{}', got {:?}", ch, p.as_char());
            }
        }
        (Some(TokenTree::Punct(p)), Some(TokenTree::Ident(n))) => {
            if p.as_char() == ch {
                n.to_string()
            } else {
                panic!("expecting '{}', got {:?}", ch, p.as_char());
            }
        }
        (colon, num) => panic!("not a number: {:?}{:?}", colon, num),
    }
}

fn decode_rvalue<I>(strm: &mut I) -> Arg
where
    I: Iterator<Item = TokenTree>,
{
    match strm.next() {
        Some(TokenTree::Ident(i)) => {
            Arg::Variable(i.to_string(), decode_width(strm))
        }
        Some(TokenTree::Punct(ref i)) if i.as_char() == '?' => Arg::Undefined,
        Some(TokenTree::Group(g)) => match g.delimiter() {
            Delimiter::Parenthesis => Arg::Expression(g.stream().to_string()),
            Delimiter::Bracket => {
                Arg::Constant(g.stream().to_string(), decode_width(strm))
            }
            _ => panic!("not a a value: {:?}", g),
        },
        tt => panic!("not a a value: {:?}", tt),
    }
}

fn encode_rvalue(arg: Arg) -> String {
    match arg {
        Arg::Constant(val, w) => format!("{}::Value::val({} as u64, {}).unwrap()", NS, val, w),
        Arg::Expression(ex) => format!("{}::Value::from({}.clone())", NS, ex),
        Arg::Variable(var, w) => format!(r#"{}::Value::var({}::Name::new({}::Atom::from("{}"), None), {}).unwrap()"#, NS, NS, NS, var, w),
        Arg::Undefined => format!(r#"{}::Value::Undefined"#, NS),
    }
}

fn encode_lvalue(arg: Arg) -> String {
    match arg {
        Arg::Constant(_, _) => panic!("cannot use constant as result"),
        Arg::Expression(ex) => format!("{}.clone()", ex),
        Arg::Variable(var, w) => format!(r#"{}::Variable::new({}::Name::new({}::Atom::from("{}"), None), {}).unwrap()"#, NS, NS, NS, var, w),
        Arg::Undefined => panic!("cannot use undefined as result"),
    }
}

fn finalize_expr_line(
    lines: &mut String,
    op: String,
    lv: Arg,
    arg1: Arg,
    arg2: Arg,
) {
    let left = encode_rvalue(arg1);
    let right = encode_rvalue(arg2);
    let lv = encode_lvalue(lv);
    let ret = format!("{}::Statement::Expression{{ op: {}::Operation::{}{{ left: {}, right: {} }}, result: {} }}, ", NS, NS, op, left, right, lv);

    lines.push_str(&ret);
}

fn finalize_mov_line(lines: &mut String, lv: Arg, arg1: Arg) {
    let val = encode_rvalue(arg1);
    let lv = encode_lvalue(lv);
    let ret = format!("{}::Statement::Expression{{ op: {}::Operation::Move{{ value: {} }}, result: {} }}, ", NS, NS, val, lv);

    lines.push_str(&ret);
}

fn finalize_sel_line(
    lines: &mut String,
    start: String,
    width: String,
    lv: Arg,
    arg1: Arg,
) {
    let val = encode_rvalue(arg1);
    let lv = encode_lvalue(lv);
    let ret = format!("{}::Statement::Expression{{ op: {}::Operation::Select{{ start: {} as u16, bits: {} as u16, value: {} }}, result: {} }}, ", NS, NS, start, width, val, lv);

    lines.push_str(&ret);
}

fn finalize_ext_line(
    lines: &mut String,
    zext: bool,
    width: String,
    lv: Arg,
    arg1: Arg,
) {
    let val = encode_rvalue(arg1);
    let lv = encode_lvalue(lv);
    let op = if zext { "ZeroExtend" } else { "SignExtend" };
    let ret = format!("{}::Statement::Expression{{ op: {}::Operation::{}{{ bits: {} as u16, value: {} }}, result: {} }}, ", NS, NS, op, width, val, lv);

    lines.push_str(&ret);
}

fn finalize_mem_line(
    lines: &mut String,
    load: bool,
    reg: String,
    endian: String,
    width: String,
    lv: Arg,
    arg1: Arg,
) {
    let val = encode_rvalue(arg1);
    let lv = encode_lvalue(lv);
    let e = match endian.as_str() {
        "le" => format!("{}::Endianess::Little", NS),
        "be" => format!("{}::Endianess::Big", NS),
        e => {
            panic!("'{}' is not an endianess. Use le or be", e);
        }
    };
    if load {
        let ret = format!(r#"{}::Statement::Expression{{ op: {}::Operation::Load{{ segment: {}::Segment::new("{}", None), endianess: {}, bytes: {} as u8, address: {} }}, result: {} }}, "#, NS, NS, NS, reg, e, width, val, lv);
        lines.push_str(&ret);
    } else {
        let ret = format!(r#"{}::Statement::Memory{{ op: {}::MemoryOperation::Store{{ segment: {}::Segment::new("{}", None), endianess: {}, bytes: {} as u8, address: {}::Value::from({}), value: {} }}, result: {}::Segment::new("{}", None) }}, "#, NS, NS, NS, reg, e, width, NS, lv, val, NS, reg);
        lines.push_str(&ret);
    }
}

fn finalize_call_line(lines: &mut String, arg: Arg) {
    let val = encode_lvalue(arg);
    let ret = format!(r#"{}::Statement::Flow{{ op: {}::FlowOperation::IndirectCall{{ target: {} }} }}, "#, NS, NS, val);

    lines.push_str(&ret);
}

fn finalize_ret_line(lines: &mut String) {
    let ret = format!(
        r#"{}::Statement::Flow{{ op: {}::FlowOperation::Return }}, "#,
        NS, NS
    );

    lines.push_str(&ret);
}
