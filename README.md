![Panopticon](https://raw.githubusercontent.com/das-labor/panopticon/master/logo.png)

Panopticon - Libre Binary Program Analysis
==========================================

Panopticon is a library for binary program analysis. It's able to:

 * Disassemble [AMD64/x86](https://gitlab.com/p8n/amd64), [AVR](https://gitlab.com/p8n/avr), [MOS 6502](https://gitlab.com/p8n/mos6502) and [MIPS (WIP)](https://gitlab.com/p8n/mips).
 * Open PE and ELF files.
 * Translate code to [RREIL](https://bitbucket.org/mihaila/bindead/wiki/Introduction%20to%20RREIL), a reverse engineering focused intermediate language in [Single Static Assignment](http://www.cs.utexas.edu/~pingali/CS380C/2010/papers/ssaCytron.pdf) form (including memory operations).
 * Construct control flow graphs and compute liveness information for each basic block.
 * Interpolate stack pointer values using [Guilfanov's method](http://www.hexblog.com/?p=42).
 * Compute function summaries and substitute call sites with them.
 * Run [Abstract Interpretation](https://wiki.mozilla.org/Abstract_Interpretation) analysis with fixed cardinality sets, strided intervals and Value Sets.
 * Propagate constants, recover local variables and resolve indirect jumps.

The project is split across multiple repositories and crates:

 * `p8n-types`: Basic types like `Function`. Basis for all other crates.
 * `p8n-data-flow`: Data flow algorithms. Liveness, Guilfanov's method and SSA form.
 * `p8n-anstract-interp`: Abstract Interpretation machine and Abstract (Co-fibered) Domains.
 * `p8n-analysis`: High-level crate for running common analysis steps like computing function summaries and resolving indirect jumps.
 * [`p8n-amd64`](https://gitlab.com/p8n/amd64), [`p8n-avr`](https://gitlab.com/p8n/avr), [`p8n-mos6502`](https://gitlab.com/p8n/mos6502), [`p8n-mips`](https://gitlab.com/p8n/mips): Disassembler.

Panopticon is the basis for the [Verso](https://gitlab.com/p8n/verso) disassembler.

Usage
-----

```toml
# Cargo.toml
[dependencies]
p8n-types = "1.0.0"
```

Contact
-------

 * IRC: #panopticon on Freenode.
 * Twitter: [```@panopticon_re```](https://twitter.com/@panopticon_re)
 * Gitter: [panopticon](https://gitter.im/das-labor/panopticon)

License
-------

All crates in this repository are licensed under

 * GNU Lesser General Public License, Version 2.1 or later, ([LICENSE](p8n-types/LICENSE) or
   https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html.
