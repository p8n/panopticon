// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use std::default::Default;
use std::ops::Range;
use std::fmt::Debug;
use types::{Value, Variable, Statement, Function, Result, NameRef, Segment, Constraint};
use Metadata;

/// Abstract Domain. Models both under- and over-approximation.
pub trait Avalue: Debug + Default + From<Value> + Clone + PartialEq + Eq + Sized {
    /// Execute the abstract version of the operation, yielding the result.
    fn execute(&Statement, &Metadata<Self>) -> Result<Self>;
    /// Widens `self` with the argument.
    fn widen(&self, direction: &Self) -> Self;
    /// Narrows `self` with the argument.
    fn narrow(&self, constraint: &Constraint) -> Self;
    /// Computes the lowest upper bound of self and the argument.
    fn combine(&self, &Self) -> Self;
    /// Returns true if `self` <= `other`.
    fn is_better(&self, other: &Self) -> bool;
}

pub trait Amemory<A>: Clone + Debug + Default + Sized where A: Avalue {
    fn combine(&self, other: &Self) -> Self;
    fn load(&self, address: &A) -> Result<A>;
    fn store(&self, address: &A, value: &A) -> Result<Self>;
}

#[derive(Debug)]
pub struct Approximation<A: Avalue, M: Amemory<A>> {
    values: Vec<A>,
    memory: Vec<M>,
    basic_blocks: Vec<(Range<usize>,u64)>,
    statement_position: usize,
}

impl<A: Avalue,M: Amemory<A>> Approximation<A,M> {
    pub fn new<O: Into<Option<M>>>(func: &Function,memory: O) -> Self {
        use types::IntoStatementRange;

        let m = memory.into().unwrap_or_default();
        let mut bbs = func.basic_blocks().map(|(idx,bb)| {
            let rgn = idx.clone().into_statement_range(func);
            (rgn,bb.area.start)
        }).collect::<Vec<_>>();

        bbs.sort_by_key(|x| x.0.end);

        Approximation{
            values: vec![A::default(); func.names.len()],
            memory: vec![m; func.segments.len()],
            basic_blocks: bbs,
            statement_position: 0,
        }
    }

    pub fn set(&mut self, name: NameRef, value: &A) -> bool {
        let cur = &mut self.values[name.index()];
        let new = value.combine(&*cur);
        let ret = new.is_better(&*cur);

        debug!("set {:?} to {:?}",name,new);

        *cur = new;
        ret
    }

    pub fn get<'a>(&'a self, name: NameRef) -> &'a A {
        &self.values[name.index()]
    }

    pub fn get_segment<'a>(&'a self, segment: &Segment) -> &'a M {
        &self.memory[segment.name.index()]
    }

    pub fn set_segment<'a>(&'a mut self, segment: &Segment, mem: M) {
        self.memory[segment.name.index()] = mem;
    }

    pub fn set_position(&mut self, pos: usize) {
        self.statement_position = pos;
    }

    pub fn position(&self) -> usize {
        self.statement_position
    }

    pub fn basic_blocks<'a>(&'a self) -> &'a Vec<(Range<usize>,u64)> {
        &self.basic_blocks
    }
}

pub fn approximate<A: Avalue,M: Amemory<A>,O: Into<Option<M>> + Sized>(func: &Function, mem: O) -> Result<Approximation<A,M>> {
    use types::Operation::*;
    use types::MemoryOperation::*;

    let mut approx = Approximation::<A,M>::new(func,mem);
    let mut fixedpoint = false;

    while !fixedpoint {
        fixedpoint = true;

        for (pos,stmt) in func.statements(..).enumerate() {
            approx.set_position(pos);

            debug!("{:?}",stmt);

            match &*stmt {
                &Statement::Expression{ op: Load(ref segment,_,_,ref address), result: Variable{ name,.. } } => {
                    let avalue = {
                        let mem = approx.get_segment(segment);
                        mem.load(&*approx.value(address))?
                    };

                    fixedpoint &= !approx.set(name,&avalue);
                }
                &Statement::Expression{ result: Variable{ name,.. },.. } => {
                    let avalue = A::execute(&stmt,&approx)?;
                    debug!("{:?}: {:?}",avalue,stmt);
                    fixedpoint &= !approx.set(name,&avalue);
                }

                &Statement::Memory{ op: Store{ ref segment, ref address, ref value,.. }, ref result } => {
                    let new = {
                        let mem = approx.get_segment(segment);
                        let addr = approx.value(address);
                        let val = approx.value(value);
                        mem.store(&*addr,&*val)?
                    };

                    approx.set_segment(result,new);
                }
                &Statement::Memory{ op: MemoryPhi(Some(ref a),ref b,ref c), ref result,.. } => {
                    let mut x = approx.get_segment(a).clone();

                    if let &Some(ref b) = b {
                        x = x.combine(approx.get_segment(b));
                    }

                    if let &Some(ref c) = c {
                        x = x.combine(approx.get_segment(c));
                    }

                    approx.set_segment(result,x);
                }
                &Statement::Memory{ op: MemoryPhi(_,_,_),.. } => { /* skip */ }
                &Statement::Memory{ op: Allocate{ .. },.. } => { /* skip */ }

                &Statement::Flow{ .. } => { /* skip */ }
            }
        }
    }

    Ok(approx)
}

#[cfg(test)]
mod tests {
    use super::*;
    use {NoMemory,Sign};
    use types::{Region, Name, TestArch, UUID};
    use dflow::rewrite_to_ssa;
    use simple_logger;

    /*
     * 0:  Mx0
     * 3:  Mn1
     *
     * 6:  Cfn9
     * 10: Bf24
     * 14: Axxn
     * 18: Ann1
     * 22: J6
     *
     * 24: R
     */
    #[test]
    fn signedness_analysis() {
        let _ = simple_logger::init();
        let data = b"Mx0Mn1Cfn9Bf24AxxnAnn1J6R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let nam_x0 = func.names.insert(&Name::new("x".into(),0));
        let nam_x1 = func.names.insert(&Name::new("x".into(),1));
        let nam_x2 = func.names.insert(&Name::new("x".into(),2));
        let nam_n0 = func.names.insert(&Name::new("n".into(),0));
        let nam_n1 = func.names.insert(&Name::new("n".into(),1));
        let nam_n2 = func.names.insert(&Name::new("n".into(),2));

        assert!(rewrite_to_ssa(&mut func).is_ok());

        let vals = approximate::<Sign,NoMemory<_>,_>(&func,None).unwrap();
        debug!("{:?}",vals);

        assert_eq!(vals.get(nam_x0), &Sign::Zero);
        assert_eq!(vals.get(nam_x1), &Sign::Join);
        assert_eq!(vals.get(nam_x2), &Sign::Join);
        assert_eq!(vals.get(nam_n0), &Sign::Positive);
        assert_eq!(vals.get(nam_n1), &Sign::Positive);
        assert_eq!(vals.get(nam_n2), &Sign::Positive);
    }

    /*
     * 0:  Mx0
     * 3:  Mn1
     *
     * 6:  Cfn9
     * 10: Bf27
     * 14: Axxn
     * 18: Ann1
     * 22: S1n
     * 25: J6
     *
     * 27: Lz1
     * 30: R
     */
    #[test]
    fn nomemory_signedness_analysis() {
        let _ = simple_logger::init();
        let data = b"Mx0Mn1Cfn9Bf27AxxnAnn1S1nJ6Lz1R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let nam_x0 = func.names.insert(&Name::new("x".into(),0));
        let nam_x1 = func.names.insert(&Name::new("x".into(),1));
        let nam_x2 = func.names.insert(&Name::new("x".into(),2));
        let nam_n0 = func.names.insert(&Name::new("n".into(),0));
        let nam_n1 = func.names.insert(&Name::new("n".into(),1));
        let nam_n2 = func.names.insert(&Name::new("n".into(),2));
        let nam_z0 = func.names.insert(&Name::new("z".into(),0));

        assert!(rewrite_to_ssa(&mut func).is_ok());

        let vals = approximate::<Sign,NoMemory<_>,_>(&func,None).unwrap();
        debug!("{:?}",vals);

        assert_eq!(vals.get(nam_x0), &Sign::Zero);
        assert_eq!(vals.get(nam_x1), &Sign::Join);
        assert_eq!(vals.get(nam_x2), &Sign::Join);
        assert_eq!(vals.get(nam_n0), &Sign::Positive);
        assert_eq!(vals.get(nam_n1), &Sign::Positive);
        assert_eq!(vals.get(nam_n2), &Sign::Positive);
        assert_eq!(vals.get(nam_z0), &Sign::Join);
    }
}
