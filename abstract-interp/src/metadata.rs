// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use types::{Value,Variable};
use {Avalue,Amemory,Approximation};
use std::borrow::Cow;

#[derive(Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash,Debug)]
pub struct ProgramPoint{
    pub basic_block_address: u64,
    pub statement_position: usize,
}

impl Default for ProgramPoint {
    fn default() -> Self {
        ProgramPoint{
            basic_block_address: 0,
            statement_position: 0,
        }
    }
}

impl ProgramPoint {
    pub fn new(bb: u64, pos: usize) -> Self {
        ProgramPoint{
            basic_block_address: bb,
            statement_position: pos,
        }
    }
}


pub trait Metadata<A: Avalue> {
    fn value<'a>(&'a self, value: &Value) -> Cow<'a,A>;
    fn program_point(&self) -> ProgramPoint;
}

pub struct MetadataMap<'a, A: 'a + Avalue, B: 'a + Avalue,F> where F: Fn(Cow<'a,A>) -> Cow<'a,B> {
    inner: &'a Metadata<A>,
    map: F,
}

impl<'a,A: Avalue,B: Avalue,F: Fn(Cow<'a,A>) -> Cow<'a,B>> MetadataMap<'a,A,B,F> {
    pub fn new(m: &'a Metadata<A>, f: F) -> Self {
        MetadataMap{
            inner: m,
            map: f,
        }
    }
}

impl<'a,A: Avalue,B: Avalue,F: Fn(Cow<'a,A>) -> Cow<'a,B>> Metadata<B> for MetadataMap<'a,A,B,F> {
    fn value<'b>(&'b self, value: &Value) -> Cow<'b,B> {
        (self.map)(self.inner.value(value))
    }

    fn program_point(&self) -> ProgramPoint { self.inner.program_point() }
}


impl<A: Avalue, M: Amemory<A>> Metadata<A> for Approximation<A,M> {
    fn value<'a>(&'a self, value: &Value) -> Cow<'a,A> {
        match value {
            &Value::Undefined => Cow::Owned(A::from(Value::Undefined)),
            &Value::Constant(_) => Cow::Owned(A::from(value.clone())),
            &Value::Variable(Variable{ name,.. }) => Cow::Borrowed(self.get(name)),
        }
    }

    fn program_point(&self) -> ProgramPoint {
        let pos = self.position();
        let x = self.basic_blocks().binary_search_by_key(&pos,|x| {
            x.0.end
        });
        let idx = match x {
            Ok(idx) => idx + 1,
            Err(idx) => idx,
        };
        let rgn = &self.basic_blocks()[idx];

        ProgramPoint::new(rgn.1,pos - rgn.0.start)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use {Sign,NoMemory};
    use types::{TestArch, Region, Function, UUID};
    use simple_logger;

    /*
     * 0:  Mx0
     * 3:  Mn1
     *
     * 6:  Cfn9
     * 10: Bf24
     *
     * 14: Axxn
     * 18: Ann1
     * 22: J6
     *
     * 24: R
     */
    #[test]
    fn program_point_test() {
        let _ = simple_logger::init();
        let data = b"Mx0Mn1Cfn9Bf24AxxnAnn1J6R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert_eq!(func.statements(..).count(), 6);

        let mut vals = Approximation::<Sign,NoMemory<Sign>>::new(&func,None);

        for i in 0..6 {
            vals.set_position(i);

            let pp = vals.program_point();
            match i {
                0 => assert_eq!(ProgramPoint::new(0,0),pp),
                1 => assert_eq!(ProgramPoint::new(0,1),pp),
                2 => assert_eq!(ProgramPoint::new(6,0),pp),
                3 => assert_eq!(ProgramPoint::new(24,0),pp),
                4 => assert_eq!(ProgramPoint::new(14,0),pp),
                5 => assert_eq!(ProgramPoint::new(14,1),pp),
                _ => unreachable!()
            }
        }
    }

    #[test]
    fn program_point_ord() {
        assert_eq!(ProgramPoint::new(1,42),ProgramPoint::new(1,42));
        assert!(ProgramPoint::new(1,42) < ProgramPoint::new(1,43));
        assert!(ProgramPoint::new(1,42) < ProgramPoint::new(2,41));
    }
}
