// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use {Metadata,Avalue};
use types::{Result,Constant,Value,Statement,Constraint};

#[derive(Debug,Clone,PartialEq,Eq)]
pub enum Sign {
    Join,
    Positive,
    Negative,
    Zero,
    Meet,
}

impl Default for Sign {
    fn default() -> Sign { Sign::Meet }
}

impl From<Value> for Sign {
    fn from(v: Value) -> Sign {
        match v {
            Value::Constant(Constant{ value: 0, .. }) => Sign::Zero,
            Value::Constant(Constant{ value, bits }) if value & (1 << (bits - 1)) != 0 => Sign::Negative,
            Value::Constant(Constant{ value, bits }) if value & (1 << (bits - 1)) == 0 => Sign::Positive,
            _ => Sign::Join,
        }
    }
}

impl Avalue for Sign {
    fn execute(stmt: &Statement, approx: &Metadata<Sign>) -> Result<Self> {
        use self::Sign::*;
        use types::Operation::*;
        use types::Statement::*;

        let ret: Sign = match stmt {
            &Expression{ op: Add(ref a, ref b),.. } => {
                match (&*approx.value(a),&*approx.value(b)) {
                    (&Positive, &Positive) => Positive,
                    (&Positive, &Zero)     => Positive,
                    (&Zero,     &Positive) => Positive,
                    (&Negative, &Negative) => Negative,
                    (&Negative, &Zero)     => Negative,
                    (&Zero,     &Negative) => Negative,
                    (&Positive, &Negative) => Join,
                    (&Negative, &Positive) => Join,
                    (_,         &Join)     => Join,
                    (&Join,     _)         => Join,
                    (&Zero,     &Zero)     => Zero,
                    (a,         &Meet)     => a.clone(),
                    (&Meet,     b)         => b.clone(),
                }
            }

            &Expression{ op: Subtract(ref a, ref b),.. } => {
                match (&*approx.value(a),&*approx.value(b)) {
                    (&Positive, &Positive) => Join,
                    (&Positive, &Zero)     => Positive,
                    (&Zero,     &Positive) => Negative,
                    (&Negative, &Negative) => Join,
                    (&Negative, &Zero)     => Negative,
                    (&Zero,     &Negative) => Positive,
                    (&Positive, &Negative) => Positive,
                    (&Negative, &Positive) => Negative,
                    (&Zero,     &Zero)     => Zero,
                    (_,         &Join)     => Join,
                    (&Join,      _)        => Join,
                    (a,         &Meet)     => a.clone(),
                    (&Meet,     b)         => b.clone(),
                }
            }

            &Expression{ op: DivideSigned(ref a, ref b),.. } => {
                match (&*approx.value(a),&*approx.value(b)) {
                    (&Positive, &Positive) => Positive,
                    (&Negative, &Negative) => Positive,
                    (&Positive, &Negative) => Negative,
                    (&Negative, &Positive) => Negative,
                    (_,         &Zero)     => Zero,
                    (&Zero,     _)         => Zero,
                    (_,         &Join)     => Join,
                    (&Join,     _)         => Join,
                    (a,         &Meet)     => a.clone(),
                    (&Meet,     b)         => b.clone(),
                }
            }

            &Expression{ op: Multiply(ref a, ref b),.. } => {
                match (&*approx.value(a),&*approx.value(b)) {
                    (&Positive, &Positive) => Positive,
                    (&Negative, &Negative) => Positive,
                    (&Positive, &Negative) => Negative,
                    (&Negative, &Positive) => Negative,
                    (_,         &Zero)     => Zero,
                    (&Zero,     _)         => Zero,
                    (_,         &Join)     => Join,
                    (&Join,     _)         => Join,
                    (a,         &Meet)     => a.clone(),
                    (&Meet,     b)         => b.clone(),
                }
            }

            &Expression{ op: DivideUnsigned(ref a, ref b),.. } => {
                match (&*approx.value(a),&*approx.value(b)) {
                    (&Positive, &Positive) => Positive,
                    (&Negative, &Negative) => Positive,
                    (&Positive, &Negative) => Negative,
                    (&Negative, &Positive) => Negative,
                    (_,         &Zero)     => Zero,
                    (&Zero,     _)         => Zero,
                    (_,         &Join)     => Join,
                    (&Join,     _)         => Join,
                    (a,         &Meet)     => a.clone(),
                    (&Meet,     b)         => b.clone(),
                }
            }

            &Expression{ op: Modulo(ref a, ref b),.. } => {
                match (&*approx.value(a),&*approx.value(b)) {
                    (&Positive, &Positive) => Positive,
                    (&Negative, &Negative) => Positive,
                    (&Positive, &Negative) => Negative,
                    (&Negative, &Positive) => Negative,
                    (_,         &Zero)     => Zero,
                    (&Zero,     _)         => Zero,
                    (_,         &Join)     => Join,
                    (&Join,     _)         => Join,
                    (a,         &Meet)     => a.clone(),
                    (&Meet,     b)         => b.clone(),
                }
            }

            &Expression{ op: Move(ref a),.. } => (*approx.value(a)).clone(),
            &Expression{ op: ZeroExtend(_, ref a),.. } => {
                let v = &*approx.value(a);
                if v == &Negative {
                    Join.into()
                } else {
                    (*v).clone()
                }
            }
            &Expression{ op: SignExtend(_, ref a),.. } => (*approx.value(a)).clone(),

            &Expression{ op: Phi(ref a@Value::Variable(_),Value::Undefined,Value::Undefined),.. } => {
                (*approx.value(a)).clone()
            }
            &Expression{ op: Phi(ref a@Value::Variable(_),ref b@Value::Variable(_),Value::Undefined),.. } => {
                let a = &*approx.value(a);
                let b = &*approx.value(b);

                a.combine(b)
            }
            &Expression{ op: Phi(ref a@Value::Variable(_),ref b@Value::Variable(_),ref c@Value::Variable(_)),.. } => {
                let a = &*approx.value(a);
                let b = &*approx.value(b);
                let c = &*approx.value(c);

                a.combine(b).combine(c)
            }
            _ => Join.into(),
        };

        Ok(ret)
    }

    fn combine(&self, b: &Self) -> Self {
        match (self, b) {
            (x, y) if x == y => x.clone(),
            (&Sign::Meet, x) => x.clone(),
            (x, &Sign::Meet) => x.clone(),
            _ => Sign::Join,
        }
    }

    fn widen(&self, b: &Self) -> Self {
        if *b == *self {
            self.clone()
        } else {
            Sign::Join
        }
    }

    // Meet < (Positive = Negative = Zero) < Join
    fn is_better(&self, b: &Self) -> bool {
        if self != b {
            match (self, b) {
                (_, &Sign::Meet) => true,
                (&Sign::Join, _) => true,
                _ => false,
            }
        } else {
            false
        }
    }

    fn narrow(&self,_: &Constraint) -> Self {
        self.clone()
    }
}
