// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//! Linus Kaellberg: "Circular Linear Progressions in SWEET"
//! Rathijit Sen & Y. N. Srikant: "Executable Analysis using Abstract Interpretation with Circular Linear Progressions"

use num::{BigInt, BigUint, One, Signed, ToPrimitive, Zero};
use num::bigint::{Sign, ToBigInt};
use num::integer::{div_mod_floor, gcd, lcm};
use {Metadata, Avalue};
use types::{Result,Value,Constant,Statement,Constraint};

use quickcheck::{Arbitrary, Gen};
use std::cmp;
use std::fmt;
use std::fmt::{Debug, Formatter};
use std::result;
use std::collections::HashSet;

#[derive(PartialEq,Eq,Clone,Hash)]
pub enum CircLinearProg {
    Bottom,
    Interval { width: usize, lower: BigInt, upper: BigInt, stride: BigInt },
    Top,
}

impl Default for CircLinearProg {
    fn default() -> Self {
        CircLinearProg::Bottom
    }
}

impl From<Value> for CircLinearProg {
    fn from(v: Value) -> CircLinearProg {
        match v {
            Value::Constant(Constant{ value, bits }) => {
                CircLinearProg::new(bits, value as i64, value as i64, 1).unwrap()
            }
            _ => CircLinearProg::default()
        }
    }
}

impl Avalue for CircLinearProg {
    fn execute(stmt: &Statement, res: &Metadata<Self>) -> Result<Self> {
        use types::Operation::*;
        use types::Statement::*;

        match stmt {
            // EvalOp
            &Expression{ op: Add(ref a, ref b),.. } => Self::add(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: Subtract(ref a, ref b),.. } => Self::subtract(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: Multiply(ref a, ref b),.. } => Self::multiply(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: DivideUnsigned(ref a, ref b),.. } => {
                let a = &*res.value(a);
                let b = &*res.value(b);
                let x = Self::split_unsigned(a)
                    .into_iter()
                    .map(|a| {
                        Self::union_vec(Self::split_unsigned(b).into_iter().map(|b| {
                            let div = Self::divide_signed(&a, &b).ok().unwrap_or_else(|| Self::top());
                            Self::unsigned_wraparound(div).ok().unwrap_or_else(|| Self::top())
                        }).collect()).ok().unwrap_or_else(|| Self::top())
                    })
                    .collect();

                Self::union_vec(x)
            }
            &Expression{ op: DivideSigned(ref a, ref b),.. } => Self::divide_signed(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: Modulo(ref a, ref b),.. } => Self::modulo(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: ShiftLeft(ref a, ref b),.. } => Self::left_shift(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: ShiftRightUnsigned(ref a, ref b),.. } => Self::right_shift_unsigned(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: ShiftRightSigned(ref a, ref b),.. } => Self::right_shift_signed(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: And(ref a, ref b),.. } => Self::and(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: InclusiveOr(ref a, ref b),.. } => Self::or(&*res.value(a).clone(),&*res.value(b).clone()),
            &Expression{ op: ExclusiveOr(ref a, ref b),.. } => Self::xor(&*res.value(a).clone(),&*res.value(b).clone()),

            &Expression{ op: LessOrEqualSigned(_,_),.. } => Ok(Self::top()),
            &Expression{ op: LessOrEqualUnsigned(_,_),.. } => Ok(Self::top()),
            &Expression{ op: LessSigned(_,_),.. } => Ok(Self::top()),
            &Expression{ op: LessUnsigned(_,_),.. } => Ok(Self::top()),
            &Expression{ op: Equal(_,_),.. } => Ok(Self::top()),

            &Expression{ op: ZeroExtend(sz, ref a),.. } => {
                let a = &*res.value(a);
                let a = Self::unsigned_wraparound(a.clone())?;

                if let CircLinearProg::Interval { lower, upper, stride, width } = a {
                    if width <= sz {
                        Ok(CircLinearProg::Interval { lower: lower, upper: upper, stride: stride, width: sz })
                    } else {
                        Ok(CircLinearProg::top())
                    }
                } else {
                    Ok(a)
                }
            }

            &Expression{ op: SignExtend(sz, ref a),.. } => {
                let a = &*res.value(a);
                let a = Self::signed_wraparound(a.clone())?;
                if let CircLinearProg::Interval { lower, upper, stride, width } = a {
                    if width <= sz {
                        Ok(CircLinearProg::Interval { lower: lower, upper: upper, stride: stride, width: sz })
                    } else {
                        Ok(CircLinearProg::top())
                    }
                } else {
                    Ok(a)
                }
            }

            &Expression{ op: Select(off, sz, ref a),.. } => {
                if let &CircLinearProg::Interval { ref width, ref lower, ref upper, ref stride } = &*res.value(a) {
                    let one = Value::val(1,*width)?.into();
                    let off = Value::val(off as u64,*width)?.into();
                    let ret = Self::right_shift_unsigned(&CircLinearProg::from(Value::val(1 << sz, *width)?),&off)?;

                    Self::modulo(
                        &Self::left_shift(
                            &one,
                            &CircLinearProg::Interval {
                                width: *width,
                                lower: lower.clone(),
                                upper: upper.clone(),
                                stride: stride.clone(),
                            },
                            )?,
                            &ret
                        )
                } else {
                    Ok(Self::top())
                }
            }

            &Expression{ op: Move(ref a),.. } => Ok((&*res.value(a)).clone()),
            &Expression{ op: Phi(_,_,_),.. } => Ok(Self::top()),
            &Expression{ op: Assume(ref c,ref a),.. } => Ok(res.value(a).narrow(c)),
            &Expression{ op: Initialize(_, _),.. } => Ok(Self::top()),
            &Expression{ op: Load(_,_,_,_),.. } => Ok(Self::top()),
            &Flow{ .. } => Ok(Self::top()),
            &Memory{ .. } => Ok(Self::top()),
        }
    }

    fn combine(&self, a: &Self) -> Self {
        if *a == *self {
            a.clone()
        } else {
            Self::union(self, a).unwrap_or(CircLinearProg::Top)
        }
    }

    fn widen(&self, _: &Self) -> Self {
        CircLinearProg::top()
    }

    fn is_better(&self, a: &Self) -> bool {
        if self == a {
            false
        } else {
            match (self, a) {
                (&CircLinearProg::Top, _) => true,
                (_, &CircLinearProg::Bottom) => true,
                (&CircLinearProg::Interval { .. }, &CircLinearProg::Interval { .. }) => {
                    Self::intersection(self, a).unwrap() == *a
                }
                _ => false,
            }
        }
    }

    fn narrow(&self, constr: &Constraint) -> Self {
        match (constr,self) {
            (&Constraint::Empty{ .. },_) => CircLinearProg::Bottom,
            (&Constraint::Unsigned{ from, to, bits },a@&CircLinearProg::Interval{ .. }) => {
                let c = CircLinearProg::Interval{
                    width: bits,
                    lower: from.into(),
                    upper: to.into(),
                    stride: BigInt::one(),
                };

                Self::intersection(&c,a).unwrap()
            }
            (&Constraint::Unsigned{ from, to, bits },&CircLinearProg::Top) => {
                CircLinearProg::Interval{
                    width: bits,
                    lower: from.into(),
                    upper: to.into(),
                    stride: BigInt::one(),
                }
            }
            (_,k) => k.clone()
        }
    }
}

impl Arbitrary for CircLinearProg {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        if !g.gen_weighted_bool(10) {
            let (lower, upper) = if g.gen_weighted_bool(10) {
                let bounds = g.gen::<(i16, i16)>();
                (cmp::min(bounds.0, bounds.1), cmp::max(bounds.0, bounds.1))
            } else {
                let bounds = g.gen::<(i8, i8)>();
                (cmp::min(bounds.0, bounds.1) as i16, cmp::max(bounds.0, bounds.1) as i16)
            };

            if g.gen_weighted_bool(2) {
                let stride = if lower == upper {
                    1
                } else {
                    g.gen_range(1, (upper as i64 - lower as i64) as u16 + 1)
                };
                let diff = (upper as i64 - lower as i64) % (stride as i64);

                CircLinearProg::new(
                    16,
                    lower as i64,
                    (upper - diff as i16) as i64,
                    stride as u64,
                )
                        .unwrap()
            } else {
                CircLinearProg::new(16, lower as i64, lower as i64, 1).unwrap()
            }
        } else {
            if g.gen_weighted_bool(2) {
                CircLinearProg::bottom()
            } else {
                CircLinearProg::top()
            }
        }
    }
}

impl Debug for CircLinearProg {
    fn fmt(&self, f: &mut Formatter) -> result::Result<(), fmt::Error> {
        match self {
            &CircLinearProg::Bottom => f.write_str("⟂"),
            &CircLinearProg::Interval { ref lower, ref upper, ref stride, ref width } => {
                f.write_fmt(format_args!("CLP_{}({},{},{})", width, lower, upper, stride))
            }
            &CircLinearProg::Top => f.write_str("⊤"),
        }
    }
}

impl CircLinearProg {
    pub fn new(w: usize, l: i64, u: i64, s: u64) -> Result<Self> {
        if s == 0 || w == 0 || l > u || (u - l) % (s as i64) != 0 {
            return Err(
                format!(
                    "Invalid Self parameters: width={}, lower={}, upper={}, stride={}",
                    w,
                    l,
                    u,
                    s
                    )
                .into()
                );
        }

        Ok(CircLinearProg::Interval { width: w, lower: l.into(), upper: u.into(), stride: s.into() })
    }

    pub fn top() -> Self {
        CircLinearProg::Top
    }

    pub fn bottom() -> Self {
        CircLinearProg::Bottom
    }

    pub fn concrete(&self) -> HashSet<u64> {
        use num::ToPrimitive;
        use num::bigint::ToBigInt;

        match self {
            &CircLinearProg::Bottom => HashSet::new(),
            &CircLinearProg::Interval { ref lower, ref upper, ref stride, width } => {
                let mut ret = HashSet::new();

                if *stride == BigInt::zero() {
                    ret.insert(Self::to_unsigned(lower, width).to_u64().unwrap());
                } else {
                    for i in 0..0xFFFF {
                        let val: BigInt = i.to_bigint().unwrap() * stride + lower;

                        if val > *upper || val > 0xFFFF_FFFF_FFFF_FFFFu64.to_bigint().unwrap() {
                            break;
                        }
                        let u = Self::to_unsigned(&val, width);
                        ret.insert(u.to_u64().unwrap());
                    }
                }

                ret
            }
            &CircLinearProg::Top => unreachable!(),
        }
    }

    pub fn is_bottom(&self) -> bool {
        if let &CircLinearProg::Bottom = self {
            true
        } else {
            false
        }
    }

    pub fn is_top(&self) -> bool {
        if let &CircLinearProg::Top = self {
            true
        } else {
            false
        }
    }

    pub fn width(&self) -> Option<usize> {
        match self {
            &CircLinearProg::Bottom => None,
            &CircLinearProg::Interval { width, .. } => Some(width),
            &CircLinearProg::Top => None,
        }
    }

    pub fn is_singular(&self) -> Option<BigInt> {
        if let &CircLinearProg::Interval { ref lower, ref upper, ref stride, .. } = self {
            if upper - lower < *stride || *stride == BigInt::zero() {
                Some(lower.clone())
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn cardinality(&self) -> Option<usize> {
        if let &CircLinearProg::Interval { ref lower, ref upper, ref stride, .. } = self {
            ((upper - lower) / stride).to_usize().map(|x| x + 1)
        } else {
            None
        }
    }

    pub fn add(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, upper: ref u1, stride: ref s1, width },
             &CircLinearProg::Interval { lower: ref l2, upper: ref u2, stride: ref s2, .. }) => {
                match (a.is_singular(), b.is_singular()) {
                    (Some(k1), Some(k2)) => {
                        let k = k1 + k2;
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: k.clone(),
                                upper: k,
                                stride: BigInt::one(),
                            }
                          )
                    }
                    (Some(k1), None) => {
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: l2 + k1.clone(),
                                upper: u2 + k1,
                                stride: s2.clone(),
                            }
                          )
                    }
                    (None, Some(k2)) => {
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: l1 + k2.clone(),
                                upper: u1 + k2,
                                stride: s1.clone(),
                            }
                          )
                    }
                    (None, None) => {
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: l1 + l2,
                                upper: u1 + u2,
                                stride: gcd(s1.clone(), s2.clone()),
                            }
                          )
                    }
                }
            }
        }
    }

    pub fn subtract(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, upper: ref u1, stride: ref s1, width },
             &CircLinearProg::Interval { lower: ref l2, upper: ref u2, stride: ref s2, .. }) => {
                match (a.is_singular(), b.is_singular()) {
                    (Some(k1), Some(k2)) => {
                        let k = k1 - k2;
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: k.clone(),
                                upper: k,
                                stride: BigInt::one(),
                            }
                          )
                    }
                    (Some(k1), None) => {
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: k1.clone() - l2,
                                upper: k1 - u2,
                                stride: s2.clone(),
                            }
                          )
                    }
                    (None, Some(k2)) => {
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: l1 - k2.clone(),
                                upper: u1 - k2,
                                stride: s1.clone(),
                            }
                          )
                    }
                    (None, None) => {
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: l1 - u2,
                                upper: u1 - l2,
                                stride: gcd(s1.clone(), s2.clone()),
                            }
                          )
                    }
                }
            }
        }
    }

    pub fn multiply(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, upper: ref u1, stride: ref s1, width },
             &CircLinearProg::Interval { lower: ref l2, upper: ref u2, stride: ref s2, .. }) => {
                match (a.is_singular(), b.is_singular()) {
                    (Some(k1), Some(k2)) => {
                        let k = k1 * k2;
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: k.clone(),
                                upper: k,
                                stride: BigInt::one(),
                            }
                          )
                    }
                    (Some(k1), None) => {
                        let a = k1.clone() * l2;
                        let b = k1.clone() * u2;

                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: cmp::min(a.clone(), b.clone()),
                                upper: cmp::max(a, b),
                                stride: (s2.clone() * k1).abs(),
                            }
                          )
                    }
                    (None, Some(k2)) => {
                        let a = k2.clone() * l1;
                        let b = k2.clone() * u1;

                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: cmp::min(a.clone(), b.clone()),
                                upper: cmp::max(a, b),
                                stride: (s1.clone() * k2).abs(),
                            }
                          )
                    }
                    (None, None) => {
                        let a = l1 * l2;
                        let b = u1 * u2;
                        let c = l1 * u2;
                        let d = u1 * l2;

                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: cmp::min(
                                    cmp::min(cmp::min(a.clone(), b.clone()), c.clone()),
                                    d.clone(),
                                    ),
                                    upper: cmp::max(cmp::max(cmp::max(a, b), c), d),
                                    stride: gcd(
                                        gcd((l1 * s2.clone()).abs(), (l2 * s1.clone()).abs()),
                                        (s1 * s2).abs(),
                                        ),
                            }
                          )
                    }
                }
            }
        }
    }

    pub fn divide_signed(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, upper: ref u1, width, .. },
             &CircLinearProg::Interval { lower: ref l2, upper: ref u2, stride: ref s2, .. }) => {
                match (a.is_singular(), b.is_singular()) {
                    (Some(k1), Some(k2)) => {
                        if k2 == BigInt::zero() {
                            Ok(CircLinearProg::Top)
                        } else {
                            let k = k1 / k2;
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: k.clone(),
                                    upper: k,
                                    stride: BigInt::one(),
                                }
                              )
                        }
                    }
                    (Some(_), None) | (None, None) => {
                        let z = BigInt::zero();
                        let (i, r) = div_mod_floor(-l2.clone(), s2.clone());

                        // if b includes 0 we return Top (div by 0)
                        if l2 <= &z && u2 >= &z && r == z && l2.clone() + s2.clone() * i.clone() <= u2.clone() {
                            return Ok(CircLinearProg::Top);
                        }


                        let alpha = l2.clone() + (i.clone() + BigInt::one()) * s2.clone();
                        let beta = l2.clone() + i.clone() * s2.clone();
                        let beta = if beta == z {
                            l2.clone() + (i.clone() - BigInt::one()) * s2.clone()
                        } else {
                            beta
                        };
                        let sl1 = if l1 == &z { u1 >= &z } else { l1 >= &z };
                        let su1 = if u1 == &z { l1 >= &z } else { u1 >= &z };
                        let sl2 = if l2 == &z { u2 >= &z } else { l2 >= &z };
                        let su2 = if u2 == &z { l2 >= &z } else { u2 >= &z };

                        let (a, b) = match (sl1, su1, sl2, su2) {
                            (false, false, false, false) => (u1 / l2, l1 / u2),
                            (false, false, false, true) => (l1 / alpha, l1 / beta),
                            (false, false, true, true) => (l1 / l2, u1 / u2),
                            (false, true, false, false) => (u1 / u2, l1 / u2),
                            (false, true, false, true) => (cmp::min(u1 / beta.clone(), l1 / alpha.clone()), cmp::max(l1 / beta, u1 / alpha)),
                            (true, true, false, false) => (u1 / u2, l1 / l2),
                            (true, true, false, true) => (u1 / beta, u1 / alpha),
                            (false, true, true, true) => (l1 / l2, u1 / l2),
                            (true, true, true, true) => (l1 / u2, u1 / l2),
                            _ => {
                                return Err("Invalid Self bounds".into());
                            }
                        };

                        Ok(CircLinearProg::Interval { width: width, lower: a, upper: b, stride: BigInt::one() })
                    }
                    (None, Some(k2)) => {
                        let z = BigInt::zero();
                        if k2 == z {
                            return Ok(CircLinearProg::Top);
                        }
                        let a = l1.clone() / k2.clone();
                        let b = u1.clone() / k2.clone();

                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: cmp::min(a.clone(), b.clone()),
                                upper: cmp::max(a, b),
                                stride: BigInt::one(),
                            }
                          )
                    }
                }
            }
        }
    }

    pub fn left_shift(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, upper: ref u1, stride: ref s1, width }, &CircLinearProg::Interval { lower: ref l2, upper: ref u2, .. }) => {
                // shifting by a negative number is undefined (Join)
                if *l2 < BigInt::zero() || *u2 < BigInt::zero() {
                    return Ok(CircLinearProg::Top);
                }

                // if the rhs value doesn't fit into usize we assume it's > w and return 0
                match (a.is_singular(), b.is_singular()) {
                    (Some(k1), Some(k2)) => {
                        let k = k2.to_usize().map(|k2| k1 << k2).unwrap_or(BigInt::zero());
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: k.clone(),
                                upper: k,
                                stride: BigInt::one(),
                            }
                          )
                    }
                    (None, Some(k2)) => {
                        if let Some(k2) = k2.to_usize() {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: l1 << k2.clone(),
                                    upper: u1 << k2,
                                    stride: s1.clone() << k2,
                                }
                              )
                        } else {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: BigInt::zero(),
                                    upper: BigInt::zero(),
                                    stride: BigInt::one(),
                                }
                              )
                        }
                    }
                    (None, None) | (Some(_), None) => {
                        match (l2.to_usize(), u2.to_usize()) {
                            (Some(l2), Some(u2)) => {
                                Ok(
                                    CircLinearProg::Interval {
                                        width: width,
                                        lower: l1 << l2.clone(),
                                        upper: u1 << u2,
                                        stride: gcd(l1.abs(), s1.clone()) << l2,
                                    }
                                  )
                            }
                            (None, None) => {
                                Ok(
                                    CircLinearProg::Interval {
                                        width: width,
                                        lower: BigInt::zero(),
                                        upper: BigInt::zero(),
                                        stride: BigInt::one(),
                                    }
                                  )
                            }
                            (Some(_), None) | (None, Some(_)) => Ok(CircLinearProg::Top),
                        }
                    }
                }
            }
        }
    }

    pub fn right_shift_signed(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, upper: ref u1, stride: ref s1, width }, &CircLinearProg::Interval { lower: ref l2, upper: ref u2, .. }) => {
                // shifting by a negative number is undefined (Join)
                if *l2 < BigInt::zero() || *u2 < BigInt::zero() {
                    return Ok(CircLinearProg::Top);
                }
                let endpoint = if *l1 < BigInt::zero() && *u1 < BigInt::zero() {
                    Ok(
                        CircLinearProg::Interval {
                            width: width,
                            lower: -BigInt::one(),
                            upper: -BigInt::one(),
                            stride: BigInt::zero(),
                        }
                      )
                } else if *l1 >= BigInt::zero() && *u1 >= BigInt::zero() {
                    Ok(
                        CircLinearProg::Interval {
                            width: width,
                            lower: BigInt::zero(),
                            upper: BigInt::zero(),
                            stride: BigInt::zero(),
                        }
                      )
                } else {
                    Ok(
                        CircLinearProg::Interval {
                            width: width,
                            lower: -BigInt::one(),
                            upper: BigInt::zero(),
                            stride: BigInt::one(),
                        }
                      )
                };
                fn shrs(a: BigInt, b: usize, width: usize) -> BigInt {
                    let bits = a.bits();
                    let sign = a.sign();
                    let ret = a >> b;

                    if b > 0 && sign == Sign::Minus {
                        let all = (BigInt::one() << width) - BigInt::one();
                        let used = (BigInt::one() << bits) - BigInt::one();
                        let ret = ret + (all - used);

                        if ret == BigInt::zero() { ret - 1 } else { ret }
                    } else {
                        ret
                    }
                }

                // if the rhs value doesn't fit into usize we assume it's > w and return 0/-1
                match (a.is_singular(), b.is_singular()) {
                    (Some(k1), Some(k2)) => {
                        if let Some(k) = k2.to_usize().map(|k2| shrs(k1, k2, width)) {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: k.clone(),
                                    upper: k,
                                    stride: BigInt::one(),
                                }
                              )
                        } else {
                            endpoint
                        }
                    }
                    (None, Some(k2)) => {
                        if let Some(k2) = k2.to_usize() {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: shrs(l1.clone(), k2, width),
                                    upper: shrs(u1.clone(), k2, width),
                                    stride: cmp::max(BigInt::one(), shrs(s1.clone(), k2, width)),
                                }
                              )
                        } else {
                            endpoint
                        }
                    }
                    (None, None) | (Some(_), None) => {
                        match (l2.to_usize(), u2.to_usize()) {
                            (Some(l2), Some(u2)) => {
                                match (l1.sign(), u1.sign()) {
                                    (Sign::Minus, Sign::Minus) => {
                                        Ok(
                                            CircLinearProg::Interval {
                                                width: width,
                                                lower: shrs(l1.clone(), l2, width),
                                                upper: shrs(u1.clone(), u2, width),
                                                stride: BigInt::one(),
                                            }
                                          )
                                    }
                                    (Sign::Minus, Sign::Plus) => {
                                        Ok(
                                            CircLinearProg::Interval {
                                                width: width,
                                                lower: shrs(l1.clone(), l2, width),
                                                upper: shrs(u1.clone(), l2, width),
                                                stride: BigInt::one(),
                                            }
                                          )
                                    }
                                    (Sign::Plus, Sign::Plus) => {
                                        Ok(
                                            CircLinearProg::Interval {
                                                width: width,
                                                lower: shrs(l1.clone(), u2, width),
                                                upper: shrs(u1.clone(), l2, width),
                                                stride: BigInt::one(),
                                            }
                                          )
                                    }
                                    (Sign::Plus, Sign::Minus) |
                                        (Sign::NoSign, _) |
                                        (_, Sign::NoSign) => Ok(CircLinearProg::Top),
                                }
                            }
                            (None, None) | (Some(_), None) | (None, Some(_)) => endpoint,
                        }
                    }
                }
            }
        }
    }

    pub fn right_shift_unsigned(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, upper: ref u1, width, .. }, &CircLinearProg::Interval { lower: ref l2, upper: ref u2, .. }) => {
                // shifting by a negative number is undefined (Join)
                if *l2 < BigInt::zero() || *u2 < BigInt::zero() {
                    return Ok(CircLinearProg::Top);
                }
                // shifting a negative number is undefined. Caller is expected to call split_unsigned()
                if *l1 < BigInt::zero() || *u1 < BigInt::zero() {
                    return Ok(CircLinearProg::Top);
                }

                let endpoint = Ok(
                    CircLinearProg::Interval {
                        width: width,
                        lower: BigInt::zero(),
                        upper: BigInt::zero(),
                        stride: BigInt::one(),
                    }
                    );

                // if the rhs value doesn't fit into usize we assume it's > w and return 0
                match (a.is_singular(), b.is_singular()) {
                    (Some(k1), Some(k2)) => {
                        if let Some(k) = k2.to_usize().map(|k2| k1 >> k2) {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: k.clone(),
                                    upper: k,
                                    stride: BigInt::one(),
                                }
                              )
                        } else {
                            endpoint
                        }
                    }
                    (None, Some(k2)) => {
                        if let Some(k2) = k2.to_usize() {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: l1.clone() >> k2,
                                    upper: u1.clone() >> k2,
                                    stride: BigInt::one(),
                                }
                              )
                        } else {
                            endpoint
                        }
                    }
                    (None, None) | (Some(_), None) => {
                        match (l2.to_usize(), u2.to_usize()) {
                            (Some(l2), Some(u2)) => {
                                Ok(
                                    CircLinearProg::Interval {
                                        width: width,
                                        lower: l1.clone() >> u2.clone(),
                                        upper: u1.clone() >> l2.clone(),
                                        stride: BigInt::one(),
                                    }
                                  )
                            }
                            (None, None) | (Some(_), None) | (None, Some(_)) => endpoint,
                        }
                    }
                }
            }
        }
    }

    pub fn and(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, stride: ref s1, width, .. }, &CircLinearProg::Interval { lower: ref l2, stride: ref s2, .. }) => {
                // ANDing negative values is undefined. Caller is expected to call split_unsigned()
                if *l1 < BigInt::zero() || *l2 < BigInt::zero() {
                    return Ok(CircLinearProg::Top);
                }
                match (a.is_singular(), b.is_singular()) {
                    (Some(ref k1), Some(ref k2)) => {
                        let k = (k1.to_biguint().unwrap() & k2.to_biguint().unwrap()).to_bigint().unwrap();
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: k.clone(),
                                upper: k,
                                stride: BigInt::one(),
                            }
                          )
                    }
                    (Some(ref k1), None) if b.cardinality() == Some(2) => {
                        let b1 = l2;
                        let b2 = l2 + s2;
                        let x = (k1.to_biguint().unwrap() & b1.to_biguint().unwrap()).to_bigint().unwrap();
                        let y = (k1.to_biguint().unwrap() & b2.to_biguint().unwrap()).to_bigint().unwrap();
                        let (x, y) = (cmp::min(x.clone(), y.clone()), cmp::max(x, y));

                        /*Ok(CircLinearProg::Interval{
                          width: width,
                          lower: x.clone(),
                          upper: x + y.clone(),
                          stride: y
                          })*/
                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: x.clone(),
                                upper: y.clone(),
                                stride: y - x,
                            }
                          )
                    }
                    (None, Some(ref k2)) if a.cardinality() == Some(2) => {
                        let a1 = l1;
                        let a2 = l1 + s1;
                        let x = (a1.to_biguint().unwrap() & k2.to_biguint().unwrap()).to_bigint().unwrap();
                        let y = (a2.to_biguint().unwrap() & k2.to_biguint().unwrap()).to_bigint().unwrap();
                        let (x, y) = (cmp::min(x.clone(), y.clone()), cmp::max(x, y));

                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: x.clone(),
                                upper: y.clone(),
                                stride: y - x,
                            }
                          )
                    }
                    (None, None) | (Some(_), None) | (None, Some(_)) => Ok(CircLinearProg::Top),
                }
            }
        }
    }

    pub fn or(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { .. }, &CircLinearProg::Interval { .. }) => {
                let not_a = Self::not(&a).unwrap();
                let not_b = Self::not(&b).unwrap();
                let c1 = Self::and(&not_a, &not_b).unwrap();

                Self::not(&c1)
            }
        }
    }

    pub fn xor(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { .. }, &CircLinearProg::Interval { .. }) => {
                let not_a = Self::not(&a).unwrap();
                let not_b = Self::not(&b).unwrap();
                let c1 = Self::and(&not_a, &b).unwrap();
                let c2 = Self::and(&a, &not_b).unwrap();

                Self::or(&c1, &c2)
            }
        }
    }

    pub fn modulo(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { .. }, &CircLinearProg::Interval { .. }) => {
                let quot = Self::divide_signed(&a, &b).unwrap();
                let c1 = Self::multiply(&b, &quot).unwrap();

                Self::subtract(&a, &c1)
            }
        }
    }

    pub fn not(a: &Self) -> Result<Self> {
        let w1 = a.width();

        if w1 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match a {
            &CircLinearProg::Top => Ok(a.clone()),
            &CircLinearProg::Bottom => Ok(a.clone()),
            &CircLinearProg::Interval { lower: ref l1, upper: ref u1, stride: ref s1, width } => {
                // NOTing negative values is undefined. Caller is expected to call split_unsigned()
                if *l1 < BigInt::zero() || *u1 < BigInt::zero() {
                    return Ok(CircLinearProg::Top);
                }
                let l = l1.to_biguint().unwrap();
                let u = u1.to_biguint().unwrap();
                let mask = (BigUint::one() << width) - BigUint::one();
                let l = (mask.clone() ^ l).to_bigint().unwrap();
                let u = (mask ^ u).to_bigint().unwrap();

                Ok(CircLinearProg::Interval { width: width, lower: u, upper: l, stride: s1.clone() })
            }
        }
    }

    fn signed_wraparound(a: Self) -> Result<Self> {
        if let CircLinearProg::Interval { width, lower, upper, stride } = a {
            if width == 0 {
                return Err("Invalid Self width".to_string().into());
            }

            if lower.bits() >= width || upper.bits() >= width || lower > upper {
                Ok(CircLinearProg::Top)
            } else {
                Ok(CircLinearProg::Interval { width: width, lower: lower, upper: upper, stride: stride })
            }
        } else {
            Ok(a)
        }
    }

    fn unsigned_wraparound(a: Self) -> Result<Self> {
        if let CircLinearProg::Interval { width, lower, upper, stride } = a {
            if width == 0 {
                return Err("Invalid Self width".to_string().into());
            }

            if lower.bits() >= width || upper.bits() >= width || lower > upper {
                Ok(CircLinearProg::Top)
            } else {
                Ok(CircLinearProg::Interval { width: width, lower: lower, upper: upper, stride: stride })
            }
        } else {
            Ok(a)
        }
    }

    fn to_unsigned(a: &BigInt, w: usize) -> BigUint {
        if a.sign() == Sign::Minus {
            let m = BigInt::one() << w;
            if a.bits() > w {
                (m.clone() + (a % m)).to_biguint().unwrap()
            } else {
                (m + a).to_biguint().unwrap()
            }
        } else {
            a.to_biguint().unwrap()
        }
    }

    fn split_unsigned(a: &Self) -> Vec<Self> {
        if let &CircLinearProg::Interval { ref lower, ref upper, ref stride, width } = a {
            match (lower.sign(), upper.sign()) {
                (Sign::Plus, Sign::Plus) |
                    (Sign::NoSign, Sign::Plus) |
                    (Sign::Plus, Sign::NoSign) |
                    (Sign::NoSign, Sign::NoSign) => vec![a.clone()],
                    (Sign::Minus, Sign::Minus) => {
                        let l = Self::to_unsigned(lower, width);
                        let u = Self::to_unsigned(upper, width);

                        vec![
                            CircLinearProg::Interval {
                                lower: l.to_bigint().unwrap(),
                                upper: u.to_bigint().unwrap(),
                                stride: stride.clone(),
                                width: width,
                            },
                        ]
                    }
                    (Sign::Minus, Sign::NoSign) |
                        (Sign::Minus, Sign::Plus) => {
                            let l = Self::to_unsigned(lower, width).to_bigint().unwrap();
                            let end = (BigInt::one() << width) - BigInt::one();
                            let diff = end.clone() - l.clone();
                            let (n, r) = div_mod_floor(diff.clone(), stride.clone());
                            let u1 = stride.clone() - r.clone() - BigInt::one();
                            let u2 = l.clone() + stride.clone() * n.clone();

                            vec![
                                CircLinearProg::Interval {
                                    lower: if u1 <= *upper { u1 } else { BigInt::zero() },
                                    upper: upper.clone(),
                                    stride: stride.clone(),
                                    width: width,
                                },
                                CircLinearProg::Interval {
                                    lower: l.clone(),
                                    upper: if u2 > l { u2.clone() } else { l.clone() },
                                    stride: if u2 > l {
                                        stride.clone()
                                    } else {
                                        BigInt::one()
                                    },
                                    width: width,
                                },
                            ]
                        }
                    (Sign::Plus, Sign::Minus) |
                        (Sign::NoSign, Sign::Minus) => unreachable!(),
            }
        } else {
            vec![a.clone()]
        }
    }

    fn union(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (_, x @ &CircLinearProg::Top) => Ok(x.clone()),
            (x @ &CircLinearProg::Top, _) => Ok(x.clone()),
            (&CircLinearProg::Bottom, x) => Ok(x.clone()),
            (x, &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, upper: ref u1, stride: ref s1, width, .. },
             &CircLinearProg::Interval { lower: ref l2, upper: ref u2, stride: ref s2, .. }) => {
                match (a.is_singular(), b.is_singular()) {
                    (Some(ref k1), Some(ref k2)) => {
                        let l = cmp::min(k1.clone(), k2.clone());
                        let u = cmp::max(k1.clone(), k2.clone());

                        Ok(
                            CircLinearProg::Interval {
                                width: width,
                                lower: l.clone(),
                                upper: u.clone(),
                                stride: cmp::max(BigInt::one(), u - l),
                            }
                          )
                    }
                    (Some(ref k), None) => {
                        let l = cmp::min(k.clone(), l2.clone());
                        let u = cmp::max(k.clone(), u2.clone());
                        let s = if k < l2 {
                            gcd(l2.clone() - k.clone(), s2.clone())
                        } else {
                            gcd(k.clone() - u2.clone(), s2.clone())
                        };

                        Ok(CircLinearProg::Interval { width: width, lower: l, upper: u, stride: s })
                    }
                    (None, Some(ref k)) => {
                        let l = cmp::min(k.clone(), l1.clone());
                        let u = cmp::max(k.clone(), u1.clone());
                        let s = if k < l1 {
                            gcd(l1.clone() - k.clone(), s1.clone())
                        } else {
                            gcd(k.clone() - u1.clone(), s1.clone())
                        };

                        Ok(CircLinearProg::Interval { width: width, lower: l, upper: u, stride: s })
                    }
                    (None, None) => {
                        let la = cmp::min(l1, l2);
                        let lb = cmp::max(l1, l2);
                        let u = cmp::max(u1, u2);
                        let s = if la != lb {
                            gcd(gcd(la - lb, s1.clone()), s2.clone())
                        } else {
                            gcd(s1.clone(), s2.clone())
                        };

                        Ok(CircLinearProg::Interval { width: width, lower: la.clone(), upper: u.clone(), stride: s })
                    }
                }
            }
        }
    }

    fn union_vec(a: Vec<Self>) -> Result<Self> {
        match a.len() {
            0 => Err("empty Self vector".into()),
            1 => Ok(a[0].clone()),
            _ => {
                let x = a[0].clone();
                a.into_iter().skip(1).fold(Ok(x), |acc, x| acc.and_then(|y| Self::union(&y, &x)))
            }
        }
    }

    fn intersection(a: &Self, b: &Self) -> Result<Self> {
        let w1 = a.width();
        let w2 = b.width();

        if (w1.is_some() && w2.is_some() && w1 != w2) || w1 == Some(0) || w2 == Some(0) {
            return Err("Invalid Self width".to_string().into());
        }

        match (a, b) {
            (x, &CircLinearProg::Top) => Ok(x.clone()),
            (&CircLinearProg::Top, x) => Ok(x.clone()),
            (x @ &CircLinearProg::Bottom, _) => Ok(x.clone()),
            (_, x @ &CircLinearProg::Bottom) => Ok(x.clone()),

            (&CircLinearProg::Interval { lower: ref l1, upper: ref u1, stride: ref s1, width, .. },
             &CircLinearProg::Interval { lower: ref l2, upper: ref u2, stride: ref s2, .. }) => {
                match (a.is_singular(), b.is_singular()) {
                    (Some(ref k1), Some(ref k2)) => {
                        if k1 == k2 {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: k1.clone(),
                                    upper: k2.clone(),
                                    stride: BigInt::one(),
                                }
                              )
                        } else {
                            Ok(CircLinearProg::Bottom)
                        }
                    }
                    (Some(ref k), None) => {
                        if l2 <= k && k <= u2 && div_mod_floor(k - l2, s2.clone()).1 == BigInt::zero() {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: k.clone(),
                                    upper: k.clone(),
                                    stride: BigInt::one(),
                                }
                              )
                        } else {
                            Ok(CircLinearProg::Bottom)
                        }
                    }
                    (None, Some(ref k)) => {
                        if l1 <= k && k <= u1 && div_mod_floor(k - l1, s1.clone()).1 == BigInt::zero() {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: k.clone(),
                                    upper: k.clone(),
                                    stride: BigInt::one(),
                                }
                              )
                        } else {
                            Ok(CircLinearProg::Bottom)
                        }
                    }
                    (None, None) => {
                        if !((l1 <= u2 && l1 >= l2) || (l2 <= u1 && l2 >= l1)) {
                            return Ok(CircLinearProg::Bottom);
                        }

                        let s = lcm(s1.clone(), s2.clone());
                        if s >= (BigInt::one() << width) {
                            return Ok(CircLinearProg::Bottom);
                        }

                        let la = cmp::min(l1, l2);
                        let lb = cmp::max(l1, l2);
                        let (sa, sb) = if l1 == la { (s1, s2) } else { (s2, s1) };
                        let (d, _, y) = Self::eea(sb, sa);

                        let diff = lb - la;
                        let z = BigInt::zero();
                        let (i, r) = div_mod_floor(diff.clone(), d.clone());
                        if r != z {
                            return Ok(CircLinearProg::Bottom);
                        }

                        let l = la + i.clone() * y.clone() * sa;
                        let u = cmp::min(u1, u2);
                        let (ii, rr) = div_mod_floor(lb - l.clone(), s.clone());
                        let l = if rr == z {
                            l + ii * s.clone()
                        } else {
                            l + (ii + BigInt::one()) * s.clone()
                        };

                        let diff = u - l.clone();
                        if *l1 > l || *l2 > l || *u < l {
                            Ok(CircLinearProg::Bottom)
                        } else {
                            Ok(
                                CircLinearProg::Interval {
                                    width: width,
                                    lower: l.clone(),
                                    upper: l.clone() + s.clone() * (diff / s.clone()),
                                    stride: s,
                                }
                              )
                        }
                    }
                }
            }
        }
    }

    fn eea(a: &BigInt, b: &BigInt) -> (BigInt, BigInt, BigInt) {
        if a > b {
            let (d, x, y) = Self::eea(b, a);
            (d, y, x)
        } else if *a == BigInt::zero() {
            (b.clone(), BigInt::zero(), BigInt::one())
        } else {
            let (g, x, y) = Self::eea(&(b % a), a);
            (g, y - (b / a) * x.clone(), x)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use {Kset, Cardinality, _10};

    fn xcheck_clp<N: Cardinality>(clp: &CircLinearProg, kset: &Kset<N>) -> bool {
        match kset {
            &Kset::Meet{ .. } => clp.is_bottom(),
            &Kset::Set(ref v) => {
                if clp.is_top() {
                    true
                } else if clp.is_bottom() {
                    false
                } else {
                    let c_set = clp.concrete();
                    if c_set.len() < v.len() {
                        error!("++++++++++++++++++++++++++++++++++++++++++++++++++");
                        error!("clp too small: {:?} kset: {:?}", c_set.len(), v.len());
                        error!("++++++++++++++++++++++++++++++++++++++++++++++++++");
                        false
                    } else {
                        v.iter()
                            .all(
                                |&Constant{ value,.. }| if !c_set.contains(&(value & 0xffff) ) {
                                    error!("++++++++++++++++++++++++++++++++++++++++++++++++++");
                                    error!("clp missing {:?}/{:?}", value, value as i16 as i64);
                                    let mut set: Vec<u64> = c_set.clone().into_iter().collect();
                                    set.sort();
                                    error!("set: {:?}", set);
                                    error!("kset: {:?}", kset);
                                    error!("++++++++++++++++++++++++++++++++++++++++++++++++++");
                                    false
                                } else {
                                    true
                                }
                            )
                    }
                }
            }
            &Kset::Join => clp.is_top() || clp.cardinality().unwrap_or(0) > N::value(),
        }
    }

    fn clp_to_kset<N: Cardinality>(a: &CircLinearProg) -> Kset<N> {
        match a {
            &CircLinearProg::Bottom => Kset::default(),
            &CircLinearProg::Interval { .. } => Kset::Set(a.concrete().into_iter().map(|v| {
                Constant::new(v as u16 as u64, 16).unwrap()
            }).collect()),
            &CircLinearProg::Top => Kset::Join,
        }
    }

    quickcheck!{
        fn qc_add(a: CircLinearProg,b: CircLinearProg) -> bool {
            let c = CircLinearProg::add(&a,&b).and_then(CircLinearProg::signed_wraparound).unwrap();
            let res = Kset::<_10>::add(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            debug!("{:?} + {:?} = {:?}",a,b,c);
            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_sub(a: CircLinearProg,b: CircLinearProg) -> bool {
            let c = CircLinearProg::subtract(&a,&b).and_then(CircLinearProg::signed_wraparound).unwrap();
            let res = Kset::<_10>::subtract(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            debug!("{:?} - {:?} = {:?}",a,b,c);
            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_mul(a: CircLinearProg,b: CircLinearProg) -> bool {
            let c = CircLinearProg::multiply(&a,&b).and_then(CircLinearProg::signed_wraparound).unwrap();
            let res = Kset::<_10>::multiply(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            debug!("{:?} * {:?} = {:?}",a,b,c);
            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_divs(a: CircLinearProg,b: CircLinearProg) -> bool {
            let c = CircLinearProg::divide_signed(&a,&b).and_then(CircLinearProg::signed_wraparound).unwrap();
            let res = Kset::<_10>::divide_signed(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            debug!("{:?} /s {:?} = {:?}",a,b,c);
            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_divu(a: CircLinearProg,b: CircLinearProg) -> bool {
            let c = CircLinearProg::union_vec(CircLinearProg::split_unsigned(&a).into_iter().map(|a| {
                CircLinearProg::union_vec(CircLinearProg::split_unsigned(&b).into_iter().map(|b| {
                    debug!("  {:?} / {:?}",a,b);
                    CircLinearProg::unsigned_wraparound(CircLinearProg::divide_signed(&a,&b).unwrap()).unwrap()
                }).collect()).unwrap()
            }).collect()).unwrap();
            let res = Kset::<_10>::divide_unsigned(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            debug!("{:?} / {:?} = {:?}",a,b,c);
            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_mod(a: CircLinearProg,b: CircLinearProg) -> bool {
           debug!("{:?} % {:?}:",a,b);
           let c = CircLinearProg::union_vec(CircLinearProg::split_unsigned(&a).into_iter().map(|a| {
                CircLinearProg::union_vec(CircLinearProg::split_unsigned(&b).into_iter().map(|b| {
                    let c = CircLinearProg::unsigned_wraparound(CircLinearProg::modulo(&a,&b).unwrap()).unwrap();
                    debug!("  {:?} % {:?} = {:?}",a,b,c);
                    c
                }).collect()).unwrap()
            }).collect()).unwrap();
            let res = Kset::<_10>::modulo(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            debug!("=> {:?} % {:?} = {:?}",a,b,c);
            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_shl(a: CircLinearProg,b: CircLinearProg) -> bool {
            let c = CircLinearProg::left_shift(&a,&b).and_then(CircLinearProg::signed_wraparound).unwrap();
            let res = Kset::<_10>::left_shift(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            debug!("{:?} << {:?} = {:?}",a,b,c);
            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_shrs(a: CircLinearProg,b: CircLinearProg) -> bool {
            let c = CircLinearProg::right_shift_signed(&a,&b).and_then(CircLinearProg::signed_wraparound).unwrap();
            let res = Kset::<_10>::right_shift_signed(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            debug!("{:?} >>s {:?} = {:?}",a,b,c);
            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_shru(a: CircLinearProg,b: CircLinearProg) -> bool {
            debug!("{:?} >> {:?}",a,b);
            let c = CircLinearProg::union_vec(CircLinearProg::split_unsigned(&a).into_iter().map(|a| {
                CircLinearProg::unsigned_wraparound(CircLinearProg::right_shift_unsigned(&a,&b).unwrap()).unwrap()
            }).collect()).unwrap();
            let res = Kset::<_10>::right_shift_unsigned(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_and(a: CircLinearProg,b: CircLinearProg) -> bool {
            debug!("{:?} & {:?}",a,b);
            let c = CircLinearProg::union_vec(CircLinearProg::split_unsigned(&a).into_iter().map(|a| {
                CircLinearProg::unsigned_wraparound(CircLinearProg::and(&a,&b).unwrap()).unwrap()
            }).collect()).unwrap();
            let res = Kset::<_10>::and(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_or(a: CircLinearProg,b: CircLinearProg) -> bool {
            debug!("{:?} | {:?}",a,b);
            let c = CircLinearProg::union_vec(CircLinearProg::split_unsigned(&a).into_iter().map(|a| {
                CircLinearProg::unsigned_wraparound(CircLinearProg::or(&a,&b).unwrap()).unwrap()
            }).collect()).unwrap();
            let res = Kset::<_10>::or(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_xor(a: CircLinearProg,b: CircLinearProg) -> bool {
            debug!("{:?} ^ {:?}",a,b);
            let c = CircLinearProg::union_vec(CircLinearProg::split_unsigned(&a).into_iter().map(|a| {
                CircLinearProg::unsigned_wraparound(CircLinearProg::xor(&a,&b).unwrap()).unwrap()
            }).collect()).unwrap();
            let res = Kset::<_10>::xor(&clp_to_kset(&a),&clp_to_kset(&b)).unwrap();

            xcheck_clp(&c,&res)
        }
    }

    quickcheck!{
        fn qc_union(a: CircLinearProg,b: CircLinearProg) -> bool {
            let c = CircLinearProg::union(&a,&b).unwrap();
            let res: Kset<_10> = clp_to_kset(&a).combine(&clp_to_kset(&b));

            debug!("{:?} || {:?} = {:?}",a,b,c);
            xcheck_clp(&c,&res)
        }
    }
}
