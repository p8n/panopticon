// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use std::borrow::Cow;
use std::cmp;
use {Avalue,Metadata,MetadataMap,ProgramPoint};
use types::{Statement,Result,Value,Constraint};

// Widening as Abstract Domain
#[derive(Clone,Hash,PartialEq,Eq,Debug)]
pub struct Widening<A: Avalue> {
    program_point: ProgramPoint,
    observed_backedge: bool,
    child: A,
}

impl<A: Avalue> Widening<A> {
    pub fn child<'a>(&'a self) -> &'a A {
        &self.child
    }
}

impl<A: Avalue> Default for Widening<A> {
    fn default() -> Widening<A> {
        Widening{
            program_point: ProgramPoint::default(),
            observed_backedge: false,
            child: A::default(),
        }
    }
}

impl<A: Avalue> From<Value> for Widening<A> {
    fn from(v: Value) -> Widening<A> {
        Widening{
            program_point: ProgramPoint::default(),
            observed_backedge: false,
            child: A::from(v),
        }
    }
}

impl<A: Avalue> Avalue for Widening<A> {
    fn execute(stmt: &Statement, meta: &Metadata<Widening<A>>) -> Result<Self> {
        let child_meta = MetadataMap::new(meta,|v| match v {
            Cow::Borrowed(v) => Cow::Borrowed(&v.child),
            Cow::Owned(ref v) => Cow::Owned(v.child.clone()),
        });
        let child = A::execute(stmt,&child_meta)?;
        let addr = meta.program_point();
        let flag = match stmt {
            &Statement::Expression{ ref op,.. } => {
                op.reads().into_iter().any(|v| {
                    match v {
                        &Value::Variable(_) => {
                            let &Widening{ program_point, observed_backedge,.. } = &*meta.value(v);
                            observed_backedge || addr < program_point
                        }
                        _ => false
                    }
                })
            }
            _ => false
        };

        Ok(Widening{
            program_point: addr,
            observed_backedge: flag,
            child: child,
        })
    }

    fn widen(&self, other: &Self) -> Self {
        Widening{
            program_point: cmp::max(self.program_point,other.program_point),
            observed_backedge: false,
            child: self.child.widen(&other.child),
        }
    }

    fn combine(&self, other: &Self) -> Self {
        if self.observed_backedge || other.observed_backedge {
            Widening{
                program_point: cmp::max(self.program_point,other.program_point),
                observed_backedge: false,
                child: self.child.widen(&other.child),
            }
        } else {
            Widening{
                program_point: cmp::max(self.program_point,other.program_point),
                observed_backedge: false,
                child: self.child.combine(&other.child),
            }
        }
    }

    fn is_better(&self, other: &Self) -> bool {
        self.child.is_better(&other.child)
    }

    fn narrow(&self,_: &Constraint) -> Self {
        self.clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use {Sign,NoMemory,approximate};
    use types::{Region, TestArch, Operation, Function, UUID};
    use dflow::rewrite_to_ssa;
    use simple_logger;

    /*
     * 0:  Mx0
     * 3:  Mn1
     *
     *     Px
     *     Pn
     * 6:  Cfn9
     * 10: Bf24
     *
     * 14: Axxn
     * 18: Ann1
     * 22: J6
     *
     * 24: R
     */
    #[test]
    fn widening_find_backedge() {
        let _ = simple_logger::init();
        let data = b"Mx0Mn1Cfn9Bf24AxxnAnn1J6R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert!(rewrite_to_ssa(&mut func).is_ok());

        for bb in func.basic_blocks() {
            let mut i = bb.1.statements.start;

            debug!("");
            debug!("# loc_{}",bb.1.area.start);
            for mne in func.mnemonics(bb.0) {
                debug!("{:?}: {}",mne.1.area,func.strings.value(mne.1.opcode).unwrap());
                for stmt in func.statements(i..(i + mne.1.statement_count)) {
                    debug!("  {:?}",stmt);
                }
                i += mne.1.statement_count;
            }
        }

        assert_eq!(func.statements(..).count(), 8);

        let mut vals = approximate::<Widening<Sign>,NoMemory<Widening<Sign>>,_>(&func,None).unwrap();

        for i in 0..8 {
            vals.set_position(i);

            let pp = vals.program_point();
            match i {
                0 => assert_eq!(ProgramPoint::new(0,0),pp),
                1 => assert_eq!(ProgramPoint::new(0,1),pp),
                2 => {
                    assert_eq!(ProgramPoint::new(6,0),pp);
                    if let &Statement::Expression{ op: Operation::Phi(ref a,ref b,Value::Undefined), ref result } = &*func.statements(2..3).next().unwrap() {
                        let a = vals.value(a);
                        let b = vals.value(b);
                        let res = vals.value(&Value::Variable(result.clone()));

                        assert_eq!(res.program_point, pp);
                        assert!(a.program_point > res.program_point || b.program_point > res.program_point);
                    } else {
                        unreachable!();
                    }
                }
                3 => {
                    assert_eq!(ProgramPoint::new(6,1),pp);
                }
                4 => assert_eq!(ProgramPoint::new(6,2),pp),
                5 => assert_eq!(ProgramPoint::new(24,0),pp),
                6 => assert_eq!(ProgramPoint::new(14,0),pp),
                7 => assert_eq!(ProgramPoint::new(14,1),pp),
                _ => unreachable!()
            }
        }
    }
}
