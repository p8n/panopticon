// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//! Kindler et.al style Kset domain.
//!
//! TODO

use {Avalue, Metadata, ToConstants};
use types::{Statement, Constant, Operation, Value, Result, Constraint};
use std::fmt;
use std::vec::IntoIter;
use std::default::Default;
use std::convert::From;
use std::hash::Hash;
use std::marker::PhantomData;
use std::cmp;
use quickcheck::{Arbitrary, Gen};


/*
 * Where are my type level integers!?
 */
pub trait Cardinality: fmt::Debug + Clone + Hash + PartialEq + Eq + Send {
    fn value() -> usize;
}

#[derive(Debug,Clone,Hash,PartialEq,Eq)]
pub struct _1;
impl Cardinality for _1 {
    fn value() -> usize { 1 }
}

#[derive(Debug,Clone,Hash,PartialEq,Eq)]
pub struct _10;
impl Cardinality for _10 {
    fn value() -> usize { 10 }
}

#[derive(Debug,Clone,Hash,PartialEq,Eq)]
pub struct _100;
impl Cardinality for _100 {
    fn value() -> usize { 100 }
}

/// Kindler et.al style Kset domain. Domain elements are sets of concrete values. Sets have a
/// maximum cardinality. Every set larger than that is equal the lattice join. The partial order is
/// set inclusion.
#[derive(Debug,Clone,Hash,PartialEq,Eq)]
pub enum Kset<N: Cardinality> {
    /// Lattice join. Sets larger than N.
    Join,
    /// Set of concrete values sorted by value. The set is never empty and never larger than
    /// N.
    Set(Vec<Constant>),
    /// Lattice meet, equal to the empty set.
    Meet{ marker: PhantomData<N> },
}

impl<N: Cardinality> Kset<N> {
    fn permute(a: &Self, b: &Self, f: &Fn(Value, Value) -> Result<Value>) -> Result<Kset<N>> {
        match (a, b) {
            (&Kset::Join, _) => Ok(Kset::Join),
            (_, &Kset::Join) => Ok(Kset::Join),
            (&Kset::Set(ref a), &Kset::Set(ref b)) => {
                let mut res = Vec::with_capacity(a.len() * b.len());

                for &Constant{ value: val_a, bits: bits_a } in a.iter() {
                    for &Constant{ value: val_b, bits: bits_b } in b.iter() {
                        let bits = cmp::max(bits_a, bits_b);
                        let a = Constant{ value: val_a, bits: bits };
                        let b = Constant{ value: val_b, bits: bits };

                        match f(Value::Constant(a), Value::Constant(b))? {
                            Value::Constant(x) if res.len() < N::value() => {
                                match res.binary_search(&x) {
                                    Ok(_) => {},
                                    Err(pos) => {
                                        res.insert(pos,x);
                                    }
                                }
                            }
                            _ => {
                                return Ok(Kset::Join);
                            }
                        }
                    }
                }

                if res.is_empty() {
                    Ok(Kset::default())
                } else {
                    Ok(Kset::Set(res))
                }
            }
            _ => Ok(Kset::default()),
        }
    }

    fn map(a: &Value, res: &Metadata<Kset<N>>, f: &Fn(Value) -> Result<Value>) -> Result<Kset<N>> {
        match &*res.value(a) {
            &Kset::Set(ref a) => {
                let mut res = Vec::with_capacity(a.len());

                for a in a.iter() {
                    match f(Value::Constant(*a))? {
                        Value::Constant(x) if res.len() < N::value() => {
                            match res.binary_search(&x) {
                                Ok(_) => {},
                                Err(pos) => {
                                    res.insert(pos,x);
                                }
                            }
                        }
                        _ => {
                            return Ok(Kset::Join);
                        }
                    }
                }

                if res.is_empty() {
                    Ok(Kset::default())
                } else {
                    Ok(Kset::Set(res))
                }
            }
            a => {
                Ok(a.clone())
            }
        }
    }

    pub fn add(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::Add(a, b)))
    }

    pub fn subtract(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::Subtract(a, b)))
    }

    pub fn multiply(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::Multiply(a, b)))
    }

    pub fn divide_signed(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::DivideSigned(a, b)))
    }

    pub fn divide_unsigned(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::DivideUnsigned(a, b)))
    }

    pub fn left_shift(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::ShiftLeft(a, b)))
    }

    pub fn right_shift_signed(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::ShiftRightSigned(a, b)))
    }

    pub fn right_shift_unsigned(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::ShiftRightUnsigned(a, b)))
    }

    pub fn and(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::And(a, b)))
    }

    pub fn or(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::InclusiveOr(a, b)))
    }

    pub fn xor(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::ExclusiveOr(a, b)))
    }

    pub fn modulo(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::Modulo(a, b)))
    }

    pub fn equal(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::Equal(a, b)))
    }

    pub fn less_or_equal_unsigned(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::LessOrEqualUnsigned(a, b)))
    }

    pub fn less_or_equal_signed(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::LessOrEqualSigned(a, b)))
    }

    pub fn less_unsigned(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::LessUnsigned(a, b)))
    }

    pub fn less_signed(a: &Self, b: &Self) -> Result<Self> {
        Self::permute(a,b,&|a,b| Operation::execute(&Operation::LessSigned(a, b)))
    }
}

impl<N: Cardinality> Default for Kset<N> {
    fn default() -> Kset<N> { Kset::Meet{ marker: PhantomData::default() } }
}

impl<N: Cardinality> From<Value> for Kset<N> {
    fn from(v: Value) -> Kset<N> {
        match v {
            Value::Constant(c) => Kset::Set(vec![c]),
            _ => Kset::Join,
        }
    }
}

impl<N: Cardinality> fmt::Display for Kset<N> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Kset::Meet{ .. } => write!(f, "Ø"),
            &Kset::Set(ref vec) if vec.is_empty() => write!(f, "Ø"),
            &Kset::Set(ref vec) if vec.len() == 1 => write!(f, "{{0x{:x}}}", vec[0].value),
            &Kset::Set(ref vec) => {
                write!(f, "{{0x{:x}", vec[0].value)?;
                for c in vec.iter() {
                    write!(f, ", 0x{:x}", c.value)?;
                }
                write!(f, "}}")
            }
            &Kset::Join => write!(f, "⫟"),
        }
    }
}

impl<N: Cardinality> Avalue for Kset<N> {
    fn execute(stmt: &Statement, res: &Metadata<Kset<N>>) -> Result<Self> {
        use types::Statement::*;
        use types::Operation::*;

        match stmt {
            &Expression{ op: And(ref a, ref b),.. } => Self::and(&*res.value(a),&*res.value(b)),
            &Expression{ op: InclusiveOr(ref a, ref b),.. } => Self::or(&*res.value(a),&*res.value(b)),
            &Expression{ op: ExclusiveOr(ref a, ref b),.. } => Self::xor(&*res.value(a),&*res.value(b)),
            &Expression{ op: Add(ref a, ref b),.. } => Self::add(&*res.value(a),&*res.value(b)),
            &Expression{ op: Subtract(ref a, ref b),.. } => Self::subtract(&*res.value(a),&*res.value(b)),
            &Expression{ op: Multiply(ref a, ref b),.. } => Self::multiply(&*res.value(a),&*res.value(b)),
            &Expression{ op: DivideSigned(ref a, ref b),.. } => Self::divide_signed(&*res.value(a),&*res.value(b)),
            &Expression{ op: DivideUnsigned(ref a, ref b),.. } => Self::divide_unsigned(&*res.value(a),&*res.value(b)),
            &Expression{ op: Modulo(ref a, ref b),.. } => Self::modulo(&*res.value(a),&*res.value(b)),
            &Expression{ op: ShiftRightSigned(ref a, ref b),.. } => Self::right_shift_signed(&*res.value(a),&*res.value(b)),
            &Expression{ op: ShiftRightUnsigned(ref a, ref b),.. } => Self::right_shift_unsigned(&*res.value(a),&*res.value(b)),
            &Expression{ op: ShiftLeft(ref a, ref b),.. } => Self::left_shift(&*res.value(a),&*res.value(b)),

            &Expression{ op: LessOrEqualSigned(ref a, ref b),.. } => Self::less_or_equal_signed(&*res.value(a),&*res.value(b)),
            &Expression{ op: LessOrEqualUnsigned(ref a, ref b),.. } => Self::less_or_equal_unsigned(&*res.value(a),&*res.value(b)),
            &Expression{ op: LessSigned(ref a, ref b),.. } => Self::less_signed(&*res.value(a),&*res.value(b)),
            &Expression{ op: LessUnsigned(ref a, ref b),.. } => Self::less_unsigned(&*res.value(a),&*res.value(b)),
            &Expression{ op: Equal(ref a, ref b),.. } => Self::equal(&*res.value(a),&*res.value(b)),

            &Expression{ op: Move(ref a),.. } => Self::map(a, res, &|a| Operation::execute(&Move(a))),
            &Expression{ op: Initialize(_,_),.. } => Ok(Kset::Join),
            &Expression{ op: ZeroExtend(ref sz, ref a),.. } => Self::map(a, res, &|a| Operation::execute(&ZeroExtend(*sz, a))),
            &Expression{ op: SignExtend(ref sz, ref a),.. } => Self::map(a, res, &|a| Operation::execute(&SignExtend(*sz, a))),
            &Expression{ op: Select(off, sz, ref a),.. } => Self::map(a, res, &|a| Operation::execute(&Select(off, sz, a))),

            &Expression{ op: Load(_,_,_,_),.. } => Ok(Kset::Join),

            &Expression{ op: Phi(ref a@Value::Variable(_),Value::Undefined,Value::Undefined),.. } => {
                Ok((*res.value(a)).clone())
            }
            &Expression{ op: Phi(ref a@Value::Variable(_),ref b@Value::Variable(_),Value::Undefined),.. } => {
                let a = &*res.value(a);
                let b = &*res.value(b);

                Ok(a.combine(b))
            }
            &Expression{ op: Phi(ref a@Value::Variable(_),ref b@Value::Variable(_),ref c@Value::Variable(_)),.. } => {
                let a = &*res.value(a);
                let b = &*res.value(b);
                let c = &*res.value(c);

                Ok(a.combine(b).combine(c))
            }
            &Expression{ op: Phi(_,_,_),.. } => Ok(Kset::Join),
            &Expression{ op: Assume(ref c,ref a),.. } => {
                Ok(res.value(a).narrow(c))
            }

            &Flow{ .. } => Ok(Kset::Join),
            &Memory{ .. } => Ok(Kset::Join),
        }
    }

    fn combine(&self, a: &Self) -> Self {
        match (self, a) {
            (&Kset::Join, _) => Kset::Join,
            (_, &Kset::Join) => Kset::Join,
            (a, &Kset::Meet{ .. }) => a.clone(),
            (&Kset::Meet{ .. }, b) => b.clone(),
            (&Kset::Set(ref a), &Kset::Set(ref b)) => {
                let mut ret = a.iter().chain(b.iter()).cloned().collect::<Vec<_>>();

                ret.sort();
                ret.dedup();
                if ret.is_empty() {
                    Kset::default()
                } else if ret.len() > N::value() {
                    Kset::Join
                } else {
                    Kset::Set(ret)
                }
            }
        }
    }

    fn widen(&self, other: &Self) -> Self {
        if *other == *self {
            self.clone()
        } else {
            Kset::Join
        }
    }

    fn is_better(&self, a: &Self) -> bool {
        if self == a {
            false
        } else {
            match (self, a) {
                (&Kset::Join, _) => true,
                (_, &Kset::Meet{ .. }) => true,
                (&Kset::Set(ref a), &Kset::Set(ref b)) => a.len() > b.len(),
                _ => false,
            }
        }
    }

    fn narrow(&self, constr: &Constraint) -> Self {
        match (constr,self) {
            (&Constraint::Empty{ .. },_) => Kset::Meet{ marker: PhantomData::default() },
            (&Constraint::Unsigned{ from, to,.. },&Kset::Set(ref vec)) => {
                let new = vec.iter().filter(|&&Constant{ value,.. }| {
                    value >= from && value <= to
                }).cloned().collect::<Vec<_>>();

                if new.is_empty() {
                    Kset::Meet{ marker: PhantomData::default() }
                } else {
                    Kset::Set(new)
                }
            }
            (&Constraint::Signed{ from, to,.. },&Kset::Set(ref vec)) => {
                let new = vec.iter().filter(|&&Constant{ value, bits }| {
                    let value = Constraint::signed(value,bits);
                    value >= from && value <= to
                }).cloned().collect::<Vec<_>>();

                if new.is_empty() {
                    Kset::Meet{ marker: PhantomData::default() }
                } else {
                    Kset::Set(new)
                }
            }
            (&Constraint::Unsigned{ from, to, bits },&Kset::Join) if to - from <= N::value() as u64 => {
                let new = (from..(to+1)).into_iter().map(|v| Constant{ value: v, bits: bits }).collect::<Vec<_>>();
                Kset::Set(new)
            }
            (&Constraint::Signed{ from, to, bits },&Kset::Join) if to.saturating_sub(from) <= N::value() as i64 => {
                let new = (from..(to+1)).into_iter().map(|v| Constant{ value: v as u64, bits: bits }).collect::<Vec<_>>();
                Kset::Set(new)
            }
            (_,k) => k.clone()
        }
    }
}

impl<N: 'static + Cardinality> Arbitrary for Kset<N> {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        let variant = g.gen_range(0, 3);
        match variant {
            0 => Kset::default(),
            1 => Kset::Set(Vec::<Constant>::arbitrary(g)),
            2 => Kset::Join,
            _ => unreachable!(),
        }
    }
}

impl<N: Cardinality> ToConstants for Kset<N> {
    type Iterator = IntoIter<Constant>;

    fn to_constants(&self) -> Self::Iterator {
        let v = match self {
            &Kset::Meet{ .. } => vec![],
            &Kset::Set(ref v) => v.clone(),
            &Kset::Join => vec![],
        };
        v.into_iter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use types::{TestArch, Region, Function, Name, UUID};
    use {approximate,NoMemory,Widening};
    use dflow::rewrite_to_ssa;
    use simple_logger;

    /*
     * (B0)
     * 0:  Ma10
     * 4:  Mb0
     * 7:  Mc4
     * 10: Cfc1
     * 14: Bf32
     *
     * (B1)
     * 18: Aaa5
     * 22: Abac
     * 26: Mc2
     * 29: J54
     *
     * (B2)
     * 32: Cfa0
     * 36: Bf54
     * 40: Aaa1
     * 44: Abb3
     * 48: Mc3
     * 51: J32
     *
     * (B3)
     * 54: Axab
     * 58: Mcc
     * 61: Maa
     * 64: Mbb
     * 67: R
     */
    #[test]
    fn kset_no_widening() {
        use petgraph::dot::Dot;

        let _ = simple_logger::init();
        let data = b"Ma10Mb0Mc4Cfc1Bf32Aaa5AbacMc2J54Cfa0Bf54Aaa1Abb3Mc3J32AxabMccMaaMbbR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let nam_a4 = func.names.insert(&Name::new("a".into(),4));
        let nam_b4 = func.names.insert(&Name::new("b".into(),4));
        let nam_c4 = func.names.insert(&Name::new("c".into(),4));
        let nam_x0 = func.names.insert(&Name::new("x".into(),0));
        let vals = approximate::<Kset<_10>,NoMemory<Kset<_10>>,_>(&func,None).unwrap();

        debug!("{:?}",vals);
        debug!("{:?}",Dot::new(func.cflow_graph()));
        for bb in func.basic_blocks() {
            let mut i = bb.1.statements.start;

            debug!("");
            debug!("# loc_{}",bb.1.area.start);
            for mne in func.mnemonics(bb.0) {
                debug!("{:?}: {}",mne.1.area,func.strings.value(mne.1.opcode).unwrap());
                for stmt in func.statements(i..(i + mne.1.statement_count)) {
                    debug!("  {:?}",stmt);
                }
                i += mne.1.statement_count;
            }
        }

        assert_eq!(vals.get(nam_a4), &Kset::Join);
        assert_eq!(vals.get(nam_b4), &Kset::Join);
        assert_eq!(vals.get(nam_c4), &Kset::Set(vec![
            Constant::new(2,32).unwrap(),
            Constant::new(3,32).unwrap(),
            Constant::new(4,32).unwrap()]));
        assert_eq!(vals.get(nam_x0), &Kset::Join);
    }

    #[test]
    fn kset_widening() {
        let _ = simple_logger::init();
        let data = b"Ma10Mb0Mc4Cfc1Bf32Aaa5AbacMc2J54Cfa0Bf54Aaa1Abb3Mc3J32AxabMccMaaMbbR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let nam_a4 = func.names.insert(&Name::new("a".into(),4));
        let nam_b4 = func.names.insert(&Name::new("b".into(),4));
        let nam_c4 = func.names.insert(&Name::new("c".into(),4));
        let nam_x0 = func.names.insert(&Name::new("x".into(),0));
        let vals = approximate::<Widening<Kset<_10>>,NoMemory<Widening<Kset<_10>>>,_>(&func,None).unwrap();

        assert_eq!(vals.get(nam_a4).child(), &Kset::Join);
        assert_eq!(vals.get(nam_b4).child(), &Kset::Join);
        assert_eq!(vals.get(nam_c4).child(), &Kset::Join);
        assert_eq!(vals.get(nam_x0).child(), &Kset::Join);
    }
}
