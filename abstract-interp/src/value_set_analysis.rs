// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//! Bounded Address Tracking.
//!
//! This abstract domain is used to resolve indirect jumps. Domain elements have the form
//! `<register> +/- <integer>`. For example `eax + 3" or "rbp - 5`. A special GLOBAL register is
//! used to represent absolute values. The (absolute) address 0x11223344 would be handled as
//! `GLOBAL + 0x11223344`.
//!
//! For a complete description of Bounded Address Tracking see the original paper "Precise Static
//! Analysis of Untrusted Driver Binaries" by J. Kinder and H. Veith.

use {Avalue, Metadata, MetadataMap, ToConstants, ToAlocs, Aloc};
use types::{Value, Result, NameRef, Statement, Constant, Constraint};
use quickcheck::{Arbitrary, Gen};
use std::borrow::Cow;

/// Bounded Address Tracking domain element
///
/// Follows Kinder et.al. The GLOBAL region is represented by None.
#[derive(Debug,PartialEq,Eq,Clone,Hash)]
pub enum ValueSetAnalysis<A: Avalue> {
    /// Lattice top
    Join,
    /// Pointer pointing somewhere into `region`
    Offset{
        /// Name of pointed to region and pointer version. None stands for the global region.
        region: Option<NameRef>,
        /// Offset into region.
        offset: A,
    },
    /// Lattice bottom
    Meet,
}

impl<A: Avalue> From<Value> for ValueSetAnalysis<A> {
    fn from(v: Value) -> Self {
        use std::cmp;
        use types::Variable;

        match v {
            v@Value::Constant(_) | v@Value::Undefined => {
                ValueSetAnalysis::Offset{
                    region: None,
                    offset: A::from(v)
                }
            }
            Value::Variable(Variable{ name, bits }) => {
                ValueSetAnalysis::Offset{
                    region: Some(name),
                    offset: A::from(Value::val(0, cmp::max(1,bits)).unwrap())
                }
            }
        }
    }
}

impl<A: Avalue> Default for ValueSetAnalysis<A> {
    fn default() -> Self {
        ValueSetAnalysis::Meet
    }
}

impl<A: Avalue> ValueSetAnalysis<A> {
    fn project<'a>(val: Cow<'a,ValueSetAnalysis<A>>) -> Cow<'a,A> {
        match val {
            Cow::Borrowed(&ValueSetAnalysis::Join) | Cow::Owned(ValueSetAnalysis::Join) => {
                Cow::Owned(A::from(Value::Undefined))
            }
            Cow::Borrowed(&ValueSetAnalysis::Offset{ region: None, ref offset }) => {
                Cow::Borrowed(offset)
            }
            Cow::Owned(ValueSetAnalysis::Offset{ region: None, offset }) => {
                Cow::Owned(offset)
            }
            Cow::Borrowed(&ValueSetAnalysis::Offset{ .. }) | Cow::Owned(ValueSetAnalysis::Offset{ .. }) => {
                Cow::Owned(A::from(Value::Undefined))
            }
            Cow::Borrowed(&ValueSetAnalysis::Meet) | Cow::Owned(ValueSetAnalysis::Meet) => {
                Cow::Owned(A::default())
            }
        }
    }

    fn project_ignore_region<'a>(val: Cow<'a,ValueSetAnalysis<A>>) -> Cow<'a,A> {
        match val {
            Cow::Borrowed(&ValueSetAnalysis::Join) | Cow::Owned(ValueSetAnalysis::Join) => {
                Cow::Owned(A::from(Value::Undefined))
            }
            Cow::Borrowed(&ValueSetAnalysis::Offset{ ref offset,.. }) => {
                Cow::Borrowed(offset)
            }
            Cow::Owned(ValueSetAnalysis::Offset{ offset,.. }) => {
                Cow::Owned(offset)
            }
            Cow::Borrowed(&ValueSetAnalysis::Meet) | Cow::Owned(ValueSetAnalysis::Meet) => {
                Cow::Owned(A::default())
            }
        }
    }
}

impl<A: Avalue> Avalue for ValueSetAnalysis<A> {
    fn execute(stmt: &Statement, res: &Metadata<Self>) -> Result<Self> {
        use types::Operation::*;
        use types::Statement::*;

        match stmt {
            &Expression{ ref op, ref result } => {
                let join = ValueSetAnalysis::Offset{
                    region: Some(result.name),
                    offset: Value::val(0,result.bits)?.into(),
                };

                match op {
                    &Add(ref a, ref b) | &Subtract(ref a,ref b) => {
                        let a = &*res.value(a);
                        let b = &*res.value(b);

                        match (a,b) {
                            (&ValueSetAnalysis::Offset{ region: None,.. },
                             &ValueSetAnalysis::Offset{ region: None,.. }) => {
                                let meta = MetadataMap::<ValueSetAnalysis<A>,A,_>::new(res,&Self::project);
                                let offset = A::execute(stmt,&meta)?;
                                let ret = ValueSetAnalysis::Offset{
                                    region: None,
                                    offset: offset,
                                };

                                Ok(ret)
                            }
                            (&ValueSetAnalysis::Offset{ region: Some(r),.. },
                             &ValueSetAnalysis::Offset{ region: None,.. }) => {
                                let meta = MetadataMap::<ValueSetAnalysis<A>,A,_>::new(res,&Self::project_ignore_region);
                                let offset = A::execute(stmt,&meta)?;
                                let ret = ValueSetAnalysis::Offset{
                                    region: Some(r),
                                    offset: offset,
                                };

                                Ok(ret)
                            }
                            (&ValueSetAnalysis::Offset{ region: None,.. },
                             &ValueSetAnalysis::Offset{ region: Some(r),.. }) => {
                                let meta = MetadataMap::<ValueSetAnalysis<A>,A,_>::new(res,&Self::project_ignore_region);
                                let offset = A::execute(stmt,&meta)?;
                                let ret = ValueSetAnalysis::Offset{
                                    region: Some(r),
                                    offset: offset,
                                };

                                Ok(ret)
                            }

                            _ => Ok(join)
                        }
                    }

                    &Phi(ref a@Value::Variable(_),Value::Undefined,Value::Undefined) => {
                        Ok((*res.value(a)).clone())
                    }
                    &Phi(ref a@Value::Variable(_),ref b@Value::Variable(_),Value::Undefined) => {
                        let a = &*res.value(a);
                        let b = &*res.value(b);

                        Ok(a.combine(b))
                    }
                    &Phi(ref a@Value::Variable(_),ref b@Value::Variable(_),ref c@Value::Variable(_)) => {
                        let a = &*res.value(a);
                        let b = &*res.value(b);
                        let c = &*res.value(c);

                        Ok(a.combine(b).combine(c))
                    }
                    &Phi(_,_,_) => Ok(join),
                    &Assume(ref c,ref a) => {
                        Ok(res.value(a).narrow(c))
                    }
                    &Initialize(_,_) => Ok(join),

                    &ZeroExtend(_, ref a) | &Move(ref a) => {
                        let meta = MetadataMap::<ValueSetAnalysis<A>,A,_>::new(res,&Self::project_ignore_region);
                        let offset = A::execute(stmt,&meta)?;
                        let ret = match &*res.value(a) {
                            &ValueSetAnalysis::Offset{ ref region,.. } => {
                                ValueSetAnalysis::Offset{
                                    region: region.clone(),
                                    offset: offset,
                                }
                            }
                            _ => {
                                ValueSetAnalysis::Offset{
                                    region: None,
                                    offset: offset,
                                }
                            }
                        };

                        Ok(ret)
                    }

                    _ => {
                        let meta = MetadataMap::<ValueSetAnalysis<A>,A,_>::new(res,&Self::project);
                        let offset = A::execute(stmt,&meta)?;
                        let ret = ValueSetAnalysis::Offset{
                            region: None,
                            offset: offset,
                        };

                        Ok(ret)
                    }
                }
            }

            &Flow{ .. } => Ok(ValueSetAnalysis::Join),
            &Memory{ .. } => Ok(ValueSetAnalysis::Join),
        }
    }

    fn combine(&self, a: &Self) -> Self {
        if *a == *self {
            a.clone()
        } else {
            match (self, a) {
                (&ValueSetAnalysis::Join, _) => ValueSetAnalysis::Join,
                (_, &ValueSetAnalysis::Join) => ValueSetAnalysis::Join,
                (&ValueSetAnalysis::Offset { region: ref reg_a, offset: ref off_a },
                 &ValueSetAnalysis::Offset { region: ref reg_b, offset: ref off_b }) => {
                    if reg_a == reg_b {
                        ValueSetAnalysis::Offset { region: reg_a.clone(), offset: off_a.combine(off_b) }
                    } else {
                        ValueSetAnalysis::Join
                    }
                }
                (&ValueSetAnalysis::Meet, b) => b.clone(),
                (a, &ValueSetAnalysis::Meet) => a.clone(),
            }
        }
    }

    fn widen(&self, _: &Self) -> Self {
        ValueSetAnalysis::Join
    }

    fn is_better(&self, a: &Self) -> bool {
        if self == a {
            false
        } else {
            match (self, a) {
                (&ValueSetAnalysis::Join, _) => false,
                (_, &ValueSetAnalysis::Join) => true,
                (_, &ValueSetAnalysis::Meet) => false,
                (&ValueSetAnalysis::Meet, _) => true,
                (&ValueSetAnalysis::Offset { .. }, &ValueSetAnalysis::Offset { .. }) => false,
            }
        }
    }

    fn narrow(&self,c: &Constraint) -> Self {
        match self {
            &ValueSetAnalysis::Offset{ region: None, ref offset } => {
                ValueSetAnalysis::Offset{
                    region: None,
                    offset: offset.narrow(c)
                }
            }
            &ValueSetAnalysis::Join => {
                ValueSetAnalysis::Offset{
                    region: None,
                    offset: A::from(Value::Undefined).narrow(c)
                }
            }
            a => a.clone()
        }
    }
}

pub struct VsaAlocIterator<A: ToConstants> {
    region: Option<NameRef>,
    iter: Option<A::Iterator>,
}

impl<A: ToConstants> Iterator for VsaAlocIterator<A> {
    type Item = Aloc;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(ref mut iter) = self.iter {
            if let Some(Constant{ value, bits}) = iter.next() {
                Some(Aloc{
                    base: self.region,
                    offset: value,
                    bytes: bits / 8,
                })
            } else {
                None
            }
        } else {
            None
        }
    }
}

impl<A: ToConstants> ToAlocs for ValueSetAnalysis<A> {
    type Iterator = VsaAlocIterator<A>;

    fn to_alocs(&self) -> Self::Iterator {
        match self {
            &ValueSetAnalysis::Offset{ ref region, ref offset } => {
                VsaAlocIterator{
                    region: region.clone(),
                    iter: Some(offset.to_constants()),
                }
            }
            _ => {
                VsaAlocIterator{
                    region: None,
                    iter: None
                }
            }
        }
    }
}

impl<A: 'static + Avalue + Arbitrary> Arbitrary for ValueSetAnalysis<A> {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        let variant = g.gen_range(0, 3);
        match variant {
            0 => ValueSetAnalysis::Meet,
            1 => {
                let reg = if g.gen::<bool>() {
                    Some(NameRef::arbitrary(g))
                } else {
                    None
                };

                ValueSetAnalysis::Offset {
                    region: reg,
                    offset: A::arbitrary(g),
                }
            }
            2 => ValueSetAnalysis::Join,
            _ => unreachable!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use quickcheck::TestResult;
    use simple_logger;
    use types::{Region, TestArch, Function, Name, Segment, UUID};
    use dflow::{rewrite_to_ssa,rewrite_with_assume};
    use {approximate,ValueSets,Amemory,ToAlocs,CircLinearProg,Kset,_100};

    quickcheck! {
        fn qc_combine_clp(a: ValueSetAnalysis<CircLinearProg>, b: ValueSetAnalysis<CircLinearProg>) -> bool {
            let c = a.combine(&b);

            debug!("a={:?}, b={:?}, c={:?}",a,b,c);
            !c.is_better(&a) && !c.is_better(&b)
        }
    }

    quickcheck! {
        fn qc_widen_clp(a: ValueSetAnalysis<CircLinearProg>, b: ValueSetAnalysis<CircLinearProg>) -> TestResult {
            // widening op is only defined for increasing sequences
            if a.is_better(&b) {
                let c = a.widen(&b);

                debug!("a={:?}, b={:?}, c={:?}",a,b,c);
                // a <= (a V b) >= b
                TestResult::from_bool(!c.is_better(&a) && !c.is_better(&b))
            } else {
                TestResult::discard()
            }
        }
    }

    quickcheck! {
        fn qc_combine_kset(a: ValueSetAnalysis<Kset<_100>>, b: ValueSetAnalysis<Kset<_100>>) -> bool {
            let c = a.combine(&b);

            debug!("a={:?}, b={:?}, c={:?}",a,b,c);
            !c.is_better(&a) && !c.is_better(&b)
        }
    }

    quickcheck! {
        fn qc_widen_kset(a: ValueSetAnalysis<Kset<_100>>, b: ValueSetAnalysis<Kset<_100>>) -> TestResult {
            // widening op is only defined for increasing sequences
            if a.is_better(&b) {
                let c = a.widen(&b);

                debug!("a={:?}, b={:?}, c={:?}",a,b,c);
                // a <= (a V b) >= b
                TestResult::from_bool(!c.is_better(&a) && !c.is_better(&b))
            } else {
                TestResult::discard()
            }
        }
    }


    /*
     * Ss42
     * Ass4
     * Ss23
     * R
     */
    #[test]
    fn track_stack_value() {
        let _ = simple_logger::init();
        let data = b"Ss42Ass4Ss23R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let nam_s1 = func.names.insert(&Name::new("s".into(),0));
        let nam_s2 = func.names.insert(&Name::new("s".into(),1));
        let nam_seg = func.segments.insert(&Name::new("ram".into(),2));

        for bb in func.basic_blocks() {
            let mut i = bb.1.statements.start;

            debug!("");
            debug!("# loc_{}",bb.1.area.start);
            for mne in func.mnemonics(bb.0) {
                debug!("{:?}: {}",mne.1.area,func.strings.value(mne.1.opcode).unwrap());
                for stmt in func.statements(i..(i + mne.1.statement_count)) {
                    debug!("  {:?}",stmt);
                }
                i += mne.1.statement_count;
            }
        }

        let vals = approximate::<ValueSetAnalysis<Kset<_100>>,ValueSets<ValueSetAnalysis<Kset<_100>>>,_>(&func,None).unwrap();
        let val_s1 = vals.get(nam_s1);
        let val_s2 = vals.get(nam_s2);
        let seg = vals.get_segment(&Segment{ name: nam_seg });

        debug!("seg {:?}",seg);
        debug!("vals {:?} {:?}",val_s1,val_s2);

        let mem_s1 = seg.load(val_s1).unwrap();
        let mem_s2 = seg.load(val_s2).unwrap();
        let aloc_s1 = mem_s1.to_alocs().collect::<Vec<_>>();
        let aloc_s2 = mem_s2.to_alocs().collect::<Vec<_>>();

        assert_eq!(aloc_s1.len(), 1);
        assert_eq!(aloc_s1[0], Aloc{ base: None, offset: 42, bytes: 4 });
        assert_eq!(aloc_s2.len(), 1);
        assert_eq!(aloc_s2[0], Aloc{ base: None, offset: 23, bytes: 4 });
    }

    /*
     * Table of 11 32 bit words at 0
     *
     * 0:  Aii4294967292 ; -4
     * 13: Cfi10
     * 18: Bf28
     *
     * 22: Mp0
     * 25: J35
     *
     * (BB1)
     * 28: Xi4i
     * 32: Lpi
     *
     * 35: Mpp
     * 38: R
     */
    #[test]
    fn jump_table() {
        let _ = simple_logger::init();
        let code = b"Aii4294967292Cfi10Bf28Mp0J35Xi4iLpiMppR".to_vec();
        let data = b"\x41\x00\x00\x00\x42\x00\x00\x00\x43\x00\x00\x00\x44\x00\x00\x00\x44\x00\x00\x00\x45\x00\x00\x00\x46\x00\x00\x00\x47\x00\x00\x00\x48\x00\x00\x00\x49\x00\x00\x00\x50\x00\x00\x00\x51\x00\x00\x00".to_vec();
        let code_reg = Region::from_buf("", 16, code, 0, None);
        let data_reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &code_reg, UUID::now()).unwrap();
        let _ = rewrite_with_assume(&mut func).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let mem = ValueSets::from_region(&data_reg);
        let vals = approximate::<Kset<_100>,ValueSets<Kset<_100>>,_>(&func,mem).unwrap();
        let nam = NameRef::new(9);
        let val = vals.get(nam);
        let aloc = val.to_alocs().collect::<Vec<_>>();

        assert_eq!(aloc.len(), 10);
        assert_eq!(aloc[0], Aloc{ base: None, offset: 0x41, bytes: 4 });
        assert_eq!(aloc[1], Aloc{ base: None, offset: 0x42, bytes: 4 });
        assert_eq!(aloc[2], Aloc{ base: None, offset: 0x43, bytes: 4 });
        assert_eq!(aloc[3], Aloc{ base: None, offset: 0x44, bytes: 4 });
        assert_eq!(aloc[4], Aloc{ base: None, offset: 0x45, bytes: 4 });
        assert_eq!(aloc[5], Aloc{ base: None, offset: 0x46, bytes: 4 });
        assert_eq!(aloc[6], Aloc{ base: None, offset: 0x47, bytes: 4 });
        assert_eq!(aloc[7], Aloc{ base: None, offset: 0x48, bytes: 4 });
        assert_eq!(aloc[8], Aloc{ base: None, offset: 0x49, bytes: 4 });
        assert_eq!(aloc[9], Aloc{ base: None, offset: 0x50, bytes: 4 });
    }
}
