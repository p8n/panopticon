// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use types::{Endianess,Region,Value,Constant,NameRef,Result};
use {Avalue,Amemory};
use std::default::Default;
use std::marker::PhantomData;
use std::collections::HashMap;
use std::vec::IntoIter;
use std::cmp::Ordering;

pub trait ToConstants: Avalue {
    type Iterator: Iterator<Item=Constant>;
    fn to_constants(&self) -> Self::Iterator;
}

#[derive(Debug,Clone,Hash,PartialEq,Eq)]
pub struct Aloc {
    pub base: Option<NameRef>,
    pub offset: u64,
    pub bytes: usize,
}

pub trait ToAlocs: Avalue {
    type Iterator: Iterator<Item=Aloc>;
    fn to_alocs(&self) -> Self::Iterator;
}

impl<A: ToConstants> ToAlocs for A {
    type Iterator = IntoIter<Aloc>;

    fn to_alocs(&self) -> Self::Iterator {
        let ret = self.to_constants().map(|x| {
            Aloc{
                base: None,
                offset: x.value,
                bytes: x.bits / 8
            }
        }).collect::<Vec<_>>();
        ret.into_iter()
    }
}

#[derive(Debug,Clone)]
pub struct NoMemory<A: Avalue> {
    marker: PhantomData<A>,
}

impl<A: Avalue> Default for NoMemory<A> {
    fn default() -> Self {
        NoMemory{
            marker: PhantomData::default()
        }
    }
}

impl<A: Avalue> Amemory<A> for NoMemory<A> {
    fn load(&self,_: &A) -> Result<A> {
        Ok(A::from(Value::Undefined))
    }

    fn store(&self,_: &A,_: &A) -> Result<Self> {
        Ok(NoMemory::default())
    }

    fn combine(&self,_: &Self) -> Self {
        Self::default()
    }
}

#[derive(Debug,Clone)]
pub struct ValueSets<'a,A: ToAlocs> {
    memory: HashMap<Aloc,A>,
    regions: Box<[&'a Region]>,
}

impl<'a,A: ToAlocs> ValueSets<'a,A> {
    pub fn new(memory: HashMap<Aloc,A>, regs: Box<[&'a Region]>) -> Self {
        ValueSets{
            memory: memory,
            regions: regs,
        }
    }

    pub fn from_region(reg: &'a Region) -> Self {
        ValueSets{
            memory: HashMap::default(),
            regions: vec![reg].into_boxed_slice(),
        }
    }

    pub fn from_sparse(memory: HashMap<Aloc,A>) -> Self {
        ValueSets{
            memory: memory,
            regions: Default::default(),
        }
    }
}

impl<'a,A: ToAlocs> Default for ValueSets<'a,A> {
    fn default() -> Self {
        ValueSets{
            memory: HashMap::default(),
            regions: Default::default(),
        }
    }
}

impl<'a,A: ToAlocs> Amemory<A> for ValueSets<'a,A> {
    fn load(&self, address: &A) -> Result<A> {
        let mut ret = A::default();

        'outer: for address in address.to_alocs() {
            if let Some(val) = self.memory.get(&address) {
                ret = ret.combine(val);
            } else if address.base.is_none() {
                let off = address.offset;
                let f = |reg: &&Region| if reg.is_defined(off..off + 1) {
                    Ordering::Equal
                } else {
                    reg.defined().start.cmp(&off)
                };

                match self.regions.binary_search_by(f).ok().map(|idx| &self.regions[idx]) {
                    Some(reg) => {
                        if let Ok(val) = reg.read_integer(address.offset,Endianess::Little,address.bytes) {
                            ret = ret.combine(&A::from(Value::val(val,address.bytes * 8)?));
                        }
                    }
                    None => {}
                }
            }
        }

        Ok(ret)
    }

    fn store(&self,address: &A, value: &A) -> Result<Self> {
        let mut new = self.memory.clone();

        for address in address.to_alocs() {
            new.insert(address,value.clone());
        }

        Ok(ValueSets{ memory: new, regions: self.regions.clone() })
    }

    fn combine(&self, other: &Self) -> Self {
        let mut new = self.memory.clone();

        new.extend(other.memory.iter().map(|x| (x.0.clone(),x.1.clone())));
        ValueSets{ memory: new, regions: self.regions.clone() }
    }
}
