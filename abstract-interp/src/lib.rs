// Panopticon - A libre program analysis library for machine code
// Copyright (C) 2014-2018  The Panopticon Developers
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//! Abstract Interpretation Framework.
//!
//! Abstract Interpretation executes an program over sets of concrete values. Each operation is
//! extended to work on some kind of abstract value called abstract domain that approximates a
//! set of concrete values (called the concrete domain). A simple example is the domain of signs.
//! Each value can either be positive, negative, both or none. An abstract interpretation will first
//! replace all constant values with their signs and then execute all basic blocks using the
//! abstract sign domain. For example multiplying two positive values yields a positive value.
//! Adding a positive and a negative sign yields an abstract value representing both signs (called
//! join).

#[macro_use]
extern crate log;

#[cfg(test)]
#[macro_use]
extern crate quickcheck;

#[cfg(test)]
extern crate simple_logger;

extern crate p8n_types as types;
extern crate p8n_data_flow as dflow;
#[cfg(not(test))]
extern crate quickcheck;
extern crate petgraph;
extern crate num;
extern crate bit_set;

mod interpreter;
pub use interpreter::{Avalue,Amemory,Approximation,approximate};

mod metadata;
pub use metadata::{Metadata,MetadataMap,ProgramPoint};

mod memory;
pub use memory::{ToAlocs, ToConstants, Aloc, NoMemory, ValueSets};

mod sign;
pub use sign::Sign;

mod kset;
pub use kset::{Kset,Cardinality,_1,_10,_100};

mod circular_linear_prog;
pub use circular_linear_prog::CircLinearProg;

mod value_set_analysis;
pub use value_set_analysis::ValueSetAnalysis;

mod widening;
pub use widening::Widening;
