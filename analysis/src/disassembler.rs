/*
enum DisassemblerInput {
    Function(Function),
    Reference(Reference),
}

struct Disassembler<A> {
    type Future = futures::Future<Item=Function,Error=Error>;

    fn disassemble(&self, r: Reference) -> Option<Future>;
    fn continue(&self, f: Function) -> Future;
}

fn disassembler<A>(regions: &[Region]) -> Result<(Sink<DisassemblerInput>,Stream<Function>)>;

let Contents{ entry_points, regions,.. } = loader::load(fd)?;
let dis = Disassembler::<A>::new(&dead)?;
let ssa = SingleStaticAssignment::default();
let vsa = ValueSetAnalysis::default();
let res = Resolve::default();
let glibc = SimulateGlibc::default();
let stack = RecoverStackFrame::default();

fn disassemble(f: Function) -> Result<Function> {}
fn analyze(f: Function) -> Result<Function> {}

// XXX: gulianov?
// single function
let mut func = dis.disassemble(e)?;

// main disassembly loop. When a function exits this loop 
loop {
    let dead_end = tails.run(&mut func)?;
    for d in dead_end {
        mark_dead(&func);
    }

    ssa.run(&mut func)?;
    let values = vsa.run(&mut func)?;

    if simulate_start_main.run(&mut func, &values)? {
        ssa.run(&mut func)?;
        values = vsa.run(&mut func)?;
    } else if has_unresolved_jumps {
        if !res.run(&mut func, &values) {
            break;
        }
    } else {
        if !rename_plt_stubs(&mut func)? {
            // XXX: make lazy?
            let adj = guilfanovs_method(&mut func)?;
            /*
            if !apply_guilfanovs_adjustments(adj, &mut func)? {
                break;
            }
            */
        }
    }

    dis.continue_with(&mut func)?;
}

let callees = calls.run(&func)?;

for c in callees {
    try_schedule(c)?;
}

propagate.run(&mut func, &values)?;
stack.run(&mut func, &values)?;

let sum = summary.run(&mut func, values)?;
func.pack();

// call graph
for func in scc {
    func.unpack();

    let dead_end = tails.run(&mut func)?;
    for d in dead_end {
        mark_dead(&func);
    }

    if update_summary.run(&mut func, summaries)? {
        ssa.run(&mut func)?;
        value = vsa.run(&func)?;
        summaries[func] = summary.run(&func, values)?;
    }

    let types = types.run(&f, &values, &mut globals)?;
    func.pack();
}

// misc
let ast = decompile(&func, &values, &globals, &types)?;
let cfg = layout(&func, &metrics)?;
let vals = approximate(&func, &fixed)?;

// future
let diff = compare(&func1, &func2)?;
let model = unroll(&func, 100, &functions, &stubs)?;
// XXX: symbolic exec
*/

use types::{
    Function, Architecture, Result, CallTarget, Str, UUID, Machine, Image,
};
use dflow::{
    rewrite_with_assume, rewrite_to_ssa,
};
use absint::{
    ValueSets, Kset, _100, ValueSetAnalysis, approximate,
};
use {
    resolve_indirections, returns, update_summaries, glibc, Summary,
};

use std::iter;
use std::collections::{HashSet, HashMap};

enum QueueItem {
    Existing(Function),
    New{ name: Option<Str>, entry: u64, uuid: UUID },
}

#[derive(Clone, Debug)]
pub enum Event<'a> {
    Discover{ uuid: UUID, entry_address: u64, name: Str, },
    Update{ function: &'a Function, },
    Call{ from: UUID, to: UUID, },
    Failure{ function: UUID, message: Str, },
}

type Vsa = ValueSetAnalysis<Kset<_100>>;

fn queue_to_events<F: FnMut(Event)>(queue: &[QueueItem], mut f: F) {
    for q in queue.iter() {
        match q {
            &QueueItem::New{ ref name, entry, ref uuid } => {
                let ev = Event::Discover{
                    uuid: uuid.clone(),
                    entry_address: entry,
                    name: name.clone().unwrap_or(format!("fn_{:x}", entry).into())
                };

                f(ev);
            }
            &QueueItem::Existing(ref func) => {
                let ev = Event::Discover{
                    uuid: func.uuid().clone(),
                    entry_address: func.entry_address(),
                    name: func.name.clone(),
                };

                f(ev);
            }
        }
    }
}

pub fn disassemble<A: Architecture, F: FnMut(Event)>(init: A::Configuration, image: &Image, mut f: Option<F>) -> Result<Vec<Function>>
{
    let mut queue = image.known.into_iter().map(|r| QueueItem::New{ name: r.1.clone(), entry: r.0, uuid: UUID::now() }).collect::<Vec<_>>();
    let mut by_entry = image.symbolic.iter().map(|(&addr, name)| (addr, CallTarget::External(name.clone()))).collect::<HashMap<_,_>>();
    let mut dead_ends = HashSet::default();
    let mut done = Vec::default();
    let summaries = glibc::known_summaries(Machine::Amd64);

    // notify caller
    if let Some(ref mut f) = f { queue_to_events(&queue, f); }

    // intraprocedural
    while !queue.is_empty() {
        let (new_queue, new_done): (Vec<QueueItem>, Vec<Function>) = queue.into_iter().map(|i| -> (Vec<QueueItem>, Option<Function>) {
            let mut func = match i {
                QueueItem::New{ name, entry, uuid, } => {
                    let code = match image.region(entry) {
                        Some(code) => code,
                        None => { return (vec![], None); }
                    };
                    match Function::new::<A>(init.clone(), entry, code, uuid) {
                        Ok(mut func) => {
                            match name {
                                Some(name) => { func.name = name; }
                                None => { /* do nothing */ }
                            }

                            func
                        }
                        Err(e) => {
                            error!("{}", e);
                            return (vec![], None);
                        }
                    }
                }

                QueueItem::Existing(mut func) => {
                    let code = match image.region(func.entry_address()) {
                        Some(code) => code,
                        None => { return (vec![], None); }
                    };
                     match func.extend::<A>(init.clone(), code) {
                        Ok(()) => func,
                        Err(e) => {
                            error!("{}", e);
                            return (vec![], None);
                        }
                    }
                }
            };

            match analyze_intraproc(&mut func, &by_entry, &mut dead_ends, &summaries, image) {
                Ok((true, new)) => {
                    let new = iter::once(QueueItem::Existing(func))
                        .chain(new.into_iter()
                               .map(|x| QueueItem::New{ name: None, entry: x.0, uuid: x.1 }));

                    (new.collect(), None)
                }

                Ok((false, new)) => {
                    let new = new.into_iter()
                        .map(|x| QueueItem::New{ name: None, entry: x.0, uuid: x.1 });

                    (new.collect(), Some(func))
                }

                Err(e) => {
                    error!("{}", e);
                    (vec![], None)
                }
            }
        }).fold((vec![], vec![]), |(mut queue, mut done), (q, d)| {
            if let Some(ref mut f) = f { queue_to_events(&q, f); }
            queue.extend(q);

            for d in d {
                if let Some(ref mut f) = f { f(Event::Update{ function: &d }); }
                done.push(d);
            }
            (queue, done)
        });

        for d in new_done {
            by_entry.insert(d.entry_address(), CallTarget::Function(d.uuid().clone()));
            done.push(d);
        }

        queue = new_queue;
    }

    // interprocedural
    // XXX build call graph

    Ok(done)
}

// (rerun, new_targets)
fn analyze_intraproc(func: &mut Function, done: &HashMap<u64, CallTarget>,
                     dead_ends: &mut HashSet<CallTarget>,
                     summaries: &HashMap<CallTarget, Summary>,
                     image: &Image)
    -> Result<(bool, Vec<(u64, UUID)>)>
{
    use absint::Aloc;
    use types::{Value, Name};
    use std::iter::FromIterator;

    let ct = CallTarget::Function(func.uuid().clone());

    if func.name == "fn_db60" {
        println!("{}: {} ({})", func.name, done.contains_key(&func.entry_address()), func.uuid());
    }

    // check if this function returns
    if returns(func) {
        dead_ends.remove(&ct);
    } else {
        dead_ends.insert(ct);
    }

    update_summaries(func, summaries)?;
    rewrite_with_assume(func)?;
    rewrite_to_ssa(func)?;

    let map = HashMap::from_iter(image.symbolic.iter().map(|(&addr, name)| {
        let addr = Aloc{
            base: None,
            offset: addr,
            bytes: 4,
        };
        let nam = func.names.insert(&Name::new(name.clone(), None));
        let val = ValueSetAnalysis::Offset{
            region: Some(nam),
            offset: Kset::from(Value::val(0, 32).unwrap()),
        };

        (addr, val)
    }));
    //let memory = ValueSets::from_region(data);
    let memory = ValueSets::new(map, image.regions.iter().collect::<Vec<_>>().into_boxed_slice());
    let values = approximate::<Vsa, ValueSets<Vsa>, _>(&func, Some(memory))?;

    match resolve_indirections(func, done, &image.symbolic, &values) {
        (true, new) => {
            return Ok((true, new));
        }
        (false, _) => { /* continue */ }
    }

    match glibc::simulate_start_main(func, &values)? {
       Some(main) => { return Ok((false, vec![(main, UUID::now())])); }
       None => { /* continue */ }
    }

    /*
    if rename_plt_stub(func)? {
        return Ok((true, vec![]));
    }
    */

    // XXX: gulianov

    // XXX: new call targets
    Ok((false, vec![]))
}
