use types::{
    Function, Variable, NameRef, CallTarget, UUID,
    RewriteControl, Constant, Statement, FlowOperation,
    Value, Str,
};
use absint::{
    Approximation, ValueSets, Kset, _100, ValueSetAnalysis
};

use std::collections::HashMap;
use smallvec::SmallVec;

type Vsa = ValueSetAnalysis<Kset<_100>>;

pub fn resolve_indirections(func: &mut Function, known: &HashMap<u64, CallTarget>,
                            symbolic: &HashMap<u64, Str>, approx: &Approximation<Vsa, ValueSets<Vsa>>)
    -> (bool, Vec<(u64, UUID)>)
{
    let new_ctrans = resolve_indirect_jumps(func, approx);
    let (new_calls, new_fns) = resolve_indirect_calls(func, known, symbolic, approx);

    (new_ctrans || new_calls, new_fns)
}

fn resolve_variable_to_values(name: NameRef, approx: &Approximation<Vsa, ValueSets<Vsa>>)
    -> SmallVec<[Constant; 1]>
{
    match approx.get(name) {
        &ValueSetAnalysis::Offset{ region: None, offset: Kset::Set(ref s) } => {
            s.clone().into()
        }
        _ => {
            SmallVec::default()
        }
    }
}

fn resolve_variable_to_symbol(name: NameRef, approx: &Approximation<Vsa, ValueSets<Vsa>>)
    -> Option<NameRef>
{
    match approx.get(name) {
        &ValueSetAnalysis::Offset{ region: Some(sym), offset: Kset::Set(ref s) } if s.len() == 1 && s[0].value == 0 => {
            Some(sym)
        }
        _ => None,
    }
}

fn resolve_indirect_jumps(func: &mut Function,
                          approx: &Approximation<Vsa, ValueSets<Vsa>>)
    -> bool
{
    func.resolve_indirect_jumps(|var| {
        resolve_variable_to_values(var.name, approx)
    })
}

fn resolve_indirect_calls(func: &mut Function, known: &HashMap<u64, CallTarget>,
                          symbolic: &HashMap<u64, Str>, approx: &Approximation<Vsa, ValueSets<Vsa>>)
    -> (bool, Vec<(u64, UUID)>)
{
    use std::mem::swap;

    let bb_idx = func.basic_blocks().map(|x| x.0).collect::<Vec<_>>();
    let mut retval = false;
    let mut new_uu: Vec<(u64, UUID)> = vec![];

    for bb in bb_idx {
        let _ = func.rewrite_basic_block(bb, |stmt, names, strs, _| {
            match stmt {
                &mut Statement::Flow{ op: FlowOperation::IndirectCall{ target: Variable{ name,.. } } } => {
                    let vals = resolve_variable_to_values(name, approx);

                    if vals.len() == 1 {
                        let val = vals[0];

                        let mut new_stmt = match resolve_call(val.value, &mut new_uu, known, symbolic) {
                            CallTarget::Function(uu) => {
                                Statement::Flow{ op: FlowOperation::Call{ function: uu } }
                            }
                            CallTarget::External(ref s) => {
                                Statement::Flow{ op: FlowOperation::ExternalCall{ external: strs.insert(s) } }
                            }
                        };

                        retval = true;
                        swap(stmt, &mut new_stmt);
                    }

                    if let Some(n) = resolve_variable_to_symbol(name, approx) {
                        let n = names.value(n)?;
                        let is_call = n.subscript().is_none() && known.iter().any(|x| {
                            match x.1 {
                                &CallTarget::External(ref x) => x == n.base(),
                                &CallTarget::Function(_) => false,
                            }
                        });

                        if is_call {
                            let mut new_stmt = Statement::Flow{ op: FlowOperation::ExternalCall{ external: strs.insert(n.base()) } };

                            retval = true;
                            swap(stmt, &mut new_stmt);
                        }
                    }
                }

                _ => {}
            }

            Ok(RewriteControl::Continue)
        });
    }

    (retval, new_uu)
}

fn resolve_call(value: u64, new_uuids: &mut Vec<(u64, UUID)>,
                known: &HashMap<u64, CallTarget>,
                symbolic: &HashMap<u64, Str>) -> CallTarget {
    match known.get(&value) {
        Some(ct) => {
            ct.clone()
        }
        None => match symbolic.get(&value) {
            Some(name) => {
                CallTarget::External(name.clone())
            }
            None => match new_uuids.binary_search_by_key(&value, |x| x.0) {
                Ok(p) => {
                    CallTarget::Function(new_uuids[p].1.clone())
                }
                Err(p) => {
                    let uu = UUID::now();
                    let ct = CallTarget::Function(uu.clone());

                    new_uuids.insert(p, (value, uu));
                    ct.clone()
                }
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use simple_logger;
    use types::{Region, BasicBlockIndex, TestArch, UUID};
    use absint::approximate;
    use dflow::{rewrite_with_assume, rewrite_to_ssa};

    // Mi10
    // Ji
    #[test]
    fn simple_indirect_jump() {
        let _ = simple_logger::init();
        let data = b"Mi10JiRRRRRR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Vsa, ValueSets<Vsa>, _>(&func, None).unwrap();

        assert_eq!(func.basic_blocks().len(), 1);
        assert_eq!(func.basic_block(BasicBlockIndex::new(0)).area, (0..6).into());

        let (changed, new_fns) = resolve_indirections(&mut func, &HashMap::default(), &approx);

        assert!(changed);
        assert!(new_fns.is_empty());

        func.extend::<TestArch>((), &reg).unwrap();

        assert_eq!(func.basic_blocks().len(), 2);
        assert_eq!(func.basic_block(BasicBlockIndex::new(0)).area, (0..6).into());
        assert_eq!(func.basic_block(BasicBlockIndex::new(1)).area, (10..11).into());
    }

    // 0:  Bf22
    // 4:  Bg15
    // 8:  Mi31
    // 12: J26
    // 15: Mi32
    // 19: J26
    // 22: Mi33
    // 26: Ji
    // 28: RRRRR
    #[test]
    fn multi_indirect_jump() {
        let _ = simple_logger::init();
        let data = b"Bf22Bg15Mi31J26Mi32J26Mi33JiRRRRRR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Vsa, ValueSets<Vsa>, _>(&func, None).unwrap();

        assert_eq!(func.basic_blocks().len(), 6);

        let (changed, new_fns) = resolve_indirections(&mut func, &HashMap::default(), &approx);

        assert!(changed);
        assert!(new_fns.is_empty());

        func.extend::<TestArch>((), &reg).unwrap();
        for bb in func.basic_blocks() {
            println!("{:?}", bb);
        }
        assert_eq!(func.basic_blocks().len(), 9);
    }

    /*
     * Table of 11 32 bit words at 0
     *
     * 0:  Uii4
     * 4:  Cfi10
     * 9:  Bf14
     *
     * 12: R
     *
     * (BB1)
     * 13: Xi4i
     * 17: Lpi
     * 20: Jp
     * 22: -----------------
     * 39: RRRRRRRRRRRR
     */
    #[test]
    fn jump_array_indirect() {
        let _ = simple_logger::init();
        let code = b"Uii4Cfi10Bf14RXi4iLpiJp-----------------RRRRRRRRRRRR".to_vec();
        let data = b"\x28\x00\x00\x00\x29\x00\x00\x00\x2a\x00\x00\x00\x2b\x00\x00\x00\x2c\x00\x00\x00\x2d\x00\x00\x00\x2e\x00\x00\x00\x2f\x00\x00\x00\x30\x00\x00\x00\x31\x00\x00\x00\x32\x00\x00\x00\x33\x00\x00\x00".to_vec();
        let code_reg = Region::from_buf("", 16, code, 0, None);
        let data_reg = Region::from_buf("ram", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &code_reg, UUID::now()).unwrap();
        let _ = rewrite_with_assume(&mut func).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let mem = ValueSets::from_region(&data_reg);
        let approx = approximate::<Vsa, ValueSets<Vsa>, _>(&func, mem).unwrap();
        let (changed, new_fns) = resolve_indirections(&mut func, &HashMap::default(), &approx);

        assert!(changed);
        assert!(new_fns.is_empty());

        func.extend::<TestArch>((), &code_reg).unwrap();
        assert_eq!(func.basic_blocks().len(), 14);
    }

    // 0:  Bf22
    // 4:  Bg15
    // 8:  Mi31
    // 12: J26
    // 15: Mi32
    // 19: J26
    // 22: Mi33
    // 26: Gi
    // 28: R
    #[test]
    fn multi_indirect_call() {
        let _ = simple_logger::init();
        let data = b"Bf22Bg15Mi31J26Mi32J26Mi33GiR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Vsa, ValueSets<Vsa>, _>(&func, None).unwrap();
        let (changed, new_fns) = resolve_indirections(&mut func, &HashMap::default(), &approx);

        assert!(!changed);
        assert!(new_fns.is_empty());
    }

    #[test]
    fn indirect_call() {
        let _ = simple_logger::init();
        let data = b"Mi10GiMi9GiR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Vsa, ValueSets<Vsa>, _>(&func, None).unwrap();

        assert_eq!(func.basic_blocks().len(), 1);
        assert_eq!(func.basic_block(BasicBlockIndex::new(0)).area, (0..12).into());

        let (changed, new_fns) = resolve_indirections(&mut func, &HashMap::default(), &approx);

        assert!(changed);
        assert_eq!(new_fns.len(), 2);
        assert!((new_fns[0].0 == 10) ^ (new_fns[1].0 == 10));
        assert!((new_fns[0].0 == 9) ^ (new_fns[1].0 == 9));

        func.extend::<TestArch>((), &reg).unwrap();

        assert_eq!(func.basic_blocks().len(), 1);
        assert_eq!(func.basic_block(BasicBlockIndex::new(0)).area, (0..12).into());
    }

    #[test]
    fn indirect_call_same() {
        let _ = simple_logger::init();
        let data = b"Mi10GiMi10GiR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Vsa, ValueSets<Vsa>, _>(&func, None).unwrap();

        assert_eq!(func.basic_blocks().len(), 1);
        assert_eq!(func.basic_block(BasicBlockIndex::new(0)).area, (0..13).into());

        let (changed, new_fns) = resolve_indirections(&mut func, &HashMap::default(), &approx);

        assert!(changed);
        assert_eq!(new_fns.len(), 1);
        assert!(if let (10, _) = new_fns[0] { true } else { false });

        func.extend::<TestArch>((), &reg).unwrap();

        assert_eq!(func.basic_blocks().len(), 1);
        assert_eq!(func.basic_block(BasicBlockIndex::new(0)).area, (0..13).into());
    }

    #[test]
    fn indirect_call_known() {
        let _ = simple_logger::init();
        let data = b"Mi10GiMi10GiR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Vsa, ValueSets<Vsa>, _>(&func, None).unwrap();
        let mut known = HashMap::default();

        known.insert(10, CallTarget::Function(UUID::now()));

        let (changed, new_fns) = resolve_indirections(&mut func, &known, &approx);

        assert!(changed);
        assert!(new_fns.is_empty());
    }


    #[test]
    fn indirect_stub() {
        let _ = simple_logger::init();
        let data = b"Mi10GiMi10GiR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Vsa, ValueSets<Vsa>, _>(&func, None).unwrap();
        let mut known = HashMap::default();

        known.insert(10, CallTarget::External("test".into()));

        let (changed, new_fns) = resolve_indirections(&mut func, &known, &approx);

        assert!(changed);
        assert!(new_fns.is_empty());
    }
}
