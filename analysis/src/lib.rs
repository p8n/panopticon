/*
 * Panopticon - A libre disassembler
 * Copyright (C) 2017  Panopticon authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//! Disassembly-Analysis loop.

#[cfg(test)]
extern crate simple_logger;

extern crate p8n_types as types;
extern crate p8n_abstract_interp as absint;
extern crate p8n_data_flow as dflow;
extern crate vec_map;
extern crate bit_set;
extern crate smallvec;
extern crate petgraph;
#[macro_use] extern crate log;

//extern crate futures;
//extern crate chashmap;
//extern crate rayon;
//extern crate uuid;
//extern crate parking_lot;

//mod pipeline;
//pub use pipeline::pipeline;
//pub use pipeline::analyze;

mod summary;
pub use summary::{
    update_summaries,
    summarise,
    Summary,
};

mod resolve;
pub use resolve::resolve_indirections;

mod call_flow;
pub use call_flow::{
    rewrite_non_returning_calls,
    returns,
    truncate_dead_ends,
};

mod disassembler;
pub use disassembler::{
    disassemble,
    Event,
};

mod glibc;
pub use glibc::{
    rename_plt_stub, simulate_start_main, known_summaries,
};
