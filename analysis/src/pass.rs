enum PassResult {
    NextPass,
    ModifiedCode,
    NewCode,
}

trait Pass {
    fn run(func: &mut Function) -> Result<PassResult>;
}

trait InterproceduralPass {
    fn run(scc: &[&mut Function]) -> Result<PassResult>;
}

impl Pass for SingleStaticAssignment {}
impl Pass for RecoverStackFrame {}
impl Pass for Guilanov {}
impl Pass for PropagateConstants {}
impl Pass for SimulateGlibc {}
impl InterproceduralPass for FunctionSummaries {}


enum AssociatedData {
    ValueSetAnalysis,

