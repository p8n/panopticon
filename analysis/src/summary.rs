//! function summary computation & propagation

use types::{
    Result,
    Function,
    Operation,
    FlowOperation,
    Statement,
    Value,
    Variable,
    CallTarget,
    CfgNode,
    Segments,
    Strings,
    Names,
    NameRef, StrRef, Segment, SegmentRef, MemoryOperation
};
use dflow::{
    ReachingDefs,
};
use absint::{
    Approximation,
    ValueSetAnalysis,
    Kset,
    _1,
    ValueSets,
    Avalue,
};

use std::collections::HashMap;

use petgraph::visit::{
    IntoNodeReferences,
    NodeRef,
};
use bit_set::BitSet;
use vec_map::VecMap;
use smallvec::SmallVec;

#[derive(Debug, Clone)]
pub struct Summary {
    names: Names,
    segments: Segments,
    strings: Strings,
    statements: Vec<Statement>,
}

impl Summary {
    pub fn from_stmts<I: IntoIterator<Item=Statement>>(stmts: I, names: &Names, segments: &Segments, strings: &Strings) -> Self {
        let iter = stmts.into_iter();
        let mut my_names = Names::default();
        let mut my_segments = Segments::default();
        let mut my_strings = Strings::default();
        let mut statements = Vec::with_capacity(iter.size_hint().0);

        for mut stmt in iter {
            Self::translate_stmt(&mut stmt, names, segments, strings,
                                 &mut my_names, &mut my_segments, &mut my_strings);
            statements.push(stmt);
        }

        Summary{
            names: my_names,
            segments: my_segments,
            strings: my_strings,
            statements: statements,
        }
    }

    pub fn map_to(&self, func: &mut Function) -> Vec<Statement> {
        let mut statements = Vec::with_capacity(self.statements.len());

        for mut stmt in self.statements.iter().cloned() {
            Self::translate_stmt(&mut stmt, &self.names, &self.segments, &self.strings,
                                 &mut func.names, &mut func.segments, &mut func.strings);
            statements.push(stmt);
        }

        statements
    }

    fn translate_stmt(stmt: &mut Statement, from_names: &Names,
                      from_segments: &Segments, from_strings: &Strings,
                      to_names: &mut Names, to_segments: &mut Segments,
                      to_strings: &mut Strings) {
        match stmt {
            &mut Statement::Expression{ ref mut result, op: Operation::Initialize(ref mut base,_) } => {
                Self::translate_name(&mut result.name, from_names, to_names);
                Self::translate_str(base, from_strings, to_strings);
            }
            &mut Statement::Expression{ ref mut result, op: Operation::Load(Segment{ ref mut name },_,_,ref mut v) } => {
                Self::translate_name(&mut result.name, from_names, to_names);
                Self::translate_val(v, from_names, to_names);
                Self::translate_seg(name, from_segments, to_segments);
            }
            &mut Statement::Expression{ ref mut result, ref mut op } => {
                Self::translate_name(&mut result.name, from_names, to_names);
                for v in op.reads_mut() { Self::translate_val(v, from_names, to_names); }
            }

            &mut Statement::Flow{ op: FlowOperation::IndirectCall{ target: Variable{ ref mut name,.. } } } => {
                Self::translate_name(name, from_names, to_names);
            }

            &mut Statement::Flow{ op: FlowOperation::ExternalCall{ ref mut external } } => {
                Self::translate_str(external, from_strings, to_strings);
            }

            &mut Statement::Flow{ op: FlowOperation::Call{ .. } } => {}
            &mut Statement::Flow{ op: FlowOperation::Return } => {}

            &mut Statement::Memory{ op: MemoryOperation::Store{ segment: Segment{ ref mut name }, ref mut address, ref mut value,.. }, ref mut result } => {
                Self::translate_seg(name, from_segments, to_segments);
                Self::translate_val(address, from_names, to_names);
                Self::translate_val(value, from_names, to_names);
                Self::translate_seg(&mut result.name, from_segments, to_segments);
            }

            &mut Statement::Memory{ op: MemoryOperation::Allocate{ ref mut base }, ref mut result } => {
                Self::translate_seg(&mut result.name, from_segments, to_segments);
                Self::translate_str(base, from_strings, to_strings);
            }

            &mut Statement::Memory{ op: MemoryOperation::MemoryPhi(ref mut a, ref mut b, ref mut c), ref mut result } => {
                Self::translate_seg(&mut result.name, from_segments, to_segments);
                if let &mut Some(Segment{ ref mut name }) = a { Self::translate_seg(name, from_segments, to_segments); }
                if let &mut Some(Segment{ ref mut name }) = b { Self::translate_seg(name, from_segments, to_segments); }
                if let &mut Some(Segment{ ref mut name }) = c { Self::translate_seg(name, from_segments, to_segments); }
            }
        }
    }

    fn translate_name(idx: &mut NameRef, from: &Names, to: &mut Names) {
        let val = from.value(*idx).unwrap();
        *idx = to.insert(val);
    }

    fn translate_val(val: &mut Value, from: &Names, to: &mut Names) {
        match val {
            &mut Value::Variable(Variable{ ref mut name,.. }) => {
                Self::translate_name(name, from, to);
            }
            &mut Value::Constant(_) | &mut Value::Undefined => {}
        }
    }

    fn translate_seg(idx: &mut SegmentRef, from: &Segments, to: &mut Segments) {
        let val = from.value(*idx).unwrap();
        *idx = to.insert(val);
    }

    fn translate_str(idx: &mut StrRef, from: &Strings, to: &mut Strings) {
        let val = from.value(*idx).unwrap();
        *idx = to.insert(val);
    }
}

type Sccp = ValueSetAnalysis<Kset<_1>>;

fn values_at_exit(func: &Function, approx: &Approximation<Sccp, ValueSets<Sccp>>) -> Result<VecMap<Sccp>> {
    let reach_defs = ReachingDefs::new(&*func)?;
    let exits = func.cflow_graph().node_references().filter_map(|r| {
        match r.weight() {
            &CfgNode::BasicBlock(bb) => {
                let mut last_stmt = None;
                let mut stmts = func.statements(bb);

                while let Some(stmt) = stmts.next() {
                    last_stmt = Some(stmt.into_owned());
                }

                match last_stmt {
                    Some(Statement::Flow{ op: FlowOperation::Return }) =>
                        Some(bb),
                    _ => None,
                }
            }
            &CfgNode::Value(_) => None,
        }
    });
    let mut exit_values = VecMap::<Sccp>::with_capacity(func.names.len());
    for bb in exits {
        for (base, &name) in reach_defs.gen_var[bb.index()].iter() {
            if exit_values.contains_key(base) {
                let val = exit_values[base].combine(approx.get(name));
                exit_values.insert(base, val);
            } else {
                let val = approx.get(name).clone();
                exit_values.insert(base, val);
            }
        }
    }

    Ok(exit_values)
}

pub fn summarise(func: &mut Function, approx: &Approximation<Sccp, ValueSets<Sccp>>) -> Result<Summary> {
    let mut bits = VecMap::<usize>::default();
    let mut read_set = BitSet::<usize>::default();
    let mut call_set = Vec::default();
    let mut excall_set = Vec::default();
    let mut exit_vals = values_at_exit(func, approx)?;

    // Remove variables that have the same value at exit and entry.
    // Record upwards exposes varaibles as inputs.
    for stmt in func.statements(func.entry_point()) {
        match &*stmt {
            &Statement::Expression{ op: Operation::Initialize(_, _), ref result } => {
                let base = func.names.base_name(result.name)?;
                let val = approx.get(result.name).clone();

                bits.insert(base.into(), result.bits);

                if exit_vals.get(base.index()).cloned() == Some(val) {
                    exit_vals.remove(base.index());
                    read_set.insert(base.index());
                }
            }
            _ => { /* skip */ }
        }
    }

    // Record called functions, read and written memory segments.
    for stmt in func.statements(..) {
        match &*stmt {
            &Statement::Expression{ ref result,.. } => {
                let base = func.names.base_name(result.name)?;
                bits.insert(base.index(), result.bits);
            }

            &Statement::Flow{ op: FlowOperation::Call{ ref function } } => {
                match call_set.binary_search(function) {
                    Ok(_) => { /* skip */ }
                    Err(i) => {
                        call_set.insert(i,function.clone());
                    }
                }
            }

            &Statement::Flow{ op: FlowOperation::ExternalCall{ ref external } } => {
                match excall_set.binary_search(external) {
                    Ok(_) => { /* skip */ }
                    Err(i) => {
                        excall_set.insert(i, *external);
                    }
                }
            }


            _ => { /* skip */ }
        }
    }

    let stmts = read_set.into_iter().map(|idx| {
        let b = bits[idx];
        Statement::Expression{
            op: Operation::Move(Value::var(idx.into(), b).unwrap()),
            result: Variable::new(idx.into(), b).unwrap(),
        }
    }).chain(exit_vals.into_iter().map(|(idx, _)| {
        let b = bits[idx];
        Statement::Expression{
            op: Operation::Move(Value::undef()),
            result: Variable::new(idx.into(), b).unwrap(),
        }
    })).chain(call_set.into_iter().map(|tgt| {
        Statement::Flow{
            op: FlowOperation::Call{ function: tgt },
        }
    })).chain(excall_set.into_iter().map(|tgt| {
        Statement::Flow{
            op: FlowOperation::ExternalCall{ external: tgt },
        }
    }));

    Ok(Summary::from_stmts(stmts, &func.names, &func.segments, &func.strings))
}

pub fn update_summaries(func: &mut Function, summaries: &HashMap<CallTarget, Summary>) -> Result<bool> {
    use types::{
        MnemonicIndex,
        Mnemonic,
    };
    use std::borrow::Cow;

    let mut ret = false;
    let sum_str = func.strings.insert(&"__summary".into());
    let bbs = func.basic_blocks().map(|x| x.0).collect::<Vec<_>>();

    for bb_idx in bbs {
        let mut mnes_with_calls = VecMap::<SmallVec<[CallTarget; 1]>>::default();

        {
            let bb = func.basic_block(bb_idx);
            let mut stmt_idx = bb.statements.start;

            for mne_idx in bb.mnemonics.start.index()..bb.mnemonics.end.index() {
                let mne = func.mnemonic(MnemonicIndex::new(mne_idx));
                let rgn = stmt_idx..stmt_idx + mne.statement_count;

                for stmt in func.statements(rgn) {
                    match &*stmt {
                        &Statement::Flow{ op: FlowOperation::Call{ ref function } } => {
                            if mnes_with_calls.contains_key(mne_idx) {
                                mnes_with_calls[mne_idx].push(CallTarget::Function(function.clone()));
                            } else {
                                mnes_with_calls.insert(mne_idx, vec![CallTarget::Function(function.clone())].into());
                            }
                        }

                        &Statement::Flow{ op: FlowOperation::ExternalCall{ external } } => {
                            if mnes_with_calls.contains_key(mne_idx) {
                                mnes_with_calls[mne_idx].push(CallTarget::External(func.strings.value(external)?.clone()));
                            } else {
                                mnes_with_calls.insert(mne_idx, vec![CallTarget::External(func.strings.value(external)?.clone())].into());
                            }
                        }

                        _ => { /* do nothing */ }
                    }
                }

                stmt_idx += mne.statement_count;
            }
        }

        for (mut mne_idx, calls) in mnes_with_calls {
            let c = calls.into_iter()
                         .flat_map(|x|
                             summaries.get(&x).map(|x| x.map_to(func)).unwrap_or(vec![]))
                         .collect::<Vec<_>>();
            mne_idx += 1;

            let summary_present = match func.mnemonic(MnemonicIndex::new(mne_idx)) {
                &Mnemonic{ opcode,.. } if opcode == sum_str => true,
                _ => false,
            };
            let fst_mne = func.basic_block(bb_idx).mnemonics.start.index();
            let stmt_begin: usize = func.mnemonics(fst_mne..mne_idx).map(|m| m.1.statement_count).sum();
            let stmt_end: usize = func.mnemonics(fst_mne..mne_idx + 1).map(|m| m.1.statement_count).sum();
            let stmt_off = func.basic_block(bb_idx).statements.start;
            let needs_update = !summary_present
                || !func.statements((stmt_off + stmt_begin)..(stmt_off + stmt_end))
                .eq(c.iter().map(|x| Cow::Borrowed(x)));

            if needs_update {
                ret = true;

                if summary_present {
                    // a __summary mnemonic already exists, update its contents.
                    func.remove_mnemonic(MnemonicIndex::new(mne_idx))?;
                }

                let mne_off = mne_idx - func.basic_block(bb_idx).mnemonics.start.index();
                // no __summary mnemonic exists for this call, insert a new one,
                func.insert_mnemonic(bb_idx, mne_off, sum_str, SmallVec::default(), c.clone())?;
            }
        }
    }

    Ok(ret)
}

#[cfg(test)]
mod tests {
    use super::*;
    use absint::approximate;
    use simple_logger;
    use types::{
        NameRef,
        TestArch,
        Name,
        Region,
        UUID,
    };
    use dflow::rewrite_to_ssa;

    fn is_read(sum: &[Statement], nam: NameRef) -> bool {
        use types::{
            Statement::*,
            Operation::*,
            Value::*,
            Variable,
        };

        let mut found = false;

        for stmt in sum.iter() {
            match stmt {
                &Expression{ op: Move(Variable(Variable{ name,.. })), result } if result.name == nam && name == nam => {
                    found = true;
                }
                &Expression{ op: Move(Undefined), result } if result.name == nam => {
                    return false;
                }
                _ => {}
            }
        }

        found
    }

    fn is_written(sum: &[Statement], nam: NameRef) -> bool {
        use types::{
            Statement::*,
            Operation::*,
            Value::*,
        };

        for stmt in sum.iter() {
            match stmt {
                &Expression{ op: Move(Undefined), result } if result.name == nam => {
                    return true;
                }
                _ => {}
            }
        }

        false
    }


    /*
     * 00: Uss4 ; push a
     * 04: Ssa
     * 07: Uss4 ; push b
     * 11: Ssb
     * 14: Ac2c ; use c
     * 18: Mf1
     * 21: Bf29
     *  L2:
     * 25: Aa2a
     *  L1:
     * 29: Ab2a
     * 33: Lbs  ; pop b
     * 36: Ass4
     * 40: Las  ; pop a
     * 43: Ass4
     * 47: R    ; ret
     */
    #[test]
    fn summary() {
        let _ = simple_logger::init();
        let data = b"Uss4SsaUss4SsbAc2cMf1Bf29Aa2aAb2aLbsAss4LasAss4R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Sccp, ValueSets<Sccp>, _>(&func, None).unwrap();
        let sum = summarise(&mut func, &approx).unwrap().map_to(&mut func);
        let base_a = func.names.insert(&Name::new("a".into(), None));
        let base_b = func.names.insert(&Name::new("b".into(), None));
        let base_c = func.names.insert(&Name::new("c".into(), None));
        let base_f = func.names.insert(&Name::new("f".into(), None));

        assert!(is_read(&sum, base_a));
        assert!(is_read(&sum, base_b));
        assert!(!is_written(&sum, base_a));
        assert!(!is_written(&sum, base_b));
        assert!(is_written(&sum, base_c));
        assert!(is_written(&sum, base_f));
        assert!(sum.iter().all(|x| match x {
            &Statement::Flow{ .. } => false,
            _ => true,
        }));
        println!("{:?}", sum);
    }

    /*
     * 00: Uss4 ; push a
     * 04: Ssa
     * 07: Uss4 ; push b
     * 11: Ssb
     * 14: Ac2c ; use c
     * 18: Mf1
     * 21: Bf48
     *  L2:
     * 25: Aa2a
     * 29: Ab2a
     * 33: Lbs  ; pop b
     * 36: Ass4
     * 40: Las  ; pop a
     * 43: Ass4
     * 47: R    ; ret
     *  L1:
     * 48: Ab2a
     * xx: Lbs  ; pop b
     * xx: Ass4
     * xx: Las  ; pop a
     * xx: Ass4
     * xx: R    ; ret
     */
    #[test]
    fn summary_branch() {
        let _ = simple_logger::init();
        let data = b"Uss4SsaUss4SsbAc2cMf1Bf48Aa2aAb2aLbsAss4LasAss4RAa2aAb2aLbsAss4LasAss4R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Sccp, ValueSets<Sccp>, _>(&func, None).unwrap();
        let sum = summarise(&mut func, &approx).unwrap().map_to(&mut func);
        let base_a = func.names.insert(&Name::new("a".into(), None));
        let base_b = func.names.insert(&Name::new("b".into(), None));
        let base_c = func.names.insert(&Name::new("c".into(), None));
        let base_f = func.names.insert(&Name::new("f".into(), None));

        assert!(is_read(&sum, base_a));
        assert!(is_read(&sum, base_b));
        assert!(!is_written(&sum, base_a));
        assert!(!is_written(&sum, base_b));
        assert!(is_written(&sum, base_c));
        assert!(is_written(&sum, base_f));
        assert!(sum.iter().all(|x| match x {
            &Statement::Flow{ .. } => false,
            _ => true,
        }));

        println!("{:?}", sum);
    }

    /*
     * 00: Mf1
     * 03: Bf08
     *  L2:
     * 07: R
     *  L1:
     * 08: Uss4 ; push a
     * 12: Ssa
     * 15: Uss4 ; push b
     * 19: Ssb
     * 22: Ac2c ; use c
     * 26: Ab2a
     * 30: Lbs  ; pop b
     * 33: Ass4
     * 37: Las  ; pop a
     * 40: Ass4
     * 44: R    ; ret
     */
    #[test]
    fn summary_early_exit() {
        let _ = simple_logger::init();
        let data = b"Mf1Bf08RUss4SsaUss4SsbAc2cAb2aLbsAss4LasAss4R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let approx = approximate::<Sccp, ValueSets<Sccp>, _>(&func, None).unwrap();
        let sum = summarise(&mut func, &approx).unwrap().map_to(&mut func);
        let base_a = func.names.insert(&Name::new("a".into(), None));
        let base_b = func.names.insert(&Name::new("b".into(), None));
        let base_c = func.names.insert(&Name::new("c".into(), None));
        let base_f = func.names.insert(&Name::new("f".into(), None));

        assert!(is_read(&sum, base_a));
        assert!(is_read(&sum, base_b));
        assert!(!is_written(&sum, base_a));
        assert!(!is_written(&sum, base_b));
        assert!(is_written(&sum, base_c));
        assert!(is_written(&sum, base_f));
        assert!(sum.iter().all(|x| match x {
            &Statement::Flow{ .. } => false,
            _ => true,
        }));

        println!("{:?}", sum);
    }

    /*
     * Ma1
     * F
     * Aaa1
     * R
     */
    #[test]
    fn fresh_summary() {
        let _ = simple_logger::init();
        let data = b"Ma1FAaa1R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let sum = Summary::from_stmts(vec![
            Statement::Expression{ op: Operation::Move(Value::Undefined), result: func.names.var("a", None, 16).unwrap() },
        ], &func.names, &func.segments, &func.strings);
        let mut ct = None;

        for stmt in func.statements(..) {
            match &*stmt {
                &Statement::Flow{ op: FlowOperation::Call{ ref function } } => {
                    ct = Some(CallTarget::Function(function.clone()));
                    break;
                }

                _ => {}
            }
        }

        let mut sums = HashMap::default();

        sums.insert(ct.unwrap(), sum);
        assert_eq!(update_summaries(&mut func, &sums).ok(), Some(true));
        assert_eq!(update_summaries(&mut func, &sums).ok(), Some(false));
    }

    #[test]
    fn updated_summary() {
        let _ = simple_logger::init();
        let data = b"Ma1FAaa1R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let sum1 = Summary::from_stmts(vec![
            Statement::Expression{ op: Operation::Move(Value::Undefined), result: func.names.var("a", None, 16).unwrap() },
        ], &func.names, &func.segments, &func.strings);
        let sum2 = Summary::from_stmts(vec![
            Statement::Expression{ op: Operation::Move(Value::Undefined), result: func.names.var("a", None, 16).unwrap() },
            Statement::Expression{ op: Operation::Move(Value::Undefined), result: func.names.var("b", None, 16).unwrap() },
        ], &func.names, &func.segments, &func.strings);
        let mut ct = None;

        for stmt in func.statements(..) {
            match &*stmt {
                &Statement::Flow{ op: FlowOperation::Call{ ref function } } => {
                    ct = Some(CallTarget::Function(function.clone()));
                    break;
                }

                _ => {}
            }
        }

        let mut sums = HashMap::default();

        sums.insert(ct.clone().unwrap(), sum1);
        assert_eq!(update_summaries(&mut func, &sums).ok(), Some(true));
        sums.insert(ct.unwrap(), sum2);
        assert_eq!(update_summaries(&mut func, &sums).ok(), Some(true));
        assert_eq!(update_summaries(&mut func, &sums).ok(), Some(false));
    }

    #[test]
    fn two_call_mnemonic() {
        use types::{
            UUID,
            BasicBlockIndex,
        };

        let _ = simple_logger::init();
        let data = b"Ma1Aaa1R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let _ = rewrite_to_ssa(&mut func).unwrap();
        let sum1 = Summary::from_stmts(vec![
            Statement::Expression{ op: Operation::Move(Value::Undefined), result: func.names.var("a", None, 16).unwrap() },
        ], &func.names, &func.segments, &func.strings);
        let sum2 = Summary::from_stmts(vec![
            Statement::Expression{ op: Operation::Move(Value::Undefined), result: func.names.var("a", None, 16).unwrap() },
            Statement::Expression{ op: Operation::Move(Value::Undefined), result: func.names.var("b", None, 16).unwrap() },
        ], &func.names, &func.segments, &func.strings);
        let uu1 = UUID::now();
        let uu2 = UUID::now();
        let ct1 = CallTarget::Function(uu1.clone());
        let ct2 = CallTarget::Function(uu2.clone());
        let stmts = vec![
            Statement::Flow{ op: FlowOperation::Call{ function: uu1.clone() } },
            Statement::Flow{ op: FlowOperation::Call{ function: uu2.clone() } },
        ];
        let opc = func.strings.insert(&"double-call".into());

        func.insert_mnemonic(BasicBlockIndex::new(0), 1, opc, Default::default(), stmts).unwrap();
        let mut sums = HashMap::default();

        sums.insert(ct1.clone(), sum1);
        assert_eq!(update_summaries(&mut func, &sums).ok(), Some(true));
        sums.insert(ct2.clone(), sum2);
        assert_eq!(update_summaries(&mut func, &sums).ok(), Some(true));
        assert_eq!(update_summaries(&mut func, &sums).ok(), Some(false));
    }
}
