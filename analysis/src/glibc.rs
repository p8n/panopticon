use types::{
    Function, Result, CallTarget, Machine, Statement, Operation, Names, Segments, Strings, FlowOperation, MnemonicIndex, Mnemonic, Variable, Constant,
};
use absint::{
    ValueSetAnalysis, ValueSets, Kset, _100, Approximation,
};
use Summary;

use std::collections::HashMap;

type Vsa = ValueSetAnalysis<Kset<_100>>;

pub fn known_summaries(m: Machine) -> HashMap<CallTarget, Summary> {
    let mut ret = HashMap::default();

    match m {
        Machine::Amd64 => {
            {
                let mut names = Names::default();
                let rdi = names.var("RDI", None, 64).unwrap();
                let sum = Summary::from_stmts(vec![
                                              Statement::Expression{ result: rdi.clone(), op: Operation::Move(rdi.clone().into()) }
                ], &names, &Segments::default(), &Strings::default());

                ret.insert(CallTarget::External("__libc_start_main".into()), sum);
            }
        }

        Machine::Ia32 => {}
        Machine::Avr => {}
    }

    ret
}



pub fn rename_plt_stub(_: &mut Function) -> Result<bool> {
    unimplemented!();
}

pub fn simulate_start_main(func: &Function, values: &Approximation<Vsa, ValueSets<Vsa>>) -> Result<Option<u64>> {
    let lst = match func.strings.index(&"__libc_start_main".into()) {
        Ok(l) => l,
        Err(_) => { return Ok(None); }
    };
    let sum = match func.strings.index(&"__summary".into()) {
        Ok(l) => l,
        Err(_) => { return Ok(None); }
    };

    for (bb_idx, bb) in func.basic_blocks() {
        let mut stmt_idx = bb.statements.start;
        for (mne_idx, mne) in func.mnemonics(bb_idx) {
            for stmt in func.statements(stmt_idx..stmt_idx + mne.statement_count) {
                match &*stmt {
                    &Statement::Flow{ op: FlowOperation::ExternalCall{ external } } if external == lst => {
                        // get the summary mnemonic
                        let next_mne = func.mnemonic(MnemonicIndex::new(mne_idx.index()+1));
                        if next_mne.opcode == sum {
                            let start = stmt_idx + mne.statement_count;
                            for stmt in func.statements(start..start + next_mne.statement_count) {
                                match &*stmt {
                                    &Statement::Expression{ op: Operation::Move(_), result: Variable{ name,.. } } => {
                                        match values.get(name) {
                                            ValueSetAnalysis::Offset{ region: None, offset: Kset::Set(ref s) } if s.len() == 1 => {
                                                match s.get(0) {
                                                    Some(&Constant{ value,.. }) => {
                                                        return Ok(Some(value));
                                                    }
                                                    _ => {}
                                                }
                                            }
                                            _ => {}
                                        }
                                    }
                                    _ => {}
                                }
                            }
                        }
                    }
                    _ => {}
                }
            }
            stmt_idx += mne.statement_count;
        }
    }

    Ok(None)
}

/*
pub trait GlibcArchitecture: Architecture {
    type State: Default + Sized;
    fn simulate_libc_start_main(state: &mut Self::State, func: &Function, region: &Region, config: &Self::Configuration) -> Result<Option<u64>>;
}

impl GlibcArchitecture for Amd64 {
    type State = Option<Uuid>;

    fn simulate_libc_start_main(state: &mut Self::State, func: &Function, region: &Region, config: &Self::Configuration) -> Result<Option<u64>> {
        match *config {
            Mode::Real | Mode::Protected => {
                if let &mut Some(ref start_main) = state {
                    let cfg = &func.cflow_graph;

                    for vx in cfg.vertices() {
                        if let Some(&ControlFlowTarget::Resolved(ref bb)) = cfg.vertex_label(vx) {
                            for mne in bb.mnemonics.iter() {
                                for stmt in mne.instructions.iter() {
                                    match stmt {
                                        &Statement::ResolvedCall{ function: CallTarget::Function(ref uu),.. } if uu == start_main => {
                                            return Ok(simulate_libc_start_main32(func,start_main,vx,region));
                                        }
                                        _ => {}
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if func.name == "__libc_start_main@plt" {
                        *state = Some(func.uuid.clone())
                    }
                }
            }

            Mode::Long => {
                let cfg = &func.cflow_graph;

                for vx in cfg.vertices() {
                    if let Some(&ControlFlowTarget::Resolved(ref bb)) = cfg.vertex_label(vx) {
                        for mne in bb.mnemonics.iter() {
                            for stmt in mne.instructions.iter() {
                                match stmt {
                                    &Statement::ResolvedCall{ function: CallTarget::Stub(ref name),.. } if name == "__libc_start_main" => {
                                        return Ok(simulate_libc_start_main64(func,vx,region));
                                    }
                                    _ => {}
                                }
                            }
                        }
                    }
                }
            }
        }

        Ok(None)
    }
}

fn rewrite_plt_entry(func: &mut Function, registers: &HashSet<&'static str>) -> Result<bool> {
    use panopticon_graph_algos::MutableGraphTrait;

    let cfg = &mut func.cflow_graph;
    let vxs = cfg.vertices().collect::<Vec<_>>();

    if vxs.len() != 1 { return Ok(false); }
    if func.entry_point.is_none() { return Ok(false); }
    let vx = vxs[0];
    let mut new_name = None;

    if let Some(&mut ControlFlowTarget::Resolved(ref mut bb)) = cfg.vertex_label_mut(vx) {
        if let Some(mne) = bb.mnemonics.last_mut() {
            if mne.opcode == "__tail_call" {
                for stmt in mne.instructions.iter_mut() {
                    if let &mut Statement::ResolvedCall{ function: CallTarget::Stub(ref name), ref mut reads, ref mut writes } = stmt {
                        if new_name.is_none() {
                            new_name = Some(name.clone());
                        } else {
                            return Ok(false);
                        }
                    }
                }
            } else {
                return Ok(false);
            }
        } else {
            return Ok(false);
        }
    } else {
        return Ok(false);
    }

    if let Some(name) = new_name {
        func.name = format!("{}@plt",name);
        Ok(true)
    } else {
        Ok(false)
    }
}

fn simulate_libc_start_main64(func: &Function, region: &Region) -> Result<Option<u64>> {
    // main is in rdi
    let memory = ValueSets<Kset<_1>>::from_region(region);
    let values = approximate::<Kset<_1>, ValueSets<Kset<_1>>>(func, Some(memory))?;

    for stmt in func.statements(..) {
        match &*stmt {
            &Statement::Flow{
                op: FlowOperation::Call{ function: CallTarget::External(n) }
            } if n == str__libc_start_main => {

    if let Some(&ControlFlowTarget::Resolved(ref bb)) = cfg.vertex_label(call_bb) {
        for mne in bb.mnemonics.iter() {
            for stmt in mne.instructions.iter() {
                match stmt {
                    &Statement::ResolvedCall{ function: CallTarget::Stub(ref f), ref reads,.. } if *f == "__libc_start_main" => {
                        for rv in reads.iter() {
                            if let &Rvalue::Variable{ ref name, subscript: Some(subscript), size,.. } = rv {
                                if *name == "RDI" && size == 64 {
                                    let lv = Lvalue::Variable{ name: name.clone(), subscript: Some(subscript), size: size };
                                    if let Some(&BoundedAddrTrack::Offset{ region: None, offset, offset_size }) = values.get(&lv) {
                                        return Some(offset);
                                    }
                                }
                            }
                        }
                    }
                    _ => {}
                }
            }
        }
    }

    None
}

fn simulate_libc_start_main32(func: &Function, start_main_uuid: &Uuid, call_bb: ControlFlowRef, region: &Region) -> Option<u64> {
    use num_traits::cast::ToPrimitive;

    let mut initial = HashMap::new();
    let nam32 = Cow::Borrowed("ESP");
    let z32 = CircLinearProg::abstract_value(&Rvalue::Constant { value: 0, size: 32 });

    initial.insert(
        nam32.clone(),
        ValueSetAnalysis::Set(ValueSet{ region: Some("initial".into()), offset: z32 }),
        );

    let env = Environment{
        overrides: HashMap::new(),
        region: region,
        initial_memory: (Hamt::new(),()),
        initial_values: initial,
        symbolic_values: HashMap::new(),
    };
    let (values,memory) = approximate::<ValueSetAnalysis<CircLinearProg>>(func,&env).unwrap();
    let cfg = &func.cflow_graph;
    for vx in cfg.vertices() {
        if let Some(&ControlFlowTarget::Resolved(ref bb)) = cfg.vertex_label(call_bb) {
            let mut pos = 0;

            for mne in bb.mnemonics.iter() {
                for stmt in mne.instructions.iter() {
                    let pp = ProgramPoint{ address: bb.area.start, position: pos };
                    match stmt {
                        &Statement::ResolvedCall{ function: CallTarget::Function(f), ref reads,.. } if f == *start_main_uuid => {
                            let mem = memory.get(&pp).unwrap();

                            for rv in reads.iter() {
                                if let &Rvalue::Variable{ ref name, subscript: Some(subscript), size,.. } = rv {
                                    let lv = Lvalue::Variable{ name: name.clone(), subscript: Some(subscript), size: size };
                                    if let Some(aval) = values.get(&lv) {
                                        let (tops, _) = CircLinearProg::load(&aval,8,&mem.0);
                                        for top in tops {
                                            for aloc in CircLinearProg::as_aloc(&top, size) {
                                                let Aloc{ position: ValueSet{ region, offset },.. } = aloc;
                                                if region.is_none() {
                                                    return offset.is_singular().and_then(|o| o.to_u64());
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        _ => {}
                    }
                    pos += 1;
                }
            }
        }
    }
    None
}
*/
