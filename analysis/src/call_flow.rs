use types::{
    Function,
    CfgNode,
    Statement,
    CallTarget,
    FlowOperation,
    Result,
    MnemonicIndex,
    UUID,
    Value,
    BasicBlockIndex,
};

use std::collections::{HashMap, HashSet};

use bit_set::BitSet;
use petgraph::Direction;
use petgraph::visit::{
    EdgeRef,
    DfsEvent,
    depth_first_search,
};
use petgraph::graph::{
    EdgeIndex,
    NodeIndex,
};

// tail call handing & non returning functions

pub fn rewrite_non_returning_calls(func: &mut Function, leafs: &HashMap<u64, (Value, UUID)>)
    -> Result<bool>
{
    let mut ret = false;
    let mut skipped = HashSet::<u64>::default();

    loop {
        let (node, start) = {
            let bb = func.basic_blocks().find(|(_, bb)| {
                bb.area.offset_start == 0 &&
                !skipped.contains(&bb.area.start) &&
                leafs.contains_key(&bb.area.start)
            });
            match bb {
                Some((_, bb)) => (bb.node,bb.area.start),
                None => { return Ok(ret) }
            }
        };

        match leafs.get(&start).cloned() {
            Some(uu) => {
                let in_edges = func.cflow_graph()
                    .edges_directed(node, Direction::Incoming)
                    .count();

                if in_edges == 1 {
                    let in_edge = func
                        .cflow_graph().edges_directed(node, Direction::Incoming)
                        .next().unwrap().id();
                    let (src, _) = func
                        .cflow_graph().edge_endpoints(in_edge).unwrap();
                    let is_branch = func.cflow_graph()
                        .edges_directed(src, Direction::Outgoing)
                        .skip(2).next().is_some();

                    if is_branch {
                        convert_branch_to_call(func, in_edge, uu)?;
                    } else {
                        convert_jump_to_call(func, in_edge, uu)?;
                    }
                    delete_unreachable(func)?;
                    ret = true;
                } else {
                    skipped.insert(start);
                }
            }

            None => unreachable!(),
        }
    }
}

fn convert_branch_to_call(func: &mut Function, edge: EdgeIndex, call: (Value, UUID))
    -> Result<()>
{
    let (_, block_vx) = func.cflow_graph().edge_endpoints(edge).unwrap();
    let block = match func.cflow_graph().node_weight(block_vx) {
        Some(&CfgNode::BasicBlock(bb)) => {
            bb
        }
        _ => {
            return Err("Edge does not start at a basic block".into());
        }
    };
    let edges_to_del = func.cflow_graph()
        .edges_directed(block_vx, Direction::Incoming)
        .map(|x| x.id().index())
        .collect::<BitSet>();

    // remove incoming edges
    func.cflow_graph_mut().retain_edges(|_, x| {
        !edges_to_del.contains(x.index())
    });

    // insert call mnemonic
    let stmts = vec![
        Statement::Flow{ op: FlowOperation::Call{
            function: call.1
        } },
    ];
    let call_str = func.strings.insert(&"call".into());

    func.insert_mnemonic(block, 0, call_str, vec![call.0].into(), stmts)?;

    // delete remaining mnemonics
    loop {
        let mnes = func.basic_block(block).mnemonics.clone();

        if mnes.start.index() + 1 >= mnes.end.index() {
            break;
        } else {
            func.remove_mnemonic(MnemonicIndex::new(mnes.start.index() + 1))?;
        }
    }
    Ok(())
}

fn convert_jump_to_call(func: &mut Function, edge: EdgeIndex, call: (Value, UUID))
    -> Result<()>
{
    let (block_vx, _) = func.cflow_graph().edge_endpoints(edge).unwrap();
    let (mne_pos, block) = match func.cflow_graph().node_weight(block_vx) {
        Some(&CfgNode::BasicBlock(bb)) => {
            let m = func.basic_block(bb).mnemonics.clone();
            (MnemonicIndex::new(m.end.index() - m.start.index()), bb)
        }
        _ => {
            return Err("Edge does not start at a basic block".into());
        }
    };

    // remove call edge
    func.cflow_graph_mut().remove_edge(edge);

    // append call mnemonic
    let stmts = vec![
        Statement::Flow{ op: FlowOperation::Call{
            function: call.1
        } },
    ];
    let call_str = func.strings.insert(&"call".into());

    func.insert_mnemonic(block, mne_pos.index(), call_str, vec![call.0].into(), stmts)?;

    Ok(())
}

fn delete_unreachable(func: &mut Function) -> Result<()> {
    use std::iter;

    let mut subgraph = BitSet::<usize>::default();
    let entry = func.entry_point();
    let entry = func.basic_block(entry).node;

    {
        let visitor = |ev: DfsEvent<NodeIndex>| {
            match ev {
                DfsEvent::Discover(vx, _) => { subgraph.insert(vx.index()); }
                _ => { /* do nothing */ }
            }
        };

        depth_first_search(func.cflow_graph(), iter::once(entry), visitor);
    }

    let mut bb_to_del = BitSet::<usize>::default();
    let mut val_to_del = BitSet::<usize>::default();

    for n in func.cflow_graph().node_indices() {
        if !subgraph.contains(n.index()) {
            match func.cflow_graph().node_weight(n) {
                Some(&CfgNode::BasicBlock(idx)) => {
                    bb_to_del.insert(idx.index());
                }
                Some(&CfgNode::Value(_)) => {
                    val_to_del.insert(n.index());
                }
                None => {}
            }
        }
    }

    func.retain_basic_blocks(|_, bb| {
        !bb_to_del.contains(bb.index())
    })?;

    func.cflow_graph_mut().retain_nodes(|_, n| !val_to_del.contains(n.index()));
    Ok(())
}

pub fn returns(func: &Function) -> bool {
    func.basic_blocks().any(|(_, bb)| {
        let stmts = &bb.statements;

        if stmts.end > stmts.start {
            let last_stmt = func.statements((stmts.end - 1)..stmts.end)
                .next().map(|x| x.into_owned());

            match last_stmt {
                Some(Statement::Flow{ op: FlowOperation::Return }) => true,
                _ => false,
            }
        } else {
            false
        }
    })
}

pub fn truncate_dead_ends(func: &mut Function, dead_ends: &HashSet<CallTarget>) -> Result<bool> {
    let mut handled = HashSet::<(u64, usize)>::default();
    let mut ret = false;

    loop {
        let bb_idx = func.basic_blocks().find(|(_, bb)| {
            !handled.contains(&(bb.area.start, bb.area.offset_start))
        }).map(|(bb_idx, _)| bb_idx);

        match bb_idx {
            Some(bb_idx) => {
                let new_end = func.statements(bb_idx).position(|stmt| {
                    match &*stmt {
                        &Statement::Flow{ op: FlowOperation::Call{ ref function } } =>
                            dead_ends.contains(&CallTarget::Function(function.clone())),
                        &Statement::Flow{ op: FlowOperation::ExternalCall{ external } } =>
                            dead_ends.contains(&CallTarget::External(func.strings.value(external).unwrap().clone())),
                        _ => false,
                    }
                }).map(|x| x + func.basic_block(bb_idx).statements.start);

                {
                    let a = func.basic_block(bb_idx).area.clone();
                    handled.insert((a.start, a.offset_start));
                }

                match new_end {
                    Some(new_end) => {
                        func.cflow_graph_mut().retain_edges(|g,e| {
                            let (src,_) = g.edge_endpoints(e).unwrap();
                            g.node_weight(src).cloned() != Some(CfgNode::BasicBlock(bb_idx))
                        });
                        truncate_basic_block(func, bb_idx, new_end)?;
                        delete_unreachable(func)?;

                        ret = true;
                    }

                    None => { /* try next bb */ }
                }
            }

            None => { return Ok(ret); }
        }
    }
}

fn truncate_basic_block(func: &mut Function, bb: BasicBlockIndex, last_stmt: usize) -> Result<()> {
    let mut maybe_mne = None;

    {
        let bb = func.basic_block(bb);

        if !(bb.statements.start <= last_stmt && bb.statements.end > last_stmt) {
            return Err("statement out of range".into());
        }

        let mut i = bb.statements.start;

        for (mne_idx, mne) in func.mnemonics(bb) {
            if i <= last_stmt && i + mne.statement_count > last_stmt {
                maybe_mne = Some(mne_idx.index() - bb.mnemonics.start.index());
                break;
            }
            i += mne.statement_count;
        }
    }

    match maybe_mne {
        Some(mne_off) => {
            while func.mnemonics(bb).len() > mne_off + 1 {
                let last_mne = func.basic_block(bb).mnemonics.end.index() - 1;
                func.remove_mnemonic(MnemonicIndex::new(last_mne))?;
            }
            Ok(())
        }
        None => {
            Err("statement out of range".into())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use types::{
        TestArch,
        Region,
        UUID,
    };
    use simple_logger;

    /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf18
     *
     * (B2)
     * 11: Aii1
     * 15: J23
     *
     * (B3)
     * 18: Aii2
     *
     * (B4)
     * 23: Mii
     * 26: R
     */
    #[test]
    fn returns_positive_diamond() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf18Aii1J23Aii2MiiR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert!(returns(&func));
    }

    /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf18
     *
     * (B2)
     * 11: Aii1
     * 15: J11
     *
     * (B3)
     * 18: Aii2
     * 23: R
     */
    #[test]
    fn returns_positive_half_dead() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf18Aii1J11Aii2R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert!(returns(&func));
    }

   /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf17
     *
     * (B2)
     * 11: Aii1
     * 15: Hx
     *
     * (B3)
     * 17: Aii2
     * 22: R
     */
    #[test]
    fn returns_positive_half_dead_function() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf17Aii1HxAii2R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert!(returns(&func));
    }

    /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf18
     *
     * (B2)
     * 11: Aii1
     * 15: J23
     *
     * (B3)
     * 18: Aii2
     *
     * (B4)
     * 23: Mii
     */
    #[test]
    fn returns_negative_diamond() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf18Aii1J23Aii2Mii".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert!(!returns(&func));
    }

    /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf18
     *
     * (B2)
     * 11: Aii1
     * 15: J11
     *
     * (B3)
     * 18: Aii2
     */
    #[test]
    fn returns_negative_both_dead() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf18Aii1J11Aii2J11".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert!(!returns(&func));
    }

   /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf17
     *
     * (B2)
     * 11: Aii1
     * 15: Hx
     *
     * (B3)
     * 17: Aii2
     */
    #[test]
    fn returns_negative_dead_function() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf17Aii1HxAii2".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();

        assert!(!returns(&func));
    }

    /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf18
     *
     * (B2)
     * 11: Aii1
     * 15: J22
     *
     * (B3)
     * 18: Aii2
     *
     * (B4)
     * 22: Mii
     * 25: R
     *
     * 26: J0
     */
    #[test]
    fn rewrite_non_returns_jump_positive() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf18Aii1J22Aii2MiiRJ0".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 26, &reg, UUID::now()).unwrap();
        let mut leafs = HashMap::default();
        let var = Value::Variable(func.names.var("f", None, 16).unwrap());

        leafs.insert(0, (var, UUID::now()));

        assert!(rewrite_non_returning_calls(&mut func, &leafs).unwrap());
        assert_eq!(func.basic_blocks().len(), 1);
    }

    /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf18
     *
     * (B2)
     * 11: Aii1
     * 15: J22
     *
     * (B3)
     * 18: Aii2
     *
     * (B4)
     * 22: Mii
     * 25: R
     *
     * 26: Bf0
     * 29: R
     */
    #[test]
    fn rewrite_non_returns_positive_branch() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf18Aii1J22Aii2MiiRBf0R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 26, &reg, UUID::now()).unwrap();
        let mut leafs = HashMap::default();
        let var = Value::Variable(func.names.var("f", None, 16).unwrap());

        leafs.insert(0, (var, UUID::now()));

        assert!(rewrite_non_returning_calls(&mut func, &leafs).unwrap());
        assert_eq!(func.basic_blocks().len(), 2);
    }

    /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf18
     *
     * (B2)
     * 11: Aii1
     * 15: J22
     *
     * (B3)
     * 18: Aii2
     *
     * (B4)
     * 22: Mii
     * 25: R
     *
     * 26: Bf0
     * 29: R
     */
    #[test]
    fn rewrite_non_returns_negative() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf18Aii1J22Aii2MiiRBf0J0".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 26, &reg, UUID::now()).unwrap();
        let mut leafs = HashMap::default();
        let var = Value::Variable(func.names.var("f", None, 16).unwrap());

        leafs.insert(0, (var, UUID::now()));

        assert!(!rewrite_non_returning_calls(&mut func, &leafs).unwrap());
        assert_eq!(func.basic_blocks().len(), 6);
    }

    /*
     * (B1)
     * 00: Mi1
     * 03: Hx
     * 05: J10
     * 06: R
     * 07: R
     * 08: R
     * 09: R
     * 10: R
     */
    #[test]
    fn truncate() {
        let _ = simple_logger::init();
        let data = b"Mi1HxJ10RRRRR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let mut dead_ends = HashSet::default();

        dead_ends.insert(CallTarget::External("x".into()));

        assert!(truncate_dead_ends(&mut func, &dead_ends).unwrap());
        assert!(!returns(&func));
        assert_eq!(func.basic_blocks().len(), 1);
    }

    /*
     * (B1)
     * 03: Hx
     * 05: J10
     *     RRR
     * 06: R
     * 07: R
     * 08: R
     * 09: R
     * 10: R
     */
    #[test]
    fn truncate_all() {
        let _ = simple_logger::init();
        let data = b"HxJ10RRRRRRRR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let mut dead_ends = HashSet::default();

        dead_ends.insert(CallTarget::External("x".into()));

        assert!(truncate_dead_ends(&mut func, &dead_ends).unwrap());
        assert!(!returns(&func));
        assert_eq!(func.basic_blocks().len(), 1);
    }

    /*
     * (B1)
     * 03: Hx
     * 05: J10
     *     RRR
     * 06: R
     * 07: R
     * 08: R
     * 09: R
     * 10: R
     */
    #[test]
    fn truncate_none() {
        let _ = simple_logger::init();
        let data = b"HxJ10RRRRRRRR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let dead_ends = HashSet::default();

        assert!(!truncate_dead_ends(&mut func, &dead_ends).unwrap());
        assert!(returns(&func));
        assert_eq!(func.basic_blocks().len(), 2);
    }

    /*
     * (B1)
     * 00: Mi1
     * 03: Cfi1
     * 07: Bf14
     *
     * 11: Hx
     * 13: R
     *
     * 14: Hx
     * 16: R
     */
    #[test]
    fn truncate_twice() {
        let _ = simple_logger::init();
        let data = b"Mi1Cfi1Bf14HxRHxR".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut func = Function::new::<TestArch>((), 0, &reg, UUID::now()).unwrap();
        let mut dead_ends = HashSet::default();

        dead_ends.insert(CallTarget::External("x".into()));

        assert!(truncate_dead_ends(&mut func, &dead_ends).unwrap());
        assert!(!returns(&func));
        assert_eq!(func.basic_blocks().len(), 3);
    }
}
