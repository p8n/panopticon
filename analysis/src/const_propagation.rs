pub fn propagate_constants(func: &mut Function) -> Result<()> {
    let vals = approximate::<Kset<_1>,ValueSets<Kset<_1>>>(func,None)?;

